<?php

return [
  'modules' => [
    'Landing',
    'Dashboard',
    'Employer',
    'Applicant',
    'Recruiter',
    'EmployerRecruiter',
    'InternalRecruiter'
  ]
];
