<?php

return [

    /**
     * The endpoint to access the routes.
     */
    'url' => 'routes-url',

    /**
     * The methods to hide.
     */
    'hide_methods' => [
        'HEAD',
    ],

];
