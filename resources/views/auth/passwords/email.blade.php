@extends('Landing::template.main')

@section('content')
    <!-- START page header -->
    <section class="page-header page-header-block nm">
        <!-- pattern -->
        <div class="pattern pattern9"></div>
        <!--/ pattern -->
        <div class="container pt15 pb15">
            <div class="page-header-section">
                <h4 class="title">Request Reset Password</h4>
            </div>
            <div class="page-header-section">
                <!-- Toolbar -->
                <div class="toolbar">
                    <ol class="breadcrumb breadcrumb-transparent nm">
                        <li><a href="{{route('home')}}">Human Factor</a></li>
                        <li class="active">Request Reset Password</li>
                    </ol>
                </div>
                <!--/ Toolbar -->
            </div>
        </div>
    </section>
    <div class="clearfix"></div>
    <!--/ END page header -->

    <!-- START Register Content -->
    <section class="section">
        <div class="container">
            <!-- START Section Header -->
            <div class="row">
                <div class="col-md-12">
                    <div class="section-header text-center">
                        <h1 class="section-title font-alt">Request Reset Password</h1>
                    </div>
                </div>
            </div>
            <!--/ END Section Header -->

            <!-- START Row -->
            <div class="row">
                <div class="col-md-6 col-md-offset-3">
                  @if (session('status'))
                      <div class="alert alert-success">
                          {{ session('status') }}
                      </div>
                  @endif
                    <!-- Login form -->
                    <form class="panel no-border mb0" name="form-login" method="POST" action="{{ route('password.email') }}" data-parsley-validate>
                      {{ csrf_field() }}

                        <div class="panel-body">
                            <div class="form-group">
                                <div class="form-stack has-icon pull-left">
                                    <input name="email" type="text" value="{{old('email')}}" class="form-control input-lg" placeholder="Email" data-parsley-errors-container="#error-container" data-parsley-error-message="Please fill in your email" data-parsley-required>
                                    <i class="ico-user2 form-control-icon"></i>
                                </div>
                            </div>

                            <!-- Error container -->
                            <div id="error-container" class="mb15">
                                @if (count($errors) > 0)
                                    <ul class="parsley-errors-list filled">
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                @endif
                            </div>
                            <!--/ Error container -->

                            <div class="form-group nm">
                                <button type="submit" class="btn btn-block btn-primary"><span class="semibold">Request Reset</span></button>
                            </div>
                        </div>
                    </form>
                    <!-- Login form -->
                </div>
            </div>
            <!--/ END Row -->
        </div>
    </section>
    <!--/ END Register Content -->
@stop
