@extends('Landing::template.main')

@section('content')
  <!-- START page header -->
  <section class="page-header page-header-block nm">
      <!-- pattern -->
      <div class="pattern pattern9"></div>
      <!--/ pattern -->
      <div class="container pt15 pb15">
          <div class="page-header-section">
              <h4 class="title">Account Register</h4>
          </div>
          <div class="page-header-section">
              <!-- Toolbar -->
              <div class="toolbar">
                  <ol class="breadcrumb breadcrumb-transparent nm">
                      <li><a href="{{route('home')}}">Human Factor</a></li>
                      <li class="active">Account Register</li>
                  </ol>
              </div>
              <!--/ Toolbar -->
          </div>
      </div>
  </section>
  <div class="clearfix"></div>
  <!--/ END page header -->

  <!-- START Register Content -->
  <section class="section">
      <div class="container">
          <!-- START Section Header -->
          <div class="row">
              <div class="col-md-12">
                  <div class="section-header text-center">
                      <h1 class="section-title font-alt mb25">Create your Account</h1>
                      <div class="row">
                          <div class="col-md-8 col-md-offset-2">
                              <h4 class="thin text-muted text-center">
                                  Sign up NOW and be part of the first online recruitment marketplace in the Philippines that values you more than the usual. Get connected with the best employers, independent recruiters and job seekers in the country.
                              </h4>
                          </div>
                      </div>
                  </div>
              </div>
          </div>
          <!--/ END Section Header -->

          <!-- START Row -->
          <div class="row">
              <div class="col-md-6">
                  <!-- features #1 -->
                  <div class="table-layout">
                      <div class="col-xs-2 valign-top"><img src="{{asset('hfv1/image/icons/payperclick.png')}}" width="100%"></div>
                      <div class="col-xs-10 valign-top pl15">
                          <h4 class="section-title font-alt mt0">Get Paid by Referal OR Applying for a Job</h4>
                          <p>We provide excellent opportunities for job seekers and independent recruiters wanting to earn extra. Cash or relocation incentives await upon successful hiring.</p>
                      </div>
                  </div>
                  <!-- features #1 -->
                  <div class="visible-md visible-lg" style="margin-bottom:50px;"></div><!-- spacer -->
                  <!-- features #2 -->
                  <div class="table-layout">
                      <div class="col-xs-2 valign-top"><img src="{{asset('hfv1/image/icons/seoperfomance.png')}}" width="100%"></div>
                      <div class="col-xs-10 valign-top pl15">
                          <h4 class="section-title font-alt mt0">Control Cost & Reduce Time to Fill</h4>
                          <p>Search for people who meet your requirements the fastest possible way. Our database has a number of qualified candidates, soon to contribute largely to your company's growth.</p>
                      </div>
                  </div>
                  <!-- features #2 -->
                  <div class="visible-md visible-lg" style="margin-bottom:50px;"></div><!-- spacer -->
                  <!-- features #3 -->
                  <div class="table-layout">
                      <div class="col-xs-2 valign-top"><img src="{{asset('hfv1/image/icons/responsivewebdesign.png')}}" width="100%"></div>
                      <div class="col-xs-10 valign-top pl15">
                          <h4 class="section-title font-alt mt0">Human Factor free Applicant / Recruiter / Employer Tracking System</h4>
                          <p></p>
                      </div>
                  </div>
                  <!-- features #3 -->
                  <div class="visible-md visible-lg" style="margin-bottom:50px;"></div><!-- spacer -->
                  <!-- features #4 -->
                  <div class="table-layout">
                      <div class="col-xs-2 valign-top"><img src="{{asset('hfv1/image/icons/brandprotection.png')}}" width="100%"></div>
                      <div class="col-xs-10 valign-top pl15">
                          <h4 class="section-title font-alt mt0">Hire Quality Candidates</h4>
                          <p>This is our commitment to all employers that wish to partner with us- to provide you with eligible and competent applicants ready to take your company to the next level</p>
                      </div>
                  </div>
                  <!-- features #4 -->
              </div>

              <div class="col-md-6">
                  <!-- Register form -->
                  <form class="panel no-border nm" name="form-register" method="POST" action="{{ route('register') }}" data-parsley-validate>
                  {{ csrf_field() }}
                      <ul class="list-table pa15">
                          <li>
                              <!-- Alert message -->
                              <div class="alert alert-warning nm">
                                  <span class="semibold">Note :</span>&nbsp;&nbsp;Please fill all the below field.
                              </div>
                              <!--/ Alert message -->
                          </li>
                          <li class="text-right" style="width:20px;"><a href="javascript:void(0);"><i class="ico-question-sign fsize16"></i></a></li>
                      </ul>
                      <div class="panel-body">
                        @if (count($errors) > 0)
                          <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                          </div>
                        @endif
                          <div class="row">
                            <div class="col-md-4">
                                 <div class="radio">
                                    <label>
                                      <input type="radio" name="type" value="applicant"
                                      {{ (old('type') == 'applicant')?'checked="checked"':'' }}  required="" data-parsley-errors-container="#error-container" data-parsley-error-message="Please select any of this choices"> Job Seeker
                                    </label>
                                  </div>
                            </div>
                            <div class="col-md-4">
                                <div class="radio">
                                    <label>
                                      <input type="radio"  name="type" value="recruiter" {{ (old('type') == 'recruiter')?'checked="checked"':'""' }}> Recruiter
                                    </label>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="radio">
                                  <label>
                                    <input type="radio"  name="type" value="employer" {{ (old('type') == 'employer')?'checked="checked"':'""' }}> Employer
                                  </label>
                                </div>
                            </div>
                          </div>
                          <hr>
                          <div class="row">
                            <div class="col-md-6">
                              <label class="control-label">First Name</label>
                              <div class="has-icon pull-left">
                                  <input type="text" class="form-control" value="{{old('firstname')}}" name="firstname" data-parsley-required autofocus="">
                                  <i class="ico-user2 form-control-icon"></i>
                              </div>
                            </div>
                            <div class="col-md-6">
                              <label class="control-label">Last Name</label>
                              <div class="form-group">
                                  <input type="text" class="form-control" value="{{old('lastname')}}" name="lastname" data-parsley-required>
                              </div>
                            </div>
                          </div>
                          <div class="form-group">
                              <label class="control-label">Password</label>
                              <div class="has-icon pull-left">
                                  <input type="password" class="form-control" name="password" data-parsley-required>
                                  <i class="ico-key2 form-control-icon"></i>
                              </div>
                          </div>
                          <div class="form-group">
                              <label class="control-label">Retype Password</label>
                              <div class="has-icon pull-left">
                                  <input type="password" class="form-control" name="password_confirmation" data-parsley-equalto="input[name=password]" data-parsley-error-message="Please fill in your password and should be the same with password">
                                  <i class="ico-asterisk form-control-icon"></i>
                              </div>
                          </div>
                          <p class="semibold text-muted">To confirm and activate your new account, we will need to send the activation code to your e-mail.</p>
                          <div class="form-group">
                              <label class="control-label">Email</label>
                              <div class="has-icon pull-left">
                                  <input type="email" class="form-control" name="email" value="{{old('email')}}" placeholder="you@mail.com">
                                  <i class="ico-envelop form-control-icon"></i>
                              </div>
                          </div>
                          <div class="form-group">
                              <div class="checkbox custom-checkbox">
                                  <input type="checkbox" name="agree" id="agree" value="1" required="">
                                  <label for="agree">&nbsp;&nbsp;I agree to the <a class="semibold" href="javascript:void(0);">Term Of Services</a></label>
                              </div>
                          </div>
                      </div>
                      <div class="panel-footer">
                          <button type="submit" class="btn btn-block btn-lg btn-success"><span class="semibold">Sign up</span></button>
                      </div>
                  </form>
                  <!-- Register form -->
              </div>
          </div>
          <!--/ END Row -->
      </div>
  </section>
  <!--/ END Register Content -->
@stop
