@extends('Landing::template.main')

@section('content')
    <!-- START page header -->
    <section class="page-header page-header-block nm">
        <!-- pattern -->
        <div class="pattern pattern9"></div>
        <!--/ pattern -->
        <div class="container pt15 pb15">
            <div class="page-header-section">
                <h4 class="title">Login</h4>
            </div>
            <div class="page-header-section">
                <!-- Toolbar -->
                <div class="toolbar">
                    <ol class="breadcrumb breadcrumb-transparent nm">
                        <li><a href="{{route('home')}}">Human Factor</a></li>
                        <li class="active">Login</li>
                    </ol>
                </div>
                <!--/ Toolbar -->
            </div>
        </div>
    </section>
    <div class="clearfix"></div>
    <!--/ END page header -->

    <!-- START Register Content -->
    <section class="section">
        <div class="container">
            <!-- START Section Header -->
            <div class="row">
                <div class="col-md-12">
                    <div class="section-header text-center">
                        <h1 class="section-title font-alt">Login to your Account</h1>
                    </div>
                </div>
            </div>
            <!--/ END Section Header -->

            <!-- START Row -->
            <div class="row">
                <div class="col-md-6 col-md-offset-3">
                    <!-- Login form -->
                    <form class="panel no-border mb0" name="form-login" method="POST" action="{{ route('login') }}" data-parsley-validate>
                    {{ csrf_field() }}
                        <div class="panel-body">
                            <!-- Alert message -->
                            <div class="alert alert-warning">
                                <span class="semibold">Note :</span>&nbsp;&nbsp;Just put anything and hit 'Login' button.
                            </div>
                            <!--/ Alert message -->
                            <div class="form-group">
                                <div class="form-stack has-icon pull-left">
                                    <input name="email" type="text" value="{{old('email')}}" class="form-control input-lg" placeholder="Email" data-parsley-errors-container="#error-container" data-parsley-error-message="Please fill in your email" data-parsley-required>
                                    <i class="ico-user2 form-control-icon"></i>
                                </div>
                                <div class="form-stack has-icon pull-left">
                                    <input name="password" type="password" class="form-control input-lg" placeholder="Password" data-parsley-errors-container="#error-container" data-parsley-error-message="Please fill in your password" data-parsley-required>
                                    <i class="ico-lock2 form-control-icon"></i>
                                </div>
                            </div>

                            <!-- Error container -->
                            <div id="error-container" class="mb15">
                                @if (count($errors) > 0)
                                    <ul class="parsley-errors-list filled">
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                @endif
                            </div>
                            <!--/ Error container -->

                            <div class="form-group">
                                <div class="row">
                                    <div class="col-xs-6">
                                        <div class="checkbox custom-checkbox">
                                            <input type="checkbox" name="remember" id="remember" value="1" {{ old('remember') ? 'checked' : '' }}>
                                            <label for="remember">&nbsp;&nbsp;Remember me</label>
                                        </div>
                                    </div>
                                    <div class="col-xs-6 text-right">
                                        <a href="{{ route('password.request') }}">Lost password?</a>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group nm">
                                <button type="submit" class="btn btn-block btn-primary"><span class="semibold">Login</span></button>
                            </div>
                        </div>
                    </form>
                    <!-- Login form -->
                </div>
            </div>
            <!--/ END Row -->
        </div>
    </section>
    <!--/ END Register Content -->
@stop