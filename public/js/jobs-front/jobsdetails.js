var jobDetail = {
  init : function(){
    this.misc();
    this.sendEmailInvite()
    this.selectizeEmail()
    $('.moneymask').inputmask({
        'alias' : 'numeric',
        'groupSeparator' : ',',
        'autoGroup' : true,
        'digits' : 2,
        'digitsOptional' : false,
        'prefix' : 'Php ',
        'placeholder' : '0'
    });
    this.apply();
    let job = $('#dat').data('job');
    $('#cover_letter').val(jobDetail.coverLetter(job).trim());
  },
  apply : function(data){
    $('#applyJob').click(function(e){
      e.preventDefault();
      e.stopImmediatePropagation();
      var x = {'applicant_cover_letter' : $('#application-form').find('[name="applicant_cover_letter"]').val(),
              'expected_salary': $('[name="expected_salary"]').val(),
            'job_code':$('#application-form').find('[name="job_code"]').val()};
      // console.log(x);
      $.ajax({
        url : window.location.origin + '/jobs/applied',// '{{-- route('jobs.applied') --}}',
        type: 'post',
        data : x,
        success : function(result){
          if(result.success){
            $('#application-form').remove();
            $('#for-applicant').append($('<button type="button" class="appliedbtn btn btn-sm btn-info btn-smallpad" data-hasApplied="true"  data-toggle="tooltip" data-placement="bottom" title="Good Luck!"><i class="ico-checkmark-circle"></i> Applied</button>'))
            // applyJob.misc();
            swal(
              'Good job!',
              'You have applied for this job',
              'success'
            )
          }
        },
        error : function(result){
          swal(
            'Oops...',
            'Opps! Something went wrong!',
            'error'
          )
        }
      })
    });

  },
  coverLetter : function coverLetter(data) {
return `Dear ${data.employer.company_name},

Hello and good day!

This letter is in response to your ${data.job_title} job advertisement in HumanFactor.ph on {{date('F d Y')}}.

[Short description of your achievements and contact information here]

Thank you very much.

Sincerely yours,

`;
  },
  sendEmailInvite : function(){

  },
  selectizeEmail : function(){
    var REGEX_EMAIL = '([a-z0-9!#$%&\'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&\'*+/=?^_`{|}~-]+)*@' +
                '(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?)';

      $('#select-email').selectize({
          persist: true,
          valueField: 'email',
          labelField: 'my_name',
          searchField: ['my_name', 'email'],
          create: true,
          render: {
              item: function(item, escape) {
                  return '<div class="bg-primary">' +
                      (item.my_name ? '<span class="name">' + escape(item.my_name) + '</span>' : '') +
                      (item.email ? '<span class="email">' + escape(item.email) + '</span>' : '') +
                  '</div>';
              },
              option: function(item, escape) {
                  var label = item.my_name || item.email;
                  var caption = item.my_name ? item.email : null;
                  return '<div class="bg-primary">' +
                      '<span class="label">' + escape(label) + '</span>' +
                      (caption ? '<span class="caption">' + escape(caption) + '</span>' : '') +
                  '</div>';
              }
          },
          createFilter: function(input) {
              var match, regex;

              // email@address.com
              regex = new RegExp('^' + REGEX_EMAIL + '$', 'i');
              match = input.match(regex);
              if (match) return !this.options.hasOwnProperty(match[0]);

              // name <email@address.com>
              regex = new RegExp('^([^<]*)\<' + REGEX_EMAIL + '\>$', 'i');
              match = input.match(regex);
              if (match) return !this.options.hasOwnProperty(match[2]);

              return false;
          },
          create: function(input) {
              if ((new RegExp('^' + REGEX_EMAIL + '$', 'i')).test(input)) {
                  return {email: input};
              }
              var match = input.match(new RegExp('^([^<]*)\<' + REGEX_EMAIL + '\>$', 'i'));
              if (match) {
                  return {
                      email : match[2],
                      name  : $.trim(match[1])
                  };
              }
              alert('Invalid email address.');
              return false;
          },
          load: function(query, callback) {
              if (!query.length) return callback();
              $.ajax({
                  url: window.location.origin + '/fetch-myreferrals',
                  type: 'GET',
                  data : {
                    q: query
                  },
                  error: function() {
                      callback();
                  },
                  success: function(res) {
                    console.log(res);
                      callback(res.referrals);
                  }
              });
          }
      });
  },
  misc : function(){
    window.fbAsyncInit = function() {
      FB.init({
        appId      : '219282678573488',
        xfbml      : true,
        version    : 'v2.9'
      });
      FB.AppEvents.logPageView();
    };

    (function(d, s, id){
       var js, fjs = d.getElementsByTagName(s)[0];
       if (d.getElementById(id)) {return;}
       js = d.createElement(s); js.id = id;
       js.src = "//connect.facebook.net/en_US/sdk.js";
       fjs.parentNode.insertBefore(js, fjs);
     }(document, 'script', 'facebook-jssdk'));

     document.getElementById('shareBtn').onclick = function() {
       FB.ui({
         method: 'share',
         display: 'popup',
         href: window.location.href,
       }, function(response){});
     }

     //selectize
     $('#email').on('show.bs.modal', function (event) {
       var button = $(event.relatedTarget) // Button that triggered the modal
       var jobcode = button.data('jobcode') // Extract info from data-* attributes
       // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
       // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
       var modal = $(this)
       modal.find('[name="job_code"]').val(jobcode);
     })
  }
}
