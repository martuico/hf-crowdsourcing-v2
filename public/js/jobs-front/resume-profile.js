
var ResumeProfile = {
    init : function(){
        this.misc();
        this.addExperience();
        this.initDatePicker($('.datepicker'));
        // $('.datepicker').datepicker({ format: 'yyyy-mm-dd'});
        $('.moneymask').inputmask({
            'alias' : 'numeric',
            'groupSeparator' : ',',
            'autoGroup' : true,
            'digits' : 2,
            'digitsOptional' : false,
            'prefix' : 'Php ',
            'placeholder' : '0'
        });
        $('.numbermask').inputmask({
            'alias' : 'integer',
            'digitsOptional' : false,
            'placeholder' : '0'
        });
        $('.numbercode').inputmask('(9999) 999-9999');
        $('.skills').selectize({
                      create: true,
                      maxItems: 20
                    });

        $('.languages').selectize({
                        create: true,
                        maxItems: 20
                    });
        this.checkCurrent()
        this.yearGenerate()
        this.checkEmail()
        this.currentWork();
    },
    checkEmail : function(){
      $('[name="email"]').focusout(function(e){
          $.get(window.location.origin + '/epa/check-email',{'email':$('[name="email"]').val()})
              .success(function(res){
                  if(res.hasduplicate){
                      swal('Error!','Email is already taken','error');
                      $('[name="email"]').val('');
                      $('[name="email"]').focus();
                  }
              });
          e.preventDefault();
          e.stopImmediatePropagation();
      });
    },
    checkCurrent : function(){
      $('.checkCurrent').click(function(){
        var checkBoxDiv = $('.checkCurrent');
        // console.log($(this).find('input').is(':checked'))
        if($(this).find('input').is(':checked')){
            checkBoxDiv.parent().find('.endDropdown').hide();
            checkBoxDiv.parent().find('[name^="currently_work_here_"]').val('1');
            checkBoxDiv.parent().prepend($('<h3 class="currentText">Currently work here</h3>'));
            $('[name="current_employment_status"]').val('currently_employed')
        }else{
          checkBoxDiv.parent().find('.endDropdown').show();
          checkBoxDiv.parent().find('[name="currently_work_here"]').val('0');
          checkBoxDiv.parent().find('select').val('');
          $('.currentText').remove()
          $('[name="current_employment_status"]').val('actively_seeking')
        }
      });
    },
    currentWork : function(){
      var checkBoxDiv = $('.checkCurrent');
      // console.log(checkBoxDiv.find('input').is(':checked'))
      if(checkBoxDiv.find('input').is(':checked')){
          checkBoxDiv.parent().find('.endDropdown').hide();
          checkBoxDiv.parent().find('[name^="currently_work_here_"]').val('1');
          checkBoxDiv.parent().prepend($('<h3 class="currentText">Currently work here</h3>'));
          $('[name="current_employment_status"]').val('currently_employed')
      }else{
        checkBoxDiv.parent().find('.endDropdown').show();
        checkBoxDiv.parent().find('[name="currently_work_here"]').val('0');
        $('.currentText').remove()
        $('[name="current_employment_status"]').val('actively_seeking')
      }
    },
    addExperience : function(){
        $('#addExperience').click(function(){
            let countExp = $('.experiences').children().length + 1;
            $('.experiences').append(ResumeProfile.experienceForm(countExp));
            ResumeProfile.yearGenerate();
            ResumeProfile.checkCurrent();
            ResumeProfile.removeExp();
        });
    },
    yearGenerate : function(){
      var gen = $('.year').attr('data-years');//{!! json_encode($year_arr) !!};
      gen = $.parseJSON(gen);
    //   console.log(gen);
      $.each(gen,function(k,v){
        $('.yeargen').append($('<option value="'+v+'">'+v+'</option>'))
      })
    },
    removeExp : function(){
      $('.removableexp').click(function(){
        $(this).parent().parent().remove();
      });
    },
    experienceForm : function(countExp) {
        return `
        <div class="row" id="exp_${countExp}">
            <div class="col-md-6">
                <div class="form-group">
                    <div class="row">
                      <div class="col-md-6">
                        <label>Position</label>
                        <input type="text" class="form-control" value=""  name="position_${countExp}" placeholder="Postion">
                      </div>
                      <div class="col-md-6">
                        <label>Company</label>
                        <input type="text" class="form-control" value=""  name="company_name_${countExp}" placeholder="Company Name">
                      </div>
                    </div>
                </div>

                <div class="form-group">
                    <div class="row">
                        <div class="col-md-6">
                          <label>From</label>
                          <select name="startMonth_${countExp}" class="form-control">
                            <option value="">Month</option>
                            <option value="1">January</option>
                            <option value="2">February</option>
                            <option value="3">March</option>
                            <option value="4">April</option>
                            <option value="5">May</option>
                            <option value="6">June</option>
                            <option value="7">July</option>
                            <option value="8">August</option>
                            <option value="9">September</option>
                            <option value="10">October</option>
                            <option value="11">November</option>
                            <option value="12">December</option>
                          </select>
                          <br>
                          <select name="startYear_${countExp}" class="form-control yeargen">
                            <option value="">Year</option>
                          </select>
                        </div>
                        <div class="col-md-6">
                          <div class="endDropdown">
                            <label>To</label>
                            <select name="endMonth_${countExp}" class="form-control">
                              <option value="">Month</option>
                              <option value="1">January</option>
                              <option value="2">February</option>
                              <option value="3">March</option>
                              <option value="4">April</option>
                              <option value="5">May</option>
                              <option value="6">June</option>
                              <option value="7">July</option>
                              <option value="8">August</option>
                              <option value="9">September</option>
                              <option value="10">October</option>
                              <option value="11">November</option>
                              <option value="12">December</option>
                            </select>
                            <br>
                            <select name="endYear_${countExp}" class="form-control yeargen">
                              <option value="">Year</option>
                            </select>
                          </div>
                          <br>
                          <input type="hidden" name="currently_work_here_${countExp}">
                          <div class="checkbox custom-checkbox checkCurrent">
                              <input type="checkbox" name="toCurrent_${countExp}" value="1" id="toCurrent_${countExp}">
                              <label for="toCurrent_${countExp}">&nbsp;&nbsp;I currently work here</label>
                          </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <button class="btn btn-danger btn-xs removableexp pull-right"><i class="fa fa-times-circle"></i></button>
                <div class="form-group">
                  <div class="col-md-12">
                    <label>Reason for leaving</label>
                      <textarea class="form-control" rows="3" name="resposibilities_1" placeholder="Tell us about your responsibilities" data-parsley-error-message="Reason for leaving"></textarea>
                  </div>
                </div>
            </div>
          </div>
        `;
    },
    initDatePicker: function(elem){
        elem.datepicker({ format: 'yyyy-mm-dd'});
    },
    misc : function(){

        window.ParsleyValidator
          .addValidator('filemaxsize', function (val, req) {
              var reqs = req.split('|');
              var input = $('input[type="file"][name="'+reqs[0]+'"]');
              var maxsize = reqs[1];
              var max_bytes = maxsize * 1000000, file = input.files[0];

              return file.size <= max_bytes;

          }, 32).addMessage('en', 'filemaxsize', 'The File size is too big.')
        //   if($('.selc').is(':visible') && $('.selc').length > 0){
        //       $('.selc').selectize({
        //           sortField: 'text'
        //       });
        //   }


          $('.select2').select2({
            allowClear : true
          });


        $('[name="current_region"]').change(function(e){
          var regcode = $(this).val();
          console.log('current_region',regcode);
          $.ajax({
            url : window.location.origin + '/epa/fetch-province/' + regcode,
            type : 'get',
            success : function(res){
              if(res.success){
                var x = '';
                $('[name="current_province" ]').children().remove();
                $('[name="current_city" ]').children().remove();
                $('[name="current_city" ]').prop('disabled','disabled');
                $('[name="current_province" ]').select2('val',null);
                $('[name="current_city" ]').select2('val',null);
                $.each(res.provinces,function(k,v){
                  x  +='<option value="'+v.provCode+'">'+v.provDesc+'</option>';
                });
                $('[name="current_province" ]').append($(x));
                $('[name="current_province" ]').removeAttr('disabled');
              }
            },error : function(res){

            }
          });
          e.stopImmediatePropagation();
          e.preventDefault();
        });

        $('[name="current_province" ]').change(function(e){
          var provcode = $(this).val();
          $.ajax({
            url : window.location.origin + '/epa/fetch-cities/' + provcode,
            type : 'get',
            success : function(res){
              if(res.success){
                var x = '';
                $('[name="current_city" ]').children().remove();
                $('[name="current_city" ]').select2('val',null);
                $.each(res.cities,function(k,v){
                  x  +='<option value="'+v.citymunCode+'">'+v.citymunDesc+'</option>';
                });
                $('[name="current_city" ]').append($(x));
                $('[name="current_city" ]').removeAttr('disabled');
              }
            },error : function(res){

            }
          });
          e.preventDefault();
          e.stopImmediatePropagation();
        });

        $('[name="hometown_region"]').change(function(e){
          var regcode = $(this).val();
          console.log('hometown_region',regcode);
          $.ajax({
            url : window.location.origin + '/epa/fetch-province/' + regcode,
            type : 'get',
            success : function(res){
              if(res.success){
                var x = '';
                $('[name="hometown_province" ]').children().remove();
                $('[name="hometown_city" ]').children().remove();
                $('[name="hometown_city" ]').prop('disabled','disabled');
                $('[name="hometown_province" ]').select2('val',null);
                $('[name="hometown_city" ]').select2('val',null);
                $.each(res.provinces,function(k,v){
                  x  +='<option value="'+v.provCode+'">'+v.provDesc+'</option>';
                });
                $('[name="hometown_province" ]').append($(x));
                $('[name="hometown_province" ]').removeAttr('disabled');
              }
            },error : function(res){

            }
          });
          e.stopImmediatePropagation();
          e.preventDefault();
        });

        $('[name="hometown_province" ]').change(function(e){
          var provcode = $(this).val();
          $.ajax({
            url : window.location.origin + '/epa/fetch-cities/' + provcode,
            type : 'get',
            success : function(res){
              if(res.success){
                var x = '';
                $('[name="hometown_city" ]').children().remove();
                $('[name="hometown_city" ]').select2('val',null);
                $.each(res.cities,function(k,v){
                  x  +='<option value="'+v.citymunCode+'">'+v.citymunDesc+'</option>';
                });
                $('[name="hometown_city" ]').append($(x));
                $('[name="hometown_city" ]').removeAttr('disabled');
              }
            },error : function(res){

            }
          });
          e.preventDefault();
          e.stopImmediatePropagation();
        });


    }
}
