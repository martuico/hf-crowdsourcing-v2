<?php

use Illuminate\Database\Seeder;

class ApplicantEducationLevelSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        App\ApplicantEducationLevel::create(['name' => 'Undergraduate Degree']);
        App\ApplicantEducationLevel::create(['name' => '2 Years in College']);
        App\ApplicantEducationLevel::create(['name' => 'High School Graduate']);
        App\ApplicantEducationLevel::create(['name' => 'Graduate Degree']);
        App\ApplicantEducationLevel::create(['name' => 'Diploma/Associate Holder']);
        App\ApplicantEducationLevel::create(['name' => 'Doctorate Degree']);
        App\ApplicantEducationLevel::create(['name' => "Master's Degree"]);
    }
}
