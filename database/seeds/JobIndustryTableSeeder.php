<?php

use Illuminate\Database\Seeder;

class JobIndustryTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\JobIndustry::truncate();
        \App\JobType::truncate();
        \App\JobLevel::truncate();
        \App\Shift::truncate();
        foreach(categories() as $key => $value){
            \App\JobIndustry::create(['name' => $value]);
        }

        \App\JobType::create([
            'name' => 'Full Time'
        ]);

        \App\JobType::create([
            'name' => 'Full Time / Home Base'
        ]);

        \App\JobType::create([
           'name' => 'Part Time'
        ]);

        \App\JobType::create([
           'name' => 'Part Time / Home Base'
        ]);

        \App\JobLevel::create([
           'name' => 'OJT'
        ]);
        \App\JobLevel::create([
           'name' => 'No Experience'
        ]);
        \App\JobLevel::create([
           'name' => '1 Year or Less'
        ]);
        \App\JobLevel::create([
           'name' => '2 to 4 Years'
        ]);
        \App\JobLevel::create([
           'name' => '5+ Years'
        ]);
        \App\JobLevel::create([
           'name' => 'Managerial / Supervisory'
        ]);
        \App\JobLevel::create([
           'name' => 'Top Management'
        ]);
        \App\Shift::create([
            'name' => 'Shifting'
        ]);
        \App\Shift::create([
            'name' => 'Day Shift'
        ]);
        \App\Shift::create([
            'name' => 'Mid Shift'
        ]);
        \App\Shift::create([
            'name' => 'Night Shift'
        ]);
    }
}
