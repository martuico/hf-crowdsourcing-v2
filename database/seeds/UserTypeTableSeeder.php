<?php

use Illuminate\Database\Seeder;

class UserTypeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $admin = \App\UserType::create([
            'name' => 'Admin'
        ]);
        $employer = \App\UserType::create([
            'name' => 'Employer'
        ]);
        \App\UserType::create([
            'name' => 'Employer-Recruiter'
        ]);
        $internal = \App\UserType::create([
            'name' => 'Internal-Recruiter'
        ]);
        \App\UserType::create([
            'name' => 'Applicant'
        ]);
        \App\UserType::create([
            'name' => 'Recruiter'
        ]);

        \App\User::create([
            'user_type_id' => $employer->id,
            'firstname' => 'Mar',
            'lastname' => 'Tuico',
            'email' => 'mar.tuico@gmail.com',
            'password' => \Hash::make('password'),
            'is_active' => 1
        ]);

        \App\User::create([
            'user_type_id' => $admin->id,
            'firstname' => 'Justin',
            'lastname' => 'Medel',
            'email' => 'justin.medel@humanfactor.ph',
            'password' => \Hash::make('password'),
            'is_active' => 1
        ]);

        $internal_user_1 = \App\User::create([
            'user_type_id' => $internal->id,
            'firstname' => 'One',
            'lastname' => 'Recruiter',
            'email' => 'recruiter.one@humanfactor.ph',
            'password' => \Hash::make('password'),
            'is_active' => 1
        ]);

        \App\IRecruiter::create([
          'user_id' => $internal_user_1->id,
          'task_completed' => 0,
          'task_on_hand' => 0,
          'status' => 'active',
        ]);

        $internal_user_2 = \App\User::create([
            'user_type_id' => $internal->id,
            'firstname' => 'Two',
            'lastname' => 'Recruiter',
            'email' => 'recruiter.two@humanfactor.ph',
            'password' => \Hash::make('password'),
            'is_active' => 1
        ]);

        \App\IRecruiter::create([
          'user_id' => $internal_user_2->id,
          'task_completed' => 0,
          'task_on_hand' => 0,
          'status' => 'active',
        ]);
    }
}
