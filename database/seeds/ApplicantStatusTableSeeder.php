<?php

use Illuminate\Database\Seeder;

class ApplicantStatusTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\ApplicantStatus::truncate();

        \App\ApplicantStatus::create([
              'name' => 'Pipeline'
        ]);
        \App\ApplicantStatus::create([
            'name' => 'Endorsed'
        ]);
        \App\ApplicantStatus::create([
            'name' => 'Re profile to Premium Job post'
        ]);
        \App\ApplicantStatus::create([
            'name' => 'Active File'
        ]);

        \App\ApplicantStatus::create([
            'name' => 'Refer to Regular Job Post'
        ]);


        \App\ApplicantStatus::create([
            'name' => 'No Show'
        ]);

        \App\ApplicantStatus::create([
            'name' => 'Re schedule'
        ]);

        \App\ApplicantStatus::create([
            'name' => 'Hired'
        ]);
    }
}
