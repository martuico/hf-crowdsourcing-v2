<?php

use Illuminate\Database\Seeder;

class EmployerApplicantStatusSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        App\EmployerApplicantStatus::truncate();

        \App\EmployerApplicantStatus::create([
              'name' => 'No Show'
        ]);
        \App\EmployerApplicantStatus::create([
            'name' => 'Passed Initial Interview'
        ]);
        \App\EmployerApplicantStatus::create([
            'name' => 'No Go Initial Interview'
        ]);
        \App\EmployerApplicantStatus::create([
            'name' => 'Passed Final Interview'
        ]);

        \App\EmployerApplicantStatus::create([
            'name' => 'No Go Final Interview'
        ]);


        \App\EmployerApplicantStatus::create([
            'name' => 'Passed Account Validation'
        ]);

        \App\EmployerApplicantStatus::create([
            'name' => 'Failed Account Validation'
        ]);

        \App\EmployerApplicantStatus::create([
            'name' => 'Hired'
        ]);
    }
}
