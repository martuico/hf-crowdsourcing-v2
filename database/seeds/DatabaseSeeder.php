<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UserTableSeeder::class);
        $this->call(ApplicantEducationLevelSeeder::class);
        $this->call(ApplicantStatusTableSeeder::class);
        $this->call(EmployerApplicantStatusSeeder::class);
        $this->call(JobIndustryTableSeeder::class);
        $this->call(UserTypeTableSeeder::class);
    }
}
