<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateApplicantSkillTable extends Migration {

	public function up()
	{
		Schema::create('applicant_skill', function(Blueprint $table) {
			$table->increments('id');
			$table->integer('applicant_id')->unsigned();
			$table->integer('skill_id')->unsigned();
		});
	}

	public function down()
	{
		Schema::drop('applicant_skill');
	}
}