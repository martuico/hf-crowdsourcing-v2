<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateApplicantEducationsTable extends Migration {

	public function up()
	{
		Schema::create('applicant_educations', function(Blueprint $table) {
			$table->increments('id');
			$table->integer('applicant_id')->unsigned();
			$table->string('school_name')->nullable();
			$table->string('attainment')->nullable();
			$table->date('start_date')->nullable();
			$table->date('end_date')->nullable();
			$table->text('awards')->nullable();
			$table->timestamps();
		});
	}

	public function down()
	{
		Schema::drop('applicant_educations');
	}
}