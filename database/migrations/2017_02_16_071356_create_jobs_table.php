<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateJobsTable extends Migration {

	public function up()
	{
		Schema::create('jobs', function(Blueprint $table) {
			$table->increments('id');
			$table->integer('employer_id')->unsigned();
			$table->string('job_code',60)->unique();
			$table->enum('status', array('pending', 'open', 'closed'))->default('pending');
			$table->string('job_title')->nullable();
			$table->string('slug')->nullable();
			$table->integer('job_type_id')->unsigned();
			$table->integer('job_industry_id')->unsigned();
			$table->integer('job_level_id')->unsigned();
			$table->string('min_exp')->nullable();
			$table->string('max_exp')->nullable();
			$table->string('salary_from')->nullable();
			$table->string('salary_to')->nullable();
			$table->integer('work_shift_id')->unsigned();
			$table->string('location_region')->nulable();
			$table->string('location_province')->nulable();
			$table->string('location_city')->nulable();
			$table->integer('education_level_id')->unsigned();
			$table->boolean('provide_relocation')->default('0');
			$table->text('description')->nullable();
			$table->integer('order_level')->default('0');
			$table->string('finders_fee')->nullable();
			$table->string('referral_reward')->nullable();
			$table->string('applicant_reward')->nullable();
			$table->string('designation')->nullable();
			$table->text('work_location')->nullable();
			$table->text('skills')->nullable();
			$table->date('valid_until')->nullable();
			$table->boolean('show_salary_range')->default(0)->nullable();
			$table->timestamps();
			$table->softDeletes();
		});
	}

	public function down()
	{
		Schema::drop('jobs');
	}
}
