<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateApplicantEmployerFeedbacksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('applicant_employer_feedbacks', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('applicant_id')->unsigned();
            $table->integer('employer_id')->unsigned();
            $table->integer('applicant_job_id')->unsigned();
            $table->integer('job_id')->unsigned();
            $table->text('reason')->nullable();
      		$table->text('notes')->nullable();
            $table->timestamp('schedule_account_validation')->nullable();
            $table->timestamp('schedule_job_offer')->nullable();
            $table->timestamp('schedule_final_interview')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('applicant_employer_feedbacks');
    }
}
