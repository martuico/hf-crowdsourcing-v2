<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateEmployersTable extends Migration {

	public function up()
	{
		Schema::create('employers', function(Blueprint $table) {
			$table->increments('id');
			$table->boolean('is_premium')->default(0);
			$table->integer('user_id')->unsigned();
			$table->string('logo_url')->nullable();
			$table->string('contact_designation')->nullable();
			$table->string('office_address');
			$table->string('company_name');
			$table->string('slug');
			$table->text('description')->nullable();
			$table->string('office_number_1')->nullable();
			$table->string('office_number_2')->nullable();
			$table->string('contact_person_mobile')->nullable();
			$table->string('contact_person_mobil_2')->nullable();
			$table->string('website')->nullable();
			$table->string('sm_facebook')->nullable();
			$table->string('sm_linkedin')->nullable();
			$table->string('sm_twitter')->nullable();
			$table->boolean('is_verified')->default(0);
			$table->timestamps();
		});
	}

	public function down()
	{
		Schema::drop('employers');
	}
}