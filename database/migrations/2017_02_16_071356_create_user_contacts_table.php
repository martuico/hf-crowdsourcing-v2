<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateUserContactsTable extends Migration {

	public function up()
	{
		Schema::create('user_contacts', function(Blueprint $table) {
			$table->increments('id');
			$table->integer('user_id')->unsigned();
			$table->enum('com_type', array('mobile', 'landline', 'fax'));
			$table->enum('network', array('sun', 'globle', 'smart', 'pldt'));
			$table->string('number')->nullable();
		 $table->timestamps();
		});
	}

	public function down()
	{
		Schema::drop('user_contacts');
	}
}