<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRewardsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rewards', function (Blueprint $table) {
            $table->increments('id');
            $table->string('reward_code')->unique();
            $table->integer('applicant_job_id')->unsigned();
            $table->integer('applicant_id')->unsigned()->nullable();
            $table->date('date_applicant_paid')->nullabe();
            $table->enum('applicant_reward_status',['pending','confirmed','paid'])->default('pending');
            $table->string('hiring_incentives')->nullable();
            $table->date('due_hiring_incentives')->nullabe();
            $table->integer('referred_by_id')->unsigned()->nullable();
            $table->enum('referral_reward_status',['pending','confirmed','paid'])->default('pending');
            $table->date('date_referral_paid')->nullabe();
            $table->string('referral_incentives')->nullable();
            $table->date('due_referral_incentives')->nullabe();
            $table->integer('hf_assgined_id')->unsigned()->nullable();
            $table->integer('employer_id')->unsigned()->nullable();
            $table->enum('employer_payment_status',['pending','confirmed','paid'])->default('pending');
            $table->date('date_employer_paid')->nullabe();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rewards');
    }
}
