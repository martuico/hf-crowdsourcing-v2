<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateApplicantsTable extends Migration {

	public function up()
	{
		Schema::create('applicants', function(Blueprint $table) {
			$table->increments('id');
			$table->string('auid',90)->unique();
			$table->integer('user_id')->unsigned();
			$table->integer('job_industry_id')->unsigned();
			$table->integer('education_level_id')->unsigned();
			$table->string('last_school')->nullable();
			$table->boolean('has_professional_license')->default(0)->nullable();
			$table->string('location_region')->unsigned()->nullable();
			$table->string('location_province')->unsigned()->nullable();
			$table->string('location_city')->unsigned()->nullable();
			$table->string('prefered_work_location')->unsigned()->nullable();
			$table->boolean('willing_to_relocate')->default(0)->nullabe();
			$table->enum('current_employment_status', array('actively_seeking', 'currently_employed'));
			$table->enum('availability', array('asap', 'within_a_month','others'));
			$table->string('expected_salary')->nullabe();
			$table->string('field_of_study')->nullabe();
			$table->string('position')->nullable();
			$table->string('current_province')->nullable();
			$table->string('current_city')->nullable();
			$table->string('current_address')->nullable();
			$table->string('current_region')->nullable();
			$table->string('hometown_city')->nullable();
			$table->string('hometown_region')->nullable();
			$table->string('hometown_province')->nullable();
			$table->string('hometown_address')->nullable();
			$table->string('mobile_number_1')->nullable();
			$table->string('mobile_number_2')->nullable();
			$table->string('house_number_1')->nullable();
			$table->date('date_of_birth')->nullable();
			$table->text('languages')->nullable();
			$table->text('skills')->nullable();
			$table->text('biography')->nullable();
			$table->string('attached_resume')->nullable();
			$table->integer('hf_assigned')->unsigned()->default(0);
			$table->integer('referred_by_id')->unsigned()->default(0);
			$table->date('referred_start_date')->nullable();
			$table->date('referred_expired_date')->nullable();
			$table->date('referred_expired_date')->nullable();
			$table->integer('referral_link')->unsigned()->default(0)->nullabe();
			$table->timestamps();
		});
	}

	public function down()
	{
		Schema::drop('applicants');
	}
}
