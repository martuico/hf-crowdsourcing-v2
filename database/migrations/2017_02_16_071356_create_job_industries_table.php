<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateJobIndustriesTable extends Migration {

	public function up()
	{
		Schema::create('job_industries', function(Blueprint $table) {
			$table->increments('id');
			$table->string('name')->nullable();
			$table->string('slug')->nullable();
		});
	}

	public function down()
	{
		Schema::drop('job_industries');
	}
}