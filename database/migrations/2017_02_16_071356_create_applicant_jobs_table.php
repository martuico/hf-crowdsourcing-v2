<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateApplicantJobsTable extends Migration {

	public function up()
	{
		Schema::create('applicant_jobs', function(Blueprint $table) {
			$table->increments('id');
			$table->string('application_code',60)->unique();
			$table->integer('applicant_id')->unsigned();
			$table->integer('employer_id')->unsigned();
			$table->integer('job_id')->unsigned();
			// $table->integer('referred_by_id')->unsigned();
			// $table->integer('hf_assigned')->unsigned();
			$table->integer('applicant_status_id')->unsigned();
			$table->integer('employer_applicant_status_id')->unsigned()->default(0);
			// $table->date('referred_start_date')->nullable();
			// $table->date('referred_expired_date')->nullable();
			$table->boolean('is_visible_to_employer')->default(0);
			$table->timestamp('employer_appointment_sched')->nullable();
			$table->enum('employer_appointment_type', array('on-site','call'))->nullable();
			$table->enum('emplooyer_appointment_status', array('pending','confirmed','completed'))->default('pending')->nullable();
			$table->enum('billing_status', array('open', 'pending', 'paid'))->nullable();
			$table->text('applicant_cover_letter')->nullable();
			$table->string('expected_salary')->nullable();
			$table->string('endorsement_slip')->nullable();
			$table->string('hire_account')->nullable();
			$table->text('reason')->nullable();
			$table->text('notes')->nullable();
			$table->date('hire_date')->nullable();
			$table->date('date_of_training')->nullable();
			$table->softDeletes();
			$table->timestamps();
		});
	}

	public function down()
	{
		Schema::drop('applicant_jobs');
	}
}
