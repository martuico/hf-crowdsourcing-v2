<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCompanyRecruiterTable extends Migration {

	public function up()
	{
		Schema::create('company_recruiter', function(Blueprint $table) {
			$table->increments('id');
			$table->integer('employer_id')->unsigned();
			$table->integer('recruiter_id')->unsigned();
			$table->timestamps();
		});
	}

	public function down()
	{
		Schema::drop('company_recruiter');
	}
}