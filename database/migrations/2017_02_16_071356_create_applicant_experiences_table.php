<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateApplicantExperiencesTable extends Migration {

	public function up()
	{
		Schema::create('applicant_experiences', function(Blueprint $table) {
			$table->increments('id');
			$table->integer('applicant_id')->unsigned();
			$table->string('position')->nullable();
			$table->string('company_name')->nullable();
			$table->string('startMonth')->nullable();
			$table->string('startYear')->nullable();
			$table->string('endMonth')->nullable();
			$table->string('endYear')->nullable();
			$table->boolean('currently_work_here')->default(0);
			$table->date('start_date')->nullable();
			$table->date('end_date')->nullable();
			$table->text('resposibilities')->nullable();
			$table->timestamps();
		});
	}

	public function down()
	{
		Schema::drop('applicant_experiences');
	}
}
