<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateApplicantStatusTable extends Migration {

	public function up()
	{
		Schema::create('applicant_status', function(Blueprint $table) {
			$table->increments('id');
			$table->string('name')->nullable();
			$table->string('slug');
		});
	}

	public function down()
	{
		Schema::drop('applicant_status');
	}
}