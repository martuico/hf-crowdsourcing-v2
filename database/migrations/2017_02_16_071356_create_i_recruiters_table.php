<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateIRecruitersTable extends Migration {

	public function up()
	{
		Schema::create('i_recruiters', function(Blueprint $table) {
			$table->increments('id');
			$table->integer('user_id')->unsigned();
			$table->bigInteger('task_completed')->default(0);
			$table->bigInteger('task_on_hand')->default(0);
			$table->enum('status',['active','inactive'])->default('active');
			// $table->string('current_address')->nullable();
			// $table->string('current_city');
			// $table->string('current_province')->nullable();
			// $table->boolean('is_verfied')->default(0);
			$table->timestamps();
		});
	}

	public function down()
	{
		Schema::drop('i_recruiters');
	}
}