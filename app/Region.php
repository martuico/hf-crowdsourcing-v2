<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Region extends Model
{
    protected $table = 'refregion';

    public function provinces()
    {
        return $this->hasMany('\App\Province','regCode','regCode');
    }

    public function cities()
    {
        return $this->hasMany('\App\City','regDesc','regCode');
    }
}
