<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable;

class JobLevel extends Model {

	use Sluggable;

	protected $table = 'job_levels';
	public $timestamps = false;

	public function sluggable()
	{
	    return [
	        'slug' => [
	            'source' => 'name'
	        ]
	    ];
	}

}