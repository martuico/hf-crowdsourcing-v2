<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable;

class Shift extends Model {

	use Sluggable;

	protected $table = 'shifts';
	public $timestamps = false;

	public function sluggable()
	{
	    return [
	        'slug' => [
	            'source' => 'name'
	        ]
	    ];
	}
}