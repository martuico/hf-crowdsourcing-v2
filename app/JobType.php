<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable;

class JobType extends Model {

  use Sluggable;
	protected $table = 'job_types';
	public $timestamps = false;

  public function sluggable()
  {
      return [
          'slug' => [
              'source' => 'name'
          ]
      ];
  }

	public function jobs()
	{
		return $this->hasMany('App\Job', 'job_type_id');
	}

}