<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Billing extends Model
{
    protected $fillable = [
      'billing_code',
      'job_id',
      'account_manager',
      'date_paid',
      'staus'
    ];
}
