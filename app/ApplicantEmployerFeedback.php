<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ApplicantEmployerFeedback extends Model
{
    protected $fillable = [
      'applicant_id',
      'employer_id',
      'applicant_job_id',
      'job_id',
      'reason',
      'notes',
      'schedule_account_validation',
      'schedule_job_offer',
      'schedule_final_interview'
    ];


}
