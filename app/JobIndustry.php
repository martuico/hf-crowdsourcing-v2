<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable;

class JobIndustry extends Model {
  use Sluggable;
	protected $table = 'job_industries';
	public $timestamps = false;

  public function sluggable()
  {
      return [
          'slug' => [
              'source' => 'name'
          ]
      ];
  }

  public function jobs()
  {
      return $this->hasMany('\App\Job','job_industry_id','id')
                    ->where('status','=','open');
  }

  public function scopeJobOpenCount($query,$value)
  {
      return $this->hasMany('\App\Job','job_industry_id','id')
                    ->where('status','=','open')
                    ->count();
  }
}
