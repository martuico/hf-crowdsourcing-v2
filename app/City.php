<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class City extends Model
{
    protected $table = 'refcitymun';

    public function province()
    {
        return $this->belongsTo('\App\Province','provCode');
    }

    public function region()
    {
        return $this->belongsTo('\App\Region','regDesc');
    }

}
