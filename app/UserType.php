<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable;

class UserType extends Model {

  use Sluggable;
	protected $table = 'user_types';
	public $timestamps = true;

  public function sluggable()
  {
      return [
          'slug' => [
              'source' => 'name'
          ]
      ];
  }

	public function users()
	{
		return $this->hasMany('App\User', 'user_id');
	}

}