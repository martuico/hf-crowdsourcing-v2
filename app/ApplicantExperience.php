<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Traits\DateFormats;

class ApplicantExperience extends Model {

  use DateFormats;

	protected $table = 'applicant_experiences';
	public $timestamps = true;

  protected $fillable = [
    'applicant_id',
    'position',
    'company_name',
    'start_date',
    'end_date',
    'resposibilities',
    'startMonth',
    'startYear',
    'endMonth',
    'endYear',
    'currently_work_here'
  ];

  protected $appends = ['from','to'];

  public function getFromAttribute($value)
  {
      return $this->fromTo($this->start_date);
  }

  public function getToAttribute($value)
  {
      return $this->fromTo($this->end_date);
  }

	public function applicant()
	{
		return $this->belongsTo('App\Applicant', 'applicant_id');
	}

}
