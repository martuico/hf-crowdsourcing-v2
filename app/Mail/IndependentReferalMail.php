<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class IndependentReferalMail extends Mailable
{
    use Queueable, SerializesModels;
    protected $inputs;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($inputs)
    {
        $this->inputs = $inputs;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $name = 'Support Humanfactor.ph';
        $email = 'support@humanfactor.ph';
        $inputs = $this->inputs;
        return $this->view('Recruiter::emails.referred')
                    ->with(compact('inputs'))
                    ->from('no-reply@humanfactor.ph','No-Reply')
                    ->replyTo($email,$name)
                    ->to($inputs['email'],$inputs['firstname'].' '.$inputs['lastname'])
                    ->cc('mar.tuico@gmail.com','justin.medel@humanfactor.ph')
                    ->subject('You have been referred for an opening at Human Factor');
    }
}
