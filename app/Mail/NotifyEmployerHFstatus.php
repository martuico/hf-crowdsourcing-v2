<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class NotifyEmployerHFstatus extends Mailable
{
    use Queueable, SerializesModels;

    protected $inputs;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($inputs)
    {
        $this->inputs = $inputs;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $name = 'Support Humanfactor.ph';
        $email = 'support@humanfactor.ph';
        $inputs = $this->inputs;
        $emails = [$inputs['employer_email'],
                   $inputs['applicant_email']];

        if(array_key_exists('hfassgined_by_email',$inputs)){
            $emails = array_merge($emails,[$inputs['hfassgined_by_email']]);
        }
        if(array_key_exists('referred_by_email',$inputs) ){
            $emails = array_merge($emails,[$inputs['referred_by_email'] ]);
        }
        $emails = array_filter($emails);

        $view = $this->view('Dashboard::emails.notify-employer-status')
                    ->with(compact('inputs'))
                    ->from('no-reply@humanfactor.ph','No-Reply')
                    ->replyTo($email,$name)
                    ->to($emails)
                    ->cc('mar.tuico@gmail.com','justin.medel@humanfactor.ph')
                    ->subject($inputs['subject']);
        return $view;
    }
}
