<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class ApplicantQuickApply extends Mailable
{
    use Queueable, SerializesModels;
    protected $inputs;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($inputs)
    {
        $this->inputs = $inputs;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $name = 'Support Humanfactor.ph';
        $email = 'support@humanfactor.ph';
        $inputs = $this->inputs;
        return $this->view('Applicant::emails.thankyouApplication')
                    ->with(compact('inputs'))
                    ->from('no-reply@humanfactor.ph','No-Reply')
                    ->replyTo($email,$name)
                    ->to($inputs['email'],$inputs['firstname'].' '.$inputs['lastname'])
                    ->cc('mar.tuico@gmail.com','justin.medel@humanfactor.ph')
                    ->subject('Profile Registration and Submission at HumanFactor');
    }
}
