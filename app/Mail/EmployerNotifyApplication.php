<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class EmployerNotifyApplication extends Mailable
{
    use Queueable, SerializesModels;
    protected $inputs;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($inputs)
    {
        $this->inputs = $inputs;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $name_no = 'Support Humanfactor.ph';
        $email_no = 'support@humanfactor.ph';
        $inputs = $this->inputs;
        $emails = [$inputs['employer_email'],
                   $inputs['applicant_email']];

        if(array_key_exists('hfassgined_by_email',$inputs)){
            $emails = array_merge($emails,[$inputs['hfassgined_by_email']]);
        }
        if(array_key_exists('referred_by_email',$inputs) ){
            $emails = array_merge($emails,[$inputs['referred_by_email'] ]);
        }
        return $this->view('Employer::emails.regular-application')
                        ->with(compact('inputs'))
                        ->from('no-reply@humanfactor.ph','No-Reply')
                        ->replyTo($email_no,$name_no)
                        ->to($emails)
                        ->cc('mar.tuico@gmail.com','justin.medel@humanfactor.ph')
                        ->subject('A candidate has applied for '.$inputs['job_title']);
    }
}
