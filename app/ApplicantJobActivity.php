<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Traits\DateFormats;

class ApplicantJobActivity extends Model
{
    use DateFormats;

    protected $fillable = ['applicant_job_id','icon','user_id','title','description'];

    public function getCreatedAtAttribute($value)
    {
      return $this->dateForHumans($value);
    }

    public function application()
    {
      return $this->belongsTo('\App\ApplicantJob','applicant_job_id');
    }

    public function user()
    {
      return $this->belongsTo('\App\User','user_id');
    }

}
