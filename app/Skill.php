<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Skill extends Model {

	protected $table = 'skills';
	public $timestamps = false;

	public function applicants()
	{
		return $this->belongsToMany('App\Applicant', 'applicant_skill', 'applicant_id', 'skill_id');
	}

	public function jobs()
	{
		return $this->belongsToMany('App\Job', 'job_skill', 'job_id', 'skill_id');
	}

}