<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Province extends Model
{
    protected $table = 'refprovince';

    public function cities()
    {
      return $this->hasMany('\App\City','provCode','provCode');
    }
}
