<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable;

class ApplicantStatus extends Model {

  use Sluggable;

	protected $table = 'applicant_status';
	public $timestamps = false;
  protected $fillable = ['name','slug'];

  public function sluggable()
  {
      return [
          'slug' => [
              'source' => 'name'
          ]
      ];
  }


}