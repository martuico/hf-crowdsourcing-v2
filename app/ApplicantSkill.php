<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ApplicantSkill extends Model {

	protected $table = 'applicant_skill';
	public $timestamps = false;

}