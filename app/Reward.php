<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Reward extends Model
{
    protected $fillable = [
      'reward_code',
      'applicant_job_id',
      'applicant_id',
      'date_applicant_paid',
      'hiring_incentives',
      'due_hiring_incentives',
      'referred_by_id',
      'date_referral_paid',
      'referral_incentives',
      'due_referral_incentives',
      'hf_assgined_id',
      'employer_id',
      'applicant_reward_status',
      'referral_reward_status',
      'employer_payment_status'
    ];
}
