<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CompanyRecruiter extends Model {

	protected $table = 'company_recruiter';
	public $timestamps = true;

	protected $fillable = ['employer_id','recruiter_id'];

	public function employer()
	{
		return $this->belongsTo('App\Employer','employer_id');
	}

	public function recruiter()
	{
		return $this->belongsTo('App\User','recruiter_id');
	}

}
