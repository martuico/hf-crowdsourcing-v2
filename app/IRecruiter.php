<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class IRecruiter extends Model {

	protected $table = 'i_recruiters';
	public $timestamps = true;
  protected $fillable = ['user_id','task_completed','task_on_hand','status'];


	public function user()
	{
		return $this->belongsTo('App\User', 'user_id');
	}

}
