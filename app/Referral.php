<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Traits\Generator;

class Referral extends Model
{
    use Generator;
    protected $fillable = ['recruiter_id',
                          'uniq_link',
                          'job_code',
                          'firstname',
                          'lastname',
                          'email',
                          'contact_number'];

    public static function boot()
  	{
  	    parent::boot();
  	    static::creating(function($model){
          $model->recruiter_id = auth()->user()->id;
  	       $model->uniq_link =  self::generateLinkNumber();
  	    });
  	}

}
