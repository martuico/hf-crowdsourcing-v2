<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserContact extends Model {

	protected $table = 'user_contacts';
	public $timestamps = true;

	public function user()
	{
		return $this->belongsTo('App\User', 'user_id');
	}

}