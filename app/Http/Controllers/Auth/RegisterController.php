<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Auth\Events\Registered;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/applicant';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    // public function showRegistrationForm()
    // {
    //     return redirect('login');
    // }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        if(\Session::has('referral_code')){
            return Validator::make($data, [
                'password' => 'required|min:6|confirmed',
            ]);
        }
        return Validator::make($data, [
            'firstname' => 'required|max:255',
            'lastname' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|min:6|confirmed',
            'type' => 'required',//\Rule::in(['employer','recruiter','applicant'])
        ]);


    }

    /**
     * Handle a registration request for the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function register(Request $request)
    {
        $v = $this->validator($request->all());;
        if($v->fails()){
            return redirect()->route('register')
                            ->withErrors($v)
                            ->withInputs(\Request::except(['password','password_confirmation']));
        }

        $v->validate();

        event(new Registered($user = $this->create($request->all())));

        $this->guard()->login($user);

        return $this->registered($request, $user)
                        ?: redirect($this->redirectPath());
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(array $data)
    {

        if(\Session::has('referral_code')){
            $referred = \App\Referral::where('uniq_link','=',\Session::get('referral_code'))->first();
            return User::create([
                'firstname' => $referred['firstname'],
                'lastname' => $referred['lastname'],
                'email' => $referred['email'],
                'password' => bcrypt($data['password']),
                'user_type_id' => 5,
                'is_active' => 1
            ]);
        }else{
          $user_type_id = \App\UserType::whereSlug($data['type'])->first()->id;
          return User::create([
              'firstname' => $data['firstname'],
              'lastname' => $data['lastname'],
              'email' => $data['email'],
              'password' => bcrypt($data['password']),
              'user_type_id' => $user_type_id,
              'is_active' => 1
          ]);
        }

    }
}
