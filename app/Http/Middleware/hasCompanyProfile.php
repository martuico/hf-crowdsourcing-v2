<?php

namespace App\Http\Middleware;

use Closure;

class hasCompanyProfile
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(auth()->user()->type->name == 'Employer' AND count(auth()->user()->employer)<1){
            return redirect()->route('employer.create.profile');
        }

        return $next($request);
    }
}
