<?php

namespace App\Http\Middleware;

use Closure;

class ApplicantOnly
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(auth()->user()->type->name !== 'Applicant'){
            $type = strtolower(auth()->user()->type->name);
            return redirect()->route($type.'.home');
        }
        return $next($request);
    }
}
