<?php

namespace App\Http\Middleware;

use Closure;

class protectMultipleSubmit
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        if(in_array($request->method(), ['POST', 'PUT'])){
            \Session::put('_token', sha1(microtime()));
            return $next($request);
        }
        return $next($request);
    }
}
