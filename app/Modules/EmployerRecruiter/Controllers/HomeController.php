<?php namespace App\Modules\EmployerRecruiter\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Traits\ImageUpload,
    App\Traits\ShortNumberFormat;
use App\Modules\Employer\Requests\JobCreateRequest;

use App\Employer,
    App\Job,
    App\JobIndustry,
    App\JobType,
    App\Skill,
    App\Shift,
    App\ApplicantEducationLevel,
    App\JobLevel,
    App\ApplicantJob,
    App\ApplicantJobActivity,
    App\Region,
    App\Province,
    App\ApplicantEmployerFeedback,
    App\City,
    App\CompanyRecruiter;

class HomeController extends Controller
{
  use ImageUpload,ShortNumberFormat;
  protected $employer;
  protected $job;
  protected $jtype;
  protected $jindustry;
  protected $jskill;
  protected $shift;
  protected $edu_level;
  protected $applicantJob;
  protected $applicantJobActivity;
  protected $appEmployerFeedback;
  protected $company_recruiter;
  protected $location_region, $location_province, $location_city;

  public function __construct(Employer $employer,
                              Job $job,
                              JobIndustry $jindustry,
                              JobType $jtype,
                              Skill $skill,
                              Shift $shift,
                              ApplicantEducationLevel $edu_level,
                              JobLevel $job_level,
                              ApplicantJob $applicantJob,
                              ApplicantJobActivity $applicantJobActivity,
                              Region $location_region,
                              Province $location_province,
                              City $location_city,
                              ApplicantEmployerFeedback $appEmployerFeedback,
                              CompanyRecruiter $company_recruiter)
  {
      $this->employer = $employer;
      $this->job = $job;
      $this->jindustry = $jindustry;
      $this->jtype = $jtype;
      $this->skill = $skill;
      $this->shift = $shift;
      $this->edu_level = $edu_level;
      $this->job_level = $job_level;
      $this->applicantJob = $applicantJob;
      $this->applicantJobActivity = $applicantJobActivity;
      $this->location_region = $location_region;
      $this->location_province = $location_province;
      $this->location_city = $location_city;
      $this->appEmployerFeedback = $appEmployerFeedback;
      $this->company_recruiter = $company_recruiter;
      $this->middleware('hasCompanyProfile');
  }

  public function index()
  {
    $c_recruiter = $this->company_recruiter
                        ->where('recruiter_id','=',auth()->user()->id)
                        ->first();

    $employer = $this->employer
                    ->find($c_recruiter->employer_id);
     $jobs = (count($employer)>0)?$employer->jobs()
                   ->orderBy('created_at','desc')
                   ->take(50)
                   ->get(): collect([]);
      $applicants = (count($employer)>0)?$employer->applicants()
                      ->orderBy('created_at','desc')
                      ->take(50)
                      ->get():collect([]);
      $applicants_acc = ($employer)?$employer->applicants()
                      ->orderBy('created_at','desc')
                      ->take(15)
                      ->pluck('id'):[0];
      $activities = $this->applicantJobActivity
                         ->whereIn('applicant_job_id',$applicants_acc)
                         ->take(15)
                         ->get();
      $total_jobs = (count($employer)>0)?
                              $this->number_format_short($employer->jobs()->count()):0;

      $total_applicants = (count($employer)>0)?
                          $this->number_format_short($employer->applicants()->count()):0;

      $total_hired =  (count($employer)>0)?
                          $this->number_format_short($employer->applicants()
                                              ->where('applicant_status_id','=',6)
                                              ->count()):0;


      return view('EmployerRecruiter::index')
                  ->with(compact('jobs','applicants','activities','total_jobs','total_jobs','total_applicants','total_hired'));
  }

    public function applicants()
    {
      $c_recruiter = $this->company_recruiter
                          ->where('recruiter_id','=',auth()->user()->id)
                          ->first();

      $employer = $this->employer
                      ->find($c_recruiter->employer_id);
      if($employer->is_premium){
          $applicants = $employer
                            ->applicants()
                            ->where('applicant_status_id','<>',1)
                            ->orderBy('created_at','desc')
                            ->paginate(25);
      }else{
          $applicants = $employer
                            ->applicants()
                            ->orderBy('created_at','desc')
                            ->paginate(25);
      }

        return view('EmployerRecruiter::applicants')
                    ->with(compact('applicants','employer'));
    }

    public function careers()
    {
      $c_recruiter = $this->company_recruiter
                          ->where('recruiter_id','=',auth()->user()->id)
                          ->first();
      $employer = $this->employer
                      ->find($c_recruiter->employer_id);

      $jobs = $this->job->where('employer_id','=',$employer->id)
                      ->orderBy('created_at','desc')
                      ->paginate(30);
      return view('EmployerRecruiter::careers')
                ->with(compact('jobs'));
    }

    public function careersDetail($jobcode)
    {
        $c_recruiter = $this->company_recruiter
                            ->where('recruiter_id','=',auth()->user()->id)
                            ->first();
        $employer = $this->employer
                        ->find($c_recruiter->employer_id);
        $job = $this->job
                    ->where('employer_id','=',$employer->id)
                    ->whereJobCode($jobcode)
                    ->first();
        if(count($job)<1){
            return abort(404);
        }
        $applicants = $this->applicantJob
                            ->where('job_id','=',$job->id)
                            ->where('employer_id','=',$employer->id)
                            ->orderBy('created_at','asc')
                            ->paginate(50);
        return view('EmployerRecruiter::careers-details')
                    ->with(compact('employer','job','applicants'));
    }

    public function pipeline($jobCode)
    {
        $c_recruiter = $this->company_recruiter
                            ->where('recruiter_id','=',auth()->user()->id)
                            ->first();
        $employer = $this->employer
                        ->find($c_recruiter->employer_id);
        $job = $this->job
                    ->where('employer_id','=',$employer->id)
                    ->whereJobCode($jobCode)
                    ->first();
        if(count($job)<1){
            return abort(404);
        }
        $applicants = $this->applicantJob
                            ->where('job_id','=',$job->id)
                            ->where('employer_id','=',$employer->id)
                            ->where('applicant_status_id','<>',1)
                            ->orderBy('created_at','asc')
                            ->paginate(50);

        return view('EmployerRecruiter::pipeline')
                        ->with(compact('employer','job','applicants'));
    }

    public function applicantJob($job, $applicant)
    {

        $application = $this->applicantJob
                            ->whereApplicationCode($applicant)
                            ->first();

        if(count($application->applicant)<1){
            return abort(404);
        }
        $applicant = $application->applicant;
        if(\Session::get('viewing-application') !== $application->application_code){
            $this->applicantJobActivity->create(['applicant_job_id' => $application->id,
                                         'user_id' => auth()->user()->id,
                                         'icon' => '<i class="ico-eye-open bgcolor-success"></i>',
                                         'title' => 'Viewing Application',
                                         'description' => 'Viewed applicant profile ('.$application->application_code.')']);
            \Session::put('viewing-application',$application->application_code);
        }

        return view('EmployerRecruiter::applicant-detailv2')
                        ->with(compact('application','applicant'));
    }

    public function updateStage(Request $request,$applicant)
    {
        // return $request->all();
        $inputs = array_filter($request->only('employer_applicant_status_id',
                                              'schedule_final_interview',
                                              'schedule_account_validation',
                                              'reason',
                                              'notes',
                                              'schedule_job_offer'
                                            ));
        $application = $this->applicantJob->whereApplicationCode($applicant)
                    ->first();
        if(count($application)<1){
            return abort(404);
        }
        if($application->employer_applicant_status_id == 8){
            return redirect()->back()->with(['successMessage' => 'Can\'t update once applicant is hired']);
        }

        $fb = $this->appEmployerFeedback->where('applicant_job_id','=',$application->id)->first();
        if(count($fb)<1){
            $inputs1 = ['applicant_job_id' => $application->id,
                        'applicant_id' => $application->applicant_id,
                        'employer_id' => $application->employer_id,
                        'job_id' => $application->job_id
            ];
            $new_inputs = array_merge($inputs,$inputs1);
            $this->appEmployerFeedback->create($new_inputs);
            $application->update(['employer_applicant_status_id' => $request->get('employer_applicant_status_id')]);
        }else{
            $fb->update($inputs);
        }

        if($request->get('employer_applicant_status_id') == '8'){
            $application->update(['applicant_status_id' => 8,
                                  'employer_applicant_status_id' => 8,
                                  'hire_account' => $request->get('hire_account'),
                                  'hire_date' => $request->get('hire_date'),
                                  'date_of_training' => $request->get('date_of_training')
                                ]);
        }

        $this->applicantActivity->create(['applicant_id' => $application->applicant->id,
                                     'user_id' => auth()->user()->id,
                                     'icon' => '<i class="ico-eye-open bgcolor-success"></i>',
                                     'title' => 'Updated Status Application',
                                     'description' => 'Update applicant from employer feedback ('.$application->application_code.')']);

        return redirect()->back()->with(['successMessage' => 'Updated Status']);
    }


}
