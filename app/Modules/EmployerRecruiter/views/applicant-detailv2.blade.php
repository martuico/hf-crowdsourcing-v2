@extends('Dashboard::template.main')

@section('content')
  <!-- Page Header -->
  <div class="page-header page-header-block">
      <div class="page-header-section">
          <h4 class="title semibold">{{ $applicant->user->my_name }}<br>
            <small>CODE: {{ $applicant->auid }}</small>
          </h4>
      </div>
      <div class="page-header-section">
          <!-- Toolbar -->
          <div class="toolbar clearfix text-right">
              <a href="{{ \URL::previous() }}" class="btn btn-sm btn-default"  data-toggle="tooltip" data-placement="bottom" title="Create Job Post"><i class="ico-chevron-left"></i></a>
          </div>
          <!--/ Toolbar -->
      </div>
  </div>

  <div class="row">
    <div class="col-md-12">
      @if(\Session::has('successMessage'))
      <div class="alert alert-success">
        <strong>{{ \Session::get('successMessage') }}</strong>
      </div>
      @endif
      <div class="container-fluid">
      <!-- Nav tabs -->
      <ul class="nav nav-tabs" role="tablist">
        @php
        $url_a = route('employer-recruiter.careers.applicant.details',['job_code' => $application->job->job_code ,'code' => $application->application_code])
        @endphp
        <li role="presentation" class="{{ (!isset($_GET['t']))?'active':'' }}"><a href="{{ $url_a }}">Resume Profile</a></li>
        <li role="presentation" class="{{ (isset($_GET['t']) AND $_GET['t' ] =='cover')?'active':'' }}"><a href="{{ $url_a }}?t=cover">Cover Letter</a></li>
      </ul>
      <div class="tab-content">
        <div role="tabpanel" class="tab-pane {{ (!isset($_GET['t']))?'active':'' }}" id="resume">
          <div class="widget panel">
            <div class="panel-body">
              <div class="col-md-4" style="border:#ccc dashed 1px;padding:13px;">
                <ul class="list-unstyled">
                    <li class="text-center">
                        <img class="img-circle img-bordered-success" src="{{ $applicant->user->profile_pic }}" alt="" width="75px" height="75px">
                        <br>
                        <h5 class="semibold mb0">{{ $applicant->user->my_name }}</h5>
                        <p class="nm text-muted">{{ $applicant->position }}</p>
                        <a href="javscript:void(0);" class="nm text-teal"><i class="fa fa-link"></i> {{ str_replace(url('upload-resume').'/', '', $applicant->attached_resume) }} </a>
                    </li>
                </ul>
                <div class="row">
                  <div class="col-md-6">
                    <h4><i class="ico-bubble-dots3"></i> Languages</h4>
                    @if(strlen($applicant->languages)>0)
                      <ul class="list-inline">
                      @foreach(explode(',',$applicant->languages) as $language)
                      <li><span class="label label-default">{{ $language }}</span></li>
                      @endforeach
                      </ul>
                    @endif
                  </div>
                  <div class="col-md-6">
                    <h4><i class="ico-bubble-dots3"></i> Skills</h4>
                    @if(strlen($applicant->skills)>0)
                    <ul class="list-inline">
                      @foreach(explode(',',$applicant->skills) as $skill)
                      <li><span class="label label-default">{{ $skill }}</span></li>
                      @endforeach
                    </ul>
                    @endif
                  </div>
                  <div class="col-md-12">
                    @if(count($applicant->internal_recruiter)>0)
                      <h4>Assigned HF: {{ $applicant->internal_recruiter->my_name }}</h4>
                    @endif
                    @if(count($applicant->external_recruiter)>0)
                      <h4>Referred By: {{ $applicant->external_recruiter->my_name }}</h4>
                    @endif
                  </div>
                  <div class="col-md-12">
                    <div class="alert alert-info">
                      <h3 class="panel-title">Human Factor Feedback</h3>
                      <p><strong>Current Status :</strong> {{ $application->applicant_status->name }} </p>
                      @if($application->employer_appointment_type)
                      <p class="app_type"><strong>Appointments :</strong> {{ ucwords(str_replace('-',' ',$application->employer_appointment_type)) }} | {{ $application->employer_appointment_sched }} </p>
                      @endif
                      @if($application->endorsement_slip)
                      <p class="attachment"><strong>Edorsement Slip :</strong> <i class="fa fa-file"></i> {{ str_replace(url('/').'/endorsement-slips-media/','',$application->endorsement_slip) }} </p>
                      @endif
                      @if($application->hire_date && $application->hire_date !== '0000-00-00')
                      <p class="hire_date"><strong>Hire Date :</strong> {{ $application->hire_date }} </p>
                      @endif
                      @if($application->hire_account)
                      <p class="hire_account"><strong>Account :</strong> {{ $application->hire_account }} </p>
                      @endif
                      @if($application->reason)
                      <p class="reason"><strong>Reason :</strong> {{ $application->reason }} </p>
                      @endif
                      @if($application->note)
                      <p class="notes"><strong>Note :</strong> {{ $application->note }} </p>
                      @endif
                      <input type="hidden" value="{{$application->applicant_status_id}}" id="stages">
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-md-8">
                <div class="row">
                  <div class="col-md-12">
                    <div class="alert alert-info">
                      <p><strong>Applicant Feedback Status :</strong>  {{ ($application->employer_applicant_status)? $application->employer_applicant_status->name:'No Status' }} </p>
                      @if($application->employerFeedback)
                        @if($sched_final = $application->employerFeedback->schedule_final_interview)
                        <p class="sched_finalinterview"><strong>Final Interview :</strong>  {{ $sched_final }} </p>
                        @endif
                        @if($application->employerFeedback->reason)
                        <p class="employer_reason"><strong>Reason :</strong> {{ $application->employerFeedback->reason }}</p>
                        @endif
                        @if($application->employerFeedback->notes)
                        <p class="employer_notes"><strong>Notes :</strong> {{ $application->employerFeedback->notes }}</p>
                        @endif
                        @if($sched_valid = $application->employerFeedback->schedule_account_validation)
                        <p class="sched_validation"><strong>Account Validation :</strong>  {{ $sched_valid }}</p>
                        @endif
                        @if($sched_offer = $application->employerFeedback->schedule_job_offer)
                        <p class="sched_joboffer"><strong>Job Offer :</strong>  {{ $sched_offer }}</p>
                        @endif
                        @if($sched_training = $application->date_of_training)
                        <p class="date_of_training"><strong>Date Training :</strong>  {{ $sched_training }}</p>
                        @endif
                        @if($hire_acc = $application->hire_account)
                        <p class="hire_account"><strong>Hire Account :</strong>  {{ $hire_acc }}</p>
                        @endif
                        @if($hire_date = $application->hire_date)
                        <p class="hire_date"><strong>Hire Date :</strong>  {{ $hire_date }}</p>
                        @endif
                      @endif
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-12">
                    <input type="hidden" value="{{ $application->employer_applicant_status_id }}" class="employer_stages">
                    @if( $application->employer_applicant_status_id !== 8 )
                    <button class="btn btn-primary" role="button" data-toggle="collapse" href="#collapseExample" aria-expanded="false" aria-controls="collapseExample">
                      Update Status
                    </button>

                    <div class="collapse" id="collapseExample">

                      <div class="well">
                        @if($application->employer_applicant_status_id !== 8)
                        <h3 class="panel-title">Applicant Status</h3>
                        {{ \Form::open(['route' => ['employer-recruiter.application.employerstages', $application->application_code],
                                        'id' => 'form',
                                        'files' => true]) }}
                          <div class="form-group">
                            <select name="employer_applicant_status_id" class="form-control employer_stages">
                              <option value="1" {{($application->employer_applicant_status_id == '1' )?'selected':''}}>No Show</option>
                              <option value="2" {{($application->employer_applicant_status_id == '2' )?'selected':''}}>Passed Initial Interview</option>
                              <option value="3" {{($application->employer_applicant_status_id == '3' )?'selected':''}}>No Go Initial Interview</option>
                              <option value="4" {{($application->employer_applicant_status_id == '4' )?'selected':''}}>Passed Final Interview</option>
                              <option value="5" {{($application->employer_applicant_status_id == '5' )?'selected':''}}>No Go Final Interview</option>
                              <option value="6" {{($application->employer_applicant_status_id == '6' )?'selected':''}}>Passed Account Validation</option>
                              <option value="7" {{($application->employer_applicant_status_id == '7' )?'selected':''}}>Failed Account Validation</option>
                              <option value="8" {{($application->employer_applicant_status_id == '8' )?'selected':''}}>Hired</option>
                            </select>
                          </div>
                          <div class="row sched_finalinterview" style="margin-top:20px;">
                            <div class="col-md-12">
                                <h4 class="panel-title">Schedule Final Interview</h4>
                               <div id="datetimepickerFinalInterview" class="mt10"></div>
                            </div>
                          </div>
                          <div class="row mt10 sched_finalinterview">
                              <div class="panel-heading">
                              <h4 class="panel-title fsize13">Final Interview Date & Time: <span id="final-interviewtext"></span></h4>
                              <input type="hidden" name="schedule_final_interview" id="schedule_final_interview">
                              </div>
                          </div>
                          <div class="row sched_validation" style="margin-top:20px;">
                            <div class="col-md-12">
                                <h4 class="panel-title">Schedule Account Validation</h4>
                               <div id="datetimepickvalidation" class="mt10"></div>
                            </div>
                          </div>
                          <div class="row mt10 sched_validation">
                              <div class="panel-heading">
                              <h4 class="panel-title fsize13">Account Validation Date & Time: <span id="account-validationtext"></span></h4>
                              <input type="hidden" name="schedule_account_validation" id="schedule_account_validation">
                              </div>
                          </div>
                          <div class="form-group mt10 employer_reason">
                            <div class="has-icon mb10">
                                <input type="text" name="reason" class="form-control" placeholder="Reason">
                                <i class="ico-pencil2 form-control-icon"></i>
                            </div>
                          </div>
                          <div class="form-group mt10 employer_notes">
                            <div class="has-icon mb10">
                                <input type="text" name="notes" class="form-control" placeholder="Notes">
                                <i class="ico-file form-control-icon"></i>
                            </div>
                          </div>
                          <div class="row sched_joboffer" style="margin-top:20px;">
                            <div class="col-md-12">
                                <h4 class="panel-title">Schedule for Job Offer</h4>
                               <div id="datetimepickjoboffer" class="mt10"></div>
                            </div>
                          </div>
                          <div class="row mt10 sched_joboffer">
                              <div class="panel-heading">
                              <h4 class="panel-title fsize13">Job Offer Date & Time: <span id="joboffer-text"></span></h4>
                              <input type="hidden" name="schedule_job_offer" id="schedule_job_offer">
                              </div>
                          </div>
                          <div class="form-group mt10 date_of_training">
                            <label>Training Date</label>
                            <input type="text" name="date_of_training" class="form-control">
                          </div>
                          <div class="form-group mt10 hire_date">
                            <label>Hire Date</label>
                            <input type="text" name="hire_date" class="form-control">
                          </div>
                          <div class="form-group mt10 hire_account">
                            <label>Account</label>
                            <input type="text" name="hire_account" class="form-control" placeholder="Account/Department">
                          </div>
                          <div class="form-group">
                            <button type="submit" class="btn btn-sm btn-info">Save</button>
                          </div>
                        </form>
                        @endif
                      </div>

                    </div>
                    @endif
                </div>
                </div><!-- row-->
                <div class="table-layout">
                  <h4>Biography</h4>
                  {!! $applicant->biography !!}
                  <hr>
                  <div class="row">
                    <div class="col-md-6">
                      <h4><i class="ico-vcard"></i> Personal Information</h4>
                      <p class="mb5">Full name : <strong class="text-uppercase">{{ $applicant->user->my_name }}</strong></p>
                      <p class="mb5">D.O.B : <strong class="text-uppercase">{{ $applicant->date_of_birth?:'None' }}</strong></p>
                      <p class="mb5">Email : <strong class="text-uppercase"><a href="mailto:{{ $applicant->user->email }}?Subject=From%20Human%20Factor">{{ $applicant->user->email }}</a></strong></p>
                      <p class="mb5">Employment Status: <strong class="text-uppercase">{{ $applicant->employment_status?:'None' }}</strong></p>
                    </div>
                    <div class="col-md-6">
                      <div class="row">
                        <h4><i class="ico-map-marker"></i> Address</h4>
                        <p>{{ $applicant->full_address }}</p>
                      </div>
                      <div class="row">
                        <p class="mb5">Mobile Number: <strong class="text-uppercase">{{ $applicant->mobile_number_1?:'None' }}</strong></p>
                        <p class="mb5">House Number: <strong class="text-uppercase">{{ $applicant->house_number_1?:'None' }}</strong></p>
                      </div>
                    </div>
                  </div><!-- row-->

                  <hr>
                  <div class="row">
                    <div class="col-md-12">
                      <h4>Experiences</h4>
                      @if(count($applicant->experiences )>0)
                      @foreach($applicant->experiences as $exp)
                        @if(strlen($exp->company_name )>0 AND strlen($exp->position)>0)
                        <h5 class="text-uppercase">{{ $exp->position }} @ {{ $exp->company_name }}</h5>
                        @endif
                        @if(strlen($exp->from)>0 AND strlen($exp->to)>0)
                        <p class="text-muted">{{ $exp->from }} - {{ $exp->to }}</p>
                        @endif
                        @if(strlen($exp->resposibilities)>0)
                        <p>{{ $exp->resposibilities }}</p>
                        @endif
                      @endforeach
                      @else
                      <div class="alert alert-info">
                        No data
                      </div>
                      @endif
                    </div>
                  </div><!-- row -->

                  <hr>
                  <div class="row">
                    <div class="col-md-12">
                      <h4>Educations</h4>
                      @if(count($applicant->educations )>0)
                      @foreach($applicant->educations as $edu)
                        @if(strlen($edu->school_name)>0 AND strlen($edu->attainment)>0)
                        <h5 class="text-uppercase">{{ $edu->attainment }} @ {{ $edu->school_name }}</h5>
                        @endif
                        @if(strlen($edu->from_to)>0 AND strlen($edu->to)>0)
                        <p class="text-muted">{{ $edu->from }} - {{ $edu->to }}</p>
                        @endif
                        @if(strlen($edu->awards)>0)
                        <p>{{ $edu->awards }}</p>
                        @endif
                      @endforeach
                      @else
                      <div class="alert alert-info">
                        No data
                      </div>
                      @endif
                    </div>
                  </div><!-- row -->

                </div>
              </div>
            </div>
          </div>
        </div><!-- Resume -->
        <div role="tabpanel" class="tab-pane {{ (isset($_GET['t']) AND $_GET['t' ] =='cover')?'active':'' }}">
          <div class="widget panel">
            <div class="panel-body">
              <h3 class="panel-title">Cover Letter</h3>
              {!! nl2br($application->applicant_cover_letter) !!}
              <br>
              <br>
              <h3 class="panel-title">Expected Salary: {{ $application->expected_salary }}</h3>
            </div>
          </div>
        </div>
      </div><!-- tab contant -->
      </div>
    </div>
  </div>

  <div class="modal left fade application" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title" id="myModalLabel">Application</h4>
        </div>
        <div class="modal-body"></div>
        <!--div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          <button type="button" class="btn btn-primary">Save changes</button>
        </div-->
      </div>
    </div>
  </div>
@stop


@section('styles')
  <link rel="stylesheet" href="{{ asset('hfv1/plugins/sweetalert/dist/sweetalert.css') }}">
  <link rel="stylesheet" href="{{asset('bower_components/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css')}}" />
  <link rel="stylesheet" href="{{ asset('hfv1/plugins/bootstrap-datepicker/css/datepicker3.css') }}">
@stop
@section('scripts')
  <script src="{{ asset('hfv1/plugins/sweetalert/dist/sweetalert.min.js') }}"></script>
  <script type="text/javascript" src="{{asset('bower_components/moment/min/moment.min.js')}}"></script>
  <script type="text/javascript" src="{{asset('bower_components/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js')}}"></script>
  <script src="{{ asset('hfv1/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js') }}"></script>
  <script>
  var JobDetail = {
    init : function(){
      this.showApplication()
      this.elemHide()
      this.employerFeedback()
      this.elemStageShow($('.employer_stages').val())
      this.datePickFinalInterview('#datetimepickerFinalInterview','#schedule_final_interview','#final-interviewtext')
      this.datePickFinalInterview('#datetimepickvalidation','#schedule_account_validation','#account-validationtext')
      this.datePickFinalInterview('#datetimepickjoboffer','#schedule_job_offer','#joboffer-text')
    },
    datePickFinalInterview : function(elem,elemval,elemtext){
      $(elem).datetimepicker({
        inline: true,
        sideBySide: true
      });
      $(elem).on('dp.change', function(event) {
        $('#selected-date').text(event.date);
        var formatted_date = event.date.format('MM/DD/YYYY h:mm a');
        var dt = moment(formatted_date, ["MM/DD/YYYY h:mm A"]).format("YYYY-MM-DD HH:mm:ss");
        $(elemval).val(dt);
        $(elemtext).text(formatted_date);
      });
    },
    elemHide : function(){
      $('.sched_finalinterview').hide();
      $('.sched_validation').hide();
      $('.employer_reason').hide();
      $('.employer_notes').hide();
      $('.sched_joboffer').hide();
      $('.date_of_training').hide();
      $('.hire_date').hide();
      $('.hire_account').hide();
    },
    elemStageShow: function(stage){
      switch (stage) {
        case '1':
          JobDetail.elemHide()
          $('.employer_notes').show();
        break;
        case '2':
          JobDetail.elemHide()
          $('.sched_finalinterview').show();
          $('.employer_notes').show();
          break;
        case '3':
          JobDetail.elemHide()
          $('.employer_reason').show();
          break;
        case '4':
          JobDetail.elemHide()
          $('.sched_validation').show();
          $('.employer_notes').show();
          break;
        case '5':
          JobDetail.elemHide()
          $('.employer_reason').show();
          break;
        case '6':
          JobDetail.elemHide()
          $('.sched_joboffer').show();
          $('.employer_notes').show();
          break;
        case '7':
          JobDetail.elemHide()
          $('.employer_reason').show();
          break;
        case '8':
          JobDetail.elemHide()
          $('.date_of_training').show();
          $('.hire_date').show();
          $('.hire_account').show();
          $('.notes').show();
          break;
        default:
          JobDetail.elemHide()
        break;
      }
    },
    employerFeedback: function(){
      $('.employer_stages').change(function(){
        var stage = $(this).val();
        JobDetail.elemStageShow(stage);
      });
    },
    showApplication : function(){
      $('.application').on('show.bs.modal' ,function(e){
        var button = $(e.relatedTarget)
        var applicant = button.data('applicant')
        var modal = $(this)
        modal.find('.modal-body').children().remove()
        $.ajax({
          url : window.location.origin + '/admin/fetch-applicantion/' + applicant,
          type : 'get',
          success : function(res){
            modal.find('.modal-body').append($(res))
            ApplicationDetail.init()
          },error : function(res){
            console.log(res);
          }
        })
        // e.preventDefault();
        // e.stopImmediatePropagation();
      });
    }
  }
  var ApplicationDetail = {
      init : function(){
        this.changeStatus();
        this.addComment();
        this.datePick();
        this.appointmentType();
        // this.setAppoiment();
        this.stages();
        this.elemStageShow($('#stages').val())
        $('[name="hire_date"]').datepicker({
          format:'yyyy-mm-dd'
        })
        $('[name="date_of_training"]').datepicker({
          format:'yyyy-mm-dd'
        })
      },
      elemHide : function(){
        $('.app_type').hide();
        $('.pick_datetime').hide();
        $('.attachment').hide();
        $('.reason').hide();
        $('.refer_premium').hide();
        $('.refer_regular').hide();
        $('.notes').hide();
        $('.hire_date').hide();
        $('.hire_account').hide();
      },
      elemStageShow: function(stage){
        switch (stage) {
          case '2':
            ApplicationDetail.elemHide()
            $('.app_type').show();
            $('.pick_datetime').show();
            $('.attachment').show();
            break;
          case '3':
            ApplicationDetail.elemHide()
            $('.app_type').show();
            $('.pick_datetime').show();
            $('.attachment').show();
            $('.refer_premium').show();
            break;
          case '4':
            ApplicationDetail.elemHide()
            $('.reason').show();
            break;
          case '5':
            ApplicationDetail.elemHide()
            $('.refer_regular').show();
            $('.reason').show();
            break;
          case '6':
            ApplicationDetail.elemHide()
            $('.notes').show();
            break;
          case '7':
            ApplicationDetail.elemHide()
            $('.app_type').show();
            $('.pick_datetime').show();
            $('.attachment').show();
            $('.notes').show();
            break;
          case '8':
            ApplicationDetail.elemHide()
            $('.hire_date').show();
            $('.hire_account').show();
            $('.notes').show();
            break;
          default:
            ApplicationDetail.elemHide()
          break;
        }
      },
      stages: function(){
        ApplicationDetail.elemHide()
        $('#stages').change(function(){
          var stage = $(this).val();
          ApplicationDetail.elemStageShow(stage);
        });
      },
      // setAppoiment: function(){
      //   $('#setAppoiment').click(function(e){
      //     var type = $('[name="my_hidden_type"]').val();
      //     var datetime = $('#my_hidden_input').val();
      //     var data = {
      //       'employer_appointment_sched' : datetime,
      //       'employer_appointment_type' : type
      //     }
      //     $.ajax({
      //       url : window.location.origin + '/employer/set-appointment',
      //       type : 'post',
      //       data : data,
      //       success : function(res){
      //
      //       },error : function(res){
      //
      //       }
      //     })
      //     e.preventDefault();
      //     e.stopImmediatePropagation();
      //   });
      // },
      appointmentType : function(){
        $('[name="employer_appointment_type"]').click(function(e){
          var txt = $(this).parent().find('span').text()
          var val = $(this).val()
          $('#appointment-type').text(txt.toUpperCase());
          $('[name="my_hidden_type"]').val(val);

        });
      },
      datePick : function(){
        $('#datetimepicker1').datetimepicker({
          inline: true,
          sideBySide: true
        });
        $('#datetimepicker1').on('dp.change', function(event) {
          //console.log(moment(event.date).format('MM/DD/YYYY h:mm a'));
          //console.log(event.date.format('MM/DD/YYYY h:mm a'));
          $('#selected-date').text(event.date);
          var formatted_date = event.date.format('MM/DD/YYYY h:mm a');
          var dt = moment(formatted_date, ["MM/DD/YYYY h:mm A"]).format("YYYY-MM-DD HH:mm:ss");
          $('#my_hidden_input').val(dt);
          $('#hidden-val').text(formatted_date);
        });
      },
      addComment : function(){
        $('#comment').keypress(function(e){
          var box = $(this);
          var comment =  box.val();
          var application_code = '{{ $applicant->auid }}';
          if(e.which == 13 && comment !='' && comment.length > 0){
            $.ajax({
              url : window.location.origin + '/admin/applicant-comment',
              type : 'post',
              data : { 'comment': comment, 'application_code' : application_code},
              success : function(res){
                console.log(res);
                $('.media-list-feed').prepend($(ApplicationDetail.boxTemplate(res.activity)));
                swal('Sent!','Commented added in activities','success');
                box.val('');
              },error : function(res){
                console.log(res);
                swal('Opps!','Something went wrong','error');
              }
            });

          }
        });
      },
      boxTemplate: function(activity){
        return `<li class="media">
            <div class="media-object pull-left">
                ${activity.icon}
            </div>
            <div class="media-body">
                <p class="media-heading">${activity.title}</p>
                <p class="media-text">
                <span class="text-primary semibold">${activity.description}</span>
                by ${activity.created_by}</p>
                <p class="media-meta">${activity.created_at}</p>
            </div>
        </li>`
      },
      changeStatus: function(){
        $('.status').click(function(e){
          var option = $(this);
          var status = option.find('input').attr('data-status');
          var applicant_code = option.find('input').attr('data-applicantcode');
          console.log(applicant_code,status);
          $.ajax({
            url : window.location.origin + '/admin/update-applicant-status',
            type : 'post',
            data : {'applicant_code' : applicant_code,
                    'status' : status},
            success : function(res){
              if(res.success){
                if(option.hasClass('hasChoices')){
                  $('#statuses').children().remove();
                  // $('#statuses').append(ApplicationDetail.statusTemplate(applicant_code));
                  $('#statustext').text('Current Status:'+res.status);
                }
                if(option.hasClass('lastChoice')){
                  $('#statuses').children().remove();
                  $('#statustext').text('Current Status:'+res.status);
                }
                swal('Updated!','You have change the status','success');
              }
            },
            error : function(res){
              console.log(res);
              swal('Opps!','Something went wrong','error');
            }
          });
          e.stopImmediatePropagation();
          e.preventDefault();
        });
      },
      statusTemplate: function(applicant_code){
        return  `
          <label class="btn btn-primary status lastChoice">
            <input type="radio" name="options4" id="option4" autocomplete="off" data-applicantcode="${applicant_code}" data-status="4"> No Show
          </label>
          <label class="btn btn-primary status lastChoice">
            <input type="radio" name="options5" id="option5" autocomplete="off" data-applicantcode="${applicant_code}" data-status="5"> Rejected
          </label>
          <label class="btn btn-primary status lastChoice">
            <input type="radio" name="options6" id="option6" autocomplete="off" data-applicantcode="${applicant_code}" data-status="6"> Hired
          </label>
        `;
      }
  }
  $(function(){
    ApplicationDetail.init()
    JobDetail.init()
  });
  </script>
@stop
