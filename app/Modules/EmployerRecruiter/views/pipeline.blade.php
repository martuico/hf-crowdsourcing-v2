@extends('Dashboard::template.main')

@section('content')
  <!-- Page Header -->
  <div class="page-header page-header-block">
      <div class="page-header-section">
          <h4 class="title semibold">Job Detail - {{ $job->job_code }}</h4>
      </div>
      <div class="page-header-section">
          <!-- Toolbar -->
          <div class="toolbar clearfix text-right">
              <a href="{{route('employer.careers')}}" class="btn btn-sm btn-default"  data-toggle="tooltip" data-placement="bottom" title="Back to careers"><i class="fa fa-chevron-left"></i> Back to Listings</a>
              <a href="{{ route('employer.careers.details',['job_code' => $job->job_code]) }}" class="btn btn-default btn-sm"><i class="fa fa-list"></i> Details</a>
          </div>
          <!--/ Toolbar -->
      </div>
  </div>
  <div class="row">
    <div class="col-md-12">
      <div class="widget panel">
          <!-- panel body -->
          <div class="panel-body">
              <ul class="list-table">
                  <li class="text-left">
                      <h5 class="semibold ellipsis">
                          {{ $job->job_title }}<br>
                          <small class="text-muted">Created {{ $job->created_at}} - Valid Until: {{ $job->valid_until }}</small>
                      </h5>
                  </li>
                  <li class="text-right"><button type="button" class="btn btn-info">{{ strtoupper($job->status) }}</button></li>
              </ul>
              <!-- Nav section -->
              <ul class="nav nav-section nav-justified mt15">
                  <li>
                      <div class="section">
                        @if($employer->is_premium)
                          <h4 class="nm">{{ $job->applicant_premium_count }}</h4>
                        @else
                          <h4 class="nm">{{ $job->applicant_count }}</h4>
                        @endif
                          <p class="nm text-muted">Applicants</p>
                      </div>
                  </li>
                  <li>
                      <div class="section">
                          <h4 class="nm">{{ $job->forinterview_count }}</h4>
                          <p class="nm text-muted">For Interview</p>
                      </div>
                  </li>
                  <li>
                      <div class="section">
                          <h4 class="nm">{{ $job->hired_count }}</h4>
                          <p class="nm text-muted">Hired</p>
                      </div>
                  </li>
              </ul>
              <!--/ Nav section -->
          </div>
          <!--/ panel body -->
      </div>
    </div><!-- col-md-12 -->
  </div><!-- row -->

  <div class="row">
    <div class="col-md-12">
      <div class="panel panel-default">
        <!-- panel heading/header -->
        <div class="panel-heading">
            <h3 class="panel-title">Applicants</h3>
            <!-- panel toolbar -->
            <div class="panel-toolbar text-right">
                <!-- option -->
                <div class="option">
                    <button class="btn up" data-toggle="panelcollapse"><i class="arrow"></i></button>
                    <button class="btn" data-toggle="panelremove" data-parent=".col-md-12"><i class="remove"></i></button>
                </div>
                <!--/ option -->
            </div>
            <!--/ panel toolbar -->
        </div>
        <!--/ panel heading/header -->
        <!-- panel body with collapse capabale -->
        <div class="table-responsive panel-collapse pull out">
          @if(count($applicants)>0)
            <table class="table table-bordered table-condensed table-hover">
                <thead>
                    <tr>
                        <th>Name</th>
                        <th>Status</th>
                        <th>HF Feedback</th>
                        <th>Company Feedback</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($applicants as $application)
                    <tr>
                        <td>{{ ($application->applicant)?$application->applicant->user->my_name:'Err' }}</td>
                        <td><span class="label label-warning">{{ strtoupper($application->applicant_status->name) }}</span></td>
                        <td>
                          TYPE: {{ strtoupper(str_replace('-','',$application->employer_appointment_type)) }}
                          <br>
                          DATETIME: {{ $application->employer_appointment_sched }}
                          @if($application->endorsement_slip)
                          <p class="attachment"><strong>Edorsement Slip :</strong> <i class="fa fa-file"></i> {{ str_replace(url('/').'/endorsement-slips-media/','',$application->endorsement_slip) }} </p>
                          @endif
                        </td>
                        <td>
                          @if($application->employerFeedback)
                            @if($sched_final = $application->employerFeedback->schedule_final_interview)
                            <p class="sched_finalinterview"><strong>Final Interview :</strong>  {{ $sched_final }} </p>
                            @endif
                            @if($application->employerFeedback->reason)
                            <p class="employer_reason"><strong>Reason :</strong> {{ $application->employerFeedback->reason }}</p>
                            @endif
                            @if($application->employerFeedback->notes)
                            <p class="employer_notes"><strong>Notes :</strong> {{ $application->employerFeedback->notes }}</p>
                            @endif
                            @if($sched_valid = $application->employerFeedback->schedule_account_validation)
                            <p class="sched_validation"><strong>Account Validation :</strong>  {{ $sched_valid }}</p>
                            @endif
                            @if($sched_offer = $application->employerFeedback->schedule_job_offer)
                            <p class="sched_joboffer"><strong>Job Offer :</strong>  {{ $sched_offer }}</p>
                            @endif
                            @if($sched_training = $application->date_of_training)
                            <p class="date_of_training"><strong>Date Training :</strong>  {{ $sched_training }}</p>
                            @endif
                            @if($hire_acc = $application->hire_account)
                            <p class="hire_account"><strong>Hire Account :</strong>  {{ $hire_acc }}</p>
                            @endif
                            @if($hire_date = $application->hire_date)
                            <p class="hire_date"><strong>Hire Date :</strong>  {{ $hire_date }}</p>
                            @endif
                          @else
                            <p>No Status</p>
                          @endif
                        </td>
                        <td>
                          <a href="{{ route('employer-recruiter.careers.applicant.details',['job_code' => $job->job_code,'applicant' => $application->application_code])}}" class="btn btn-sm btn-default"><i class="ico-eye"></i></a>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
            {{ $applicants->links() }}
            @else
            <div class="panel-body">
              <div class="alert alert-info">
                <strong>No Data</strong>
              </div>
            </div>
            @endif
        </div>
        <!--/ panel body with collapse capabale -->
    </div>
    </div>
  </div>
@stop
