@extends('Dashboard::template.main')

@section('content')
  <!-- Page Header -->
  <div class="page-header page-header-block">
      <div class="page-header-section">
          <h4 class="title semibold">Applicant Pipeline</h4>
      </div>
      <div class="page-header-section">
          <!-- Toolbar -->
          <div class="toolbar clearfix text-right">
              <div class="input-group">
                    <input type="text" class="form-control" placeholder="Search Applicant">
                    <span class="input-group-btn">
                      <button class="btn btn-default" type="button">Go!</button>
                    </span>
                  </div><!-- /input-group -->
          </div>
          <!--/ Toolbar -->
      </div>
  </div>
  <!-- Page Header -->
  <!-- START row -->
  <div class="row">
      <div class="col-md-12">
          <!-- START panel -->

          <div class="panel panel-primary">
            @if(count($applicants)>0)
              <!-- panel heading/header -->
              <div class="panel-heading">
                  <h3 class="panel-title"><span class="panel-icon mr5"><i class="ico-table22"></i></span> Applicants</h3>
                  <!-- panel toolbar -->
                  <div class="panel-toolbar text-right">
                  </div>
                  <!--/ panel toolbar -->
              </div>
              <!--/ panel heading/header -->
              <!-- panel toolbar wrapper -->

              <div class="panel-toolbar-wrapper pl0 pt5 pb5">
                  @if(auth()->user()->type->slug == 'employer' AND auth()->user()->employer->is_premium)
                  <div class="panel-toolbar text-right">

                      <button type="button" class="btn btn-sm btn-default">Endorsed</button>
                      <button type="button" class="btn btn-sm btn-default">Rejected</button>
                      <button type="button" class="btn btn-sm btn-default">No Show</button>
                      <button type="button" class="btn btn-sm btn-default">Hired</button>
                  </div>
                  @endif
              </div>
              <!--/ panel toolbar wrapper -->

              <!-- panel body with collapse capabale -->
              <div class="table-responsive pull out">
                  <table class="table table-bordered table-hover" id="table1">
                      <thead>
                          <tr>
                              <th width="5%"></th>
                              <th width="20%">Name</th>
                              <th>Address</th>
                              <th>Applied For</th>
                              <th>Designation</th>
                              <th width="13%">Status</th>
                              <th></th>
                          </tr>
                      </thead>
                      <tbody>
                        @foreach($applicants as $applicant)
                          <tr>
                              <td><div class="media-object"><img src="{{ $applicant->applicant->user->profile_pic}}" alt="" class="img-circle"></div>
                              </td>
                              <td>{{ $applicant->applicant->user->myname }} <br>
                                  <p class="no-margin text-muted"><small>CODE: {{ $applicant->application_code }}</small></p>
                              </td>
                              <td>
                                <small>{{ $applicant->applicant->full_address}}</small>
                              </td>
                              <td>{{ $applicant->job->job_title}}</tdzz>
                              <td>{{ $applicant->job->designation}}</td>
                              <td>
                                <span class="label label-info">{{ $applicant->applicant_status->name }}</span>
                              </td>
                              <td class="text-center">
                                @if($employer->is_premium)
                                  <a href="{{ route('employer-recruiter.careers.applicant.details',['job_code' => $applicant->job->job_code,'applicant' => $applicant->application_code]) }}" class="btn btn-sm btn-default"target="_blank"><i class="ico-profile"></i></a>
                                @else
                                  <button class="btn btn-sm btn-default" title="View Resume" data-toggle="modal" data-target=".resume" data-applicant="{{ $applicant->application_code }}"><i class="ico-profile"></i></button>
                                @endif
                              </td>
                          </tr>
                          @endforeach
                      </tbody>
                  </table>
              </div>
              <!--/ panel body with collapse capabale -->
              {{ $applicants->links() }}
              @else
                <div class="panel-footer">
                  <div class="alert alert-info">
                    <strong>No data</strong>
                  </div>
                </div>
              @endif
          </div>
      </div>
  </div>
  <!--/ END row -->
@if(auth()->user()->type->slug == 'employer' AND auth()->user()->employer->is_premium == 0)
  <!-- Modal -->
<div class="modal left fade resume" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Resume</h4>
      </div>
      <div class="modal-body"></div>
      <!--div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Save changes</button>
      </div-->
    </div>
  </div>
</div>
@endif
@stop
