<?php


Route::group(['module' => 'EmployerRecruiter',
              'namespace' => 'App\Modules\EmployerRecruiter\Controllers',
              'middleware' => ['web','auth','EmployerRecruiterOnly'],
              'prefix' => 'employer-recruiter'
          ],
            function() {


    Route::get('careers',['as' => 'employer-recruiter.careers','uses' => 'JobController@index']);
    Route::get('applicants',['as' => 'employer-recruiter.applicants','uses' => 'EmployerController@applicants']);
    Route::get('/',['as' => 'employer-recruiter.home','uses' => 'HomeController@index']);

});
