<?php namespace App\Modules\Dashboard\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Modules\Dashboard\Requests\IRecruiterRequest;

use App\IRecruiter,
    App\User;

class InternalRecruiterController extends Controller
{

    protected $irecruiter,$user;

    public function __construct(IRecruiter $irecruiter,
                                User $user)
    {
        $this->irecruiter = $irecruiter;
        $this->user = $user;
    }

    public function index()
    {
        $irecruiters = $this->irecruiter
                      ->with(['user' => function($query){
                        $query->orderBy('email','asc');
                      }])
                      ->paginate(50);
        return view('Dashboard::irecruiters.index')
                ->with(compact('irecruiters'));
    }

    public function create()
    {
        return view('Dashboard::irecruiters.index');
    }

    public function store(IRecruiterRequest $request)
    {
        $inputs = $request->only('firstname','lastname','email','password');
        $inputs['user_type_id'] = 4;
        $inputs['password'] = \Hash::make($inputs['password']);
        $inputs['is_active'] = 1;
        $user = $this->user->create($inputs);
        $this->irecruiter->create(['user_id' => $user->id,
                                   'status' => 'active']);
        if(count($user)<1){
            return redirect()->back();
        }
        return redirect()
                    ->back()
                    ->with(['successMessage' => 'Created new recruiter']);
    }

    public function update(Request $request)
    {
        $inputs = $request->only('id','firstname','lastname');
        $inputs = array_filter($inputs);
        $inputs['user_type_id'] = 4;
        $inputs['is_active'] = ($request->get('is_active') == '1')?1:0;
        $inputs['status'] = ($request->get('is_active') == '1')?'active':'inactive';
        if($request->has('password')){
            $inputs['password'] = \Hash::make($request->get('password'));
        }
        $user = $this->user->find($inputs['id']);
        if(count($user)< 1){
            return redirect()->back();
        }
        $user->update($inputs);
        $this->irecruiter->where('user_id','=',$user->id)->update(['status' =>$inputs['status']]);
        return redirect()
                ->back()
                ->with(['successMessage' => 'Updated recruiter']);
    }
}
