<?php namespace App\Modules\Dashboard\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Modules\Employer\Requests\CompanyProfileRequest;
use App\Traits\ImageUpload;
use Illuminate\Validation\Rule;
use App\Modules\Employer\Requests\JobCreateRequest;

use App\Job,
	App\JobIndustry,
	App\JobType,
	App\Skill,
	App\Shift,
	App\JobLevel,
  App\Employer,
  App\User,
  App\Applicant,
  App\ApplicantJob,
	App\Region,
	App\Province,
	App\City;

class DashController extends Controller
{

  use ImageUpload;
	protected $jobs;
  protected $applicants;
  protected $applicant_job;
	protected $jtype;
	protected $jindustry;
	protected $jskill;
	protected $shift;
	protected $edu_level;
  protected $employers;
  protected $users;
	protected $location_region, $location_province, $location_city;

	public function __construct(Job $jobs,
								JobIndustry $jindustry,
								JobType $jtype,
								Skill $skill,
								Shift $shift,
								JobLevel $edu_level,
                Employer $employers,
                User $users,
                Applicant $applicants,
                ApplicantJob $applicant_job,
								Region $location_region,
								Province $location_province,
								City $location_city)
	{
      $this->jobs = $jobs;
      $this->applicants = $applicants;
  		$this->applicant_job = $applicant_job;
  		$this->jindustry = $jindustry;
  		$this->jtype = $jtype;
  		$this->skill = $skill;
  		$this->shift = $shift;
  		$this->edu_level = $edu_level;
      $this->employers = $employers;
      $this->users = $users;
			$this->location_region = $location_region;
			$this->location_province = $location_province;
			$this->location_city = $location_city;
	}

    public function index()
    {

    	$pendingjobs = $this->jobs
    						->whereStatus('pending')
    						->orderBy('created_at','desc')
    						->take(25)
    						->get();

      $pending_employers = $this->users
                              ->whereHas('type',function($query){
                                $query->where('Name','=','Employer');
                              })
                              ->whereHas('employer',function($query){
                                $query->where('is_verified','=',0);
                              })
                              ->orderBy('created_at','desc')
                              ->take(25)
                              ->get();
      $hired_applicants = $this->applicant_job
                            ->whereHas('applicant_status',function($query){
                                  $query->where('name','=','Hired');
                            })
                            ->orderBy('created_at','desc')
                            ->take(25)
                            ->get();
      $postedjobscount = $this->jobs->where('status','=','open')->count();
      $pendingjobscount = $this->jobs->where('status','=','pending')->count();
      $employers_count = $this->employers->count();
      $applicants_count = $this->applicant_job
                            ->whereHas('applicant_status',function($query){
                                  $query->where('name','=','Hired');
                            })->count();

    	return view('Dashboard::index')
    			->with(compact('pendingjobs',
                        'hired_applicants',
                        'pendingjobscount',
                        'pending_employers',
                        'postedjobscount',
                        'employers_count',
                        'applicants_count'));
    }

    public function listEmployers()
    {
        $employers = $this->employers->orderBy('created_at','desc')->paginate(50);
        return view('Dashboard::employer.list')
                    ->with(compact('employers'));
    }

    public function showEmployer($company)
    {
        $employer = $this->employers->whereSlug($company)->first();
        if(count($employer)<1){
          return abort(404);
        }

        return view('Dashboard::employer.detail')
                  ->with(compact('employer'));
    }

    public function editEmployer($company)
    {
      $employer = $this->employers->whereSlug($company)->first();
      if(count($employer)<1){
        return abort(404);
      }

      return view('Dashboard::employer.edit')
                ->with(compact('employer'));
    }

    public function updateEmployer(CompanyProfileRequest $request)
    {
        $inputs = $request->only('company_name',
                                'contact_designation',
                                'office_address',
                                'office_number_1',
                                'office_number_2',
                                'website',
                                'description',
                                'sm_facebook',
                                'sm_linkedin',
                                'sm_twitter',
                                'is_verified');

        if($request->hasFile('logo_url')){
            $files = [];
            $files[] = $request->file('logo_url');
            $arr_files = $this->imagesUpload($files, 300, 300);
            if(count(auth()->user()->employer)>0 AND strlen(auth()->user()->employer->logo_url)>0){
                $this->deleteImage(auth()->user()->employer->logo_url);
            }
            $inputs['logo_url'] = $arr_files['uploaded_file'];
        }

        // $inputs['user_id'] = auth()->user()->id;
        $check = $this->employers->where('user_id','=',auth()->user()->id)->update($inputs);
        if(!$check){
            return redirect()->back()->withErrors(['message' => 'Opps! Something went wrong']);
        }
        return redirect()->route('admin.employers');
    }

    public function updateEmployerStatus(Request $request)
    {
        if($request->ajax()){
          $inputs = $request->only('id','status');
          $v = \Validator::make($inputs,[
                'id' => 'exists:users,id',
                'status' => ['required',
                  Rule::in([0,1])
                ]
          ]);

          if($v->fails()){
            return response()->json(['error' => $v],422);
          }

          $updateEmployer = $this->employers->whereId($request->get('id'))->update([$request->get('col') => $request->get('status')]);
          return response()->json(['success' => $updateEmployer],200);
        }
        return abort(403);
    }

    public function showJob($slug)
    {
    	$job = $this->jobs->with('region','province','city')->whereSlug($slug)->first();

    	if(!$job){
    		return abort(404);
    	}
    	$industries = $this->jindustry->orderBy('name','asc')->get();
    	$types =  $this->jtype->orderBy('name','asc')->get();
    	$jskill = $this->skill->orderBy('name','asc')->get();
    	$shifts = $this->shift->orderBy('name','asc')->get();
    	$jlevels = $this->edu_level->orderBy('name','asc')->get();
			$location_region = $this->location_region->orderBy('regCode','asc')->get();
			$location_province_data = $this->location_province
                          ->where('provCode','=',$job->location_province)
                          ->first();
      $location_city_data = $this->location_city
                        ->where('citymunCode','=',$job->location_city)
                        ->first();
    	return view('Dashboard::jobs.detail')
    			->with(compact('job','jskill','types','industries','shifts','jlevels','location_region','location_province_data','location_city_data'));
    }

    public function updateJob(JobCreateRequest $request,$slug,$code)
    {
        $inputs = $request->only('job_title','job_type_id','job_industry_id','education_level_id','work_location','skills','salary_from','salary_to','work_shift_id','description','finders_fee','provide_relocation','designation','status','referral_reward','applicant_reward','location_region','location_province','location_city');
				$inputs['provide_relocation'] = $request->has('provide_relocation')?1:0;
				// return $inputs;
				$job = $this->jobs->where('job_code','=',$code)->first();
        // return $job;
        // return dd($job->update(['description' => 'fck']));
        $check = $job->update($inputs);
				// return dd($check);
        if(!$check){
            return redirect()->back()->withErrros(['Opps! Something went wrong!']);
        }
        return redirect()->route('admin.job.list');
    }

    public function listJob()
    {
        $jobs = $this->jobs
                    ->orderBy('created_at','desc')
                    ->orderBy('status','desc')
                    ->paginate(50);

        return view('Dashboard::jobs.list')
                ->with(compact('jobs'));
    }

		public function fetchProvince($regcode)
		{

				$provinces = $this->location_province
												->where('regCode','=',$regcode)
												->orderBy('provDesc','asc')
												->get();
				if(count($provinces)< 1){
						return abort(404);
				}

				return response()->json(['success' => true,
																 'provinces' => $provinces],200);
		}

		public function fetchCities($provcode)
		{
				$cities = $this->location_city
												->where('provCode','=',$provcode)
												->orderBy('citymunDesc','asc')
												->get();

				if(count($cities)< 1){
						return abort(404);
				}

				return response()->json(['success' => true,
																 'cities' => $cities],200);
		}
}
