<?php namespace App\Modules\Dashboard\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Traits\ImageUpload;
use App\Mail\NotifyEmployerHFstatus;

use App\IRecruiter,
    App\User,
    App\ApplicantJob,
    App\ApplicantJobActivity,
    App\Job,
    App\JobIndustry,
    App\ApplicantEducationLevel,
    App\Region,
    App\Province,
    App\City,
    App\ApplicantEducation,
    App\ApplicantExperience,
    App\ApplicantActivity,
    App\Applicant;

class ApplicantController extends Controller
{
    use ImageUpload;

    protected $irecruiter,$user,$applicant_job,$jobs,$applicant,$applicantActivity;
    protected $applicantJobActivity;

    public function __construct(IRecruiter $irecruiter,
                                User $user,
                                ApplicantJob $applicant_job,
                                ApplicantJobActivity $applicantJobActivity,
                                Job $jobs,
                                JobIndustry $jindustries,
                                ApplicantEducation $appEducation,
                                ApplicantEducationLevel $appEduLevel,
                                ApplicantExperience $appExperience,
                                Region $location_region,
                                Province $location_province,
                                City $location_city,
                                Applicant $applicant,
                                ApplicantActivity $applicantActivity)
    {
        $this->irecruiter = $irecruiter;
        $this->user = $user;
        $this->applicant_job = $applicant_job;
        $this->jobs = $jobs;
        $this->applicant = $applicant;
        $this->applicantJobActivity = $applicantJobActivity;
        $this->applicantActivity = $applicantActivity;
        $this->jindustries = $jindustries;
        $this->location_region = $location_region;
        $this->location_province = $location_province;
        $this->location_city = $location_city;
        $this->appEducation = $appEducation;
        $this->appEduLevel = $appEduLevel;
        $this->appExperience = $appExperience;
    }

    public function index()
    {
        $applicants = $this->applicant
                            ->orderBy('created_at','asc')
                            ->paginate(50);
        // return $applicants;
        return view('Dashboard::applicants.index')
                ->with(compact('applicants'));
    }

    public function show($auid)
    {
        $applicant = $this->applicant
                            ->whereAuid($auid)
                            ->first();
        $applied_jobs = $applicant->appliedJobs()
                                  ->orderBy('created_at','asc')
                                  ->paginate(50);

        $activities = $this->applicantActivity
                            ->where('applicant_id','=',$applicant->id)
                            ->orderBy('created_at','asc')
                            ->paginate(50);
        if(\Session::get('viewing-application') !== $auid){
            $this->applicantActivity->create(['applicant_id' => $applicant->id,
                                         'user_id' => auth()->user()->id,
                                         'icon' => '<i class="ico-eye-open bgcolor-success"></i>',
                                         'title' => 'Viewing Application',
                                         'description' => 'Viewed applicant profile ('.$auid.')']);
            \Session::put('viewing-application',$auid);
        }
        return view('Dashboard::applicants.show')
                        ->with(compact('applicant','applied_jobs','activities'));
    }

    public function fetchApplication($code)
    {
        $application = $this->applicant_job
                            ->whereApplicationCode($code)
                            ->first();
        return view('Dashboard::applicants.applied-job')
                        ->with(compact('application'));
    }

    public function edit($applicant)
    {
        $application = $this->applicant_job
                        ->whereApplicationCode($applicant)
                        ->first();
        if(count($application)<1){
          return abort(404);
        }
        $industries = $this->jindustries->orderBy('name','asc')->get();
        $levels = $this->appEduLevel->orderBy('name','asc')->get();
        $applicant = $application->applicant;
        $location_region = $this->location_region->orderBy('regCode','asc')->get();

        return view('Dashboard::applicants.edit')
                    ->with(compact('applicant','application',
                                    'industries','levels',
                                    'location_region'));
    }

    public function update(Request $request,$applicant)
    {
        $inputs = $request->all();
        $application = $this->applicant_job
                      ->whereApplicationCode($applicant)
                      ->first();
        $applicant = $application->applicant;
        $countExp = $this->countExp($inputs);
        $countEdu = $this->countEdu($inputs);

      $inputs = array_except($inputs,['id','user_id','created_at','updated_at']);
      if($request->hasFile('logo_url')){
          $files = [];
          $files[] = $request->file('logo_url');
          $arr_files = $this->imagesUpload($files, 300, 300);
          if(strlen($applicant->user->avatar)>0){
              $this->deleteImage($applicant->user->avatar);
          }
          $inputs['avatar'] = $arr_files['uploaded_file'];
      }
      $user = \App\User::find($applicant->user->id); $user->update(array_only($inputs,['email','firstname','middlename','lastname','avatar']));
    //   return $user;
      $data_applicant_profile = array_only($inputs,['current_address',
                                               'job_industry_id',
                                               'education_level_id',
                                               'current_city',
                                               'current_province',
                                               'date_of_birth',
                                               'skills',
                                               'location_region',
                                              'location_province',
                                              'location_city',
                                               'languages',
                                               'biography']);
      $data_applicant_profile['position'] = $request->get('current_position');
      $data_applicant_profile['mobile_number_1'] = $request->get('number_mobile');
      $data_applicant_profile['house_number_1'] = $request->get('number_landline');
      $data_applicant_profile['willing_to_relocate'] = $request->has('willing_to_relocate')?1:0;

      if($request->hasFile('attached_resume')){
          $files = [];
          $file = $request->file('attached_resume');
          $filename = str_random(5).'-eq-'.str_slug($applicant->user->my_name).'.'.$file->getClientOriginalExtension();
          $destination= public_path('upload-resume').'/'.$filename;
          $file->move(public_path('upload-resume'),$filename);
          if(count($applicant->user->resume)>0 AND strlen($applicant->user->resume->attached_resume)>0){
              $this->deleteImage($applicant->user->resume->attached_resume);
          }
          $data_applicant_profile['attached_resume'] = url('upload-resume').'/'.$filename;
      }

      $applicant->update($data_applicant_profile);
      $this->appEducation->whereApplicantId($applicant->id)->delete();
      $this->appExperience->whereApplicantId($applicant->id)->delete();
      $arr_runExp = $this->runExp($countExp,$inputs,$applicant->id);
      $arr_runEdu = $this->runEdu($countEdu,$inputs,$applicant->id);
      return redirect()->back()->with(['successMessage' => 'Updated '.$application->job->job_code]);
      // return redirect()->route('admin.careers.applicant.details',['job_code' => $application->job->job_code,'applicant' => $application->application_code]);
    }

    public function fetchProvince($regcode)
    {

        $provinces = $this->location_province
                        ->where('regCode','=',$regcode)
                        ->orderBy('provDesc','asc')
                        ->get();
        if(count($provinces)< 1){
            return abort(404);
        }

        return response()->json(['success' => true,
                                 'provinces' => $provinces],200);
    }

    public function fetchCities($provcode)
    {
        $cities = $this->location_city
                        ->where('provCode','=',$provcode)
                        ->orderBy('citymunDesc','asc')
                        ->get();

        if(count($cities)< 1){
            return abort(404);
        }

        return response()->json(['success' => true,
                                 'cities' => $cities],200);
    }

    public function applicantJob($job, $applicant)
    {

        $application = $this->applicant_job
                            ->whereApplicationCode($applicant)
                            ->first();
        if(count($application)<1){
            return abort(404);
        }

        if(\Session::get('viewing-application') !== $application->application_code){
            $this->applicantJobActivity->create(['applicant_job_id' => $application->id,
                                         'user_id' => auth()->user()->id,
                                         'icon' => '<i class="ico-eye-open bgcolor-success"></i>',
                                         'title' => 'Viewing Application',
                                         'description' => 'Viewed applicant profile ('.$application->application_code.')']);
            \Session::put('viewing-application',$application->application_code);
        }

        return view('Dashboard::applicants.applicant-detail-v2')
                        ->with(compact('application'));
    }

    public function applicantAppoiments($job, $applicant)
    {
      $application = $this->applicant_job
                          ->whereApplicationCode($applicant)
                          ->first();

      return view('Dashboard::applicants.applicant-detail')
                      ->with(compact('application'));
    }

    public function updateApplicantStatus(Request $request)
    {
        if($request->ajax()){
            $applicant = $this->applicant_job
                                ->whereApplicationCode($request->get('applicant_code'))
                                ->first();
            if(count($applicant)<1){
                return response()->json(['error' => true],403);
            }
            if($request->has('status')){
              $up = $applicant->update(['applicant_status_id' => $request->get('status')]);
            }
            $status = $applicant->applicant_status->name;
            return response()->json(['success' => $up,
                                      'status' => $status],200);
        }
    }

    public function applicantActivityComment(Request $request)
    {
        if($request->ajax()){
            $auid = $request->get('application_code');
            $applicant = $this->applicant->where('auid','=',$auid)->first();

            // $application = $this->applicantActivity
            //                     ->whereApplicantId($applicant->id)
            //                     ->first();
            if(count($applicant)<1){
                return response()->json(['error' => 'opps!'],404);
            }
            $activity = $this->applicantActivity
                             ->create(['applicant_id' => $applicant->id,
                                     'user_id' => auth()->user()->id,
                                     'icon' => '<i class="ico-mail bgcolor-success"></i>',
                                     'title' => 'Commented Application',
                                      'description' =>  $request->get('comment').' ('.$auid.')']);
            $activity->created_by = auth()->user()->my_name;
            return response()->json(['success' => true,
                                     'activity' => $activity],200);
        }

    }

    public function updateStages(Request $request,$applicant_code)
    {
        // return dd($request->has('refer_premium'));
        $inputs = $request->only('applicant_status_id',
                            'employer_appointment_type',
                             'employer_appointment_sched',
                             'reason',
                             'refer_premium',
                             'refer_regular',
                             'notes',
                             'hire_date',
                             'hire_account');
        $application = $this->applicant_job
                            ->whereApplicationCode($applicant_code)
                            ->first();
        $stat = \App\ApplicantStatus::find($inputs['applicant_status_id']);
        $comment = 'Change stages status '.$stat->name. 'on job '.$application->job->job_title;

        if(count($application)<1){
            return redirect()->back();
        }

        $check_refer = $request->has('refer_premium') OR $request->has('refer_regular');
        if($check_refer){
            $job_code = ($request->has('refer_premium'))?
                            $request->get('refer_premium'):
                            $request->get('refer_regular');

            $job = $this->jobs->whereJobCode($job_code)
                       ->first();
            if(count($job)<1){
                return redirect()
                            ->back();
            }
            $inputs['job_id'] = $job->id;
            $inputs['employer_id'] = $job->employer_id;
            $inputs['applicant_status_id'] = 1;
        }

        if($request->hasFile('endorsement_slip')){
            $inputs['endorsement_slip'] = $this->uploadEndorsementSlip($request,
                                                $applicant_code,
                                                $application);
        }
        $inputs = array_filter($inputs);
        $application->update($inputs);

        $activity = $this->applicantActivity
                         ->create(['applicant_id' => $application->applicant_id,
                                 'user_id' => auth()->user()->id,
                                 'icon' => '<i class="ico-mail bgcolor-success"></i>',
                                 'title' => 'Stages Update',
                                'description' =>  $comment.' ('.$application->application_code.')']);

        $email_inputs['subject'] = 'Candidate status update '.$stat->name;
        $email_inputs['hf_status'] = $stat->name;
        $email_inputs['employer_email'] =  $application->employer->user->email;
        $email_inputs['employer'] = $application->employer->company_name;
        $email_inputs['applicant_name'] = $application->applicant->user->my_name;
        $email_inputs['applicant_email'] = $application->applicant->user->email;
        $email_inputs['application_code'] = $application->application_code;
        $email_inputs['job_title'] = $application->job->job_title;
        $email_inputs['job_title_slug'] = $application->job->slug;
        $email_inputs['job_code'] = $application->job->job_code;
        $email_inputs['referred_by_email'] = ($application->applicant->external_recruiter)?$application->applicant->external_recruiter->email:'';
        $email_inputs['referred_by_name'] = ($application->applicant->external_recruiter)?$application->applicant->external_recruiter->my_name:'';
        $email_inputs['hfassgined_by_email'] = ($application->applicant->internal_recruiter)?$application->applicant->internal_recruiter->email:'';
        $email_inputs['hfassgined_by_name'] = ($application->applicant->internal_recruiter)?$application->applicant->internal_recruiter->my_name:'';
        $arr_stat = hfStages($inputs['applicant_status_id']);
        foreach($arr_stat as $email_stat){
            $email_inputs[$email_stat] = $application->$email_stat;
        }

        // return $email_inputs;
        $when = \Carbon\Carbon::now()->addMinutes(2);
        // notify employer for endorse, active file, no show, reschedule, hired
        $notify_employer = [2,6,7,8];
        if(in_array($inputs['applicant_status_id'],$notify_employer)){
            \Mail::later($when,new NotifyEmployerHFstatus($email_inputs));
        }

        return redirect()->back()
                          ->with(['successMessage' => 'Updated Status']);
        //return redirect()->route('admin.careers.applicant.details',['job_code' => $application->job->job_code,'application_code' => $applicant_code]);
    }

    protected function uploadEndorsementSlip($request,$applicant_code,$application)
    {
      if($request->hasFile('endorsement_slip')){
          $files = [];
          $file = $request->file('endorsement_slip');
          $filename = $applicant_code.'-'.str_random(5).'.'.$file->getClientOriginalExtension();
          $destination= public_path('endorsement-slips-media').'/'.$filename;
          $file->move(public_path('endorsement-slips-media'),$filename);
          if(strlen($application->endorsement_slip)>0){
              $this->deleteImage($application->endorsement_slip);
          }
          return url('endorsement-slips-media').'/'.$filename;
      }
    }

    protected function runExp($countExp,$inputs,$applicant_id)
    {   $experience = [];
        $check = [];
        if(!$countExp){
            return $check;
        }
        foreach(range(1,$countExp) as $expIndex => $expVal){
            $experience['applicant_id'] = $applicant_id;
            $experience['company_name'] = isset($inputs['company_name_'.$expVal])?$inputs['company_name_'.$expVal]:null;
            $experience['position'] = isset($inputs['position_'.$expVal])?$inputs['position_'.$expVal]:null;
            $experience['resposibilities'] = isset($inputs['resposibilities_'.$expVal])?$inputs['resposibilities_'.$expVal]:null;
            $experience['start_date'] = isset($inputs['start_date_exp_'.$expVal])?$inputs['start_date_exp_'.$expVal]:null;
            $experience['end_date'] = isset($inputs['end_date_exp_'.$expVal])?$inputs['end_date_exp_'.$expVal]:null;
            $check[] = $this->appExperience->create($experience);
        }
        return $check;
    }

    protected function runEdu($countExp,$inputs,$applicant_id)
    {   $experience = [];
        $check = [];
        if(!$countExp){
            return $check;
        }
        foreach(range(1,$countExp) as $expIndex => $expVal){
            $experience['applicant_id'] = $applicant_id;
            $experience['school_name'] = isset($inputs['school_name_'.$expVal])?$inputs['school_name_'.$expVal]:null;
            $experience['attainment'] = isset($inputs['attainment_'.$expVal])?$inputs['attainment_'.$expVal]:null;
            $experience['awards'] = isset($inputs['awards_'.$expVal])?$inputs['awards_'.$expVal]:null;
            $experience['start_date'] = isset($inputs['start_date_'.$expVal])?$inputs['start_date_'.$expVal]:null;
            $experience['end_date'] = isset($inputs['end_date_'.$expVal])?$inputs['end_date_'.$expVal]:null;
            $check[] = $this->appEducation->create($experience);
        }
        return $check;
    }

    protected function countExp($inputs)
    {
        $count_expcompany_name = count(preg_grep("/^company_name_(\d)+$/",array_keys($inputs)));
        $count_expposition = count(preg_grep("/^position_(\d)+$/",array_keys($inputs)));
        $sumexp = ($count_expcompany_name + $count_expposition) % 2;
        if($sumexp > 0){
            return redirect()->back()->withErrors(['message' => ' Please information on Experience section such as Company Name and Position']);
        }
        return $count_expcompany_name;
    }

    protected function countEdu($inputs)
    {
        $count_eduschool_name = count(preg_grep("/^school_name_(\d)+$/",array_keys($inputs)));
        $count_eduattainment = count(preg_grep("/^attainment_(\d)+$/",array_keys($inputs)));
        $sumedu = ($count_eduschool_name + $count_eduattainment) % 2;
        if($sumedu > 0){
            return redirect()->back()->withErrors(['message' => ' Please information on Experience section such as Company Name and Position']);
        }
        return $count_eduattainment;
    }

    public function downloadResume($code)
    {
        $applicant = $this->applicant->where('auid','=',$code)->first();
        if(count($applicant)<1){
            return abort(404);
        }
        $pathFIle = str_replace(url('/'), public_path() ,$applicant->attached_resume);
        return response()->download($pathFIle);
    }

    public function downloadEndorsementSlip($application_code)
    {
        $applicant = $this->applicant_job->where('application_code','=',$application_code)->first();
        if(count($applicant)<1){
            return abort(404);
        }
        $pathFIle = str_replace(url('/'), public_path() ,$applicant->endorsement_slip);
        return response()->download($pathFIle);
    }
}
