@extends('Dashboard::template.main')

@section('content')
  <!-- Page Header -->
  <div class="page-header page-header-block">
      <div class="page-header-section">
          <h4 class="title semibold">Job List</h4>
      </div>
      <div class="page-header-section">
          <!-- Toolbar -->
          <div class="toolbar clearfix">
              <div class="col-xs-8">
                <input type="text" class="form-control" placeholder="Seach Job">
              </div>
              <div class="col-xs-4">
                  <button class="btn btn-primary pull-right"><i class="ico-search"></i>Search</button>
              </div>
          </div>
          <!--/ Toolbar -->
      </div>
  </div>
  <!-- Page Header -->

  <!-- Browser Breakpoint -->
  <div class="row">
      <div class="col-lg-12">
          <!-- START panel -->
          <div class="panel panel-default">
              <!-- panel heading/header -->
              <div class="panel-heading">
                  <h3 class="panel-title ellipsis"><i class="ico-blog2 mr5"></i>Job Post</h3>
                  <!-- panel toolbar -->
                  <div class="panel-toolbar text-right">
                  </div>
                  <!--/ panel toolbar -->
              </div>
              <!--/ panel heading/header -->
              <!-- panel body with collapse capabale -->
              @if(count($jobs)>0)
              <div class="table-responsive panel-collapse pull out">
                  <table class="table table-condensed table-bordered table-hover table-striped">
                      <thead>
                          <tr>
                              <th>Job</th>
                              <th width="20%">Company</th>
                              <th>Posted Date</th>
                              <th>Due Date</th>
                              <th>Fee's</th>
                              <th>Total Hired</th>
                              <th>Total Applied</th>
                              <th class="text-center">Status</th>
                              <th></th>
                          </tr>
                      </thead>
                      <tbody>
                        @foreach($jobs as $job)
                          <tr>
                              <td><a href="{{route('admin.job.detail',['slug' => $job->slug])}}" class="semibold text-accent" target="_blank">{{ $job->job_title }}</a>
                                <br>
                                <span class="text-muted">CODE: {{ $job->job_code }}</span>
                                <br>
                                {{ $job->full_address }}
                              </td>
                              <td>
                                @if($job->employer->is_verified)
                                <span class="text-success"><i class="fa fa-check-circle"></i></span>
                                @else
                                <span class="text-muted"><i class="fa fa-circle"></i></span>
                                @endif
                                {{ $job->employer->company_name }} <br>
                                @if($job->employer->is_premium)
                                <span class="text-warning"><i class="fa fa-shield"></i> Premium</span>
                                @endif
                              </td>
                              <td>{{ $job->created_at }}</td>
                              <td>{{ $job->valid_until }}</td>
                              <td>
                                @if($job->employer->is_premium)
                                <small>Finders Fee: <span class="label label-default">{{ $job->finders_fee }}</span></small><br>
                                <small>Referral Reward: <span class="label label-default">{{ $job->referral_reward }}</span></small><br>
                                <small>Applicant Reward: <span class="label label-default">{{ $job->applicant_reward }}</span></small><br>
                                @else
                                  <span class="label label-default">Not Applicable</span>
                                @endif
                              </td>
                              <td class="text-center">
                                <a href="">
                                  <span class="label label-inverse">{{ $job->hiredApplicants->count() }}</span>
                                </a>
                              </td>
                              <td class="text-center">
                                <a href="">
                                  <span class="label label-inverse">{{ $job->applicants->count() }}</span>
                                </a>
                              </td>
                              <td class="text-center">
                                  <span class="label label-inverse">{{ strtoupper($job->status) }}</span>
                              </td>
                              <td>
                                <a href="{{ route('admin.job.detail',['slug' => $job->slug]) }}" class="btn btn-warning btn-xs"><i class="fa fa-edit"></i></a>
                              </td>
                          </tr>
                        @endforeach
                      </tbody>
                  </table>
              </div>
              {{ $jobs->links() }}
              <!--/ panel body with collapse capabale -->
              @else
              <div class="panel-body">
                <div class="alert alert-info">
                  <strong>No Data!</strong>
                </div>
              </div>
              @endif
          </div>
          <!--/ END panel -->
      </div>
  </div>
  <!-- Browser Breakpoint -->
@stop
