@extends('Dashboard::template.main')

@section('content')
  <!-- Page Header -->
  <div class="page-header page-header-block">
      <div class="page-header-section">
          <h4 class="title semibold">Applicants</h4>
      </div>
      <div class="page-header-section">
        <!-- Toolbar -->
        <div class="toolbar clearfix text-right">
            <a href="#" class="btn btn-sm btn-default" title="Create Job Post" data-toggle="modal" data-target="#createRecruiter"><i class="ico-file-plus"></i></a>
        </div>
        <!--/ Toolbar -->
      </div>
  </div>
  <!-- Page Header -->
  <div class="row">
    <div class="col-md-12">
      @if(\Session::has('successMessage'))
      <div class="row">
        <div class="col-md-12">
          <div class="alert alert-success">
            <strong>{{ \Session::get('successMessage') }}</strong>
          </div>
        </div>
      </div>
      @endif
      <!-- Browser Breakpoint -->
      <div class="row">
          <div class="col-lg-12">
              <!-- START panel -->
              <div class="panel panel-default">
                  <!-- panel heading/header -->
                  <div class="panel-heading">
                      <h3 class="panel-title ellipsis"><i class="ico-reading mr5"></i>Applicants</h3>
                      <!-- panel toolbar -->
                      <div class="panel-toolbar text-right">
                      </div>
                      <!--/ panel toolbar -->
                  </div>
                  <!--/ panel heading/header -->
                  <!-- panel body with collapse capabale -->
                  @if(count($applicants)>0)
                  <div class="table-responsive panel-collapse pull out">
                      <table class="table">
                          <thead>
                              <tr>
                                  <th></th>
                                  <th>Name</th>
                                  <th class="text-center"># Jobs <br>Applied</th>
                                  <th>Status</th>
                                  <th>Assigned HF</th>
                                  <th>Refered By</th>
                                  <th class="text-center"></th>
                              </tr>
                          </thead>
                          <tbody>
                            @foreach($applicants as $applicant)
                            <tr>
                              <td>
                                <div class="media-object">
                                  <img src="{{ $applicant->user->profile_pic }}" alt="" class="img-circle">
                                </div>
                              </td>
                              <td>
                                {{ $applicant->user->myname }}
                              </td>
                              <td class="text-center">
                                <a href="{{ route('admin.careers.applicant.details-v2',['application_code'  => $applicant->auid]) }}?t=jobs">
                                  <span class="label label-inverse">{{ $applicant->appliedJobs->count() }}</span>
                                </a>
                              </td>
                              <td>{{ ($applicant->current_employment_status)?strtoupper(str_replace('_',' ',$applicant->current_employment_status)):'None' }}</td>
                              <td>{{ ($applicant->internal_recruiter )?$applicant->internal_recruiter->myname:'None' }}</td>
                              <td>{{ ($applicant->external_recruiter)?$applicant->external_recruiter->myname:'None' }}</td>
                              <td>
                                <a href="{{ route('admin.careers.applicant.details-v2',['application_code'  => $applicant->auid]) }}" class="btn btn-default btn-xs"><i class="ico-profile"></i></a>
                              </td>
                            </tr>
                            @endforeach
                          </tbody>
                      </table>
                  </div>
                  {{ $applicants->links() }}
                  <!--/ panel body with collapse capabale -->
                  @else
                  <div class="panel-body">
                    <div class="alert alert-info">
                      <strong>No Data!</strong>
                    </div>
                  </div>
                  @endif
              </div>
              <!--/ END panel -->
          </div>
      </div>
      <!-- Browser Breakpoint -->

    </div>
  </div>

@stop


@section('scripts')
  <script>
  var AdminApplicant = {
      init : function(){

      }
  }
  $(function(){
    AdminApplicant.init();
  });
  </script>
@stop
