@extends('Dashboard::template.main')

@section('content')
  <!-- Page Header -->
  <div class="page-header page-header-block">
      <div class="page-header-section">
          <h4 class="title semibold">Application - {{ $application->applicant->user->my_name }} for {{ $application->job->job_title}} <br>
            <small>CODE: {{ $application->application_code }}</small>
          </h4>
      </div>
      <div class="page-header-section">
          <!-- Toolbar -->
          <div class="toolbar clearfix text-right">
              <a href="{{ \URL::previous() }}" class="btn btn-sm btn-default"  data-toggle="tooltip" data-placement="bottom" title="Create Job Post"><i class="ico-chevron-left"></i></a>
          </div>
          <!--/ Toolbar -->
      </div>
  </div>

  <div class="row">
    <div class="col-md-6">
      <!-- Nav tabs -->
      <ul class="nav nav-tabs" role="tablist">
        <li role="presentation" class="active"><a href="#resume" aria-controls="resume" role="tab" data-toggle="tab">Resume</a></li>
        <li role="presentation"><a href="#cover" aria-controls="cover" role="tab" data-toggle="tab">Cover Letter</a></li>
      </ul>
      <div class="tab-content">
        <div role="tabpanel" class="tab-pane active" id="resume">
          <div class="widget panel">
              <!-- panel body -->
              <div class="panel-body">
                  <ul class="list-unstyled">
                      <li class="text-center">
                          <img class="img-circle img-bordered-success" src="{{ $application->applicant->user->profile_pic }}" alt="" width="75px" height="75px">
                          <br>
                          <h5 class="semibold mb0">{{ $application->applicant->user->my_name }}</h5>
                          <p class="nm text-muted">{{ $application->applicant->position }}</p>
                          <a href="javscript:void(0);" class="nm text-teal"><i class="fa fa-link"></i> {{ str_replace(url('upload-resume').'/', '', $application->applicant->attached_resume) }} </a>
                          <p><a href="{{ route('admin.applicant.edit',['applicant' => $application->application_code ]) }}" target="_blank" class="btn btn-warning btn-sm">Edit Profile</a></p>
                      </li>
                  </ul>
                  <hr>
                  <div class="row">
                    <div class="col-md-6">
                      <h4><i class="ico-bubble-dots3"></i> Languages</h4>
                      @if(strlen($application->applicant->languages)>0)
                        <ul class="list-inline">
                        @foreach(explode(',',$application->applicant->languages) as $language)
                        <li><span class="label label-default">{{ $language }}</span></li>
                        @endforeach
                        </ul>
                      @endif
                    </div>
                    <div class="col-md-6">
                      <h4><i class="ico-bubble-dots3"></i> Skills</h4>
                      @if(strlen($application->applicant->skills)>0)
                      <ul class="list-inline">
                        @foreach(explode(',',$application->applicant->skills) as $skill)
                        <li><span class="label label-default">{{ $skill }}</span></li>
                        @endforeach
                      </ul>
                      @endif
                    </div>
                  </div>
              </div>
              <!--/ panel body -->
          </div>

          <div class="widget panel">
            <div class="panel-body">
              <div class="table-layout">
                <h4>Biography</h4>
                {!! $application->applicant->biography !!}
                <hr>

                <div class="row">
                  <div class="col-md-6">
                    <h4><i class="ico-vcard"></i> Personal Information</h4>
                    <p class="mb5">Full name : <strong class="text-uppercase">{{ $application->applicant->user->my_name }}</strong></p>
                    <p class="mb5">D.O.B : <strong class="text-uppercase">{{ $application->applicant->date_of_birth?:'None' }}</strong></p>
                    <p class="mb5">Email : <strong class="text-uppercase"><a href="mailto:{{ $application->applicant->user->email }}?Subject=From%20Human%20Factor">{{ $application->applicant->user->email }}</a></strong></p>
                    <p class="mb5">Employment Status: <strong class="text-uppercase">{{ $application->applicant->employment_status?:'None' }}</strong></p>
                  </div>
                  <div class="col-md-6">
                    <div class="row">
                      <h4><i class="ico-map-marker"></i> Address</h4>
                      <p>{{ $application->applicant->full_address }}</p>
                    </div>
                    <div class="row">
                      <p class="mb5">Mobile Number: <strong class="text-uppercase">{{ $application->applicant->mobile_number_1?:'None' }}</strong></p>
                      <p class="mb5">House Number: <strong class="text-uppercase">{{ $application->applicant->house_number_1?:'None' }}</strong></p>
                    </div>
                  </div>
                </div>

                <hr>
                <div class="row">
                  <div class="col-md-12">
                    <h4>Experiences</h4>
                    @if(count($application->applicant->experiences )>0)
                    @foreach($application->applicant->experiences as $exp)
                      @if(strlen($exp->company_name )>0 AND strlen($exp->position)>0)
                      <h5 class="text-uppercase">{{ $exp->position }} @ {{ $exp->company_name }}</h5>
                      @endif
                      @if(strlen($exp->from)>0 AND strlen($exp->to)>0)
                      <p class="text-muted">{{ $exp->from }} - {{ $exp->to }}</p>
                      @endif
                      @if(strlen($exp->resposibilities)>0)
                      <p>{{ $exp->resposibilities }}</p>
                      @endif
                    @endforeach
                    @else
                    <div class="alert alert-info">
                      No data
                    </div>
                    @endif
                  </div>
                </div><!-- row -->

                <hr>
                <div class="row">
                  <div class="col-md-12">
                    <h4>Educations</h4>
                    @if(count($application->applicant->educations )>0)
                    @foreach($application->applicant->educations as $edu)
                      @if(strlen($edu->school_name)>0 AND strlen($edu->attainment)>0)
                      <h5 class="text-uppercase">{{ $edu->attainment }} @ {{ $edu->school_name }}</h5>
                      @endif
                      @if(strlen($edu->from_to)>0 AND strlen($edu->to)>0)
                      <p class="text-muted">{{ $edu->from }} - {{ $edu->to }}</p>
                      @endif
                      @if(strlen($edu->awards)>0)
                      <p>{{ $edu->awards }}</p>
                      @endif
                    @endforeach
                    @else
                    <div class="alert alert-info">
                      No data
                    </div>
                    @endif
                  </div>
                </div><!-- row -->

              </div>
            </div>
          </div>
        </div><!-- Resume Tab -->

        <div role="tabpanel" class="tab-pane" id="cover">
          <div class="widget panel">
            <div class="panel-body">
              <h3 class="panel-title">Cover Letter</h3>
              <p>{!! nl2br($application->applicant_cover_letter) !!}</p>
              <p>Expected Salary : <strong>{{ $application->expected_salary }}</strong></p>
            </div>
          </div>
        </div><!-- cover -->
      </div>
    </div>
    <div class="col-md-6">
      <div class="alert alert-info">
        <p><strong>Current Status :</strong> {{ $application->applicant_status->name }} </p>
        @if($application->employer_appointment_type)
        <p><strong>Appointments :</strong> {{ ucwords(str_replace('-',' ',$application->employer_appointment_type)) }} | {{ $application->employer_appointment_sched }} </p>
        @endif
        @if($application->endorsement_slip)
        <p><strong>Edorsement Slip :</strong> <i class="fa fa-file"></i> {{ str_replace(url('/').'/endorsement-slips-media/','',$application->endorsement_slip) }} </p>
        @endif
        @if($application->hire_date && $application->hire_date !== '0000-00-00')
        <p><strong>Hire Date :</strong> {{ $application->hire_date }} </p>
        @endif
        @if($application->hire_account)
        <p><strong>Account :</strong> {{ $application->hire_account }} </p>
        @endif
        @if($application->reason)
        <p><strong>Reason :</strong> {{ $application->reason }} </p>
        @endif
        @if($application->note)
        <p><strong>Note :</strong> {{ $application->note }} </p>
        @endif
      </div>
      <div class="widget panel">
        <div class="panel-heading">
          <h3 class="panel-title">Stages</h3>
        </div>
        <div class="panel-body">
          {{ \Form::open(['route' => ['admin.application.stages', $application->application_code],
                          'id' => 'form',
                          'files' => true]) }}
            <div class="form-group">
              <select name="applicant_status_id" id="stages" class="form-control">
                <option value="1" {{($application->applicant_status_id == '1' )?'selected':''}}>Pipeline</option>
                <option value="2" {{($application->applicant_status_id == '2' )?'selected':''}}>Endorsed</option>
                <option value="3" {{($application->applicant_status_id == '3' )?'selected':''}}>Re profile to Premium Job post</option>
                <option value="4" {{($application->applicant_status_id == '4' )?'selected':''}}>Active file</option>
                <option value="5" {{($application->applicant_status_id == '5' )?'selected':''}}>Refer to Regular Job Post</option>
                <option value="6" {{($application->applicant_status_id == '6' )?'selected':''}}>No show</option>
                <option value="7" {{($application->applicant_status_id == '7' )?'selected':''}}>Re schedule</option>
                <option value="8" {{($application->applicant_status_id == '8' )?'selected':''}}>Hired</option>
              </select>
            </div>
            <div class="form-group app_type">
              <div class="radio">
                <label>
                  <input type="radio" name="employer_appointment_type" id="optionsRadios1" value="on-site" checked>
                  <span>On Site</span>
                </label>
              </div>
              <div class="radio" style="margin-top:5px;">
                <label>
                  <input type="radio" name="employer_appointment_type" id="optionsRadios2" value="call">
                  <span>Over the phone</span>
                </label>
              </div>
            </div>
            <div class="row pick_datetime" style="margin-top:20px;">
              <div class="col-md-12">
                  <h4 class="panel-title">Schedule date and time</h4>
                 <div id="datetimepicker1" class="mt10"></div>
              </div>
            </div>
            <div class="row mt10 pick_datetime">
                <div class="panel-heading">
                <h4 class="panel-title fsize13">Date & Time: <span id="hidden-val"></span></h4>
                <input type="hidden" name="employer_appointment_sched" id="my_hidden_input">
                <input type="hidden" id="my_hidden_type">
                <h4 class="panel-title fsize13">Type: <span id="appointment-type"></span></h4>
                </div>
            </div>
            <div class="form-group mt10 attachment">
              <label for="exampleInputFile">Attached Endorsement Slip</label>
              <input type="file" name="endorsement_slip" id="exampleInputFile">
              <p class="help-block">Must be in PDF format.</p>
            </div>
            <div class="form-group mt10 reason">
              <div class="has-icon mb10">
                  <input type="text" name="reason" class="form-control" placeholder="Reason">
                  <i class="ico-pencil2 form-control-icon"></i>
              </div>
            </div>
            <div class="form-group refer_premium">
              <label>Refer to Premium Job post</label>
              <input type="text" name="refer_premium" class="form-control" placeholder="Input Job Code">
            </div>
            <div class="form-group refer_regular">
              <label>Refer to Regular Job post</label>
              <input type="text" name="refer_regular" class="form-control" placeholder="Input Job Code">
            </div>
            <div class="form-group mt10 notes">
              <div class="has-icon mb10">
                  <input type="text" name="notes" class="form-control" placeholder="Notes">
                  <i class="ico-file form-control-icon"></i>
              </div>
            </div>
            <div class="form-group mt10 hire_date">
              <label>Start Date</label>
              <input type="text" name="hire_date" class="form-control">
            </div>
            <div class="form-group mt10 hire_account">
              <label>Account</label>
              <input type="text" name="hire_account" class="form-control" placeholder="Account/Department">
            </div>
            <div class="form-group">
              <button type="submit" class="btn btn-sm btn-info">Save</button>
            </div>
          </form>
        </div>
      </div>

      <div class="widget panel">
        <div class="panel-body">
        <div class="form-group mt10">
          <div class="has-icon mb10">
              <input type="text" class="form-control" id="comment" placeholder="Comments here">
              <i class="ico-bubble form-control-icon"></i>
          </div>
        </div>
        <div class="panel-heading"><h5 class="panel-title"><i class="ico-health mr5"></i>Latest Activity  {{ date('F-d')}}</h5></div>
        <!-- Media list feed -->
        @php
          $activities = $application->activities()->paginate(15);
        @endphp
        @if(count($activities)>0)
        <ul class="media-list media-list-feed nm">
            @foreach($activities as $activity)
            <li class="media">
                <div class="media-object pull-left">
                    {!! $activity->icon !!}
                </div>
                <div class="media-body">
                    <p class="media-heading">{{ $activity->title }}</p>
                    <p class="media-text"><span class="text-primary semibold">{{ $activity->description }}</span> by {{ $activity->user->my_name }}</p>
                    <p class="media-meta">{{ $activity->created_at }}</p>
                </div>
            </li>
            @endforeach
        </ul>
        @endif
        </div>
      </div>
    </div>
  </div>
@stop


@section('styles')
  <link rel="stylesheet" href="{{ asset('hfv1/plugins/sweetalert/dist/sweetalert.css') }}">
  <link rel="stylesheet" href="{{asset('bower_components/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css')}}" />
  <link rel="stylesheet" href="{{ asset('hfv1/plugins/bootstrap-datepicker/css/datepicker3.css') }}">
@stop
@section('scripts')
  <script src="{{ asset('hfv1/plugins/sweetalert/dist/sweetalert.min.js') }}"></script>
  <script type="text/javascript" src="{{asset('bower_components/moment/min/moment.min.js')}}"></script>
  <script type="text/javascript" src="{{asset('bower_components/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js')}}"></script>
  <script src="{{ asset('hfv1/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js') }}"></script>
  <script>
  var ApplicationDetail = {
      init : function(){
        this.changeStatus();
        this.addComment();
        this.datePick();
        this.appointmentType();
        // this.setAppoiment();
        this.stages();
        this.elemStageShow($('#stages').val())
        $('[name="hire_date"]').datepicker({
          format:'YYYY-MM-DD'
        })
      },
      elemHide : function(){
        $('.app_type').hide();
        $('.pick_datetime').hide();
        $('.attachment').hide();
        $('.reason').hide();
        $('.refer_premium').hide();
        $('.refer_regular').hide();
        $('.notes').hide();
        $('.hire_date').hide();
        $('.hire_account').hide();
      },
      elemStageShow: function(stage){
        switch (stage) {
          case '2':
            ApplicationDetail.elemHide()
            $('.app_type').show();
            $('.pick_datetime').show();
            $('.attachment').show();
            break;
          case '3':
            ApplicationDetail.elemHide()
            $('.app_type').show();
            $('.pick_datetime').show();
            $('.attachment').show();
            $('.refer_premium').show();
            break;
          case '4':
            ApplicationDetail.elemHide()
            $('.reason').show();
            break;
          case '5':
            ApplicationDetail.elemHide()
            $('.reason').show();
            $('.refer_regular').show();
          case '6':
            ApplicationDetail.elemHide()
            $('.notes').show();
            break;
          case '7':
            ApplicationDetail.elemHide()
            $('.app_type').show();
            $('.pick_datetime').show();
            $('.attachment').show();
            $('.notes').show();
            break;
          case '8':
            ApplicationDetail.elemHide()
            $('.hire_date').show();
            $('.hire_account').show();
            $('.notes').show();
            break;
          default:
            ApplicationDetail.elemHide()
          break;
        }
      },
      stages: function(){
        ApplicationDetail.elemHide()
        $('#stages').change(function(){
          var stage = $(this).val();
          ApplicationDetail.elemStageShow(stage);
        });
      },
      // setAppoiment: function(){
      //   $('#setAppoiment').click(function(e){
      //     var type = $('[name="my_hidden_type"]').val();
      //     var datetime = $('#my_hidden_input').val();
      //     var data = {
      //       'employer_appointment_sched' : datetime,
      //       'employer_appointment_type' : type
      //     }
      //     $.ajax({
      //       url : window.location.origin + '/employer/set-appointment',
      //       type : 'post',
      //       data : data,
      //       success : function(res){
      //
      //       },error : function(res){
      //
      //       }
      //     })
      //     e.preventDefault();
      //     e.stopImmediatePropagation();
      //   });
      // },
      appointmentType : function(){
        $('[name="employer_appointment_type"]').click(function(e){
          var txt = $(this).parent().find('span').text()
          var val = $(this).val()
          $('#appointment-type').text(txt.toUpperCase());
          $('[name="my_hidden_type"]').val(val);

        });
      },
      datePick : function(){
        $('#datetimepicker1').datetimepicker({
          inline: true,
          sideBySide: true
        });
        $('#datetimepicker1').on('dp.change', function(event) {
          //console.log(moment(event.date).format('MM/DD/YYYY h:mm a'));
          //console.log(event.date.format('MM/DD/YYYY h:mm a'));
          $('#selected-date').text(event.date);
          var formatted_date = event.date.format('MM/DD/YYYY h:mm a');
          var dt = moment(formatted_date, ["MM/DD/YYYY h:mm A"]).format("YYYY-MM-DD HH:mm:ss");
          $('#my_hidden_input').val(dt);
          $('#hidden-val').text(formatted_date);
        });
      },
      addComment : function(){
        $('#comment').keypress(function(e){
          var box = $(this);
          var comment =  box.val();
          var application_code = '{{ $application->application_code }}';
          if(e.which == 13 && comment !='' && comment.length > 0){
            $.ajax({
              url : window.location.origin + '/admin/applicant-comment',
              type : 'post',
              data : { 'comment': comment, 'application_code' : application_code},
              success : function(res){
                console.log(res);
                $('.media-list-feed').prepend($(ApplicationDetail.boxTemplate(res.activity)));
                swal('Sent!','Commented added in activities','success');
                box.val('');
              },error : function(res){
                console.log(res);
                swal('Opps!','Something went wrong','error');
              }
            });

          }
        });
      },
      boxTemplate: function(activity){
        return `<li class="media">
            <div class="media-object pull-left">
                ${activity.icon}
            </div>
            <div class="media-body">
                <p class="media-heading">${activity.title}</p>
                <p class="media-text">
                <span class="text-primary semibold">${activity.description}</span>
                by ${activity.created_by}</p>
                <p class="media-meta">${activity.created_at}</p>
            </div>
        </li>`
      },
      changeStatus: function(){
        $('.status').click(function(e){
          var option = $(this);
          var status = option.find('input').attr('data-status');
          var applicant_code = option.find('input').attr('data-applicantcode');
          console.log(applicant_code,status);
          $.ajax({
            url : window.location.origin + '/admin/update-applicant-status',
            type : 'post',
            data : {'applicant_code' : applicant_code,
                    'status' : status},
            success : function(res){
              if(res.success){
                if(option.hasClass('hasChoices')){
                  $('#statuses').children().remove();
                  // $('#statuses').append(ApplicationDetail.statusTemplate(applicant_code));
                  $('#statustext').text('Current Status:'+res.status);
                }
                if(option.hasClass('lastChoice')){
                  $('#statuses').children().remove();
                  $('#statustext').text('Current Status:'+res.status);
                }
                swal('Updated!','You have change the status','success');
              }
            },
            error : function(res){
              console.log(res);
              swal('Opps!','Something went wrong','error');
            }
          });
          e.stopImmediatePropagation();
          e.preventDefault();
        });
      },
      statusTemplate: function(applicant_code){
        return  `
          <label class="btn btn-primary status lastChoice">
            <input type="radio" name="options4" id="option4" autocomplete="off" data-applicantcode="${applicant_code}" data-status="4"> No Show
          </label>
          <label class="btn btn-primary status lastChoice">
            <input type="radio" name="options5" id="option5" autocomplete="off" data-applicantcode="${applicant_code}" data-status="5"> Rejected
          </label>
          <label class="btn btn-primary status lastChoice">
            <input type="radio" name="options6" id="option6" autocomplete="off" data-applicantcode="${applicant_code}" data-status="6"> Hired
          </label>
        `;
      }
  }
  $(function(){
    ApplicationDetail.init()

  });
  </script>
@stop
