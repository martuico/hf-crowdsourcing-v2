@extends('Dashboard::template.main')
@section('styles')
  <style>
    tbody > tr > td:last-child {
      text-align:center;
    }
  </style>
@stop
@section('content')
  <!-- Page Header -->
  <div class="page-header page-header-block">
      <div class="page-header-section">
          <h4 class="title semibold">Admin Dashboard</h4>
      </div>
      <div class="page-header-section">
          <!-- Toolbar -->
          <div class="toolbar clearfix">
              <div class="col-xs-8">
                  <select class="form-control text-left" id="selectize-customselect">
                      <option value="0">Display metrics...</option>
                      <option value="1">Last 6 month</option>
                      <option value="2">Last 3 month</option>
                      <option value="3">Last month</option>
                  </select>
              </div>
              <div class="col-xs-4">
                  <button class="btn btn-primary pull-right"><i class="ico-loop4 mr5"></i>Update</button>
              </div>
          </div>
          <!--/ Toolbar -->
      </div>
  </div>
  <!-- Page Header -->

  <div class="row">
      <!-- START Left Side -->
      <div class="col-md-9">
          <!-- Top Stats -->
          <div class="row">
              <div class="col-sm-3">
                  <!-- START Statistic Widget -->
                  <div class="table-layout animation delay animating fadeInDown">
                      <div class="col-xs-4 panel bgcolor-info text-center">
                          <div class="ico-users3 fsize24"></div>
                      </div>
                      <div class="col-xs-8 panel">
                          <div class="panel-body text-center">
                              <h4 class="semibold nm">{{ $postedjobscount }}</h4>
                              <p class="semibold text-muted mb0 mt5 ellipsis text-uppercase">Posted jobs</p>
                          </div>
                      </div>
                  </div>
                  <!--/ END Statistic Widget -->
              </div>
              <div class="col-sm-3">
                  <!-- START Statistic Widget -->
                  <div class="table-layout animation delay animating fadeInUp">
                      <div class="col-xs-4 panel bgcolor-warning text-center">
                          <div class="ico-warning-sign fsize24"></div>
                      </div>
                      <div class="col-xs-8 panel">
                          <div class="panel-body text-center">
                              <h4 class="semibold nm">{{ $pendingjobscount }}</h4>
                              <p class="semibold text-muted mb0 mt5 ellipsis text-uppercase">PENDING Jobs</p>
                          </div>
                      </div>
                  </div>
                  <!--/ END Statistic Widget -->
              </div>
              <div class="col-sm-3">
                  <!-- START Statistic Widget -->
                  <div class="table-layout animation delay animating fadeInDown">
                      <div class="col-xs-4 panel bgcolor-primary text-center">
                          <div class="ico-building fsize24"></div>
                      </div>
                      <div class="col-xs-8 panel">
                          <div class="panel-body text-center">
                              <h4 class="semibold nm">{{ $employers_count }}</h4>
                              <p class="semibold text-muted mb0 mt5 ellipsis">Employers</p>
                          </div>
                      </div>
                  </div>
                  <!--/ END Statistic Widget -->
              </div>
              <div class="col-sm-3">
                  <!-- START Statistic Widget -->
                  <div class="table-layout animation delay animating fadeInDown">
                      <div class="col-xs-4 panel bgcolor-danger text-center">
                          <div class="ico-profile fsize24"></div>
                      </div>
                      <div class="col-xs-8 panel">
                          <div class="panel-body text-center">
                              <h4 class="semibold nm">{{ $applicants_count }}</h4>
                              <p class="semibold text-muted mb0 mt5 ellipsis">Hired Applicants</p>
                          </div>
                      </div>
                  </div>
                  <!--/ END Statistic Widget -->
              </div>
          </div>
          <!--/ Top Stats -->
          <!-- Browser Breakpoint -->
          <div class="row">
              <div class="col-lg-12">
                  <!-- START panel -->
                  <div class="panel panel-default">
                      <!-- panel heading/header -->
                      <div class="panel-heading">
                          <h3 class="panel-title ellipsis"><i class="ico-blog2 mr5"></i>Latest 50 Pending Job Post</h3>
                          <!-- panel toolbar -->
                          <div class="panel-toolbar text-right">
                          </div>
                          <!--/ panel toolbar -->
                      </div>
                      <!--/ panel heading/header -->
                      <!-- panel body with collapse capabale -->
                      @if(count($pendingjobs)>0)
                      <div class="table-responsive panel-collapse pull out">
                          <table class="table">
                              <thead>
                                  <tr>
                                      <th>Job Title</th>
                                      <th>Job Location</th>
                                      <th>Posted Date</th>
                                      <th>Due Date</th>
                                      <th class="text-center">Status</th>
                                  </tr>
                              </thead>
                              <tbody>
                                @foreach($pendingjobs as $job)
                                  <tr>
                                      <td><a href="{{route('admin.job.detail',['slug' => $job->slug])}}" class="semibold text-accent">{{ $job->job_title }}</a></td>
                                      <td>{{ $job->work_location }}</td>
                                      <td>{{ $job->created_at }}</td>
                                      <td>{{ $job->valid_until }}</td>
                                      <td><span class="label label-warning">Pending</span></td>
                                  </tr>
                                @endforeach
                              </tbody>
                          </table>
                      </div>
                      <!--/ panel body with collapse capabale -->
                      @else
                      <div class="panel-body">
                        <div class="alert alert-info">
                          <strong>No Data!</strong>
                        </div>
                      </div>
                      @endif
                  </div>
                  <!--/ END panel -->
              </div>
          </div>
          <!-- Browser Breakpoint -->
          <!-- Browser Breakpoint -->
          <div class="row">
              <div class="col-lg-12">
                  <!-- START panel -->
                  <div class="panel panel-default">
                      <!-- panel heading/header -->
                      <div class="panel-heading">
                          <h3 class="panel-title ellipsis"><i class="ico-reading mr5"></i>Latest Unverfied Employer</h3>
                          <!-- panel toolbar -->
                          <div class="panel-toolbar text-right">
                          </div>
                          <!--/ panel toolbar -->
                      </div>
                      <!--/ panel heading/header -->
                      <!-- panel body with collapse capabale -->
                      @if(count($pending_employers)>0)
                      <div class="table-responsive panel-collapse pull out">
                          <table class="table">
                              <thead>
                                  <tr>
                                      <th></th>
                                      <th>Name</th>
                                      <th>Email</th>
                                      <th></th>
                                      <th>Company</th>
                                      <th>Verified</th>
                                      <th class="text-center"></th>
                                  </tr>
                              </thead>
                              <tbody>
                                  @foreach($pending_employers as $pe)
                                  <tr>
                                      <td>
                                        <div class="media-object">
                                          <img src="{{ $pe->profile_pic }}" alt="" class="img-circle">
                                        </div>
                                      </td>
                                      <td>{{ $pe->myname }}</td>
                                      <td>{{ $pe->email }}</td>
                                      <td>
                                        <div class="media-object">
                                          <img src="{{ (count($pe->employer)>0)?$pe->employer->logo_url :'' }}" alt="" class="img-circle">
                                        </div>
                                      </td>
                                      <td><a href="" class="semibold text-accent">{{ (count($pe->employer)>0)?$pe->employer->company_name:'No Company Profile Yet' }}</a></td>
                                      <td>
                                        <select name="" class="form-control" id="">
                                          <option value="">Yes</option>
                                          <option value="">No</option>
                                        </select>
                                      </td>
                                      <td><a href="{{ route('admin.employer.profile',['slug' => $pe->employer->slug]) }}" class="btn btn-xs btn-info"><i class="fa fa-eye"></i></a></td>
                                  </tr>
                                  @endforeach
                              </tbody>
                          </table>
                      </div>
                      <!--/ panel body with collapse capabale -->
                      @else
                      <div class="panel-body">
                        <div class="alert alert-info">
                          <strong>No Data!</strong>
                        </div>
                      </div>
                      @endif
                  </div>
                  <!--/ END panel -->
              </div>
          </div>
          <!-- Browser Breakpoint -->

          <!-- Browser Breakpoint -->
          <div class="row">
              <div class="col-lg-12">
                  <!-- START panel -->
                  <div class="panel panel-default">
                      <!-- panel heading/header -->
                      <div class="panel-heading">
                          <h3 class="panel-title ellipsis"><i class="ico-reading mr5"></i>Latest Hired  Applicants</h3>
                          <!-- panel toolbar -->
                          <div class="panel-toolbar text-right">
                          </div>
                          <!--/ panel toolbar -->
                      </div>
                      <!--/ panel heading/header -->
                      @if(count($hired_applicants)>0)
                      <!-- panel body with collapse capabale -->
                      <div class="table-responsive panel-collapse pull out">
                          <table class="table">
                              <thead>
                                  <tr>
                                      <th></th>
                                      <th>Name</th>
                                      <th>Date Applied</th>
                                      <th>Job Applied For</th>
                                      <th>Designation</th>
                                      <th>Status</th>
                                      <th class="text-center"></th>
                                  </tr>
                              </thead>
                              <tbody>
                                @foreach($hired_applicants as $hired)
                                  <tr>
                                      <td>
                                        <div class="media-object">
                                          <img src="{{ $hired->applicant->user->profile_pic }}" alt="" class="img-circle">
                                        </div>
                                      </td>
                                      <td>{{ $hired->applicant->user->my_name }}</td>
                                      <td>{{ $hired->created_at }}</td>
                                      <td><a href="" class="semibold text-accent">{{ $hired->job->job_title }}</a></td>
                                      <td>{{ $hired->job->designation }}</td>
                                      <td><span class="label label-success">{{ strtoupper($hired->applicant_status->name) }}</span></td>
                                      <td><a href="" class="btn btn-xs btn-info"><i class="fa fa-eye"></i></a></td>
                                  </tr>
                                @endforeach
                              </tbody>
                          </table>
                      </div>
                      <!--/ panel body with collapse capabale -->
                      @else
                      <div class="panel-body">
                        <div class="alert alert-info">
                          <strong>No Data!</strong>
                        </div>
                      </div>
                      @endif
                  </div>
                  <!--/ END panel -->
              </div>
          </div>
          <!-- Browser Breakpoint -->
      </div><!-- col-md-9 -->
      <!-- START Right Side -->
      <div class="col-md-3">
          <div class="panel panel-minimal">
              <div class="panel-heading"><h5 class="panel-title"><i class="ico-health mr5"></i>Latest Activity</h5></div>

              <!-- Media list feed -->
              <ul class="media-list media-list-feed nm">
                  <li class="media">
                      <div class="media-object pull-left">
                          <i class="ico-pencil bgcolor-success"></i>
                      </div>
                      <div class="media-body">
                          <p class="media-heading">EDIT EXISTING PAGE</p>
                          <p class="media-text"><span class="text-primary semibold">Service Page</span> has been edited by Tamara Moon.</p>
                          <p class="media-meta">Just Now</p>
                      </div>
                  </li>
                  <li class="media">
                      <div class="media-object pull-left">
                          <i class="ico-link"></i>
                      </div>
                      <div class="media-body">
                          <a href="javascript:void(0);" class="media-heading text-primary">View Actvities</a>
                      </div>
                  </li>
              </ul>
              <!--/ Media list feed -->
          </div>
      </div>
      <!--/ END Right Side -->
  </div><!-- row -->
@stop