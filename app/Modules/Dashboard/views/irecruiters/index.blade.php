@extends('Dashboard::template.main')

@section('content')
  <!-- Page Header -->
  <div class="page-header page-header-block">
      <div class="page-header-section">
          <h4 class="title semibold">HF Recruiter</h4>
      </div>
      <div class="page-header-section">
        <!-- Toolbar -->
        <div class="toolbar clearfix text-right">
            <a href="#" class="btn btn-sm btn-default" title="Create Job Post" data-toggle="modal" data-target="#createRecruiter"><i class="ico-file-plus"></i></a>
        </div>
        <!--/ Toolbar -->
      </div>
  </div>
  <!-- Page Header -->
  <div class="row">
    <div class="col-md-12">
      @if(\Session::has('successMessage'))
      <div class="row">
        <div class="col-md-12">
          <div class="alert alert-success">
            <strong>{{ \Session::get('successMessage') }}</strong>
          </div>
        </div>
      </div>
      @endif
      <!-- Browser Breakpoint -->
      <div class="row">
          <div class="col-lg-12">
              <!-- START panel -->
              <div class="panel panel-default">
                  <!-- panel heading/header -->
                  <div class="panel-heading">
                      <h3 class="panel-title ellipsis"><i class="ico-reading mr5"></i>Recuiters</h3>
                      <!-- panel toolbar -->
                      <div class="panel-toolbar text-right">
                      </div>
                      <!--/ panel toolbar -->
                  </div>
                  <!--/ panel heading/header -->
                  <!-- panel body with collapse capabale -->
                  @if(count($irecruiters)>0)
                  <div class="table-responsive panel-collapse pull out">
                      <table class="table">
                          <thead>
                              <tr>
                                  <th></th>
                                  <th>Name</th>
                                  <th>Email</th>
                                  <th>Status</th>
                                  <th>Total Task</th>
                                  <th>Completed Task</th>
                                  <th class="text-center"></th>
                              </tr>
                          </thead>
                          <tbody>
                              @foreach($irecruiters as $pe)
                              <tr>
                                  <td>
                                    <div class="media-object">
                                      <img src="{{ $pe->user->profile_pic }}" alt="" class="img-circle">
                                    </div>
                                  </td>
                                  <td>{{ $pe->user->myname }}</td>
                                  <td>{{ $pe->user->email }}</td>
                                  <td>
                                     {{ $pe->user->is_active?'Active':'Suspended' }}
                                  </td>
                                  <td>{{ $pe->task_on_hand }}</td>
                                  <td>{{ $pe->task_completed }}</td>
                                  <td>
                                    <a href="#" class="btn btn-xs btn-warning" data-toggle="modal" data-target="#editRecruiter" data-recruiter="{{ $pe->user }}"><i class="fa fa-edit"></i></a>
                                    <a href="" class="btn btn-xs btn-info"><i class="fa fa-eye"></i></a>
                                  </td>
                              </tr>
                              @endforeach
                          </tbody>
                      </table>
                  </div>
                  {{ $irecruiters->links() }}
                  <!--/ panel body with collapse capabale -->
                  @else
                  <div class="panel-body">
                    <div class="alert alert-info">
                      <strong>No Data!</strong>
                    </div>
                  </div>
                  @endif
              </div>
              <!--/ END panel -->
          </div>
      </div>
      <!-- Browser Breakpoint -->

    </div>
  </div>

  <!--EDIT Modal -->
  <div class="modal left fade" id="editRecruiter" tabindex="-1" role="dialog" aria-labelledby="createRecruiterLabel">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title" id="createRecruiterLabel">Edit Recruiter</h4>
        </div>
        {{ \Form::open(['route' => 'admin.recruiters.update','data-parsley-validate'=> '']) }}
        <div class="modal-body">
          <div id="editform">
            <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                  <label>First Name</label>
                  <input type="text" name="firstname" class="form-control" required="">
                  <input type="hidden" name="id">
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label>Last Name</label>
                  <input type="text" name="lastname" class="form-control" required="">
                </div>
              </div>
            </div>
            <div class="form-group">
              <label>Email</label>
              <input type="email" name="disemail" class="form-control" required="" disabled="">
              <input type="hidden" name="email" >
            </div>
            <div class="form-group">
              <label>Password</label>
              <input type="password" name="password" class="form-control">
            </div>
            <div class="form-group">
              <label for="">Status</label>
              <select name="is_active" id="" class="form-control">
                <option value="1">Active</option>
                <option value="0">Suspend</option>
              </select>
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-primary">Edit</button>
        </div>
        {{ \Form::close() }}
      </div>
    </div>
  </div>

  <!--ADD Modal -->
  <div class="modal left fade" id="createRecruiter" tabindex="-1" role="dialog" aria-labelledby="createRecruiterLabel">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title" id="createRecruiterLabel">Create Recruiter</h4>
        </div>
        {{ \Form::open(['route' => 'admin.recruiters.store','data-parsley-validate'=> '']) }}
        <div class="modal-body">
          <div id="form">
            <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                  <label>First Name</label>
                  <input type="text" name="firstname" class="form-control" required="">
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label>Last Name</label>
                  <input type="text" name="lastname" class="form-control" required="">
                </div>
              </div>
            </div>
            <div class="form-group">
              <label>Email</label>
              <input type="email" name="email" class="form-control" required="">
            </div>
            <div class="form-group">
              <label>Password</label>
              <input type="password" name="password" class="form-control" required="">
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-primary">Create</button>
        </div>
        {{ \Form::close() }}
      </div>
    </div>
  </div>
@stop


@section('scripts')
  <script>
  var InternalRecruiter = {
    init : function(){
      this.validateEmail();
      this.editRecruiter();
    },
    editRecruiter : function(){
      $('#editRecruiter').on('show.bs.modal',function(event){
        var button = $(event.relatedTarget) // Button that triggered the modal
        var recruiter = button.data('recruiter')
        var modal = $(this);
        modal.find('[name="firstname"]').val(recruiter.firstname)
        modal.find('[name="lastname"]').val(recruiter.lastname)
        modal.find('[name="email"]').val(recruiter.email)
        modal.find('[name="disemail"]').val(recruiter.email)
        modal.find('[name="id"]').val(recruiter.id)
        if(recruiter.is_active == 1){
           modal.find('[name="is_active"]').val(1)
        }
        if(recruiter.is_active == 0){
          modal.find('[name="is_active"]').val(0)
        }
        console.log(recruiter,recruiter.is_active)
      });
      $('#editRecruiter').on('hide.bs.modal', function(e){
        $(this).find('input').val('')
        $(this).find('option').removeAttr('selected','selected');
        $(this).find('[name="is_active"]').val('')
      })
    },
    validateEmail : function(){
      var email = $('[name="email"]').parsley();

      $('[name="email"]').blur(function(e){
        console.log(email,email._getErrorMessage);
        email.removeError('email');
        $.ajax({
          type: 'get',
          url : window.location.origin +'/admin/check-email',
          data : { email : $('[name="email"]').val() },
          success :function(res){
            email.removeError('email');
          },error : function(res){
            // response = $.parseJSON(res.responseText);
            email.addError('email',{message: 'Email already taken'});
            $('[name="email"]').val('');
            $('[name="email"]').focus();
          }
        })
        e.preventDefault();
        e.stopImmediatePropagation();
      });

    },
    store : function(){
    }
  }
  $(function(){
    InternalRecruiter.init();
  });
  </script>
@stop
