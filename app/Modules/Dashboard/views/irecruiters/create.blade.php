@extends('Dashboard::template.main')

@section('content')
  <!-- Page Header -->
  <div class="page-header page-header-block">
      <div class="page-header-section">
          <h4 class="title semibold">HF Recruiter</h4>
      </div>
      <div class="page-header-section">
        <!-- Toolbar -->
        <div class="toolbar clearfix text-right">
            <a href="{{route('admin.recruiters.create')}}" class="btn btn-sm btn-default"  data-toggle="tooltip" data-placement="bottom" title="Create Job Post"><i class="ico-file-plus"></i></a>
        </div>
        <!--/ Toolbar -->
      </div>
  </div>
  <!-- Page Header -->

@stop
