@extends('emailLayout.main')

@section('content')
  <body class="body" style="padding:0; margin:0; display:block; background:#ffffff; -webkit-text-size-adjust:none" bgcolor="#ffffff">
  <table align="center" cellpadding="0" cellspacing="0" width="100%" height="100%" >
    <tr>
      <td align="center" valign="top" bgcolor="#ffffff"  width="100%">

      <table cellspacing="0" cellpadding="0" width="100%">
        <tr>
          <td style="border-bottom: 3px solid #3bcdc3;" width="100%">
            <center>
              <table cellspacing="0" cellpadding="0" width="500" class="w320">
                <tr>
                  <td valign="top" style="padding:10px 0; text-align:left;" class="mobile-center">
                    <img width="250" height="62" src="http://hf.martuico.me/hfv1/image/logo/logo.png" style="width:100%;">
                  </td>
                </tr>
              </table>
            </center>
          </td>
        </tr>
        <tr>
          <td valign="top">
            <center>
              <table cellspacing="0" cellpadding="0" width="500" class="w320">
                <tr>
                  <td>

                    <table cellspacing="0" cellpadding="0" width="100%">
                      <tr>
                        <td class="mobile-padding" style="text-align:left;">
                        <br>
                        Good day! {{$inputs['employer']}},
                          <br><br>
                        We have an update on a candidate <b>{{ $inputs['applicant_name'] }}</b> for a <b>{{ $inputs['job_title'] }}</b> job position. <br>
                        Humanfactor Feedback: <strong>{{ $inputs['hf_status'] }}</strong> <br>
                        @if(array_key_exists('employer_appointment_type',$inputs) AND $inputs['employer_appointment_type'])
                        <p><strong>Appointments :</strong> {{ ucwords(str_replace('-',' ',$inputs['employer_appointment_type'])) }} | {{ $inputs['employer_appointment_sched'] }} </p>
                        @endif
                        @if(array_key_exists('endorsement_slip',$inputs) AND $inputs['endorsement_slip'])

                        <p><strong>Download Edorsement Slip :</strong> <i class="fa fa-file"></i> <a href="{{ url('/').'/epa/download-endorsement-slip/'.$inputs['application_code'] }}"> Download here</a> </p>
                        @endif
                        @if(array_key_exists('hire_date',$inputs) AND $inputs['hire_date'] && $inputs['hire_date'] !== '0000-00-00')
                        <p><strong>Hire Date :</strong> {{ $inputs['hire_date'] }} </p>
                        @endif
                        @if(array_key_exists('hire_account',$inputs) AND $inputs['hire_account'])
                        <p><strong>Account :</strong> {{ $inputs['hire_account'] }} </p>
                        @endif
                        @if(array_key_exists('reason',$inputs) AND $inputs['reason'])
                        <p><strong>Reason :</strong> {{ $inputs['reason'] }} </p>
                        @endif
                        @if(array_key_exists('note',$inputs) AND $inputs['note'])
                        <p><strong>Note :</strong> {{ $inputs['note'] }} </p>
                        @endif
                        <a href="{{ route('jobs.details',['job_title'=> $inputs['job_title_slug'],'job_code' => $inputs['job_code']]) }}">{{ $inputs['job_title'] }}</a>. You may check the application status on your <a href="{{ url('/').'/employer/careers/'.$inputs['job_code'].'/applicant/'.$inputs['application_code'] }}">dashboard.</a>
                          <br><br>
                        Good luck on your application!
                          <br><br>
                        Regards, <br>
                        Human Factor Support Team.
                        </td>
                      </tr>
                    </table>
                  </td>
                </tr>
                <tr>
                  <td>
                    <table cellspacing="0" cellpadding="25" width="100%">
                      <tr>
                        <td>
                          &nbsp;
                        </td>
                      </tr>
                    </table>
                  </td>
                </tr>
              </table>
            </center>
          </td>
        </tr>
        <tr>
          <td style="background-color:#c2c2c2;">

            <center>
              <table cellspacing="0" cellpadding="0" width="500" class="w320">
                <tr>
                  <td>
                    <table cellspacing="0" cellpadding="30" width="100%">
                      <tr>
                        <td style="text-align:center;">
                          <a href="#">
                            <img width="61" height="51" src="https://www.filepicker.io/api/file/vkoOlof0QX6YCDF9cCFV" alt="twitter" />
                          </a>
                          <a href="#">
                            <img width="61" height="51" src="https://www.filepicker.io/api/file/fZaNDx7cSPaE23OX2LbB" alt="google plus" />
                          </a>
                          <a href="#">
                            <img width="61" height="51" src="https://www.filepicker.io/api/file/b3iHzECrTvCPEAcpRKPp" alt="facebook" />
                          </a>
                        </td>
                      </tr>
                    </table>
                  </td>
                </tr>
                <tr>
                  <td>
                    <center>
                      <table style="margin:0 auto;" cellspacing="0" cellpadding="5" width="100%">
                        <tr>
                          <td style="text-align:center; margin:0 auto;" width="100%">
                             <a href="#" style="text-align:center;color:#fff;">
                               HUMANFACTOR.PH
                             </a>
                          </td>
                        </tr>
                      </table>
                    </center>
                  </td>
                </tr>
              </table>
            </center>

          </td>
        </tr>
      </table>

      </td>
    </tr>
  </table>
  </body>
@stop
