<li class="{{activeClass(\Route::currentRouteName(),'employer.home')}}">
    <a href="{{route('employer.home')}}">
        <span class="figure"><i class="ico-dashboard2"></i></span>
        <span class="text">Dashboard</span>
    </a>
</li>
<li class="{{ ( Request::is('employer/company-profile/*') OR Request::is('employer/company-profile') OR Request::is('employer/invite-recruiters/*') OR Request::is('employer/invite-recruiters')) ? 'open active':'' }}">
  <a href="javascript:void(0);" data-target="#myaccount" data-toggle="submenu" data-parent=".topmenu">
      <span class="figure"><i class="ico-profile"></i></span>
      <span class="text">My Account</span>
      <span class="arrow"></span>
  </a>
  <!-- START 2nd Level Menu -->
  <ul id="myaccount" class="submenu collapse {{ ( Request::is('employer/company-profile/*') OR Request::is('employer/company-profile') OR Request::is('employer/invite-recruiters/*') OR Request::is('employer/invite-recruiters') OR  Request::is('employer/billings/*') OR  Request::is('employer/billings') ) ? 'in':'' }}">
      <li class="submenu-header ellipsis">My Account</li>
      <li class="{{ ( Request::is('employer/company-profile/*') OR Request::is('employer/company-profile')) ? 'active':'' }}">
          <a href="{{route('employer.profile')}}">
              <span class="text">Company Profile</span>
          </a>
      </li>
      <?php /*
      @if(auth()->user()->employer AND auth()->user()->type->slug == 'employer' AND auth()->user()->employer->is_premium == 1)
      <li class="{{ ( Request::is('employer/invite-recruiters/*') OR Request::is('employer/invite-recruiters')) ? 'active':'' }}">
          <a href="{{route('employer.inviteRecruiters')}}">
              <span class="text">My Recruiters</span>
          </a>
      </li>
      @endif
      */
     ?>
      @if(auth()->user()->employer AND auth()->user()->type->slug == 'employer' AND auth()->user()->employer->is_premium == 1)
      <li class="{{activeClass(\Route::currentRouteName(),'employer.billings')}}">
          <a href="{{ route('employer.billings') }}">
              <span class="text">Billings</span>
          </a>
      </li>
      @endif
  </ul>
  <!--/ END 2nd Level Menu -->
</li>
<li  class="{{ ( Request::is('employer/applicants/*') OR Request::is('employer/applicants') OR Request::is('employer/appointments')) ? 'open active':'' }}">
  <a href="javascript:void(0);" data-target="#applications" data-toggle="submenu" data-parent=".topmenu">
      <span class="figure"><i class="ico-stack3"></i></span>
      <span class="text">Applications</span>
      <span class="arrow"></span>
  </a>
  <!-- START 2nd Level Menu -->
  <ul id="applications" class="submenu collapse {{ ( Request::is('employer/applicants/*') OR Request::is('employer/applicants') OR Request::is('employer/appointments')) ? 'in':'' }}">
      <li class="submenu-header ellipsis">Applications</li>
      <li class="{{activeClass(\Route::currentRouteName(),'employer.appointments')}}">
          <a href="{{route('employer.appointments')}}">
              <span class="text">Appointments</span>
          </a>
      </li>
      <li class="{{activeClass(\Route::currentRouteName(),'employer.applicants')}}">
          <a href="{{route('employer.applicants')}}">
              <span class="text">Applicants</span>
          </a>
      </li>
  </ul>
  <!--/ END 2nd Level Menu -->
</li>
<li  class="{{ (Request::is('employer/careers/*') OR Request::is('employer/careers')) ? 'open active':'' }}">
  <a href="javascript:void(0);" data-target="#jobpost" data-toggle="submenu" data-parent=".topmenu">
      <span class="figure"><i class="ico-bullhorn"></i></span>
      <span class="text">Job Post</span>
      <span class="arrow"></span>
  </a>
  <!-- START 2nd Level Menu -->
  <ul id="jobpost" class="submenu collapse {{ (Request::is('employer/careers/*') OR Request::is('employer/careers') ) ? 'in':'' }}">
      <li class="submenu-header ellipsis">Job Post</li>
      <li class="{{activeClass(\Route::currentRouteName(),'employer.job.create')}}">
          <a href="{{route('employer.job.create')}}">
              <span class="text">Post Job</span>
          </a>
      </li>
      <li class="{{activeClass(\Route::currentRouteName(),'employer.careers')}}">
          <a href="{{route('employer.careers')}}">
              <span class="text">Job Openings</span>
          </a>
      </li>
  </ul>
  <!--/ END 2nd Level Menu -->
</li>
<?php /*

@if((auth()->user()->type->name == 'Employer' AND count(auth()->user()->employer)<1) == false)
<li class="{{activeClass(\Route::currentRouteName(),'employer.applicants')}}">
    <a href="{{route('employer.applicants')}}">
        <span class="figure"><i class="ico-filter22"></i></span>
        <span class="text">Applicants</span>
    </a>
</li>

    @if(auth()->user()->type->slug == 'employer' AND auth()->user()->employer->is_premium == 1)
    <li class="{{activeClass(\Route::currentRouteName(),'employer.appointments')}}">
        <a href="{{route('employer.appointments')}}">
            <span class="figure"><i class="ico-calendar2"></i></span>
            <span class="text">My Appointments</span>
        </a>
    </li>
    @endif
    <li class="{{( Request::is('employer/careers/*') OR Request::is('employer/careers')) ? 'open active':''}}">
        <a href="{{route('employer.careers')}}">
            <span class="figure"><i class="ico-archive"></i></span>
            <span class="text">Careers</span>
        </a>
    </li>
    @if(auth()->user()->type->slug == 'employer' AND auth()->user()->employer->is_premium == 1)
    <li class="{{activeClass(\Route::currentRouteName(),'employer.billings')}}">
        <a href="{{route('employer.billings')}}" data-toggle="submenu" data-target="#form" data-parent=".topmenu">
            <span class="figure"><i class="ico-coins"></i></span>
            <span class="text">Billing</span>
        </a>
    </li>
    @endif
@endif
<li class="{{ ( Request::is('employer/company-profile/*') OR Request::is('employer/company-profile')) ? 'open active':'' }}">
    <a href="{{route('employer.profile')}}">
        <span class="figure"><i class="ico-library"></i></span>
        <span class="text">Company Profile</span>
    </a>
</li>

@if(auth()->user()->employer AND auth()->user()->type->slug == 'employer' AND auth()->user()->employer->is_premium == 1)
<li class="{{activeClass(\Route::currentRouteName(),'employer.inviteRecruiters')}}">
    <a href="{{route('employer.inviteRecruiters')}}" data-toggle="submenu" data-target="#form" data-parent=".topmenu">
        <span class="figure"><i class="ico-user-plus"></i></span>
        <span class="text">Recruiters Access</span>
    </a>
</li>
@endif
*/
?>
