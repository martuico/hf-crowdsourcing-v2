<li class="{{activeClass(\Route::currentRouteName(),'admin.home')}}">
    <a href="{{route('admin.home')}}">
        <span class="figure"><i class="ico-dashboard2"></i></span>
        <span class="text">Dashboard</span>
    </a>
</li>
<li class="{{( Request::is('admin/job/*') OR Request::is('admin/jobs') ) ? 'open active':''}}">
    <a href="{{route('admin.job.list')}}">
        <span class="figure"><i class="ico-archive"></i></span>
        <span class="text">Jobs</span>
    </a>
</li>
<li class="{{( Request::is('admin/employer/*') OR Request::is('admin/employer') OR Request::is('admin/employers') ) ? 'open active':''}}">
    <a href="{{route('admin.employers')}}">
        <span class="figure"><i class="ico-library"></i></span>
        <span class="text">Employers</span>
    </a>
</li>
<li class="{{activeClass(\Route::currentRouteName(),'admin.recruiters.index')}}">
    <a href="{{route('admin.recruiters.index')}}" data-toggle="submenu" data-target="#form" data-parent=".topmenu">
        <span class="figure"><i class="ico-leaf"></i></span>
        <span class="text">HF Recruiters</span>
    </a>
</li>

<li class="{{( Request::is('admin/applicants/*') OR Request::is('admin/applicants') ) ? 'open active':''}}">
    <a href="{{route('admin.applicants.index')}}" data-toggle="submenu" data-target="#form" data-parent=".topmenu">
        <span class="figure"><i class="ico-profile"></i></span>
        <span class="text">Applicants</span>
    </a>
</li>
