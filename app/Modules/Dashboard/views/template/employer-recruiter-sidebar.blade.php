<li class="{{activeClass(\Route::currentRouteName(),'employer-recruiter.home')}}">
    <a href="{{route('employer-recruiter.home')}}">
        <span class="figure"><i class="ico-dashboard2"></i></span>
        <span class="text">Dashboard</span>
    </a>
</li>
<li class="{{activeClass(\Route::currentRouteName(),'employer-recruiter.applicants')}}">
    <a href="{{route('employer-recruiter.applicants')}}">
        <span class="figure"><i class="ico-filter22"></i></span>
        <span class="text">Applicants</span>
    </a>
</li>
<li class="{{( Request::is('employer-recruiter/careers/*') OR Request::is('employer-recruiter/careers')) ? 'open active':''}}">
    <a href="{{route('employer-recruiter.careers')}}">
        <span class="figure"><i class="ico-archive"></i></span>
        <span class="text">Careers</span>
    </a>
</li>
