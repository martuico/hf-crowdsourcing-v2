<!DOCTYPE html>
<html class="backend">
    <!-- START Head -->
    <head>
        <!-- START META SECTION -->
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Human Factor - Crowdsourcing Recruitment Platform</title>
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <meta name="author" content="alphanum.ph">
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">

        <link rel="apple-touch-icon-precomposed" sizes="144x144" href="{{asset('hfv1/image/touch/apple-touch-icon-144x144-precomposed.png')}}">
        <link rel="apple-touch-icon-precomposed" sizes="114x114" href="{{asset('hfv1/image/touch/apple-touch-icon-114x114-precomposed.png')}}">
        <link rel="apple-touch-icon-precomposed" sizes="72x72" href="{{asset('hfv1/image/touch/apple-touch-icon-72x72-precomposed.png')}}">
        <link rel="apple-touch-icon-precomposed" href="{{asset('hfv1/image/touch/apple-touch-icon-57x57-precomposed.png')}}">
        <link rel="shortcut icon" href="{{asset('hfv1/image/favicon.ico')}}">
        <!--/ END META SECTION -->

        <!-- START STYLESHEETS -->
        <!-- Plugins stylesheet : optional -->
        <!--/ Plugins stylesheet : optional -->

        <!-- Application stylesheet : mandatory -->
        <link rel="stylesheet" href="{{asset('hfv1/stylesheet/bootstrap.css')}}">
        <link rel="stylesheet" href="{{asset('hfv1/stylesheet/layout.css')}}">
        <link rel="stylesheet" href="{{asset('hfv1/stylesheet/uielement.css')}}">
        <link rel="stylesheet" href="{{asset('hfv1/plugins/font-awesome/css/font-awesome.min.css')}}">
        <link rel="stylesheet" href="{{asset('hfv1/plugins/select2/css/select2.css')}}">
        <!--/ Application stylesheet -->

        <!-- Theme stylesheet : optional -->
        <link href="{{asset('hfv1/plugins/pace/flace-pace.css')}}" media="all" rel="stylesheet" type="text/css" />
        <link rel="stylesheet" href="{{asset('hfv1/stylesheet/themes/layouts/fixed-sidebar.css')}}">
        <link rel="stylesheet" href="{{asset('hfv1/stylesheet/themes/theme1.css')}}">
        <link rel="stylesheet" href="{{asset('hfv1/stylesheet/admincustom.css')}}">
        <!--/ Theme stylesheet : optional -->

        <!-- modernizr script -->
        <script type="text/javascript" src="{{asset('hfv1/plugins/modernizr/js/modernizr.js')}}"></script>
        <!--/ modernizr script -->
        <!-- END STYLESHEETS -->
        @yield('styles')
    </head>
    <!--/ END Head -->

    <!-- START Body -->
    <body>
        @include('Dashboard::template.header')

        <!-- START Template Sidebar (Left) -->
        <aside class="sidebar sidebar-left sidebar-menu">
            <!-- START Sidebar Content -->
            <section class="content slimscroll">
                <h5 class="heading">Main Menu</h5>
                <!-- START Template Navigation/Menu -->
                <ul class="topmenu topmenu-responsive" data-toggle="menu">

                    @include('Dashboard::template.'.auth()->user()->type->slug.'-sidebar')
                </ul>
                <!--/ END Template Navigation/Menu -->

                <!--/ END Sidebar summary -->
            </section>
            <!--/ END Sidebar Container -->
        </aside>
        <!--/ END Template Sidebar (Left) -->


        <!-- START Template Main -->
        <section id="main" role="main">
            <!-- START Template Container -->
            <div class="container-fluid">
                @yield('content')

            </div>
            <!--/ END Template Container -->

            <!-- START To Top Scroller -->
            <a href="#" class="totop animation" data-toggle="waypoints totop" data-showanim="bounceIn" data-hideanim="bounceOut" data-offset="50%"><i class="ico-angle-up"></i></a>
            <!--/ END To Top Scroller -->
        </section>
        <!--/ END Template Main -->

        <!-- START JAVASCRIPT SECTION (Load javascripts at bottom to reduce load time) -->
        <!-- Application and vendor script : mandatory -->
        <script type="text/javascript" src="{{asset('hfv1/javascript/vendor.js')}}"></script>
        <script type="text/javascript" src="{{asset('hfv1/javascript/core.js')}}"></script>
        <script type="text/javascript"    src="{{asset('hfv1/javascript/backend/app.js')}}"></script>
        <script src="{{asset('hfv1/plugins/select2/js/select2.min.js')}}"></script>
        <script>
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $(document).ajaxComplete(function(event, request, settings) {
                setTimeout(function(){
                    $.get(window.location.origin + '/epa/zz_session')
                      .success(function(res){
                        $('meta[name="csrf-token"]').attr('content',res);
                    });
                },3000);
            })

        </script>
        <!--/ Application and vendor script : mandatory -->

        <!-- Plugins and page level script : optional -->
        @yield('scripts')
        <script type="text/javascript" src="{{asset('hfv1/plugins/parsley/js/parsley.js')}}"></script>
        <script src="{{asset('hfv1/plugins/pace/pace.min.js')}}"></script>

        <!--/ Plugins and page level script : optional -->
        <!--/ END JAVASCRIPT SECTION -->
    </body>
    <!--/ END Body -->
</html>
