<!-- START Template Header -->
<header id="header" class="navbar">
    <!-- START navbar header -->
    <div class="navbar-header">
        <!-- Brand -->
        <a class="navbar-brand" href="javascript:void(0);">
            <span class="logo-figure"></span>
            <span class="logo-text" style="margin-left: 0px;"></span>
        </a>
        <!--/ Brand -->
    </div>
    <!--/ END navbar header -->

    <!-- START Toolbar -->
    <div class="navbar-toolbar clearfix">
        <!-- START Left nav -->
        <ul class="nav navbar-nav navbar-left">
            <!-- Sidebar shrink -->
            <li class="hidden-xs hidden-sm">
                <a href="javascript:void(0);" class="sidebar-minimize" data-toggle="minimize" title="Minimize sidebar">
                    <span class="meta">
                        <span class="icon"></span>
                    </span>
                </a>
            </li>
            <!--/ Sidebar shrink -->

            <!-- Offcanvas left: This menu will take position at the top of template header (mobile only). Make sure that only #header have the `position: relative`, or it may cause unwanted behavior -->
            <li class="navbar-main hidden-lg hidden-md hidden-sm">
                <a href="javascript:void(0);" data-toggle="sidebar" data-direction="ltr" rel="tooltip" title="Menu sidebar">
                    <span class="meta">
                        <span class="icon"><i class="ico-paragraph-justify3"></i></span>
                    </span>
                </a>
            </li>
            <!--/ Offcanvas left -->

            <?php
            /*
            <!-- Notification dropdown -->
            <li class="dropdown custom" id="header-dd-notification">
                <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown">
                    <span class="meta">
                        <span class="icon"><i class="ico-bell"></i></span>
                        <span class="hasnotification hasnotification-danger"></span>
                    </span>
                </a>

                <!-- Dropdown menu -->
                <div class="dropdown-menu" role="menu">
                    <div class="dropdown-header">
                        <span class="title">Notification <span class="count"></span></span>
                        <span class="option text-right"><a href="javascript:void(0);">Clear all</a></span>
                    </div>
                    <div class="dropdown-body slimscroll">
                        <!-- indicator -->
                        <div class="indicator inline"><span class="spinner spinner16"></span></div>
                        <!--/ indicator -->

                        <!-- Message list -->
                        <div class="media-list">
                            <a href="javascript:void(0);" class="media read border-dotted">
                                <span class="media-object pull-left">
                                    <i class="ico-basket2 bgcolor-info"></i>
                                </span>
                                <span class="media-body">
                                    <span class="media-text">Duis aute irure dolor in <span class="text-primary semibold">reprehenderit</span> in voluptate.</span>
                                    <!-- meta icon -->
                                    <span class="media-meta pull-right">2d</span>
                                    <!--/ meta icon -->
                                </span>
                            </a>

                            <a href="javascript:void(0);" class="media read border-dotted">
                                <span class="media-object pull-left">
                                    <i class="ico-call-incoming"></i>
                                </span>
                                <span class="media-body">
                                    <span class="media-text">Aliquip ex ea commodo consequat.</span>
                                    <!-- meta icon -->
                                    <span class="media-meta pull-right">1w</span>
                                    <!--/ meta icon -->
                                </span>
                            </a>

                            <a href="javascript:void(0);" class="media read border-dotted">
                                <span class="media-object pull-left">
                                    <i class="ico-alarm2"></i>
                                </span>
                                <span class="media-body">
                                    <span class="media-text">Excepteur sint <span class="text-primary semibold">occaecat</span> cupidatat non.</span>
                                    <!-- meta icon -->
                                    <span class="media-meta pull-right">12w</span>
                                    <!--/ meta icon -->
                                </span>
                            </a>

                            <a href="javascript:void(0);" class="media read border-dotted">
                                <span class="media-object pull-left">
                                    <i class="ico-checkmark3 bgcolor-success"></i>
                                </span>
                                <span class="media-body">
                                    <span class="media-text">Lorem ipsum dolor sit amet, <span class="text-primary semibold">consectetur</span> adipisicing elit.</span>
                                    <!-- meta icon -->
                                    <span class="media-meta pull-right">14w</span>
                                    <!--/ meta icon -->
                                </span>
                            </a>
                        </div>
                        <!--/ Message list -->
                    </div>
                </div>
                <!--/ Dropdown menu -->
            </li>
            <!--/ Notification dropdown -->
            */
           ?>
        </ul>
        <!--/ END Left nav -->

        <!-- START Right nav -->
        <ul class="nav navbar-nav navbar-right">
            <!-- Profile dropdown -->
            <li class="dropdown profile">
                <a href="javascript:void(0);" class="dropdown-toggle dropdown-hover" data-toggle="dropdown">
                    <span class="meta">
                        <span class="avatar"><img src="{{auth()->user()->profile_pic}}" class="img-circle" alt="" /></span>
                        <span class="text hidden-xs hidden-sm pl5">{{auth()->user()->myname}}</span>
                        <span class="caret"></span>
                    </span>
                </a>
                <ul class="dropdown-menu" role="menu">
                    <li><a href="javascript:void(0);"><span class="icon"><i class="ico-question"></i></span> Help</a></li>
                    <li class="divider"></li>
                    <li><a href="{{route('do.logout')}}"><span class="icon"><i class="ico-exit"></i></span> Sign Out</a></li>
                </ul>
            </li>
            <!-- Profile dropdown -->
        </ul>
        <!--/ END Right nav -->
    </div>
    <!--/ END Toolbar -->
</header>
<!--/ END Template Header -->
