<li class="{{activeClass(\Route::currentRouteName(),'internal-recruiter.home')}}">
    <a href="{{route('internal-recruiter.home')}}">
        <span class="figure"><i class="ico-dashboard2"></i></span>
        <span class="text">Dashboard</span>
    </a>
</li>

<?php /*
<li class="{{( Request::is('internal-recruiter/employer/*') OR Request::is('internal-recruiter/employer') OR Request::is('internal-recruiter/employers') ) ? 'open active':''}}">
    <a href="{{route('internal-recruiter.employers')}}">
        <span class="figure"><i class="ico-library"></i></span>
        <span class="text">Employers</span>
    </a>
</li>
*/
?>
<li class="{{activeClass(\Route::currentRouteName(),'internal-recruiter.settings')}}">
    <a href="{{route('internal-recruiter.settings')}}">
        <span class="figure"><i class="ico-profile"></i></span>
        <span class="text">Account Settings</span>
    </a>
</li>

<li class="{{ ( Request::is('employer/company-profile/*') OR Request::is('employer/company-profile') OR Request::is('employer/invite-recruiters/*') OR Request::is('employer/invite-recruiters')) ? 'open active':'' }}">
  <a href="javascript:void(0);" data-target="#myaccount" data-toggle="submenu" data-parent=".topmenu">
      <span class="figure"><i class="ico-list"></i></span>
      <span class="text">Applicant Pipeline</span>
      <span class="arrow"></span>
  </a>
  <!-- START 2nd Level Menu -->
  <ul id="myaccount" class="submenu collapse {{ ( Request::is('internal-recruiter/jobs/*') OR Request::is('internal-recruiter/jobs') OR Request::is('internal-recruiter/applicants/*') OR Request::is('internal-recruiter/applicants') ) ? 'in':'' }}">
      <li class="submenu-header ellipsis">Applicant Pipeline</li>
      <li class="{{( Request::is('internal-recruiter/jobs/*') OR Request::is('internal-recruiter/jobs') ) ? 'active':''}}">
          <a href="{{route('internal-recruiter.jobs')}}">
              <span class="text">Jobs</span>
          </a>
      </li>
      <li class="{{ ( Request::is('internal-recruiter/applicants/*') OR Request::is('internal-recruiter/applicants')) ? 'active':'' }}">
          <a href="{{route('internal-recruiter.applicants')}}">
              <span class="text">Applicants</span>
          </a>
      </li>
  </ul>
</li>
