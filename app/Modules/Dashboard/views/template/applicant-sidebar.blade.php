<li class="{{activeClass(\Route::currentRouteName(),'applicant.home')}}">
    <a href="{{route('applicant.home')}}">
        <span class="figure"><i class="ico-dashboard2"></i></span>
        <span class="text">Dashboard</span>
    </a>
</li>
<li class="{{activeClass(\Route::currentRouteName(),'applicant.job.list')}}">
    <a href="{{route('jobs')}}" target="_blank">
        <span class="figure"><i class="ico-search"></i></span>
        <span class="text">Browse Jobs</span>
</li>
<li class="{{( Request::is('applicant/applied-jobs/*') OR Request::is('applicant/applied-jobs') ) ? 'open active':''}}">
    <a href="{{route('applicant.job.applied')}}">
        <span class="figure"><i class="ico-calendar2"></i></span>
        <span class="text">Applied Jobs</span>
    </a>
</li>
<li class="{{( Request::is('applicant/resume-profile-create') OR Request::is('applicant/resume-profile/*') OR Request::is('applicant/resume-profile') ) ? 'open active':''}}">
    <a href="{{route('applicant.resume.view')}}">
        <span class="figure"><i class="ico-profile"></i></span>
        <span class="text">Resume Profile</span>
    </a>
</li>
<li class="{{( Request::is('applicant/account-settings') OR Request::is('applicant/account-settings/*') OR Request::is('applicant/account-settings') ) ? 'open active':''}}">
    <a href="{{route('applicant.settings')}}">
        <span class="figure"><i class="ico-cog"></i></span>
        <span class="text">Account Settings</span>
    </a>
</li>
<?php /*
<li class="">
    <a href="{{route('applicant.resume.view')}}">
        <span class="figure"><i class="ico-trophy22"></i></span>
        <span class="text">Hiring Rewards</span>
    </a>
</li>

*/
?>
