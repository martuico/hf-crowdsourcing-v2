<li class="{{activeClass(\Route::currentRouteName(),'recruiter.home')}}">
    <a href="{{route('recruiter.home')}}">
        <span class="figure"><i class="ico-dashboard2"></i></span>
        <span class="text">Dashboard</span>
    </a>
</li>
<li class="{{( Request::is('independent-recruiter/referrals/*') OR Request::is('independent-recruiter/referrals')) ? 'open active':''}}">
    <a href="{{route('recruiter.referrals')}}">
        <span class="figure"><i class="ico-filter22"></i></span>
        <span class="text">Referrals</span>
    </a>
</li>
<li class="{{( Request::is('independent-recruiter/careers/*') OR Request::is('independent-recruiter/careers')) ? 'open active':''}}">
    <a href="{{route('recruiter.careers')}}">
        <span class="figure"><i class="ico-archive"></i></span>
        <span class="text">Careers</span>
    </a>
</li>
<li class="{{( Request::is('independent-recruiter/rewards/*') OR Request::is('independent-recruiter/rewards')) ? 'open active':''}}">
    <a href="{{route('recruiter.rewards')}}">
        <span class="figure"><i class="ico-dollar"></i></span>
        <span class="text">Rewards</span>
    </a>
</li>
