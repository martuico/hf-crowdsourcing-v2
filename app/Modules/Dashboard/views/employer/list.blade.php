@extends('Dashboard::template.main')

@section('content')
  <!-- Page Header -->
  <div class="page-header page-header-block">
      <div class="page-header-section">
          <h4 class="title semibold">Employers List</h4>
      </div>
      <div class="page-header-section">
          <!-- Toolbar -->
          <div class="toolbar clearfix">
              <!--
              <div class="col-xs-8">
                  <select class="form-control text-left" id="selectize-customselect">
                      <option value="0">Display metrics...</option>
                      <option value="1">Last 6 month</option>
                      <option value="2">Last 3 month</option>
                      <option value="3">Last month</option>
                  </select>
              </div>
              <div class="col-xs-4">
                  <button class="btn btn-primary pull-right"><i class="ico-loop4 mr5"></i>Update</button>
              </div>
              -->
          </div>
          <!--/ Toolbar -->
      </div>
  </div>
  <!-- Page Header -->

  <!-- Browser Breakpoint -->
  <div class="row">
      <div class="col-lg-12">
          <!-- START panel -->
          <div class="panel panel-default">
              <!-- panel heading/header -->
              <div class="panel-heading">
                  <h3 class="panel-title ellipsis"><i class="ico-reading mr5"></i>List of Employers</h3>
                  <!-- panel toolbar -->
                  <div class="panel-toolbar text-right">
                  </div>
                  <!--/ panel toolbar -->
              </div>
              <!--/ panel heading/header -->
              <!-- panel body with collapse capabale -->
              @if(count($employers)>0)
              <div class="table-responsive panel-collapse pull out">
                  <table class="table">
                      <thead>
                          <tr>
                              <th></th>
                              <th>Contact Person</th>
                              <th>Email</th>
                              <th></th>
                              <th>Company</th>
                              <th width="10%">Premium</th>
                              <th width="10%">Verified</th>
                              <th>Job Post</th>
                              <th class="text-center"></th>
                          </tr>
                      </thead>
                      <tbody>
                          @foreach($employers as $employer)
                          <tr>
                              <td>
                                <div class="media-object">
                                  <img src="{{ $employer->user->profile_pic }}" alt="" class="img-circle">
                                </div>
                              </td>
                              <td>{{ $employer->user->myname }}</td>
                              <td>{{ $employer->user->email }}</td>
                              <td>
                                <div class="media-object">
                                  <img src="{{ (count($employer->logo_url)>0)?$employer->logo_url :'' }}" alt="" class="img-circle">
                                </div>
                              </td>
                              <td><a href="" class="semibold text-accent">{{ (count($employer->company_name)>0)?$employer->company_name:'No Company Profile Yet' }}</a><br>
                                @if($employer->is_premium)
                                <span class="text-warning"><i class="fa fa-shield"></i> Premium</span>
                                @endif
                              </td>
                              <td>
                                <select name="" class="form-control updateStatus" data-col="is_premium" data-employerid="{{$employer->id}}">
                                  <option value="">-- Select Options --</option>
                                  <option value="1" {{ ($employer->is_premium == 1)?'selected="selected"':''  }}>Yes</option>
                                  <option value="0" {{ ($employer->is_premium == 0)?'selected="selected"':''  }}>No</option>
                                </select>
                              </td>
                              <td>
                                <select name="" class="form-control updateStatus" data-col="is_verified" data-employerid="{{$employer->id}}">
                                  <option value="">-- Select Options --</option>
                                  <option value="1" {{ ($employer->is_verified == 1)?'selected="selected"':''  }}>Yes</option>
                                  <option value="0" {{ ($employer->is_verified == 0)?'selected="selected"':''  }}>No</option>
                                </select>
                              </td>
                              <td><span class="label label-inverse">{{ $employer->jobs->count() }}</span></td>
                              <td>
                                <a href="{{ route('admin.employer.profile',['slug' => $employer->slug]) }}" class="btn btn-xs btn-info"><i class="fa fa-eye"></i></a>
                                <a href="{{ route('admin.employer.profile.edit',['slug' => $employer->slug]) }}" class="btn btn-xs btn-warning"><i class="fa fa-edit"></i></a>
                              </td>
                          </tr>
                          @endforeach
                      </tbody>
                  </table>
              </div>
              <!--/ panel body with collapse capabale -->
              {{ $employers->links() }}
              @else
              <div class="panel-body">
                <div class="alert alert-info">
                  <strong>No Data!</strong>
                </div>
              </div>
              @endif
          </div>
          <!--/ END panel -->
      </div>
  </div>
  <!-- Browser Breakpoint -->

@stop
@section('styles')
  <link rel="stylesheet" href="{{ asset('hfv1/plugins/sweetalert/dist/sweetalert.css') }}">
@stop
@section('scripts')
  <script src="{{ asset('hfv1/plugins/sweetalert/dist/sweetalert.min.js') }}"></script>
  <script>
    var Employer = {
      init: function(){
        this.update($('.updateStatus'));
      },
      update: function(elem){
        elem.change(function(e){
          var btn = $(this);
          var col = btn.data('col');
          var data = { 'id': btn.data('employerid'),
                       'status': parseInt(btn.val()),
                       'col': col };
          $.ajax({
            url : window.location.origin + '/admin/employer-status',
            type : 'post',
            data : data,
            success : function(result){
              if(result.success){
                swal("Good job!", "You updated the status!", "success")
              }
            },
            error : function(result){
              sweetAlert("Oops...", "Something went wrong!", "error");
            }
          });

          e.preventDefault();
          e.stopImmediatePropagation();
        });
      }
    }

    $(function(){
      Employer.init();
    });
  </script>
@stop
