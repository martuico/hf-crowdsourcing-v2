<?php


Route::group(['module' => 'Dashboard',
              'namespace' => 'App\Modules\Dashboard\Controllers',
              'middleware' => ['web','auth','AdminOnly'],
              'prefix' => 'admin'
          ],
            function() {

    if (App::environment('local')) {
    // The environment is local
    DB::enableQueryLog();
    }
    // Route::get('test',function(){
    //   return \App\IRecruiter::orderBy('task_on_hand','asc')->first();
    // });
    Route::post('application/{applicant}/stages',
              ['as' => 'admin.application.stages',
              'uses' =>'ApplicantController@updateStages']);
    Route::post('applicant-comment',['uses' => 'ApplicantController@applicantActivityComment']);
    Route::post('update-applicant-status',['uses' => 'ApplicantController@updateApplicantStatus']);

    Route::post('applicant/{applicant}/edit',
                  ['as' => 'admin.applicant.edit',
                  'uses' => 'ApplicantController@update']);

    Route::get('applicant/{applicant}/edit',
                  ['as' => 'admin.applicant.edit',
                  'uses' => 'ApplicantController@edit']);

    Route::get('careers/{job_code}/applicant/{applicant}/appointments',
                    ['as' => 'admin.careers.applicant.appointments',
                    'uses' => 'ApplicantController@applicantAppoiments']);

    Route::get('careers/{job_code}/applicant/{applicant}',
                ['as' => 'admin.careers.applicant.details',
                'uses' => 'ApplicantController@applicantJob']);

    Route::get('fetch-applicantion/{code}',[
                'uses' => 'ApplicantController@fetchApplication'
              ]);
    Route::get('applicants/{applicant}/jobs',
                ['as' => 'admin.careers.applicant.jobs',
                'uses' => 'ApplicantController@showApplicantJobs']);
    Route::get('applicants/{applicant}',
                ['as' => 'admin.careers.applicant.details-v2',
                'uses' => 'ApplicantController@show']);

    Route::get('applicants',['as' => 'admin.applicants.index','uses' => 'ApplicantController@index']);
    Route::get('fetch-cities/{provcode}',['uses' => 'DashController@fetchCities']);
    Route::get('fetch-province/{regcode}',['uses' => 'DashController@fetchProvince']);

    Route::post('recruiters/store',['as' => 'admin.recruiters.store','uses' => 'InternalRecruiterController@store']);
    // Route::post('recruiters/update',function(){
    //     return 'fkc';
    // });
    Route::post('recruiters/update',['as' => 'admin.recruiters.update','uses' => 'InternalRecruiterController@update']);

    Route::get('recruiters/create',['as' => 'admin.recruiters.create','uses' => 'InternalRecruiterController@create']);
    Route::get('recruiters',['as' => 'admin.recruiters.index','uses' => 'InternalRecruiterController@index']);
    Route::get('job/{title}',['as' => 'admin.job.detail','uses' => 'DashController@showJob']);
    Route::post('job/{title}/{job_code}',['as' => 'admin.job.update','uses' => 'DashController@updateJob']);
    Route::get('jobs',['as' => 'admin.job.list', 'uses' => 'DashController@listJob']);
    Route::post('employer-status',['as' => 'admin.employer.status.update',
                                  'uses' => 'DashController@updateEmployerStatus']);
    Route::get('employer/{company}/edit',['as' => 'admin.employer.profile.edit','uses' => 'DashController@editEmployer']);
    Route::post('employer/{company}/edit',['as' => 'employer.update.profile','uses' => 'DashController@updateEmployer']);
    Route::get('employer/{company}',['as' => 'admin.employer.profile','uses' => 'DashController@showEmployer']);
    Route::get('employers',['as' => 'admin.employers','uses' => 'DashController@listEmployers']);
    Route::get('check-email',function(){
      $user = \App\User::whereEmail(\Request::get('email'))->first();
      if($user){
          return response()->json([],404);
      }
      return response()->json([],200);
    });
    Route::get('/',['as' => 'admin.home','uses' => 'DashController@index']);

});


Route::group(['module' => 'Dashboard',
              'namespace' => 'App\Modules\Dashboard\Controllers',
              'middleware' => ['api'],
              'prefix' => 'epa'
          ],
            function() {

    Route::get('zz_session',function(){
        return csrf_token();
    });
    Route::get('check-email',function(){
        $email = \Request::get('email');
        $user = \App\User::where('email','=',$email)->first();
        return response()->json(['hasduplicate' => ($user)?1:0],200);
    });
    Route::get('download-endorsement-slip/{application_code}',['as' => 'download.endorsemmentslip','uses' => 'ApplicantController@downloadEndorsementSlip']);
    Route::get('download-resume/{code}',['as' => 'download.resume','uses' =>'ApplicantController@downloadResume']);
    Route::get('fetch-cities/{provcode}',['as' => 'api.fetch.cities','uses' => 'ApplicantController@fetchCities']);
    Route::get('fetch-province/{regcode}',['as' => 'api.fetch.province','uses' => 'ApplicantController@fetchProvince']);

});
