@extends('Dashboard::template.main')

@section('content')
  <!-- Page Header -->
  <div class="page-header page-header-block">
      <div class="page-header-section">
          <h4 class="title semibold">Job List</h4>
      </div>
      <div class="page-header-section">
          <!-- Toolbar -->
          <div class="toolbar clearfix">
              <div class="input-group">
                <input type="text" class="form-control" placeholder="Search for Jobs">
                <span class="input-group-btn">
                  <button class="btn btn-default" type="button"><i class="ico-search"></i></button>
                </span>
              </div><!-- /input-group -->
          </div>
          <!--/ Toolbar -->
      </div>
  </div>
  <!-- Page Header -->

  <!-- Browser Breakpoint -->
  <div class="row">
      <div class="col-lg-9">
        @if(count($jobs)>0)
          <div class="list-group joblist">
            @foreach($jobs as $job)
            @php
              $arr = array_except($job->toArray(),['id','created_at','status','updated_at','deleted_at','referral_reward','finders_fee','order_level','valid_until','job_type_id','job_industry_id','work_shift_id','education_level_id','job_level_id','employer_id','work_shift']);
              $arr['job_type'] = $job->type->name;
              $arr['job_industry'] = $job->job_industry->name;
              $arr['work_shift'] = $job->shift->name;
              $arr['education_level'] = $job->education_level->name;
              $arr['job_level'] = $job->job_level->name;
              $arr['employer'] = array_only($job->employer->toArray(),['company_name','logo','slug','description','is_premium','is_verified']);
              $arr = json_encode($arr);
            @endphp
            <a href="#"
                class="list-group-item job-detail"
                data-code="{{ $job->job_code}}" data-job="{{ $arr }}">
              <div class="media col-md-2">
                  <figure class="pull-left">
                      <img class="media-object img-rounded img-responsive"  src="{{ $job->employer->logo_url}}" alt="{{ $job->job_title }}" >
                  </figure>
              </div>
              <div class="col-md-6" style="padding:0px;">
                  <h4 class="list-group-item-heading"> {{ $job->job_title }} </h4>
                  <p class="list-group-item-text">
                    <i class="ico-building"></i> {{ $job->employer->company_name }}

                    @if($job->employer->is_premium)
                    <span class="label label-success" ><i class="fa fa-shield"></i> Premium Employer</span>
                    @endif
                    @if($job->employer->is_verified)
                      <span class="label label-info"><i class="ico-check"></i> Verified</span>
                    @else
                      <span class="label label-info"><i  class="ico-checkbox-partial"></i> Just Launched</span>
                    @endif
                  </p>
                  <p class="list-group-item-text"><i class="ico-map-marker"></i> {{ $job->region->regDesc.', '.$job->province->provDesc.', '.$job->city->citymunDesc }}</p>
              </div>
              <div class="col-md-4 text-center mt20" style="padding:0px;">
                  <div class="btn-group" role="group" aria-label="Apply and get rewarded">
                    @if(auth()->user()->resume AND $job->appliedjobs($job->id)->first())
                      <button type="button" class="appliedbtn btn btn-sm btn-info btn-smallpad" data-hasApplied="true"  data-toggle="tooltip" data-placement="bottom" title="Good Luck!"><i class="ico-checkmark-circle"></i> Referred</button>
                    @else
                      @if($job->employer->is_premium)
                    <button type="button" class="rewardbtn btn btn-sm btn-success btn-smallpad"  data-toggle="tooltip" data-placement="bottom" title="Get Rewarded When you Get Hired">{{ $job->applicant_reward }}</button>
                    <button type="button" class="rewardbtn btn btn-sm btn-default btn-smallpad">Refer</button>
                      @else
                        <button type="button" class="rewardbtn btn btn-sm btn-default btn-smallpad">Refer</button>
                      @endif
                    @endif
                  </div>
                  <p> {{ $job->created_at }}</p>
              </div>
            </a>
            @endforeach
          </div>
            <hr>
          {{ $jobs->links() }}
        <!--/ panel body with collapse capabale -->
        @else
          <div class="alert alert-info">
            <strong>No Data!</strong>
          </div>
        @endif
      </div>
  </div>
  <!-- Browser Breakpoint -->

  <!-- Modal -->
    <div class="modal left fade" id="jobPost" tabindex="-1" role="dialog" aria-labelledby="jobPostLabel">
      <div class="modal-dialog" role="document">
        <div class="modal-content">

          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title">Preview Job Posting</h4>
          </div>

          <div class="modal-body">
                <div class="row">
                  <div class="col-md-2">
                    <img src="" alt="" id="logo" style="width: 100%">
                  </div>
                  <div class="col-md-8">
                    <a href="javascript:void(0);">
                      <h2><span class="company_name"></span> <br>
                          <span class="label label-success" id="is_premium"><i class="fa fa-shield"></i> Premium Employer</span>
                          <span class="label label-info" id="is_verified"><i class="ico-check"></i> Verified</span>
                      </h2>
                    </a>
                  </div>
                </div>
                <h4>About the company</h4>
                <div id="company_description"></div>

                <div class="page-header mb5 pb5">
                  <div class="page-title">
                    <h2><span class="title"></span> <span class="label label-inverse" id="applicant_reward"></span></h2>
                    <span><i class="ico-map-marker"></i> <span id="work_location"></span></span><br>
                    <strong><span id="job_industry"></span></strong>
                  </div>
                </div>
                <ul class="list-inline">
                  <li><span class="label label-default">
                      <span id="job_type"></span></span>
                  </li>
                  <li><span class="label label-default">
                      <span id="job_level"></span></span>
                  </li>
                  <li><span class="label label-default">
                          <span id="education_level"></span>
                      </span>
                  </li>
                </ul>
                <div class="desc"></div>
                <hr>
                <h4>Refer Candidate</h4>
                <div class="row">
                  <div class="col-md-6">
                    <div class="form-group">
                      <label>First Name</label>
                      <input type="text" class="form-control" name="firstname" placeholder="First Name">
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                      <label>Last Name</label>
                      <input type="text" class="form-control" name="lastname" placeholder="Last Name">
                    </div>
                  </div>
                </div>
                <input type="hidden" name="job_code">
                <div class="form-group">
                  <label>Email</label>
                  <input type="email" class="form-control" name="email" placeholder="mail@domain.com">
                </div>
                <div class="form-group">
                  <label>Contact Number</label>
                  <input type="text" class="form-control" data-mask="(9999) 999-9999" name="contact_number" placeholder="0917-1111888">
                </div>
                <div class="form-group text-right">
                  <button type="button" class="btn btn-sm btn-primary" id="referCandidate">Submit</button>
                </div>
          </div><!-- modal-body -->

        </div><!-- modal-content -->
      </div><!-- modal-dialog -->
    </div><!-- modal -->
@stop

@section('styles')
<link rel="stylesheet" href="{{asset('hfv1/plugins/sweetalert/dist/sweetalert.css')}}">
<style>
  .joblist a.list-group-item {
      height:auto;
      min-height:111px;
  }
  .joblist a.list-group-item.active small {
      color:#fff;
  }
  .stars {
      margin:20px auto 1px;
  }
  .btn-light, .btn-light:hover {
    color : #fff;
  }
  .btn-fb {
    background-color: #3B5998;
  }
  .btn-tw {
    background-color: #55ACEE;
  }
</style>
@stop

@section('scripts')
  <script type="text/javascript" src="{{asset('hfv1/plugins/jquery-inputmask/dist/jquery.inputmask.bundle.js')}}"></script>
  <script type="text/javascript" src="{{asset('hfv1/plugins/sweetalert/dist/sweetalert.min.js')}}"></script>
  <script type="text/javascript" src="{{asset('hfv1/plugins/inputmask/js/inputmask.js')}}"></script>
  <script>


    var applyJob = {
      init : function(){
        this.viewResume();
        this.referCandidate();
      },
      referCandidate : function(){
        $('#referCandidate').click(function(e){
          e.preventDefault();
          e.stopImmediatePropagation();
          var data = { 'firstname': $("[name='firstname']").val(),
                       'lastname': $("[name='lastname']").val(),
                       'email': $("[name='email']").val(),
                       'contact_number': $("[name='contact_number']").val(),
                       'job_code': $("[name='job_code']").val(),
                    }
          $.ajax({
              url: '{{ route('recruiter.referral.store') }}',
              type : 'post',
              data: data,
              success : function(res){
                $('#jobPost').modal('hide');
                $('#jobPost').find('input').val('');
                swal('Success!','You have referred a candidate','success');
              },
              error : function(res){
                if(res.status == 422){
                  swal('Error!','Candidate is already in our database','error');
                }else{
                  swal('Error!','Opps! Something went wrong','error');
                }

              }
          });
        });
      },
      viewResume : function(){
        $('.job-detail').click(function(e){
            let job = $(this).data('job');
            let hasApplied = $(this).find('[data-hasApplied]').length;
            $('#jobPost').modal('show');
            let modal = $('#jobPost');
              if(hasApplied){
                $('#for-applicant').hide();
                modal.find('#applicant_reward').parent().append($('<span class="label label-info" id="hasApplied"><i class="ico-checkmark-circle"></i> Applied</span>'))
              }
              if(hasApplied<1){
               $('#for-applicant').show();
               if($('#hasApplied').length>0){
                $('#hasApplied').remove();
               }
              }
              modal.find('.title').text(job.job_title);
              if(typeof job.applicant_reward != 'undifiend' && job.applicant_reward != '' && job.applicant_reward != null){
                modal.find('#applicant_reward').text('HIRED REWARD : '+job.applicant_reward);
              }
              modal.find('#work_location').text(job.work_location);
              modal.find('#job_type').text(job.job_type);
              modal.find('#job_industry').text(job.job_industry);
              modal.find('#education_level').text(job.education_level);
              modal.find('#job_level').text(job.job_level);
              modal.find('.company_name').text(job.employer.company_name);
              modal.find('#logo').attr('src',job.employer.logo);
              modal.find('#company_description').html(job.employer.description);
              modal.find('[name="job_code"]').val(job.job_code);
              console.log(job.employer.is_verified,job.employer.is_premium)
              if(job.employer.is_verified == 1){
                $('#is_verified').show();
              }
              if(job.employer.is_verified == 0){
                $('#is_verified').hide();
              }
              if(job.employer.is_premium == 1){
                $('#is_premium').show();
              }
              if(job.employer.is_premium == 0){
                $('#is_premium').hide();
              }
              modal.find('.desc').html(job.description);
              modal.find('#cover_letter').val(applyJob.coverLetter(job).trim());
              $('.moneymask').inputmask({
                  'alias' : 'numeric',
                  'groupSeparator' : ',',
                  'autoGroup' : true,
                  'digits' : 2,
                  'digitsOptional' : false,
                  'prefix' : 'Php ',
                  'placeholder' : '0'
              });
            applyJob.referCandidate(job);
            e.preventDefault();
            e.stopImmediatePropagation();
        });
      },
      misc : function(){
        $('#is_verified').hide();
        $('#is_premium').hide();
        var job = {};
        $('#jobPost').on('hide.bs.modal',function(event) {
          job = {};
        });
        $('#jobPost').on('show.bs.modal', function (event) {
          var button = $(event.relatedTarget);
          var hasApplied = button.find('[data-hasApplied]').length;
          job = button.data('job');
          console.log(job)
          // var jobData = $.parseJSON(button.data('job'));
          var modal = $(this);
          // console.log(job);

          // modal.find('[name="job_code"]').val(job.job_code);
          // modal.find('.modal-body input').val(recipient)
          $('.moneymask').inputmask({
              'alias' : 'numeric',
              'groupSeparator' : ',',
              'autoGroup' : true,
              'digits' : 2,
              'digitsOptional' : false,
              'prefix' : 'Php ',
              'placeholder' : '0'
          });
          // event.preventDefault();
          event.stopImmediatePropagation();
        }) // show modal
      },
      coverLetter : function coverLetter(data) {
return `Dear ${data.employer.company_name},

Hello and good day!

This letter is in response to your ${data.job_title} job advertisement in HumanFactor.ph on {{date('F d Y')}}.

[Short description of your achievements and contact information here]

Thank you very much.

Sincerely yours,

{{ auth()->user()->my_name }}
`;
      }
    }
    $(function(){
      applyJob.init();


      $("a.twitter").attr("href", "https://twitter.com/home?status=" + window.location.href);
      $("a.facebook").attr("href", "https://www.facebook.com/sharer/sharer.php?u=" + window.location.href);
      $("a.google-plus").attr("href", "https://plus.google.com/share?url=" + window.location.href);
    });

  </script>
@stop
