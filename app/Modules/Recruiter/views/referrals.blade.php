@extends('Dashboard::template.main')

@section('content')
  <!-- Page Header -->
  <div class="page-header page-header-block">
      <div class="page-header-section">
          <h4 class="title semibold">Referrals</h4>
      </div>
      <div class="page-header-section">
          <!-- Toolbar -->
          <div class="toolbar clearfix">
            <!-- Toolbar -->
            <div class="toolbar clearfix text-right">

                <input type="text" placeholder="Search.." class="form-control">
            </div>
            <!--/ Toolbar -->
          </div>
          <!--/ Toolbar -->
      </div>
  </div>
  <!-- Page Header -->
  <!-- START row -->
  <div class="row">
      <div class="col-md-12">
          <!-- START panel -->

          <div class="panel panel-primary">
            @if(count($referrals)>0)
              <!-- panel heading/header -->
              <div class="panel-heading">
                  <h3 class="panel-title"><span class="panel-icon mr5"><i class="ico-table22"></i></span> Referrals</h3>
                  <!-- panel toolbar -->
                  <div class="panel-toolbar text-right">
                  </div>
                  <!--/ panel toolbar -->
              </div>
              <!--/ panel heading/header -->
              <!-- panel toolbar wrapper -->

              <div class="panel-toolbar-wrapper pl0 pt5 pb5">
                  @if(auth()->user()->type->slug == 'employer' AND auth()->user()->employer->is_premium)
                  <div class="panel-toolbar text-right">

                      <button type="button" class="btn btn-sm btn-default">Endorsed</button>
                      <button type="button" class="btn btn-sm btn-default">Rejected</button>
                      <button type="button" class="btn btn-sm btn-default">No Show</button>
                      <button type="button" class="btn btn-sm btn-default">Hired</button>
                  </div>
                  @endif
              </div>
              <!--/ panel toolbar wrapper -->

              <!-- panel body with collapse capabale -->
              <div class="table-responsive pull out">
                  <table class="table table-bordered table-hover" id="table1">
                      <thead>
                          <tr>
                              <th width="5%"></th>
                              <th width="20%">Name</th>
                              <th>Email</th>
                              <th># Applied Jobs</th>
                              <th width="13%">Status</th>
                              <th></th>
                          </tr>
                      </thead>
                      <tbody>
                        @foreach($referrals as $applicant)
                          <tr>
                              <td><div class="media-object"><img src="{{ $applicant->user->avatar }}" alt="" class="img-circle"></div>
                              </td>
                              <td>{{ $applicant->user->myname }} <br>
                                  <p class="no-margin text-muted"><small>CODE: {{ $applicant->auid }}</small></p>
                              </td>
                              <td>
                                <small>{{ $applicant->user->email }}</small>
                              </td>
                              <td>{{ $applicant->appliedJobs->count() }}</td>
                              <td>
                                <span class="label label-info">{{ strtoupper(str_replace('_','', $applicant->current_employment_status)) }}</span>
                              </td>
                              <td class="text-center">
                                <a href="{{ route('recruiter.referral.edit',['auid' => $applicant->auid]) }}" class="btn btn-sm btn-warning" target="_blank"><i class="ico-edit"></i></a>
                              </td>
                          </tr>
                          @endforeach
                      </tbody>
                  </table>
              </div>
              <!--/ panel body with collapse capabale -->
              {{ $referrals->links() }}
              @else
                <div class="panel-footer">
                  <div class="alert alert-info">
                    <strong>No data</strong>
                  </div>
                </div>
              @endif
          </div>
      </div>
  </div>
  <!--/ END row -->
@stop
