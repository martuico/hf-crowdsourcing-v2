@extends('Dashboard::template.main')

@section('content')
  <!-- Page Header -->
  <div class="page-header page-header-block">
      <div class="page-header-section">
          <h4 class="title semibold">Recruiter Dashboard</h4>
      </div>
      <div class="page-header-section">
          <!-- Toolbar -->
          <div class="toolbar clearfix">
              <div class="col-xs-8">
                  <select class="form-control text-left" id="selectize-customselect">
                      <option value="0">Display metrics...</option>
                      <option value="1">Last 6 month</option>
                      <option value="2">Last 3 month</option>
                      <option value="3">Last month</option>
                  </select>
              </div>
              <div class="col-xs-4">
                  <button class="btn btn-primary pull-right"><i class="ico-loop4 mr5"></i>Update</button>
              </div>
          </div>
          <!--/ Toolbar -->
      </div>
  </div>
  <!-- Page Header -->
  <div class="row">
    <div class="container">

    </div>
  </div>
@stop
