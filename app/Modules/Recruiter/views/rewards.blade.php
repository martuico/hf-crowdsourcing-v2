@extends('Dashboard::template.main')

@section('content')
  <!-- Page Header -->
  <div class="page-header page-header-block">
      <div class="page-header-section">
          <h4 class="title semibold">Rewards</h4>
      </div>
      <div class="page-header-section">
          <!-- Toolbar -->
          <div class="toolbar clearfix">
            <!-- Toolbar -->
            <div class="toolbar clearfix text-right">
            </div>
            <!--/ Toolbar -->
          </div>
          <!--/ Toolbar -->
      </div>
  </div>
  <!-- Page Header -->
  <!-- START row -->
  <div class="row">
      <div class="col-md-12">
          <!-- START panel -->
          @if(count($rewards)>0)
          <div class="panel panel-primary">
              <!-- panel heading/header -->
              <div class="panel-heading">
                  <h3 class="panel-title"><span class="panel-icon mr5"><i class="ico-table22"></i></span> Rewards</h3>
                  <!-- panel toolbar -->
                  <div class="panel-toolbar text-right">
                  </div>
                  <!--/ panel toolbar -->
              </div>
              <!--/ panel heading/header -->
              <!-- panel toolbar wrapper -->

              <div class="panel-toolbar-wrapper pl0 pt5 pb5">
              </div>
              <!--/ panel toolbar wrapper -->

              <!-- panel body with collapse capabale -->
              <div class="table-responsive pull out">
                  <table class="table table-bordered table-hover" id="table1">
                      <thead>
                          <tr>
                              <th width="5%"></th>
                              <th width="20%">Name</th>
                              <th>Email</th>
                              <th>Job</th>
                              <th>Employer</th>
                              <th>Hiring Status</th>
                              <th width="13%">Status</th>
                              <th></th>
                          </tr>
                      </thead>
                      <tbody>
                        @foreach($rewards as $reward)
                          <tr>
                            <td><div class="media-object"><img src="{{ $reward->applicant->user->avatar }}" alt="" class="img-circle"></div>
                            </td>
                            <td>{{ $reward->applicant->user->my_name }}</td>
                            <td>{{ $reward->applicant->user->email }}</td>
                            <td>
                              {{ $reward->job->job_title }} <br>
                              <small class="text-muted">{{ $reward->job->job_code }}</small>
                            </td>
                            <td>
                              Hired Date: {{ $reward->applicantJob->hired_date }} <br>
                              Training Date: {{ $reward->applicantJob->date_of_training }} <br>
                            </td>
                            <td>{{ strtoupper($reward->reward_status) }}</td>
                            <td>
                              <button class="btn btn-xs btn-default">Email HF</button>
                            </td>
                          </tr>
                        @endforeach
                      </tbody>
                  </table>
              </div>
              <!--/ panel body with collapse capabale -->
              {{ $rewards->links() }}
          </div>
        @else
          <div class="alert alert-info">
            <strong>No data!</strong>
          </div>
        @endif
      </div>
  </div>
  <!--/ END row -->
@stop
