<?php


Route::group(['module' => 'Recruiter',
              'namespace' => 'App\Modules\Recruiter\Controllers',
              'middleware' => ['web','auth','RecruiterOnly'],
              'prefix' => 'independent-recruiter'
          ],
            function() {

    Route::get('test',function(){
        $inputs = ['firstname' => 'mar','lastname' => 'tuico','email' => 'mar@test.com'];
      \Mail::send(new \App\Mail\IndependentReferalMail($inputs));
      return 'test';
      // return dd(Mail::send('Recruiter::emails.referred', ['title' => 'test', 'content' => 'test'], function ($message)
      //   {
      //
      //       $message->from('me@gmail.com', 'Christian Nwamba');
      //
      //       $message->to('mar.tuico@gmail.com');
      //
      //   }));

    });
    Route::get('rewards',['as' => 'recruiter.rewards','uses' => 'ReferralController@rewards']);
    Route::get('fetch-cities/{provcode}',['uses' => 'ReferralController@fetchCities']);
    Route::get('fetch-province/{regcode}',['uses' => 'ReferralController@fetchProvince']);

    Route::get('referrals/edit/{auid}',['as' => 'recruiter.referral.edit','uses' => 'ReferralController@edit']);
    Route::post('referrals/edit/{auid}',['as' => 'recruiter.referral.edit','uses' => 'ReferralController@update']);
    // Route::get('referrals/create',['as' => 'recruiter.referral.create','uses' => 'ReferralController@create']);
    Route::post('referrals/create',['as' => 'recruiter.referral.store','uses' => 'ReferralController@store']);
    Route::get('referrals',['as' => 'recruiter.referrals','uses' => 'ReferralController@index']);

    Route::get('careers/refer',['as' => 'recruiter.job.apply','uses' => 'RecruiterController@refer']);
    Route::get('careers',['as' => 'recruiter.careers','uses' => 'RecruiterController@careers']);
    Route::get('/',['as' => 'recruiter.home','uses' => 'RecruiterController@index']);
});
