<?php namespace App\Modules\Recruiter\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Applicant,
    App\JobIndustry,
    App\Job;

class RecruiterController extends Controller
{
    protected $applicant;
    protected $jobs;
    protected $jindustries;

    public function __construct(Applicant $applicant,
                                Job $jobs,
                                JobIndustry $jindustries)
    {
        $this->jobs = $jobs;
        $this->applicant = $applicant;
        $this->jindustries = $jindustries;
    }

    public function index()
    {
        return view('Recruiter::index');
    }


    public function careers()
    {
        $jobs = $this->jobs
                    ->where('status','=','open')
                    ->whereHas('employer',function($q){
                      $q->where('is_premium','=',1);
                    })
                    ->orderBy('created_at','desc')
                    ->paginate(15);

        $industries = $this->jindustries->orderBy('id','asc')->get();
        return view('Recruiter::careers')
                    ->with(compact('jobs','industries'));
    }



}
