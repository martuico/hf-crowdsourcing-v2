<?php namespace App\Modules\Recruiter\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Modules\Recruiter\Requests\ApplicantProfileRequest;
use App\Mail\IndependentReferalMail;

use App\Traits\ImageUpload;
use App\Job,
    App\ApplicantEducation,
    App\ApplicantEducationLevel,
    App\ApplicantExperience,
    App\Applicant,
    App\ApplicantJob,
    App\JobIndustry,
    App\Region,
    App\Province,
    App\City,
    App\IRecruiter,
    App\User,
    App\Reward,
    App\Referral;


class ReferralController extends Controller
{
    use ImageUpload;
    protected $applicant;
    protected $jobs;
    protected $jindustries,
              $appEducation,
              $appEduLevel,
              $appExperience,
              $location_province,
              $location_city,
              $user,
              $reward,
              $referral;

    public function __construct(Job $jobs,
                                ApplicantJob $appliedJobs,
                                JobIndustry $jindustries,
                                Applicant $applicant,
                                ApplicantEducation $appEducation,
                                ApplicantEducationLevel $appEduLevel,
                                ApplicantExperience $appExperience,
                                Region $location_region,
                                Province $location_province,
                                City $location_city,
                                IRecruiter $irecruiter,
                                User $user,
                                Reward $reward,
                                Referral $referral)
    {
        $this->jobs = $jobs;
        $this->applicant = $applicant;
        $this->user = $user;
        $this->appEducation = $appEducation;
        $this->appEduLevel = $appEduLevel;
        $this->appExperience = $appExperience;
        $this->jindustries = $jindustries;
        $this->location_region = $location_region;
        $this->location_province = $location_province;
        $this->location_city = $location_city;
        $this->reward = $reward;
        $this->referral = $referral;
    }

    public function index()
    {
        $referrals = $this->applicant
                        ->where('referred_by_id','=',auth()->user()->id)
                        ->where('referred_expired_date','>',\Carbon\Carbon::now())
                        ->paginate(50);

        return view('Recruiter::referrals')
                ->with(compact('referrals'));
    }

    public function create()
    {
      $industries = $this->jindustries->orderBy('name','asc')->get();
      $levels = $this->appEduLevel->orderBy('name','asc')->get();
      $location_region = $this->location_region->orderBy('regCode','asc')->get();
        return view('Recruiter::referrals-create')
                    ->with(compact('industries','levels','location_region'));
    }

    public function store(ApplicantProfileRequest $request)
    {
        if($request->ajax()){
            $inputs = $request->only('firstname','job_code','lastname','email','contact_number');
            $referral = $this->referral->create($inputs);
            $job = $this->jobs->where('job_code','=',$request->job_code)->first();
            $inputs['referred_by'] = auth()->user()->my_name;
            $inputs['job_title'] =  $job->slug;
            $inputs['referral_link'] = $referral->uniq_link;
            \Mail::send(new IndependentReferalMail($inputs));
            return response()->json(['success' => true],200);
        }


        //Send Email to applicant to confirm
        //email, password
        return redirect()->route('recruiter.referrals');
    }

    public function edit($auid)
    {
        $industries = $this->jindustries->orderBy('name','asc')->get();
        $levels = $this->appEduLevel->orderBy('name','asc')->get();
        $location_region = $this->location_region->orderBy('regCode','asc')->get();
        $applicant = $this->applicant->where('auid','=',$auid)->first();
        if(count($applicant)< 1){
            return abort(404);
        }
        return view('Recruiter::referrrals-edit')
                  ->with(compact('industries','levels','location_region','applicant'));
    }

    public function rewards()
    {
        $rewards = $this->reward
                        ->where('referred_by_id','=',auth()->user()->id)
                        ->paginate(15);

        return view('Recruiter::rewards')
                    ->with(compact('rewards'));
    }


}
