<?php namespace App\Modules\InternalRecruiter\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Modules\Employer\Requests\CompanyProfileRequest;
use App\Traits\ImageUpload;
use Illuminate\Validation\Rule;
use App\Modules\Employer\Requests\JobCreateRequest;

use App\Job,
	App\JobIndustry,
	App\JobType,
	App\Skill,
	App\Shift,
	App\JobLevel,
  App\Employer,
  App\User,
  App\Applicant,
  App\ApplicantJob,
	App\Region,
	App\Province,
	App\City;

class EmployerController extends Controller
{

  use ImageUpload;
	protected $jobs;
  protected $applicants;
  protected $applicant_job;
	protected $jtype;
	protected $jindustry;
	protected $jskill;
	protected $shift;
	protected $edu_level;
  protected $employers;
  protected $users;
	protected $location_region, $location_province, $location_city;

	public function __construct(Job $jobs,
								JobIndustry $jindustry,
								JobType $jtype,
								Skill $skill,
								Shift $shift,
								JobLevel $edu_level,
                Employer $employers,
                User $users,
                Applicant $applicants,
                ApplicantJob $applicant_job,
								Region $location_region,
								Province $location_province,
								City $location_city)
	{
      $this->jobs = $jobs;
      $this->applicants = $applicants;
  		$this->applicant_job = $applicant_job;
  		$this->jindustry = $jindustry;
  		$this->jtype = $jtype;
  		$this->skill = $skill;
  		$this->shift = $shift;
  		$this->edu_level = $edu_level;
      $this->employers = $employers;
      $this->users = $users;
			$this->location_region = $location_region;
			$this->location_province = $location_province;
			$this->location_city = $location_city;
	}

	public function index()
	{
			$employers = $this->employers
												->orderBy('created_at','desc')
												->paginate(50);

			return view('InternalRecruiter::employer.list')
									->with(compact('employers'));
	}

	public function show($company)
	{
			$employer = $this->employers->whereSlug($company)->first();
			if(count($employer)<1){
				return abort(404);
			}

			return view('InternalRecruiter::employer.detail')
								->with(compact('employer'));
	}
}
