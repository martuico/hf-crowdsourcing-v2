<?php


Route::group(['module' => 'InternalRecruiter',
              'namespace' => 'App\Modules\InternalRecruiter\Controllers',
              'middleware' => ['web','auth','InternalRecruiterOnly'],
              'prefix' => 'internal-recruiter'
          ],
            function() {

    Route::post('application/{applicant}/stages',
              ['as' => 'internal-recruiter.application.stages',
              'uses' =>'ApplicantController@updateStages']);
    Route::post('update-applicant-status',['uses' => 'ApplicantController@updateApplicantStatus']);
    Route::post('applicant-comment',['uses' => 'ApplicantController@applicantActivityComment']);
    Route::get('fetch-applicantion/{code}',['uses' => 'ApplicantController@fetchApplication']);
    Route::get('applicants/{applicant}',
                ['as' => 'internal-recruiter.applicant.details',
                'uses' => 'ApplicantController@show']);
    Route::get('settings',['as' => 'internal-recruiter.settings','uses' => 'RecruiterController@settings']);
    Route::post('settings',['as' => 'internal-recruiter.updateSettings','uses' => 'RecruiterController@updateSettings']);
    Route::get('applicants',['as' => 'internal-recruiter.applicants','uses' => 'ApplicantController@index']);
    Route::get('employers/{company}',['as' => 'internal-recruiter.profile','uses' => 'EmployerController@show']);
    Route::get('employers',['as' => 'internal-recruiter.employers','uses' => 'EmployerController@index']);
    Route::get('jobs/{job_code}/pipeline',['as' => 'internal-recruiter.pipeline','uses' => 'RecruiterController@pipeline']);
    Route::get('jobs',['as' => 'internal-recruiter.jobs','uses' => 'RecruiterController@listJob']);
    Route::get('/',['as' => 'internal-recruiter.home','uses' => 'RecruiterController@index']);
});
