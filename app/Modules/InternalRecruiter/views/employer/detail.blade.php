@extends('Dashboard::template.main')

@section('content')
  <!-- Page Header -->
  <div class="page-header page-header-block">
      <div class="page-header-section">
          <h4 class="title semibold">{{$employer->company_name}} Profile</h4>
      </div>
      <div class="page-header-section">
          <!-- Toolbar -->
          <!--/ Toolbar -->
      </div>
  </div>

  <!-- START row -->
  <div class="row">
      <!-- Left / Top Side -->
      <div class="col-lg-4">
        <div class="widget panel">
            <!-- panel body -->
            <div class="panel-body bgcolor-success">
                <ul class="list-unstyled mt15 mb15">
                    <li class="text-center">
                        <img class="img-circle img-bordered" src="{{$employer->logo}}" alt="" width="65px" height="65px">
                    </li>
                    <li class="text-center">
                        <h5 class="semibold mb0">{{$employer->company_name}}</h5>
                        <span><i class="ico-map-marker"></i> {{$employer->office_address}}</span>
                    </li>
                </ul>
            </div>
            <!--/ panel body -->
            <!-- panel body -->
            <div class="panel-body">
                <!-- Nav section -->
                <ul class="nav nav-section nav-justified">
                    <li>
                        <div class="section">
                            <h4 class="nm"><i class="ico-earth"></i></h4>
                            <p class="nm">Website</p>
                        </div>
                    </li>
                    <li>
                        <div class="section">
                            <h4 class="nm">30k</h4>
                            <p class="nm">Hired</p>
                        </div>
                    </li>
                    <li>
                        <div class="section">
                            <h4 class="nm">5</h4>
                            <p class="nm">Job Opening</p>
                        </div>
                    </li>
                </ul>
                <!--/ Nav section -->
            </div>
            <!--/ panel body -->
        </div>

        <div class="widget panel">
          <div class="panel-body">
              <h5 class="semibold mb0 text-center">
              @if($employer->is_verified)
                <span class="label label-success"><i class="ico-checkmark-circle2"></i> Verified</span>
              @else
                <span class="label label-info"><i class="ico-minus-circle2"></i> Not Verified</span>
              @endif
              </h5>
              <h5 class="semibold mb0">About Our Company</h5>
              <p class="text-default mb0">{!! $employer->description !!}</p>
              <hr>
              <h5 class="semibold mb0">Contacts</h5>
              <p class="text-default mb0">Office Number: <span class="semibold text-accent">{{$employer->office_number_1}}</span></p>
              <p class="text-default mb0">Mobile Number: <span class="semibold text-accent">{{$employer->office_number_2 ?:'None'}}</span></p>
              <h5 class="semibold mb0">Social Media Page</h5>
              <p class="text-default mb0">Facebook: <span class="semibold text-accent">{{$employer->sm_facebook ?:'None'}}</span></p>
              <p class="text-default mb0">Twitter: <span class="semibold text-accent">{{$employer->sm_linkedin ?:'None'}}</span></p>
              <p class="text-default mb0">Linkedin: <span class="semibold text-accent">{{$employer->sm_twitter ?:'None'}}</span></p>
          </div>
        </div>
      </div>
      <!--/ Left / Top Side -->

      <!-- Left / Bottom Side -->
      <div class="col-lg-8">
        @if(auth()->user()->type->name == 'Employer' AND count(auth()->user()->employer)>0)
        <div class="alert alert-warning text-left">
          <strong><i class="ico-warning-sign"></i> You're account has not been verified yet but you can start adding job post! </strong>
        </div>
        @endif

        <div class="widget panel mt20">
          <!--/ panel body -->
          @if(count($employer->jobs)>0)
          <!-- List group -->
          <ul class="list-group">
            @foreach($employer->jobs as $job)
              <li class="list-group-item">
                  <div class="row">
                    <div class="col-md-8">
                      <h4>{{$job->job_title}}</h4>
                      <ul class="list-inline">
                        <li>
                            <span class="label label-default">{{ $job->type->name }}</span>
                        </li>
                        <li>
                            <span class="label label-default">{{ $job->job_industry->name }}</span>
                        </li>
                        <li>
                            <span class="label label-default">{{ $job->shift->name }}</span>
                        </li>
                      </ul>
                    </div>
                    <div class="col-md-4">
                      <p class="mt10"><span class="label label-info">{{ strtoupper($job->status) }}</span></p>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-md-8">
                      <p class="nm"><i class="ico-map-marker"></i>{{ $job->region->regDesc }} ,{{ $job->province->provDesc }}, {{ $job->city->citymunDesc }}</p>
                    </div>
                    <div class="col-md-4">
                      <p class="nm"><span class="label label-default"><i class="fa fa-check-square"></i> Hired : {{ $job->hiredApplicants->count() }}</span></p>
                    </div>
                  </div>
                  <small class="text-muted">{{ $job->created_at }}</small>
              </li>
            @endforeach
          </ul>
          <!-- List group -->
          @else
          <div class="panel-body">
            <div class="alert alert-info">
              <strong>No data for careers!</strong>
            </div>
          </div>
          @endif
      </div>
      <!--/ Left / Bottom Side -->
  </div>
  <!--/ END row -->
@stop
