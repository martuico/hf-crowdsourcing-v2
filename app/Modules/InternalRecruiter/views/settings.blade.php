@extends('Dashboard::template.main')

@section('content')
  <!-- Page Header -->
  <div class="page-header page-header-block">
      <div class="page-header-section">
          <h4 class="title semibold">My Settings</h4>
      </div>
      <div class="page-header-section">
          <!-- Toolbar -->
          <div class="toolbar clearfix">

          </div>
          <!--/ Toolbar -->
      </div>
  </div>
  <!-- Page Header -->

  <div class="row">
    <div class="col-md-6">
      @if(\Session::has('successMessage'))
      <div class="alert alert-success">
        <strong>{{ \Session::get('successMessage') }}</strong>
      </div>
      @endif
      <div class="panel panel-default">
        <div class="panel-heading">
          <h1 class="panel-title">Settings</h1>
        </div>
        <div class="panel-body">
          {{ \Form::open(['route' => 'internal-recruiter.updateSettings','files' => true]) }}
            <div class="form-group">
              <label>Email</label>
              <input type="text" class="form-control" value="{{ auth()->user()->email }}" disabled="">
            </div>
            <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                  <label>Firstname</label>
                  <input type="text" name="firstname" class="form-control" value="{{ auth()->user()->firstname }}">
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label>Lastname</label>
                  <input type="text" name="lastname" class="form-control" value="{{ auth()->user()->lastname }}">
                </div>
              </div>
            </div>
            <div class="form-group">
              <label>Password</label>
              <input type="password" name="password" class="form-control">
            </div>
            <div class="form-group">
              <label>Profile Picture</label>
              <input type="file" name="avatar" class="form-control">
            </div>
            <div class="form-group">
              <button type="submit" class="btn btn-primary">Submit</button>
            </div>
          {{ \Form::close() }}
        </div>
      </div>
    </div>
  </div>
@stop
