

<div class="page-header page-header-block">
  <h4 class="title semibold">Application - {{ $application->applicant->user->my_name }} for {{ $application->job->job_title}} <br>
    <small>CODE: {{ $application->application_code }}</small>
  </h4>
</div>

<div class="widget panel">
  <div class="panel-body">
    <h3 class="panel-title">Cover Letter</h3>
    <p>{!! nl2br($application->applicant_cover_letter) !!}</p>
    <p>Expected Salary : <strong>{{ $application->expected_salary }}</strong></p>
  </div>
</div>

<div class="alert alert-info">
  <p><strong>Current Status :</strong> {{ $application->applicant_status->name }} </p>
  @if($application->employer_appointment_type)
  <p class="oapp_type"><strong>Appointments :</strong> {{ ucwords(str_replace('-',' ',$application->employer_appointment_type)) }} | {{ $application->employer_appointment_sched }} </p>
  @endif
  @if($application->endorsement_slip)
  <p class="oattachment"><strong>Edorsement Slip :</strong> <i class="fa fa-file"></i> {{ str_replace(url('/').'/endorsement-slips-media/','',$application->endorsement_slip) }} </p>
  @endif
  @if($application->hire_date && $application->hire_date !== '0000-00-00')
  <p class="ohire_date"><strong>Hire Date :</strong> {{ $application->hire_date }} </p>
  @endif
  @if($application->hire_account)
  <p class="ohire_account"><strong>Account :</strong> {{ $application->hire_account }} </p>
  @endif
  @if($application->reason)
  <p class="oreason"><strong>Reason :</strong> {{ $application->reason }} </p>
  @endif
  @if($application->note)
  <p class="onotes"><strong>Note :</strong> {{ $application->note }} </p>
  @endif
</div>
<div class="widget panel">
  <div class="panel-heading">
    <h3 class="panel-title">HF Status</h3>
  </div>
  <div class="panel-body">
    {{ \Form::open(['route' => ['internal-recruiter.application.stages', $application->application_code],
                    'id' => 'form',
                    'files' => true]) }}
      <div class="form-group">
        <select name="applicant_status_id" id="stage-options" class="form-control">
          <option value="1" {{($application->applicant_status_id == '1' )?'selected':''}}>Pipeline</option>
          <option value="2" {{($application->applicant_status_id == '2' )?'selected':''}}>Endorsed</option>
          <option value="3" {{($application->applicant_status_id == '3' )?'selected':''}}>Re profile to Premium Job post</option>
          <option value="4" {{($application->applicant_status_id == '4' )?'selected':''}}>Active file</option>
          <option value="5" {{($application->applicant_status_id == '5' )?'selected':''}}>Refer to Regular Job Post</option>
          <option value="6" {{($application->applicant_status_id == '6' )?'selected':''}}>No show</option>
          <option value="7" {{($application->applicant_status_id == '7' )?'selected':''}}>Re schedule</option>
          <option value="8" {{($application->applicant_status_id == '8' )?'selected':''}}>Hired</option>
        </select>
      </div>
      <div class="form-group oapp_type">
        <div class="radio">
          <label>
            <input type="radio" name="employer_appointment_type" id="optionsRadios1" value="on-site" checked>
            <span>On Site</span>
          </label>
        </div>
        <div class="radio" style="margin-top:5px;">
          <label>
            <input type="radio" name="employer_appointment_type" id="optionsRadios2" value="call">
            <span>Over the phone</span>
          </label>
        </div>
      </div>
      <div class="row opick_datetime" style="margin-top:20px;">
        <div class="col-md-12">
            <h4 class="panel-title">Schedule date and time</h4>
           <div id="datetimepicker1" class="mt10"></div>
        </div>
      </div>
      <div class="row mt10 opick_datetime">
          <div class="panel-heading">
          <h4 class="panel-title fsize13">Date & Time: <span id="hidden-val"></span></h4>
          <input type="hidden" name="employer_appointment_sched" id="my_hidden_input">
          <input type="hidden" id="my_hidden_type">
          <h4 class="panel-title fsize13">Type: <span id="appointment-type"></span></h4>
          </div>
      </div>
      <div class="form-group mt10 oattachment">
        <label for="exampleInputFile">Attached Endorsement Slip</label>
        <input type="file" name="endorsement_slip" id="exampleInputFile">
        <p class="help-block">Must be in PDF format.</p>
      </div>
      <div class="form-group mt10 oreason">
        <div class="has-icon mb10">
            <input type="text" name="reason" class="form-control" placeholder="Reason">
            <i class="ico-pencil2 form-control-icon"></i>
        </div>
      </div>
      <div class="form-group orefer_premium">
        <label>Refer to Premium Job post</label>
        <input type="text" name="refer_premium" class="form-control" placeholder="Input Job Code">
      </div>
      <div class="form-group orefer_regular">
        <label>Refer to Regular Job post</label>
        <input type="text" name="refer_regular" class="form-control" placeholder="Input Job Code">
      </div>
      <div class="form-group mt10 onotes">
        <div class="has-icon mb10">
            <input type="text" name="notes" class="form-control" placeholder="Notes">
            <i class="ico-file form-control-icon"></i>
        </div>
      </div>
      <div class="form-group mt10 ohire_date">
        <label>Start Date</label>
        <input type="text" name="hire_date" class="form-control">
      </div>
      <div class="form-group mt10 ohire_account">
        <label>Account</label>
        <input type="text" name="hire_account" class="form-control" placeholder="Account/Department">
      </div>
      <div class="form-group">
        <button type="submit" class="btn btn-sm btn-info">Save</button>
      </div>
    </form>
  </div>
</div>
