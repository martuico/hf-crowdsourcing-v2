@extends('Dashboard::template.main')

@section('content')
  <!-- Page Header -->
  <div class="page-header page-header-block">
      <div class="page-header-section">
          <h4 class="title semibold">{{ $applicant->user->my_name }}<br>
            <small>CODE: {{ $applicant->auid }}</small>
          </h4>
      </div>
      <div class="page-header-section">
          <!-- Toolbar -->
          <div class="toolbar clearfix text-right">
              <a href="{{ \URL::previous() }}" class="btn btn-sm btn-default"  data-toggle="tooltip" data-placement="bottom" title="Create Job Post"><i class="ico-chevron-left"></i></a>
          </div>
          <!--/ Toolbar -->
      </div>
  </div>

  <div class="row">
    <div class="col-md-12">
      @if(\Session::has('successMessage'))
      <div class="alert alert-success">
        <strong>{{ \Session::get('successMessage') }}</strong>
      </div>
      @endif
      <div class="container-fluid">
      <!-- Nav tabs -->
      <ul class="nav nav-tabs" role="tablist">
        @php
        $url_a = route('internal-recruiter.applicant.details',['code' => $applicant->auid])
        @endphp
        <li role="presentation" class="{{ (!isset($_GET['t']))?'active':'' }}"><a href="{{ $url_a }}">Resume Profile</a></li>
        <li role="presentation" class="{{ (isset($_GET['t']) AND $_GET['t' ] =='jobs')?'active':'' }}"><a href="{{ $url_a }}?t=jobs">Jobs</a></li>
        <li role="presentation" class="{{ (isset($_GET['t']) AND $_GET['t' ] =='activity')?'active':'' }}"><a href="{{ $url_a }}?t=activity">Acitvities</a></li>
      </ul>
      <div class="tab-content">
        <div role="tabpanel" class="tab-pane {{ (!isset($_GET['t']))?'active':'' }}" id="resume">
          <div class="widget panel">
            <div class="panel-body">
              <div class="col-md-4" style="border:#ccc dashed 1px;padding:13px;">
                <ul class="list-unstyled">
                    <li class="text-center">
                        <img class="img-circle img-bordered-success" src="{{ $applicant->user->profile_pic }}" alt="" width="75px" height="75px">
                        <br>
                        <h5 class="semibold mb0">{{ $applicant->user->my_name }}</h5>
                        <p class="nm text-muted">{{ $applicant->position }}</p>
                        <a href="javscript:void(0);" class="nm text-teal"><i class="fa fa-link"></i> {{ str_replace(url('upload-resume').'/', '', $applicant->attached_resume) }} </a>
                        <p><a href="{{ route('admin.applicant.edit',['applicant' => $applicant->auid ]) }}" target="_blank" class="btn btn-warning btn-sm">Edit Profile</a></p>
                    </li>
                </ul>
                <hr>
                <div class="row">
                  <div class="col-md-12">
                    <p class="nm">Availability : <strong>{{ strtoupper($applicant->availability) }}</strong></p>
                    <p class="nm">Expected Salary : <strong>{{ strtoupper($applicant->expected_salary) }}</strong></p>
                    <p class="nm">Preferred Work Location : <strong>{{ ($applicant->prefered_location)?strtoupper($applicant->prefered_location->citymunDesc):'' }}</strong></p>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-6">
                    <h4><i class="ico-bubble-dots3"></i> Languages</h4>
                    @if(strlen($applicant->languages)>0)
                      <ul class="list-inline">
                      @foreach(explode(',',$applicant->languages) as $language)
                      <li><span class="label label-default">{{ $language }}</span></li>
                      @endforeach
                      </ul>
                    @endif
                  </div>
                  <div class="col-md-6">
                    <h4><i class="ico-bubble-dots3"></i> Skills</h4>
                    @if(strlen($applicant->skills)>0)
                    <ul class="list-inline">
                      @foreach(explode(',',$applicant->skills) as $skill)
                      <li><span class="label label-default">{{ $skill }}</span></li>
                      @endforeach
                    </ul>
                    @endif
                  </div>
                  <div class="col-md-12">
                    @if(count($applicant->internal_recruiter)>0)
                      <h4>Assigned HF: {{ $applicant->internal_recruiter->my_name }}</h4>
                    @endif
                    @if(count($applicant->external_recruiter)>0)
                      <h4>Referred By: {{ $applicant->external_recruiter->my_name }}</h4>
                    @endif
                  </div>
                </div>
              </div>
              <div class="col-md-8">
                <div class="table-layout">
                  <div class="row">
                    <div class="col-md-6">
                      <h5 class="panel-title"><i class="ico-vcard"></i> Personal Information</h5>
                      <p class="mb5">Full name : <strong class="text-uppercase">{{ $applicant->user->my_name }}</strong></p>
                      <p class="mb5">D.O.B : <strong class="text-uppercase">{{ $applicant->date_of_birth?:'None' }}</strong></p>
                      <p class="mb5">Email : <strong class="text-uppercase"><a href="mailto:{{ $applicant->user->email }}?Subject=From%20Human%20Factor">{{ $applicant->user->email }}</a></strong></p>
                      <p class="mb5">Employment Status: <strong class="text-uppercase">{{ $applicant->employment_status?:'None' }}</strong></p>
                      <p class="mb5">Mobile Number: <strong class="text-uppercase">{{ $applicant->mobile_number_1?:'None' }}</strong></p>
                      <p class="mb5">House Number: <strong class="text-uppercase">{{ $applicant->house_number_1?:'None' }}</strong></p>
                    </div>
                    <div class="col-md-6">
                      <div class="row">
                        <h5 class="panel-title"><i class="ico-map-marker"></i>Current Address</h5 >
                        <p>{!! nl2br($applicant->current_fulladdress) !!}</p>
                      </div>
                      <div class="row">
                        <h5 class="panel-title"><i class="ico-map-marker"></i>Provincial Addres</h5 >
                        <p>{!! nl2br($applicant->hometown_fulladdress) !!}</p>
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-md-12">
                      <h5 class="panel-title"><i class="fa fa-graduation-cap"></i> Educational Attainment</h5>
                      <p class="nm">Highest Education Attainment: <strong>{{ (count($applicant->education_level)>0)? $applicant->education_level->name:'No Data' }}</strong></p>
                      <p class="nm">Last School Attended: <strong>{{ $applicant->last_school }}</strong></p>
                      <p class="nm">Industry of study: <strong>{{ (count($applicant->job_industry)>0)?$applicant->job_industry->name:'No Data' }}</strong></p>
                    </div>
                  </div><!-- row -->
                  <hr>
                  <div class="row">
                    <div class="col-md-12">
                      <h4>Experiences</h4>
                      @if(count($applicant->experiences )>0)
                      @foreach($applicant->experiences as $exp)
                        @if(strlen($exp->company_name )>0 AND strlen($exp->position)>0)
                        <h5 class="text-uppercase nm">{{ $exp->position }}</h5>
                        <p class="nm">Company : <strong>{{ $exp->company_name }}</strong></p>
                        @endif
                        <p class="nm"><strong>{{  makeMonthName($exp->startMonth).' '.$exp->startYear }} - {{ ($exp->currently_work_here)?'Present':makeMonthName($exp->endMonth) .' '.$exp->endYear}}</strong></p>
                        @if(strlen($exp->resposibilities)>0)
                        <p class="nm">Reason for leaving : <strong>{{ $exp->resposibilities }}</strong></p>
                        @endif
                        <hr style="border-style: dashed;">
                      @endforeach
                      @else
                      <div class="alert alert-info">
                        No data
                      </div>
                      @endif
                    </div>
                  </div><!-- row -->
                </div>
              </div>
            </div>
          </div>
        </div><!-- Resume -->
        <div role="tabpanel" class="tab-pane {{ (isset($_GET['t']) AND $_GET['t' ] =='jobs')?'active':'' }}">
          <div class="widget panel">
            <div class="panel-body">
              @if(count($applied_jobs)>0)
              <div class="table">
                <table class="table table-bordered table-hover">
                  <thead>
                    <tr>
                      <th>Job Title</th>
                      <th>Employer</th>
                      <th>Expected Salary</th>
                      <th>HF Status</th>
                      <th>Employer Feedback</th>
                      <th></th>
                    </tr>
                  </thead>
                  <tbody>
                    @foreach($applied_jobs  as $applied)
                      <tr>
                        <td>{{ $applied->job->job_title }}
                          <br>
                          <span class="text-muted">CODE: {{ $applied->job->job_code }}</span>
                        </td>
                        <td>
                          @if($applied->employer->is_verified)
                          <span class="text-success"><i class="fa fa-check-circle"></i></span>
                          @else
                          <span class="text-muted"><i class="fa fa-circle"></i></span>
                          @endif
                          {{ $applied->employer->company_name }} <br>
                          @if($applied->employer->is_premium)
                            <span class="text-warning"><i class="fa fa-shield"></i> PREMIUM</span>
                          @endif
                        </td>
                        <td>{{ $applied->expected_salary }}</td>
                        <td class="stages" data-status="{{ $applied->applicant_status_id }}">
                          <p><strong>Current Status :</strong> {{ $applied->applicant_status->name }} </p>
                          @if($applied->employer_appointment_type)
                          <p class="app_type"><strong>Appointments :</strong> {{ ucwords(str_replace('-',' ',$applied->employer_appointment_type)) }} | {{ $applied->employer_appointment_sched }} </p>
                          @endif
                          @if($applied->endorsement_slip)
                          <p class="attachment"><strong>Endorsement Slip :</strong> <i class="fa fa-file"></i> <a href="{{ route('download.endorsemmentslip',['application_code' => $applied->application_code]) }}"> Download here</a> </p>
                          @endif
                          @if($applied->hire_date && $applied->hire_date !== '0000-00-00')
                          <p class="hire_date"><strong>Hire Date :</strong> {{ $applied->hire_date }} </p>
                          @endif
                          @if($applied->hire_account)
                          <p class="hire_account"><strong>Account :</strong> {{ $applied->hire_account }} </p>
                          @endif
                          @if($applied->reason)
                          <p class="reason"><strong>Reason :</strong> {{ $applied->reason }} </p>
                          @endif
                          @if($applied->note)
                          <p class="notes"><strong>Note :</strong> {{ $applied->note }} </p>
                          @endif
                        </td>
                        <td class="employerFb" data-employerfb="{{ $applied->employer_applicant_status_id }}">
                          <p><strong>Applicant Feedback Status :</strong>  {{ ($applied->employer_applicant_status)? $applied->employer_applicant_status->name:'No Status' }} </p>
                          @if($applied->employerFeedback)
                            @if($sched_final = $applied->employerFeedback->schedule_final_interview)
                            <p class="sched_finalinterview"><strong>Final Interview :</strong>  {{ $sched_final }} </p>
                            @endif
                            @if($applied->employerFeedback->reason)
                            <p class="employer_reason"><strong>Reason :</strong> {{ $applied->employerFeedback->reason }}</p>
                            @endif
                            @if($applied->employerFeedback->notes)
                            <p class="employer_notes"><strong>Notes :</strong> {{ $applied->employerFeedback->notes }}</p>
                            @endif
                            @if($sched_valid = $applied->employerFeedback->schedule_account_validation)
                            <p class="sched_validation"><strong>Account Validation :</strong>  {{ $sched_valid }}</p>
                            @endif
                            @if($sched_offer = $applied->employerFeedback->schedule_job_offer)
                            <p class="sched_joboffer"><strong>Job Offer :</strong>  {{ $sched_offer }}</p>
                            @endif
                            @if($sched_training = $applied->date_of_training)
                            <p class="date_of_training"><strong>Date Training :</strong>  {{ $sched_training }}</p>
                            @endif
                            @if($hire_acc = $applied->hire_account)
                            <p class="hire_account"><strong>Hire Account :</strong>  {{ $hire_acc }}</p>
                            @endif
                            @if($hire_date = $applied->hire_date)
                            <p class="hire_date"><strong>Hire Date :</strong>  {{ $hire_date }}</p>
                            @endif
                          @endif
                        </td>
                        <td>
                          <button class="btn btn-default btn-xs" data-toggle="modal" data-target=".application" data-applicant="{{ $applied->application_code }}">
                            <i class="fa fa-file"></i>
                          </button>
                        </td>
                      </tr>
                    @endforeach
                  </tbody>
                </table>
              </div>
              {{ $applied_jobs->appends($_GET)->links() }}
              @else
                <div class="alert alert-info">
                  <strong>No Data</strong>
                </div>
              @endif
            </div>
          </div>
        </div>
        <div role="tabpanel" class="tab-pane {{ (isset($_GET['t']) AND $_GET['t' ] =='activity')?'active':'' }}">
          <div class="widget panel">
            <div class="panel-body">
            <div class="form-group mt10">
              <div class="has-icon mb10">
                  <input type="text" class="form-control" id="comment" placeholder="Comments here">
                  <i class="ico-bubble form-control-icon"></i>
              </div>
            </div>
            <div class="panel-heading"><h5 class="panel-title"><i class="ico-health mr5"></i>Latest Activity  {{ date('F-d')}}</h5></div>
            <!-- Media list feed -->
            @if(count($activities)>0)
            <ul class="media-list media-list-feed nm">
                @foreach($activities as $activity)
                <li class="media">
                    <div class="media-object pull-left">
                        {!! $activity->icon !!}
                    </div>
                    <div class="media-body">
                        <p class="media-heading">{{ $activity->title }}</p>
                        <p class="media-text"><span class="text-primary semibold">{{ $activity->description }}</span> by {{ $activity->user->my_name }}</p>
                        <p class="media-meta">{{ $activity->created_at }}</p>
                    </div>
                </li>
                @endforeach
            </ul>
            {{ $activities->appends($_GET)->links() }}
            @endif
            </div>
          </div>
        </div>
      </div><!-- tab contant -->
      </div>
    </div>
  </div>

  <div class="modal left fade application" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title" id="myModalLabel">Application</h4>
        </div>
        <div class="modal-body"></div>
        <!--div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          <button type="button" class="btn btn-primary">Save changes</button>
        </div-->
      </div>
    </div>
  </div>
@stop


@section('styles')
  <link rel="stylesheet" href="{{ asset('hfv1/plugins/sweetalert/dist/sweetalert.css') }}">
  <link rel="stylesheet" href="{{asset('bower_components/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css')}}" />
  <link rel="stylesheet" href="{{ asset('hfv1/plugins/bootstrap-datepicker/css/datepicker3.css') }}">
@stop
@section('scripts')
  <script src="{{ asset('hfv1/plugins/sweetalert/dist/sweetalert.min.js') }}"></script>
  <script type="text/javascript" src="{{asset('bower_components/moment/min/moment.min.js')}}"></script>
  <script type="text/javascript" src="{{asset('bower_components/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js')}}"></script>
  <script src="{{ asset('hfv1/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js') }}"></script>
  <script>
  var JobDetail = {
    init : function(){
      this.showApplication()
    },
    showApplication : function(){
      $('.application').on('show.bs.modal' ,function(e){

        var button = $(e.relatedTarget)
        var applicant = button.data('applicant')
        var modal = $(this)
        modal.find('.modal-body').children().remove()
        $.ajax({
          url : window.location.origin + '/internal-recruiter/fetch-applicantion/' + applicant,
          type : 'get',
          success : function(res){
            modal.find('.modal-body').append($(res))
            ApplicationDetail.init()
            ApplicationDetail.elemOptionStageShow($('#stage-options').val())
          },error : function(res){
            console.log(res);
          }
        })
        // e.preventDefault();
        // e.stopImmediatePropagation();
      });
    }
  }
  var ApplicationDetail = {
      init : function(){
        this.changeStatus();
        this.addComment();
        this.datePick();
        this.appointmentType();
        // this.setAppoiment();
        $('.stages').each(function(){
          var $this = $(this)
          ApplicationDetail.elemStageShow($this.attr('data-status'),$('.stages'));
        });
        $('.employerFb').each(function(){
          var $this = $(this)
          ApplicationDetail.elemStageShow($this.attr('data-employerfb'),$('.employerFb'));
        });
        // this.elemStageShow($('.stages').attr('data-status'),$('.stages'))
        // this.elemStageShow($('.employerFb').attr('data-employerfb'),$('.employerFb'))
        this.stages();
        $('[name="hire_date"]').datepicker({
          format:'YYYY-MM-DD'
        })
      },
      elemEmpHide : function(td){
        td.find('.sched_finalinterview').hide();
        td.find('.sched_validation').hide();
        td.find('.employer_reason').hide();
        td.find('.employer_notes').hide();
        td.find('.sched_joboffer').hide();
        td.find('.date_of_training').hide();
        td.find('.hire_date').hide();
        td.find('.hire_account').hide();
      },
      elemEmpStageShow: function(stage,td){
        switch (stage) {
          case '1':
            JobDetail.elemEmpHide(td)
            td.find('.employer_notes').show();
          break;
          case '2':
            JobDetail.elemEmpHide(td)
            td.find('.sched_finalinterview').show();
            td.find('.employer_notes').show();
            break;
          case '3':
            JobDetail.elemEmpHide(td)
            td.find('.employer_reason').show();
            break;
          case '4':
            JobDetail.elemEmpHide(td)
            td.find('.sched_validation').show();
            td.find('.employer_notes').show();
            break;
          case '5':
            JobDetail.elemEmpHide(td)
            td.find('.employer_reason').show();
            break;
          case '6':
            JobDetail.elemEmpHide(td)
            td.find('.sched_joboffer').show();
            td.find('.employer_notes').show();
            break;
          case '7':
            JobDetail.elemEmpHide(td)
            td.find('.employer_reason').show();
            break;
          case '8':
            JobDetail.elemEmpHide(td)
            td.find('.date_of_training').show();
            td.find('.hire_date').show();
            td.find('.hire_account').show();
            td.find('.notes').show();
            break;
          default:
            JobDetail.elemEmpHide(td)
          break;
        }
      },
      elemHide : function(td){
        td.find('.app_type').hide();
        td.find('.pick_datetime').hide();
        td.find('.attachment').hide();
        td.find('.reason').hide();
        td.find('.refer_premium').hide();
        td.find('.refer_regular').hide();
        td.find('.notes').hide();
        td.find('.hire_date').hide();
        td.find('.hire_account').hide();
      },
      elemStageShow: function(stage,td){
        switch (stage) {
          case '2':
            ApplicationDetail.elemHide(td)
            td.find('.app_type').show();
            td.find('.pick_datetime').show();
            td.find('.attachment').show();
            break;
          case '3':
            ApplicationDetail.elemHide(td)
            td.find('.app_type').show();
            td.find('.pick_datetime').show();
            td.find('.attachment').show();
            td.find('.refer_premium').show();
            break;
          case '4':
            ApplicationDetail.elemHide(td)
            td.find('.reason').show();
            break;
          case '5':
            ApplicationDetail.elemHide(td)
            td.find('.refer_regular').show();
            td.find('.reason').show();
            break;
          case '6':
            ApplicationDetail.elemHide(td)
            td.find('.notes').show();
            break;
          case '7':
            ApplicationDetail.elemHide(td)
            td.find('.app_type').show();
            td.find('.pick_datetime').show();
            td.find('.attachment').show();
            td.find('.notes').show();
            break;
          case '8':
            ApplicationDetail.elemHide(td)
            td.find('.hire_date').show();
            td.find('.hire_account').show();
            td.find('.notes').show();
            break;
          default:
            ApplicationDetail.elemHide(td)
          break;
        }
      },
      elemOptionHide : function(){
        $('.oapp_type').hide();
        $('.opick_datetime').hide();
        $('.oattachment').hide();
        $('.oreason').hide();
        $('.orefer_premium').hide();
        $('.orefer_regular').hide();
        $('.onotes').hide();
        $('.ohire_date').hide();
        $('.ohire_account').hide();
      },
      elemOptionStageShow: function(stage){
        switch (stage) {
          case '2':
            ApplicationDetail.elemOptionHide()
            $('.oapp_type').show();
            $('.opick_datetime').show();
            $('.oattachment').show();
            break;
          case '3':
            ApplicationDetail.elemOptionHide()
            $('.oapp_type').show();
            $('.opick_datetime').show();
            $('.oattachment').show();
            $('.orefer_premium').show();
            break;
          case '4':
            ApplicationDetail.elemOptionHide()
            $('.oreason').show();
            break;
          case '5':
            ApplicationDetail.elemOptionHide()
            $('.orefer_regular').show();
            $('.oreason').show();
            break;
          case '6':
            ApplicationDetail.elemOptionHide()
            $('.onotes').show();
            break;
          case '7':
            ApplicationDetail.elemOptionHide()
            $('.oapp_type').show();
            $('.opick_datetime').show();
            $('.oattachment').show();
            $('.onotes').show();
            break;
          case '8':
            ApplicationDetail.elemOptionHide()
            $('.ohire_date').show();
            $('.ohire_account').show();
            $('.onotes').show();
            break;
          default:
            ApplicationDetail.elemOptionHide()
          break;
        }
      },
      stages: function(){
        ApplicationDetail.elemOptionHide()
        $('#stage-options').change(function(){
          var stage = $(this).val();
          ApplicationDetail.elemOptionStageShow(stage);
        });
      },
      // setAppoiment: function(){
      //   $('#setAppoiment').click(function(e){
      //     var type = $('[name="my_hidden_type"]').val();
      //     var datetime = $('#my_hidden_input').val();
      //     var data = {
      //       'employer_appointment_sched' : datetime,
      //       'employer_appointment_type' : type
      //     }
      //     $.ajax({
      //       url : window.location.origin + '/employer/set-appointment',
      //       type : 'post',
      //       data : data,
      //       success : function(res){
      //
      //       },error : function(res){
      //
      //       }
      //     })
      //     e.preventDefault();
      //     e.stopImmediatePropagation();
      //   });
      // },
      appointmentType : function(){
        $('[name="employer_appointment_type"]').click(function(e){
          var txt = $(this).parent().find('span').text()
          var val = $(this).val()
          $('#appointment-type').text(txt.toUpperCase());
          $('[name="my_hidden_type"]').val(val);

        });
      },
      datePick : function(){
        $('#datetimepicker1').datetimepicker({
          inline: true,
          sideBySide: true
        });
        $('#datetimepicker1').on('dp.change', function(event) {
          //console.log(moment(event.date).format('MM/DD/YYYY h:mm a'));
          //console.log(event.date.format('MM/DD/YYYY h:mm a'));
          $('#selected-date').text(event.date);
          var formatted_date = event.date.format('MM/DD/YYYY h:mm a');
          var dt = moment(formatted_date, ["MM/DD/YYYY h:mm A"]).format("YYYY-MM-DD HH:mm:ss");
          $('#my_hidden_input').val(dt);
          $('#hidden-val').text(formatted_date);
        });
      },
      addComment : function(){
        $('#comment').keypress(function(e){
          var box = $(this);
          var comment =  box.val();
          var application_code = '{{ $applicant->auid }}';
          if(e.which == 13 && comment !='' && comment.length > 0){
            $.ajax({
              url : window.location.origin + '/internal-recruiter/applicant-comment',
              type : 'post',
              data : { 'comment': comment, 'application_code' : application_code},
              success : function(res){
                console.log(res);
                $('.media-list-feed').prepend($(ApplicationDetail.boxTemplate(res.activity)));
                swal('Sent!','Commented added in activities','success');
                box.val('');
              },error : function(res){
                console.log(res);
                swal('Opps!','Something went wrong','error');
              }
            });

          }
        });
      },
      boxTemplate: function(activity){
        return `<li class="media">
            <div class="media-object pull-left">
                ${activity.icon}
            </div>
            <div class="media-body">
                <p class="media-heading">${activity.title}</p>
                <p class="media-text">
                <span class="text-primary semibold">${activity.description}</span>
                by ${activity.created_by}</p>
                <p class="media-meta">${activity.created_at}</p>
            </div>
        </li>`
      },
      changeStatus: function(){
        $('.status').click(function(e){
          var option = $(this);
          var status = option.find('input').attr('data-status');
          var applicant_code = option.find('input').attr('data-applicantcode');
          console.log(applicant_code,status);
          $.ajax({
            url : window.location.origin + '/internal-recruiter/update-applicant-status',
            type : 'post',
            data : {'applicant_code' : applicant_code,
                    'status' : status},
            success : function(res){
              if(res.success){
                if(option.hasClass('hasChoices')){
                  $('#statuses').children().remove();
                  // $('#statuses').append(ApplicationDetail.statusTemplate(applicant_code));
                  $('#statustext').text('Current Status:'+res.status);
                }
                if(option.hasClass('lastChoice')){
                  $('#statuses').children().remove();
                  $('#statustext').text('Current Status:'+res.status);
                }
                swal('Updated!','You have change the status','success');
              }
            },
            error : function(res){
              console.log(res);
              swal('Opps!','Something went wrong','error');
            }
          });
          e.stopImmediatePropagation();
          e.preventDefault();
        });
      },
      statusTemplate: function(applicant_code){
        return  `
          <label class="btn btn-primary status lastChoice">
            <input type="radio" name="options4" id="option4" autocomplete="off" data-applicantcode="${applicant_code}" data-status="4"> No Show
          </label>
          <label class="btn btn-primary status lastChoice">
            <input type="radio" name="options5" id="option5" autocomplete="off" data-applicantcode="${applicant_code}" data-status="5"> Rejected
          </label>
          <label class="btn btn-primary status lastChoice">
            <input type="radio" name="options6" id="option6" autocomplete="off" data-applicantcode="${applicant_code}" data-status="6"> Hired
          </label>
        `;
      }
  }
  $(function(){
    ApplicationDetail.init()
    JobDetail.init()
  });
  </script>
@stop
