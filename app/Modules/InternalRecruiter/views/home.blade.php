@extends('Dashboard::template.main')

@section('content')
  <!-- Page Header -->
  <div class="page-header page-header-block">
      <div class="page-header-section">
          <h4 class="title semibold">HF Recruiter Dashboard</h4>
      </div>
      <div class="page-header-section">
          <!-- Toolbar -->
          <div class="toolbar clearfix">

          </div>
          <!--/ Toolbar -->
      </div>
  </div>
  <!-- Page Header -->
  <div class="row">
      <div class="col-md-12">
          <!-- START panel -->

          <div class="panel panel-primary">
            @if(count($applicants)>0)
              <!-- panel heading/header -->
              <div class="panel-heading">
                  <h3 class="panel-title"><span class="panel-icon mr5"><i class="ico-table22"></i></span> Applicants</h3>
                  <!-- panel toolbar -->
                  <div class="panel-toolbar text-right">
                  </div>
                  <!--/ panel toolbar -->
              </div>
              <!--/ panel heading/header -->
              <!-- panel toolbar wrapper -->

              <div class="panel-toolbar-wrapper pl0 pt5 pb5">
                  <div class="panel-toolbar text-right">
                      <a href="{{route('internal-recruiter.home')}}?{{isset($_GET['q'])?'q='.$_GET['q'].'&':''}}status=pipeline" class="btn btn-sm btn-default">Pipeline</a>
                      <a href="{{route('internal-recruiter.home')}}?{{isset($_GET['q'])?'q='.$_GET['q'].'&':''}}status=endorsed" class="btn btn-sm btn-default">Endorsed</a>
                      <a href="{{route('internal-recruiter.home')}}?{{isset($_GET['q'])?'q='.$_GET['q'].'&':''}}status=re-profile-to-premium-job-post" class="btn btn-sm btn-default">Re profile to Premium Job post</a>
                      <a href="{{route('internal-recruiter.home')}}?{{isset($_GET['q'])?'q='.$_GET['q'].'&':''}}status=active-file" class="btn btn-sm btn-default">Active File</a>
                      <a href="{{route('internal-recruiter.home')}}?{{isset($_GET['q'])?'q='.$_GET['q'].'&':''}}status=refer-to-regular-job-post" class="btn btn-sm btn-default">Refer to Regular Job Post</a>
                      <a href="{{route('internal-recruiter.home')}}?{{isset($_GET['q'])?'q='.$_GET['q'].'&':''}}status=no-show" class="btn btn-sm btn-default">No Show</a>
                      <a href="{{route('internal-recruiter.home')}}?{{isset($_GET['q'])?'q='.$_GET['q'].'&':''}}status=re-schedule" class="btn btn-sm btn-default">Re schedule</a>
                      <a href="{{route('internal-recruiter.home')}}?{{isset($_GET['q'])?'q='.$_GET['q'].'&':''}}status=hired" class="btn btn-sm btn-default">Hired</a>
                  </div>
              </div>
              <!--/ panel toolbar wrapper -->

              <!-- panel body with collapse capabale -->
              <div class="table-responsive pull out">
                  <table class="table table-bordered table-hover" id="table1">
                      <thead>
                          <tr>
                              <th width="5%"></th>
                              <th width="20%">Name</th>
                              <th>Applied</th>
                              <th width="25%">HF Status</th>
                              <th width="25%">Employer Feedback</th>
                              <th width="3%"></th>
                          </tr>
                      </thead>
                      <tbody>
                        @foreach($applicants as $applicant)
                          <tr>
                              <td><div class="media-object"><img src="{{ $applicant->applicant->user->profile_pic}}" alt="" class="img-circle"></div>
                              </td>
                              <td>{{ $applicant->applicant->user->myname }} <br>
                                  <p class="no-margin text-muted"><small>CODE: {{ $applicant->application_code }}</small></p>
                                  <small>{{ $applicant->applicant->full_address}}</small>
                              </td>
                              <td>
                                {{ $applicant->job->job_title}}
                                <br>
                                {{ $applicant->job->designation}}
                              </td>
                              <td class="stages" data-status="{{ $applicant->applicant_status_id }}">
                                <p><strong>Current Status :</strong> {{ $applicant->applicant_status->name }} </p>
                                @if($applicant->employer_appointment_type)
                                <p class="app_type"><strong>Appointments :</strong> {{ ucwords(str_replace('-',' ',$applicant->employer_appointment_type)) }} | {{ $applicant->employer_appointment_sched }} </p>
                                @endif
                                @if($applicant->endorsement_slip)
                                <p class="attachment"><strong>Edorsement Slip :</strong> <i class="fa fa-file"></i> {{ str_replace(url('/').'/endorsement-slips-media/','',$applicant->endorsement_slip) }} </p>
                                @endif
                                @if($applicant->hire_date && $applicant->hire_date !== '0000-00-00')
                                <p class="hire_date"><strong>Hire Date :</strong> {{ $applicant->hire_date }} </p>
                                @endif
                                @if($applicant->hire_account)
                                <p class="hire_account"><strong>Account :</strong> {{ $applicant->hire_account }} </p>
                                @endif
                                @if($applicant->reason)
                                <p class="reason"><strong>Reason :</strong> {{ $applicant->reason }} </p>
                                @endif
                                @if($applicant->note)
                                <p class="notes"><strong>Note :</strong> {{ $applicant->note }} </p>
                                @endif
                              </td>
                              <td class="employerFb" data-employerfb="{{ $applicant->employer_applicant_status_id }}">
                                <p><strong>Applicant Feedback Status :</strong>  {{ ($applicant->employer_applicant_status)? $applicant->employer_applicant_status->name:'No Status' }} </p>
                                @if($applicant->employerFeedback)
                                  @if($sched_final = $applicant->employerFeedback->schedule_final_interview)
                                  <p class="sched_finalinterview"><strong>Final Interview :</strong>  {{ $sched_final }} </p>
                                  @endif
                                  @if($applicant->employerFeedback->reason)
                                  <p class="employer_reason"><strong>Reason :</strong> {{ $applicant->employerFeedback->reason }}</p>
                                  @endif
                                  @if($applicant->employerFeedback->notes)
                                  <p class="employer_notes"><strong>Notes :</strong> {{ $applicant->employerFeedback->notes }}</p>
                                  @endif
                                  @if($sched_valid = $applicant->employerFeedback->schedule_account_validation)
                                  <p class="sched_validation"><strong>Account Validation :</strong>  {{ $sched_valid }}</p>
                                  @endif
                                  @if($sched_offer = $applicant->employerFeedback->schedule_job_offer)
                                  <p class="sched_joboffer"><strong>Job Offer :</strong>  {{ $sched_offer }}</p>
                                  @endif
                                  @if($sched_training = $applicant->date_of_training)
                                  <p class="date_of_training"><strong>Date Training :</strong>  {{ $sched_training }}</p>
                                  @endif
                                  @if($hire_acc = $applicant->hire_account)
                                  <p class="hire_account"><strong>Hire Account :</strong>  {{ $hire_acc }}</p>
                                  @endif
                                  @if($hire_date = $applicant->hire_date)
                                  <p class="hire_date"><strong>Hire Date :</strong>  {{ $hire_date }}</p>
                                  @endif
                                @endif
                              </td>
                              <td class="text-center">
                                  <a href="{{ route('internal-recruiter.applicant.details',['application_code'  => $applicant->auid]) }}" class="btn btn-sm btn-default"target="_blank"><i class="ico-profile"></i></a>
                              </td>
                          </tr>
                          @endforeach
                      </tbody>
                  </table>
              </div>
              <!--/ panel body with collapse capabale -->
              {{ $applicants->appends($_GET)->links() }}
              @else
                <div class="panel-footer">
                  <div class="alert alert-info">
                    <strong>No data</strong>
                  </div>
                </div>
              @endif
          </div>
      </div>
  </div>
@stop
