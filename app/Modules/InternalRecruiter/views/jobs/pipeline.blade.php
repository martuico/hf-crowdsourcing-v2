@extends('Dashboard::template.main')

@section('content')
  <!-- Page Header -->
  <div class="page-header page-header-block">
      <div class="page-header-section">
          <h4 class="title semibold">Job Detail - {{ $job->job_code }}</h4>
      </div>
      <div class="page-header-section">
          <!-- Toolbar -->
          <div class="toolbar clearfix text-right">
              <a href="{{route('internal-recruiter.jobs')}}" class="btn btn-sm btn-default"  data-toggle="tooltip" data-placement="bottom" title="Back to careers"><i class="fa fa-chevron-left"></i> Back to Listings</a>
              <a href="{{ route('jobs.details',['job_title' => $job->slug,
                                                'job_code' => $job->job_code]) }}" class="btn btn-default btn-sm"><i class="fa fa-list"></i> Job Details</a>
          </div>
          <!--/ Toolbar -->
      </div>
  </div>
  <div class="row">
    <div class="col-md-12">
      <div class="widget panel">
          <!-- panel body -->
          <div class="panel-body">
              <ul class="list-table">
                  <li class="text-left">
                      <h5 class="semibold ellipsis">
                          {{ $job->job_title }}<br>
                          <small class="text-muted">Created {{ $job->created_at}} - Valid Until: {{ $job->valid_until }}</small>
                      </h5>
                  </li>
                  <li class="text-right"><button type="button" class="btn btn-info">{{ strtoupper($job->status) }}</button></li>
              </ul>
              <!-- Nav section -->
              <ul class="nav nav-section nav-justified mt15">
                  <li>
                      <div class="section">
                          <h4 class="nm">{{ $applicants->count() }}</h4>
                          <p class="nm text-muted">Applicants</p>
                      </div>
                  </li>
                  <li>
                      <div class="section">
                          <h4 class="nm">{{ $endorsed_count }}</h4>
                          <p class="nm text-muted">Endorsed</p>
                      </div>
                  </li>
                  <li>
                      <div class="section">
                          <h4 class="nm">{{ $hired_count }}</h4>
                          <p class="nm text-muted">Hired</p>
                      </div>
                  </li>
              </ul>
              <!--/ Nav section -->
          </div>
          <!--/ panel body -->
      </div>
    </div><!-- col-md-12 -->
  </div><!-- row -->

  <div class="row">
    <div class="col-md-12">
      <div class="panel panel-default">
        <!-- panel heading/header -->
        <div class="panel-heading">
            <h3 class="panel-title">Applicants</h3>
            <!-- panel toolbar -->
            <div class="panel-toolbar text-right">
              <!-- Toolbar -->
              <div class="toolbar clearfix text-right">
                {{ \Form::open(['method' => 'GET']) }}
                  <div class="input-group">
                        <input type="text" name="q" class="form-control" placeholder="Search Applicant">
                        <span class="input-group-btn">
                          <button class="btn btn-default" type="submit">Go!</button>
                          <a class="btn btn-danger" href="{{route('employer.applicants')}}">Clear Filter</a>
                        </span>
                  </div><!-- /input-group -->
                {{ \Form::close() }}
              </div>
              <!--/ Toolbar -->
            </div>
            <!--/ panel toolbar -->
        </div>
        <div class="panel-toolbar-wrapper pl0 pt5 pb5">
            <div class="panel-toolbar text-right">

                <a href="{{route('internal-recruiter.home')}}?{{isset($_GET['q'])?'q='.$_GET['q'].'&':''}}status=pipeline" class="btn btn-sm btn-default">Pipeline</a>
                <a href="{{route('internal-recruiter.home')}}?{{isset($_GET['q'])?'q='.$_GET['q'].'&':''}}status=endorsed" class="btn btn-sm btn-default">Endorsed</a>
                <a href="{{route('internal-recruiter.home')}}?{{isset($_GET['q'])?'q='.$_GET['q'].'&':''}}status=re-profile-to-premium-job-post" class="btn btn-sm btn-default">Re profile to Premium Job post</a>
                <a href="{{route('internal-recruiter.home')}}?{{isset($_GET['q'])?'q='.$_GET['q'].'&':''}}status=active-file" class="btn btn-sm btn-default">Active File</a>
                <a href="{{route('internal-recruiter.home')}}?{{isset($_GET['q'])?'q='.$_GET['q'].'&':''}}status=refer-to-regular-job-post" class="btn btn-sm btn-default">Refer to Regular Job Post</a>
                <a href="{{route('internal-recruiter.home')}}?{{isset($_GET['q'])?'q='.$_GET['q'].'&':''}}status=no-show" class="btn btn-sm btn-default">No Show</a>
                <a href="{{route('internal-recruiter.home')}}?{{isset($_GET['q'])?'q='.$_GET['q'].'&':''}}status=re-schedule" class="btn btn-sm btn-default">Re schedule</a>
                <a href="{{route('internal-recruiter.home')}}?{{isset($_GET['q'])?'q='.$_GET['q'].'&':''}}status=hired" class="btn btn-sm btn-default">Hired</a>
            </div>
        </div>
        <!--/ panel heading/header -->
        <!-- panel body with collapse capabale -->
        <div class="table-responsive panel-collapse pull out">
          @if(count($applicants)>0)
            <table class="table table-bordered table-condensed table-hover">
                <thead>
                    <tr>
                        <th>Name</th>
                        <th>HF Status</th>
                        <th>Employer Feedback</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($applicants as $applicant)
                    <tr>
                        <td>{{ ($applicant->applicant)?$applicant->applicant->user->my_name:'Err' }}</td>
                        <td class="stages" data-status="{{ $applicant->applicant_status_id }}">
                          <p><strong>Current Status :</strong> {{ $applicant->applicant_status->name }} </p>
                          @if($applicant->employer_appointment_type)
                          <p class="app_type"><strong>Appointments :</strong> {{ ucwords(str_replace('-',' ',$applicant->employer_appointment_type)) }} | {{ $applicant->employer_appointment_sched }} </p>
                          @endif
                          @if($applicant->endorsement_slip)
                          <p class="attachment"><strong>Endorsement Slip :</strong> <i class="fa fa-file"></i> <a href="{{ route('download.endorsemmentslip',['application_code' => $applicant->application_code]) }}"> Download here</a> </p>
                          @endif
                          @if($applicant->hire_date && $applicant->hire_date !== '0000-00-00')
                          <p class="hire_date"><strong>Hire Date :</strong> {{ $applicant->hire_date }} </p>
                          @endif
                          @if($applicant->hire_account)
                          <p class="hire_account"><strong>Account :</strong> {{ $applicant->hire_account }} </p>
                          @endif
                          @if($applicant->reason)
                          <p class="reason"><strong>Reason :</strong> {{ $applicant->reason }} </p>
                          @endif
                          @if($applicant->note)
                          <p class="notes"><strong>Note :</strong> {{ $applicant->note }} </p>
                          @endif
                        </td>
                        <td class="employerFb" data-employerfb="{{ $applicant->employer_applicant_status_id }}">
                          <p><strong>Applicant Feedback Status :</strong>  {{ ($applicant->employer_applicant_status)? $applicant->employer_applicant_status->name:'No Status' }} </p>
                          @if($applicant->employerFeedback)
                            @if($sched_final = $applicant->employerFeedback->schedule_final_interview)
                            <p class="sched_finalinterview"><strong>Final Interview :</strong>  {{ $sched_final }} </p>
                            @endif
                            @if($applicant->employerFeedback->reason)
                            <p class="employer_reason"><strong>Reason :</strong> {{ $applicant->employerFeedback->reason }}</p>
                            @endif
                            @if($applicant->employerFeedback->notes)
                            <p class="employer_notes"><strong>Notes :</strong> {{ $applicant->employerFeedback->notes }}</p>
                            @endif
                            @if($sched_valid = $applicant->employerFeedback->schedule_account_validation)
                            <p class="sched_validation"><strong>Account Validation :</strong>  {{ $sched_valid }}</p>
                            @endif
                            @if($sched_offer = $applicant->employerFeedback->schedule_job_offer)
                            <p class="sched_joboffer"><strong>Job Offer :</strong>  {{ $sched_offer }}</p>
                            @endif
                            @if($sched_training = $applicant->date_of_training)
                            <p class="date_of_training"><strong>Date Training :</strong>  {{ $sched_training }}</p>
                            @endif
                            @if($hire_acc = $applicant->hire_account)
                            <p class="hire_account"><strong>Hire Account :</strong>  {{ $hire_acc }}</p>
                            @endif
                            @if($hire_date = $applicant->hire_date)
                            <p class="hire_date"><strong>Hire Date :</strong>  {{ $hire_date }}</p>
                            @endif
                          @endif
                        </td>
                        <td>
                          <button class="btn btn-default btn-xs" data-toggle="modal" data-target=".application" data-applicant="{{ $applicant->application_code }}">
                            <i class="fa fa-file"></i>
                          </button>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
            {{ $applicants->links() }}
            @else
            <div class="panel-body">
              <div class="alert alert-info">
                <strong>No Data</strong>
              </div>
            </div>
            @endif
        </div>
        <!--/ panel body with collapse capabale -->
    </div>
    </div>
  </div>
  <div class="modal left fade application" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title" id="myModalLabel">Application</h4>
        </div>
        <div class="modal-body"></div>
        <!--div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          <button type="button" class="btn btn-primary">Save changes</button>
        </div-->
      </div>
    </div>
  </div>
@stop


@section('styles')
  <link rel="stylesheet" href="{{ asset('hfv1/plugins/sweetalert/dist/sweetalert.css') }}">
  <link rel="stylesheet" href="{{asset('bower_components/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css')}}" />
  <link rel="stylesheet" href="{{ asset('hfv1/plugins/bootstrap-datepicker/css/datepicker3.css') }}">
@stop
@section('scripts')
  <script src="{{ asset('hfv1/plugins/sweetalert/dist/sweetalert.min.js') }}"></script>
  <script type="text/javascript" src="{{asset('bower_components/moment/min/moment.min.js')}}"></script>
  <script type="text/javascript" src="{{asset('bower_components/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js')}}"></script>
  <script src="{{ asset('hfv1/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js') }}"></script>
  <script>
  var JobDetail = {
    init : function(){
      this.showApplication()
    },
    showApplication : function(){
      $('.application').on('show.bs.modal' ,function(e){

        var button = $(e.relatedTarget)
        var applicant = button.data('applicant')
        var modal = $(this)
        modal.find('.modal-body').children().remove()
        $.ajax({
          url : window.location.origin + '/internal-recruiter/fetch-applicantion/' + applicant,
          type : 'get',
          success : function(res){
            modal.find('.modal-body').append($(res))
            ApplicationDetail.init()
            ApplicationDetail.elemOptionStageShow($('#stage-options').val())
          },error : function(res){
            console.log(res);
          }
        })
        // e.preventDefault();
        // e.stopImmediatePropagation();
      });
    }
  }
  var ApplicationDetail = {
      init : function(){
        this.changeStatus();
        this.addComment();
        this.datePick();
        this.appointmentType();
        // this.setAppoiment();
        this.elemStageShow($('.stages').attr('data-status'),$('.stages'))
        this.elemStageShow($('.employerFb').attr('data-status'),$('.employerFb'))
        this.stages();
        $('[name="hire_date"]').datepicker({
          format:'YYYY-MM-DD'
        })
      },
      elemEmpHide : function(td){
        td.find('.sched_finalinterview').hide();
        td.find('.sched_validation').hide();
        td.find('.employer_reason').hide();
        td.find('.employer_notes').hide();
        td.find('.sched_joboffer').hide();
        td.find('.date_of_training').hide();
        td.find('.hire_date').hide();
        td.find('.hire_account').hide();
      },
      elemEmpStageShow: function(stage,td){
        switch (stage) {
          case '1':
            JobDetail.elemEmpHide(td)
            td.find('.employer_notes').show();
          break;
          case '2':
            JobDetail.elemEmpHide(td)
            td.find('.sched_finalinterview').show();
            td.find('.employer_notes').show();
            break;
          case '3':
            JobDetail.elemEmpHide(td)
            td.find('.employer_reason').show();
            break;
          case '4':
            JobDetail.elemEmpHide(td)
            td.find('.sched_validation').show();
            td.find('.employer_notes').show();
            break;
          case '5':
            JobDetail.elemEmpHide(td)
            td.find('.employer_reason').show();
            break;
          case '6':
            JobDetail.elemEmpHide(td)
            td.find('.sched_joboffer').show();
            td.find('.employer_notes').show();
            break;
          case '7':
            JobDetail.elemEmpHide(td)
            td.find('.employer_reason').show();
            break;
          case '8':
            JobDetail.elemEmpHide(td)
            td.find('.date_of_training').show();
            td.find('.hire_date').show();
            td.find('.hire_account').show();
            td.find('.notes').show();
            break;
          default:
            JobDetail.elemEmpHide(td)
          break;
        }
      },
      elemHide : function(td){
        td.find('.app_type').hide();
        td.find('.pick_datetime').hide();
        td.find('.attachment').hide();
        td.find('.reason').hide();
        td.find('.refer_premium').hide();
        td.find('.refer_regular').hide();
        td.find('.notes').hide();
        td.find('.hire_date').hide();
        td.find('.hire_account').hide();
      },
      elemStageShow: function(stage,td){
        switch (stage) {
          case '2':
            ApplicationDetail.elemHide(td)
            td.find('.app_type').show();
            td.find('.pick_datetime').show();
            td.find('.attachment').show();
            break;
          case '3':
            ApplicationDetail.elemHide(td)
            td.find('.app_type').show();
            td.find('.pick_datetime').show();
            td.find('.attachment').show();
            td.find('.refer_premium').show();
            break;
          case '4':
            ApplicationDetail.elemHide(td)
            td.find('.reason').show();
            break;
          case '5':
            ApplicationDetail.elemHide(td)
            td.find('.refer_regular').show();
            td.find('.reason').show();
            break;
          case '6':
            ApplicationDetail.elemHide(td)
            td.find('.notes').show();
            break;
          case '7':
            ApplicationDetail.elemHide(td)
            td.find('.app_type').show();
            td.find('.pick_datetime').show();
            td.find('.attachment').show();
            td.find('.notes').show();
            break;
          case '8':
            ApplicationDetail.elemHide(td)
            td.find('.hire_date').show();
            td.find('.hire_account').show();
            td.find('.notes').show();
            break;
          default:
            ApplicationDetail.elemHide(td)
          break;
        }
      },
      elemOptionHide : function(){
        $('.oapp_type').hide();
        $('.opick_datetime').hide();
        $('.oattachment').hide();
        $('.oreason').hide();
        $('.orefer_premium').hide();
        $('.orefer_regular').hide();
        $('.onotes').hide();
        $('.ohire_date').hide();
        $('.ohire_account').hide();
      },
      elemOptionStageShow: function(stage){
        console.log(stage);
        switch (stage) {
          case '2':
            ApplicationDetail.elemOptionHide()
            $('.oapp_type').show();
            $('.opick_datetime').show();
            $('.oattachment').show();
            break;
          case '3':
            ApplicationDetail.elemOptionHide()
            $('.oapp_type').show();
            $('.opick_datetime').show();
            $('.oattachment').show();
            $('.orefer_premium').show();
            break;
          case '4':
            ApplicationDetail.elemOptionHide()
            $('.oreason').show();
            break;
          case '5':
            ApplicationDetail.elemOptionHide()
            $('.orefer_regular').show();
            $('.oreason').show();
            break;
          case '6':
            ApplicationDetail.elemOptionHide()
            $('.onotes').show();
            break;
          case '7':
            ApplicationDetail.elemOptionHide()
            $('.oapp_type').show();
            $('.opick_datetime').show();
            $('.oattachment').show();
            $('.onotes').show();
            break;
          case '8':
            ApplicationDetail.elemOptionHide()
            $('.ohire_date').show();
            $('.ohire_account').show();
            $('.onotes').show();
            break;
          default:
            ApplicationDetail.elemOptionHide()
          break;
        }
      },
      stages: function(){
        ApplicationDetail.elemOptionHide()
        $('#stage-options').change(function(){
          var stage = $(this).val();
          ApplicationDetail.elemOptionStageShow(stage);
        });
      },
      // setAppoiment: function(){
      //   $('#setAppoiment').click(function(e){
      //     var type = $('[name="my_hidden_type"]').val();
      //     var datetime = $('#my_hidden_input').val();
      //     var data = {
      //       'employer_appointment_sched' : datetime,
      //       'employer_appointment_type' : type
      //     }
      //     $.ajax({
      //       url : window.location.origin + '/employer/set-appointment',
      //       type : 'post',
      //       data : data,
      //       success : function(res){
      //
      //       },error : function(res){
      //
      //       }
      //     })
      //     e.preventDefault();
      //     e.stopImmediatePropagation();
      //   });
      // },
      appointmentType : function(){
        $('[name="employer_appointment_type"]').click(function(e){
          var txt = $(this).parent().find('span').text()
          var val = $(this).val()
          $('#appointment-type').text(txt.toUpperCase());
          $('[name="my_hidden_type"]').val(val);

        });
      },
      datePick : function(){
        $('#datetimepicker1').datetimepicker({
          inline: true,
          sideBySide: true
        });
        $('#datetimepicker1').on('dp.change', function(event) {
          //console.log(moment(event.date).format('MM/DD/YYYY h:mm a'));
          //console.log(event.date.format('MM/DD/YYYY h:mm a'));
          $('#selected-date').text(event.date);
          var formatted_date = event.date.format('MM/DD/YYYY h:mm a');
          var dt = moment(formatted_date, ["MM/DD/YYYY h:mm A"]).format("YYYY-MM-DD HH:mm:ss");
          $('#my_hidden_input').val(dt);
          $('#hidden-val').text(formatted_date);
        });
      },
      addComment : function(){
        $('#comment').keypress(function(e){
          var box = $(this);
          var comment =  box.val();
          var application_code = '{{ $applicant->auid }}';
          if(e.which == 13 && comment !='' && comment.length > 0){
            $.ajax({
              url : window.location.origin + '/internal-recruiter/applicant-comment',
              type : 'post',
              data : { 'comment': comment, 'application_code' : application_code},
              success : function(res){
                console.log(res);
                $('.media-list-feed').prepend($(ApplicationDetail.boxTemplate(res.activity)));
                swal('Sent!','Commented added in activities','success');
                box.val('');
              },error : function(res){
                console.log(res);
                swal('Opps!','Something went wrong','error');
              }
            });

          }
        });
      },
      boxTemplate: function(activity){
        return `<li class="media">
            <div class="media-object pull-left">
                ${activity.icon}
            </div>
            <div class="media-body">
                <p class="media-heading">${activity.title}</p>
                <p class="media-text">
                <span class="text-primary semibold">${activity.description}</span>
                by ${activity.created_by}</p>
                <p class="media-meta">${activity.created_at}</p>
            </div>
        </li>`
      },
      changeStatus: function(){
        $('.status').click(function(e){
          var option = $(this);
          var status = option.find('input').attr('data-status');
          var applicant_code = option.find('input').attr('data-applicantcode');
          console.log(applicant_code,status);
          $.ajax({
            url : window.location.origin + '/internal-recruiter/update-applicant-status',
            type : 'post',
            data : {'applicant_code' : applicant_code,
                    'status' : status},
            success : function(res){
              if(res.success){
                if(option.hasClass('hasChoices')){
                  $('#statuses').children().remove();
                  // $('#statuses').append(ApplicationDetail.statusTemplate(applicant_code));
                  $('#statustext').text('Current Status:'+res.status);
                }
                if(option.hasClass('lastChoice')){
                  $('#statuses').children().remove();
                  $('#statustext').text('Current Status:'+res.status);
                }
                swal('Updated!','You have change the status','success');
              }
            },
            error : function(res){
              console.log(res);
              swal('Opps!','Something went wrong','error');
            }
          });
          e.stopImmediatePropagation();
          e.preventDefault();
        });
      },
      statusTemplate: function(applicant_code){
        return  `
          <label class="btn btn-primary status lastChoice">
            <input type="radio" name="options4" id="option4" autocomplete="off" data-applicantcode="${applicant_code}" data-status="4"> No Show
          </label>
          <label class="btn btn-primary status lastChoice">
            <input type="radio" name="options5" id="option5" autocomplete="off" data-applicantcode="${applicant_code}" data-status="5"> Rejected
          </label>
          <label class="btn btn-primary status lastChoice">
            <input type="radio" name="options6" id="option6" autocomplete="off" data-applicantcode="${applicant_code}" data-status="6"> Hired
          </label>
        `;
      }
  }
  $(function(){
    ApplicationDetail.init()
    JobDetail.init()
  });
  </script>
@stop
