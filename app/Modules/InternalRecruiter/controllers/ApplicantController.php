<?php namespace App\Modules\InternalRecruiter\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Traits\ImageUpload;

use App\IRecruiter,
    App\User,
    App\ApplicantJob,
    App\ApplicantJobActivity,
    App\Job,
    App\JobIndustry,
    App\ApplicantEducationLevel,
    App\Region,
    App\Province,
    App\City,
    App\ApplicantEducation,
    App\ApplicantExperience,
    App\ApplicantActivity,
    App\Applicant;

class ApplicantController extends Controller
{
    use ImageUpload;

    protected $irecruiter,$user,$applicant_job,$jobs,$applicant,$applicantActivity;
    protected $applicantJobActivity;

    public function __construct(IRecruiter $irecruiter,
                                User $user,
                                ApplicantJob $applicant_job,
                                ApplicantJobActivity $applicantJobActivity,
                                Job $jobs,
                                JobIndustry $jindustries,
                                ApplicantEducation $appEducation,
                                ApplicantEducationLevel $appEduLevel,
                                ApplicantExperience $appExperience,
                                Region $location_region,
                                Province $location_province,
                                City $location_city,
                                Applicant $applicant,
                                ApplicantActivity $applicantActivity)
    {
        $this->irecruiter = $irecruiter;
        $this->user = $user;
        $this->applicant_job = $applicant_job;
        $this->jobs = $jobs;
        $this->applicant = $applicant;
        $this->applicantJobActivity = $applicantJobActivity;
        $this->applicantActivity = $applicantActivity;
        $this->jindustries = $jindustries;
        $this->location_region = $location_region;
        $this->location_province = $location_province;
        $this->location_city = $location_city;
        $this->appEducation = $appEducation;
        $this->appEduLevel = $appEduLevel;
        $this->appExperience = $appExperience;
    }

    public function index()
    {
        $value = (\Request::has('q'))?\Request::get('q'):'';
        $applicants = $this->applicant->query();
        $applicants->where('hf_assigned','=',auth()->user()->id);
        if(\Request::has('q')){
            $applicants->whereHas('user',function($query) use ($value){
                $query->whereRaw('(firstname like ? OR lastname like ? OR email like ?)',
                            ['%'.$value.'%','%'.$value.'%','%'.$value.'%','%'.$value.'%']);
            });
        }
        $applicants->orderBy('created_at','asc');
	    $applicants = $applicants->paginate(50);
		return view('InternalRecruiter::applicants.index')
  							->with(compact('applicants'));
    }

    public function show($auid)
    {
          $applicant = $this->applicant
                              ->whereAuid($auid)
                              ->first();
          $applied_jobs = $applicant->appliedJobs()
                                    ->orderBy('created_at','asc')
                                    ->paginate(50);

          $activities = $this->applicantActivity
                              ->where('applicant_id','=',$applicant->id)
                              ->orderBy('created_at','asc')
                              ->paginate(50);
          if(\Session::get('viewing-application') !== $auid){
              $this->applicantActivity->create(['applicant_id' => $applicant->id,
                                           'user_id' => auth()->user()->id,
                                           'icon' => '<i class="ico-eye-open bgcolor-success"></i>',
                                           'title' => 'Viewing Application',
                                           'description' => 'Viewed applicant profile ('.$auid.')']);
              \Session::put('viewing-application',$auid);
          }
          return view('InternalRecruiter::applicants.show')
                          ->with(compact('applicant','applied_jobs','activities'));
    }

    public function fetchApplication($code)
    {
        $application = $this->applicant_job
                            ->whereApplicationCode($code)
                            ->first();
        return view('InternalRecruiter::applicants.applied-job')
                        ->with(compact('application'));
    }

    public function applicantActivityComment(Request $request)
    {
        if($request->ajax()){
            $auid = $request->get('application_code');
            $applicant = $this->applicant->where('auid','=',$auid)->first();

            // $application = $this->applicantActivity
            //                     ->whereApplicantId($applicant->id)
            //                     ->first();
            if(count($applicant)<1){
                return response()->json(['error' => 'opps!'],404);
            }
            $activity = $this->applicantActivity
                             ->create(['applicant_id' => $applicant->id,
                                     'user_id' => auth()->user()->id,
                                     'icon' => '<i class="ico-mail bgcolor-success"></i>',
                                     'title' => 'Commented Application',
                                      'description' =>  $request->get('comment').' ('.$auid.')']);
            $activity->created_by = auth()->user()->my_name;
            return response()->json(['success' => true,
                                     'activity' => $activity],200);
        }

    }

    public function updateApplicantStatus(Request $request)
    {
        if($request->ajax()){
            $applicant = $this->applicant_job
                                ->whereApplicationCode($request->get('applicant_code'))
                                ->first();
            if(count($applicant)<1){
                return response()->json(['error' => true],403);
            }
            if($request->has('status')){
              $up = $applicant->update(['applicant_status_id' => $request->get('status')]);
            }
            $status = $applicant->applicant_status->name;
            return response()->json(['success' => $up,
                                      'status' => $status],200);
        }
    }

    public function updateStages(Request $request,$applicant_code)
    {
        // return dd($request->has('refer_premium'));
        $inputs = $request->only('applicant_status_id',
                            'employer_appointment_type',
                             'employer_appointment_sched',
                             'reason',
                             'refer_premium',
                             'refer_regular',
                             'notes',
                             'hire_date',
                             'hire_account');
        $application = $this->applicant_job
                            ->whereApplicationCode($applicant_code)
                            ->first();
        $stat = \App\ApplicantStatus::find($inputs['applicant_status_id']);
        $comment = 'Change stages status '.$stat->name. 'on job '.$application->job->job_title;

        if(count($application)<1){
            return redirect()->back();
        }
        $check_refer = $request->has('refer_premium') OR $request->has('refer_regular');
        $job = $application->job;
        if($check_refer){
            $job_code = ($request->has('refer_premium'))?
                            $request->get('refer_premium'):
                            $request->get('refer_regular');

            $job = $this->jobs->whereJobCode($job_code)
                       ->first();
            if(count($job)<1){
                return redirect()
                            ->back();
            }
            $inputs['job_id'] = $job->id;
            $inputs['employer_id'] = $job->employer_id;
            $inputs['applicant_status_id'] = 1;
        }

        if($request->hasFile('endorsement_slip')){
            $inputs['endorsement_slip'] = $this->uploadEndorsementSlip($request,
                                                $applicant_code,
                                                $application);
        }
        $inputs = array_filter($inputs);
        $application->update($inputs);

        $activity = $this->applicantActivity
                         ->create(['applicant_id' => $application->applicant_id,
                                 'user_id' => auth()->user()->id,
                                 'icon' => '<i class="ico-mail bgcolor-success"></i>',
                                 'title' => 'Stages Update',
                                  'description' =>  $comment.' ('.$application->application_code.')']);

        $email_inputs['subject'] = 'Candidate status update '.$stat->name;
        $email_inputs['hf_status'] = $stat->name;
        $email_inputs['employer_email'] =  $job->employer->user->email;
        $email_inputs['employer'] = $job->employer->company_name;
        $email_inputs['applicant_name'] = $application->applicant->user->my_name;
        $email_inputs['applicant_email'] = $application->applicant->user->email;
        $email_inputs['application_code'] = $application->application_code;
        $email_inputs['job_title'] = $job->job_title;
        $email_inputs['job_title_slug'] = $job->slug;
        $email_inputs['job_code'] = $job->job_code;

        $arr_stat = hfStages($inputs['applicant_status_id']);
        foreach($arr_stat as $email_stat){
            $email_inputs[$email_stat] = $application->$email_stat;
        }

        $when = Carbon\Carbon::now()->addMinutes(2);
        // notify employer for endorse, active file, no show, reschedule, hired
        $notify_employer = ['2','6','7','8'];
        if(in_array($inputs['applicant_status_id'],$notify_employer)){
            \Mail::later($when,new NotifyEmployerHFstatus($email_inputs));
        }
        return redirect()->back()
                          ->with(['successMessage' => 'Updated Status']);
        //return redirect()->route('admin.careers.applicant.details',['job_code' => $application->job->job_code,'application_code' => $applicant_code]);
    }

    protected function uploadEndorsementSlip($request,$applicant_code,$application)
    {
      if($request->hasFile('endorsement_slip')){
          $files = [];
          $file = $request->file('endorsement_slip');
          $filename = $applicant_code.'-'.str_random(5).'.'.$file->getClientOriginalExtension();
          $destination= public_path('endorsement-slips-media').'/'.$filename;
          $file->move(public_path('endorsement-slips-media'),$filename);
          if(strlen($application->endorsement_slip)>0){
              $this->deleteImage($application->endorsement_slip);
          }
          return url('endorsement-slips-media').'/'.$filename;
      }
    }
}
