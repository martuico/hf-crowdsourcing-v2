<?php namespace App\Modules\InternalRecruiter\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Modules\Employer\Requests\CompanyProfileRequest;
use App\Traits\ImageUpload;
use Illuminate\Validation\Rule;
use App\Modules\Employer\Requests\JobCreateRequest;

use App\Job,
	App\JobIndustry,
	App\JobType,
	App\Skill,
	App\Shift,
	App\JobLevel,
  App\Employer,
  App\User,
  App\Applicant,
  App\ApplicantJob,
	App\Region,
	App\Province,
	App\City;

class RecruiterController extends Controller
{
    use ImageUpload;
    protected $jobs;
    protected $applicants;
    protected $applicant_job;
    protected $jtype;
    protected $jindustry;
    protected $jskill;
    protected $shift;
    protected $edu_level;
    protected $employers;
    protected $users;
    protected $location_region, $location_province, $location_city;

    public function __construct(Job $jobs,
                  JobIndustry $jindustry,
                  JobType $jtype,
                  Skill $skill,
                  Shift $shift,
                  JobLevel $edu_level,
                  Employer $employers,
                  User $users,
                  Applicant $applicants,
                  ApplicantJob $applicant_job,
                  Region $location_region,
                  Province $location_province,
                  City $location_city)
    {
        $this->jobs = $jobs;
        $this->applicants = $applicants;
        $this->applicant_job = $applicant_job;
        $this->jindustry = $jindustry;
        $this->jtype = $jtype;
        $this->skill = $skill;
        $this->shift = $shift;
        $this->edu_level = $edu_level;
        $this->employers = $employers;
        $this->users = $users;
        $this->location_region = $location_region;
        $this->location_province = $location_province;
        $this->location_city = $location_city;
    }


    public function index()
    {
			   $applicants = $this->applicant_job
													->whereHas('assginedHFApplicants')
													->orderBy('created_at','asc')
													->paginate(50);
        return view('InternalRecruiter::home')
                    ->with(compact('applicants'));
    }

    public function listJob()
    {
			$q = \Request::has('q')?\Request::get('q'):'';

        $jobs = $this->jobs->query();
        $jobs->whereHas('assginedHFApplicants');
				if(\Request::has('q')){
          $jobs->whereRaw('(job_title like ? OR job_code like ?)',['%'.$q.'%','%'.$q.'%']);
        }
        $jobs->orderBy('created_at','desc')
            ->orderBy('status','desc');
        $jobs = $jobs->paginate(50);
        if(count($jobs)<1){
            return abort(404);
        }
        return view('InternalRecruiter::jobs.list')
                ->with(compact('jobs'));
    }

		public function pipeline($jobCode)
    {
        $job = $this->jobs
                    ->whereHas('assginedHFApplicants')
                    ->whereJobCode($jobCode)
                    ->first();
        if(count($job)<1){
            return abort(404);
        }
        $applicants = $this->applicant_job
														->whereHas('assginedHFApplicants')
                            ->where('job_id','=',$job->id)
                            ->orderBy('created_at','asc')
                            ->paginate(50);
				$endorsed_count = $applicants->where('applicant_status_id','=',2)->count();
				$hired_count = $applicants->where('applicant_status_id','=',8)->count();
        // return $job;
        return view('InternalRecruiter::jobs.pipeline')
                        ->with(compact('employer','job','applicants','endorsed_count','hired_count'));
    }

		public function settings()
		{
				return view('InternalRecruiter::settings');
		}

		public function updateSettings(Request $request)
		{
				$inputs = $request->only('firstname','lastname');
        if($request->has('password')){
          $inputs['password'] = \Hash::make($request->get('password'));
        }
				if($request->hasFile('avatar')){
            $files = [];
            $files[] = $request->file('avatar');
            $arr_files = $this->imagesUpload($files, 300, 300);
            if(strlen(auth()->user()->avatar)>0){
                $this->deleteImage(auth()->user()->avatar);
            }
            $inputs['avatar'] = $arr_files['uploaded_file'];
        }
				$check = auth()->user()->update($inputs);
        if(!($check)){
          return redirect()->back()->withErrors(['message'=>'Opps! Something went wrong']);
        }
        return redirect()->back()->with(['successMessage' => 'Updated Profile!']);
		}

}
