<?php namespace App\Modules\Applicant\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Foundation\Inspiring;
use App\Http\Requests;
use App\Modules\Applicant\Requests\ApplicantProfileRequest;
use App\Traits\ImageUpload;

use App\Job,
    App\ApplicantEducation,
    App\ApplicantEducationLevel,
    App\ApplicantExperience,
    App\Applicant,
    App\ApplicantJob,
    App\JobIndustry,
    App\Region,
    App\Province,
    App\City,
    App\Referral,
    App\IRecruiter;

class ResumeController extends Controller
{
    use ImageUpload;

    protected $jobs;
    protected $appliedJobs;
    protected $jindustries;
    protected $applicant;
    protected $irecruiter;
    protected $referral;
    protected $location_region, $location_province, $location_city;

    public function __construct(Job $jobs,
                                ApplicantJob $appliedJobs,
                                JobIndustry $jindustries,
                                Applicant $applicant,
                                ApplicantEducation $appEducation,
                                ApplicantEducationLevel $appEduLevel,
                                ApplicantExperience $appExperience,
                                Region $location_region,
                                Province $location_province,
                                City $location_city,
                                IRecruiter $irecruiter,
                                Referral $referral)
    {
        $this->jobs = $jobs;
        $this->appEducation = $appEducation;
        $this->appEduLevel = $appEduLevel;
        $this->appExperience = $appExperience;
        $this->applicant = $applicant;
        $this->appliedJobs = $appliedJobs;
        $this->jindustries = $jindustries;
        $this->location_region = $location_region;
        $this->location_province = $location_province;
        $this->location_city = $location_city;
        $this->irecruiter = $irecruiter;
        $this->referral = $referral;
    }

    public function createResume()
    {
        $industries = $this->jindustries->orderBy('name','asc')->get();
        $levels = $this->appEduLevel->orderBy('name','asc')->get();
        $location_region = $this->location_region->orderBy('regCode','asc')->get();
        $applicant = null;
        return view('Applicant::resume.createdit')
                    ->with(compact('industries','levels','location_region','applicant'));
    }

    public function fetchProvince($regcode)
    {

        $provinces = $this->location_province
                        ->where('regCode','=',$regcode)
                        ->orderBy('provDesc','asc')
                        ->get();
        if(count($provinces)< 1){
            return abort(404);
        }

        return response()->json(['success' => true,
                                 'provinces' => $provinces],200);
    }

    public function fetchCities($provcode)
    {
        $cities = $this->location_city
                        ->where('provCode','=',$provcode)
                        ->orderBy('citymunDesc','asc')
                        ->get();

        if(count($cities)< 1){
            return abort(404);
        }

        return response()->json(['success' => true,
                                 'cities' => $cities],200);
    }

    public function editResume()
    {
        $industries = $this->jindustries->orderBy('name','asc')->get();
        $levels = $this->appEduLevel->orderBy('name','asc')->get();
        $applicant = $this->applicant->with('educations','experiences')->where('user_id','=',auth()->user()->id)->first();
        $location_region = $this->location_region->orderBy('regCode','asc')->get();
        $job_referral = collect([]);
        if(\Session::has('referral_code')){
          $referral = $this->referral->where('uniq_link','=',\Session::get('referral_code'))
                       ->first();
          $job =  $this->jobs->whereJobCode($referral->job_code)->first();
          $job_referral = $job;
        }
        return view('Applicant::resume.createdit')
                    ->with(compact('industries','levels','applicant','location_region','job_referral'));
    }

    public function updateResume(ApplicantProfileRequest $request)
    {
        $inputs = $request->all();

        $countExp = $this->countExp($inputs);
        // $countEdu = $this->countEdu($inputs);

        $inputs = array_except($inputs,['id','user_id','created_at','updated_at']);
        if($request->hasFile('logo_url')){
            $files = [];
            $files[] = $request->file('logo_url');
            $arr_files = $this->imagesUpload($files, 300, 300);
            if(strlen(auth()->user()->avatar)>0){
                $this->deleteImage(auth()->user()->avatar);
            }
            $inputs['avatar'] = $arr_files['uploaded_file'];
        }

        $user = auth()->user()->update(array_only($inputs,['email','firstname','middlename','lastname','avatar']));

        $data_applicant_profile = array_only($inputs,['current_address',
                                                 'job_industry_id',
                                                 'last_school',
                                                 'education_level_id',
                                                 'has_professional_license',
                                                 'current_address',
                                                 'current_region',
                                                 'current_city',
                                                 'current_province',
                                                 'date_of_birth',
                                                 'current_employment_status',
                                                 'hometown_address',
                                                 'hometown_region',
                                                 'hometown_province',
                                                 'availability',
                                                 'hometown_city',
                                                 'field_of_study',
                                                 'expected_salary',
                                                 'skills',
                                                 'languages',
                                                 'biography']);
        $data_applicant_profile['position'] = $request->get('current_position');
        $data_applicant_profile['mobile_number_1'] = $request->get('number_mobile');
        $data_applicant_profile['house_number_1'] = $request->get('number_landline');
        $data_applicant_profile['willing_to_relocate'] = $request->has('willing_to_relocate')?'1':'0';
        // return dd($data_applicant_profile);
        if($request->hasFile('attached_resume')){
            $files = [];
            $file = $request->file('attached_resume');
            $filename = str_random(5).'-eq-'.str_slug(auth()->user()->my_name).'.'.$file->getClientOriginalExtension();
            $destination= public_path('upload-resume').'/'.$filename;
            $file->move(public_path('upload-resume'),$filename);
            if(count(auth()->user()->resume)>0 AND strlen(auth()->user()->resume->attached_resume)>0){
                $this->deleteImage(auth()->user()->resume->attached_resume);
            }
            $data_applicant_profile['attached_resume'] = url('upload-resume').'/'.$filename;
        }

        $applicant_profile = $this->applicant->where('user_id','=',auth()->user()->id)->first();
        $applicant_profile->update($data_applicant_profile);
        $this->appEducation->whereApplicantId($applicant_profile->id)->delete();
        $this->appExperience->whereApplicantId($applicant_profile->id)->delete();
        $arr_runExp = $this->runExp($countExp,$inputs,$applicant_profile->id);
        // $arr_runEdu = $this->runEdu($countEdu,$inputs,$applicant_profile->id);
        if(\Session::has('referral_code')){
            $referral = $this->referral->where('uniq_link','=',\Session::get('referral_code'))
                         ->first();
            $job =  $this->jobs->whereJobCode($referral->job_code)->first();
            $inputs_a['applicant_cover_letter'] = br2nl('<p><br></p>');
            $inputs_a['expected_salary'] = 'negotiable';
            $inputs_a['applicant_id'] =  (auth()->user()->resume)?auth()->user()->resume->id:0;
            $inputs_a['job_id'] = $job->id;
            $inputs_a['employer_id'] = $job->employer->id;
            $inputs_a['applicant_status_id'] = 1;
            $inputs_a['is_visible_to_employer'] = 0;
            $this->appliedJobs->create($inputs_a);
            \Session::forget('referral_code');
        }
        return redirect()->route('applicant.resume.view');
    }

    public function storeResume(ApplicantProfileRequest $request)
    {
        $inputs = $request->all();
        // return $inputs;

        $countExp = $this->countExp($inputs);
        // $countEdu = $this->countEdu($inputs);

        $inputs = array_except($inputs,['id','user_id','created_at','updated_at']);
        if($request->hasFile('logo_url')){
            $files = [];
            $files[] = $request->file('logo_url');
            $arr_files = $this->imagesUpload($files, 300, 300);
            if(strlen(auth()->user()->avatar)>0){
                $this->deleteImage(auth()->user()->avatar);
            }
            $inputs['avatar'] = $arr_files['uploaded_file'];
        }

        $user = auth()->user()->update(array_only($inputs,['email','firstname','middlename','lastname','avatar']));
        $applicant_profile = array_only($inputs,['current_address',
                                                 'job_industry_id',
                                                 'last_school',
                                                 'education_level_id',
                                                 'has_professional_license',
                                                 'current_address',
                                                 'current_region',
                                                 'current_city',
                                                 'current_province',
                                                 'date_of_birth',
                                                 'current_employment_status',
                                                 'hometown_address',
                                                 'hometown_region',
                                                 'hometown_province',
                                                 'availability',
                                                 'hometown_city',
                                                 'field_of_study',
                                                 'expected_salary',
                                                 'skills',
                                                 'languages',
                                                 'biography']);
        $applicant_profile['position'] = $request->get('current_position');
        $applicant_profile['mobile_number_1'] = $request->get('number_mobile');
        $applicant_profile['house_number_1'] = $request->get('number_landline');
        $applicant_profile['willing_to_relocate'] = $request->has('willing_to_relocate')?1:0;

        if($request->hasFile('attached_resume')){
            $files = [];
            $file = $request->file('attached_resume');
            $filename = str_random(5).'-eq-'.str_slug(auth()->user()->my_name).'.'.$file->getClientOriginalExtension();
            $destination= public_path('upload-resume').'/'.$filename;
            $file->move(public_path('upload-resume'),$filename);
            if(count(auth()->user()->resume)>0 AND strlen(auth()->user()->resume->attached_resume)>0){
                $this->deleteImage(auth()->user()->resume->attached_resume);
            }
            $applicant_profile['attached_resume'] = url('upload-resume').'/'.$filename;
        }

        $internal = $this->irecruiter
                        ->where('status','active')
                        ->orderBy('task_on_hand','asc')
                        ->limit(1)
                        ->first();

        $applicant_profile['hf_assigned'] = $internal->user_id;
        $inc = (int)$internal->task_on_hand + 1;
        $this->irecruiter->whereId($internal->id)->update(['task_on_hand'=> $inc]);
        $applicant_profile['user_id'] = auth()->user()->id;
        $applicant_profile = $this->applicant->create($applicant_profile);
        $arr_runExp = $this->runExp($countExp,$inputs,$applicant_profile->id);
        // $arr_runEdu = $this->runEdu($countEdu,$inputs,$applicant_profile->id);

        return redirect()->route('applicant.resume.view');
    }

    public function viewResume()
    {
        if(count(auth()->user()->resume)<1){
            return redirect()->route('applicant.resume.create');
        }
        $applicant = $this->applicant->with('educations','experiences')
                            ->where('user_id','=',auth()->user()->id)
                            ->first();

        $quote = Inspiring::quote();
        $quote = explode(' - ', $quote);
        return view('Applicant::resume.detail')
                        ->with(compact('applicant','quote'));
    }

    protected function runExp($countExp,$inputs,$applicant_id)
    {   $experience = [];
        $check = [];
        if(!$countExp){
            return $check;
        }
        foreach(range(1,$countExp) as $expIndex => $expVal){
            $experience['applicant_id'] = $applicant_id;
            $experience['company_name'] = isset($inputs['company_name_'.$expVal])?$inputs['company_name_'.$expVal]:null;
            $experience['position'] = isset($inputs['position_'.$expVal])?$inputs['position_'.$expVal]:null;
            $experience['resposibilities'] = isset($inputs['resposibilities_'.$expVal])?$inputs['resposibilities_'.$expVal]:null;
            $experience['startMonth'] = isset($inputs['startMonth_'.$expVal])?$inputs['startMonth_'.$expVal]:null;
            $experience['startYear'] = isset($inputs['startYear_'.$expVal])?$inputs['startYear_'.$expVal]:null;
            $experience['endMonth'] = isset($inputs['endMonth_'.$expVal])?$inputs['endMonth_'.$expVal]:null;
            $experience['endYear'] = isset($inputs['endYear_'.$expVal])?$inputs['endYear_'.$expVal]:null;
            $experience['currently_work_here'] = isset($inputs['currently_work_here_'.$expVal])?$inputs['currently_work_here_'.$expVal]:null;
            $check[] = $this->appExperience->create($experience);
        }
        return $check;
    }

    protected function runEdu($countExp,$inputs,$applicant_id)
    {   $experience = [];
        $check = [];
        if(!$countExp){
            return $check;
        }
        foreach(range(1,$countExp) as $expIndex => $expVal){
            $experience['applicant_id'] = $applicant_id;
            $experience['school_name'] = isset($inputs['school_name_'.$expVal])?$inputs['school_name_'.$expVal]:null;
            $experience['attainment'] = isset($inputs['attainment_'.$expVal])?$inputs['attainment_'.$expVal]:null;
            $experience['awards'] = isset($inputs['awards_'.$expVal])?$inputs['awards_'.$expVal]:null;
            $experience['start_date'] = isset($inputs['start_date_'.$expVal])?$inputs['start_date_'.$expVal]:null;
            $experience['end_date'] = isset($inputs['end_date_'.$expVal])?$inputs['end_date_'.$expVal]:null;
            $check[] = $this->appEducation->create($experience);
        }
        return $check;
    }

    protected function countExp($inputs)
    {
        $count_expcompany_name = count(preg_grep("/^company_name_(\d)+$/",array_keys($inputs)));
        $count_expposition = count(preg_grep("/^position_(\d)+$/",array_keys($inputs)));
        $sumexp = ($count_expcompany_name + $count_expposition) % 2;
        if($sumexp > 0){
            return redirect()->back()->withErrors(['message' => ' Please information on Experience section such as Company Name and Position']);
        }
        return $count_expcompany_name;
    }

    protected function countEdu($inputs)
    {
        $count_eduschool_name = count(preg_grep("/^school_name_(\d)+$/",array_keys($inputs)));
        $count_eduattainment = count(preg_grep("/^attainment_(\d)+$/",array_keys($inputs)));
        $sumedu = ($count_eduschool_name + $count_eduattainment) % 2;
        if($sumedu > 0){
            return redirect()->back()->withErrors(['message' => ' Please information on Experience section such as Company Name and Position']);
        }
        return $count_eduattainment;
    }
}
