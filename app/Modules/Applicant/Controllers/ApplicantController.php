<?php namespace App\Modules\Applicant\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Modules\Applicant\Requests\ApplicantProfileRequest;
use App\Modules\Applicant\Requests\ApplyJobRequest;
use App\Traits\ImageUpload;

use App\Job,
    App\ApplicantEducation,
    App\ApplicantEducationLevel,
    App\ApplicantExperience,
    App\Applicant,
    App\ApplicantJob,
    App\JobIndustry,
    App\IRecruiter,
    App\Referral;

class ApplicantController extends Controller
{
    use ImageUpload;

    protected $jobs;
    protected $appliedJobs;
    protected $jindustries;
    protected $applicant;
    protected $irecruiter;
    protected $referral;

    public function __construct(Job $jobs,
                                ApplicantJob $appliedJobs,
                                JobIndustry $jindustries,
                                Applicant $applicant,
                                ApplicantEducation $appEducation,
                                ApplicantEducationLevel $appEduLevel,
                                ApplicantExperience $appExperience,
                                IRecruiter $irecruiter,
                                Referral $referral)
    {
        $this->jobs = $jobs;
        $this->appEducation = $appEducation;
        $this->appEduLevel = $appEduLevel;
        $this->appExperience = $appExperience;
        $this->applicant = $applicant;
        $this->appliedJobs = $appliedJobs;
        $this->jindustries = $jindustries;
        $this->irecruiter = $irecruiter;
        $this->referral = $referral;
    }

    public function index()
    {
       if(\Session::has('referral_code') AND (count(auth()->user()->resume) <1) ){
            $referral = $this->referral->where('uniq_link','=',\Session::get('referral_code'))
                        ->first();
            // return dd($referral);
            $internal = $this->irecruiter
                                   ->orderBy('task_on_hand','asc')
                                   ->limit(1)
                                   ->first();

           $applicant_profile['hf_assigned'] = $internal->user_id;
           $inc = (int)$internal->task_on_hand + 1;
           $this->irecruiter->whereId($internal->id)->update(['task_on_hand'=> $inc]);
           $this->applicant->create([
                'user_id' => auth()->user()->id,
                'hf_assigned' => $applicant_profile['hf_assigned'],
                'referred_by_id' => $referral->recruiter_id,
                'referred_start_date' => \Carbon\Carbon::now(),
                'referred_expired_date' => \Carbon\Carbon::now()->addMonths(6)
            ]);
           return redirect()->route('resume-profile.edit');
       }

      //  return redirect()->route('applicant.job.list');

       $appliedJobs = (count(auth()->user()->resume)>0)?$this->appliedJobs
                    ->with('job')
                    ->whereApplicantId(auth()->user()->resume->id)
                    ->orderBy('created_at','desc')
                    ->with(['applicant_status' => function($query){
                      // $query->whereIn('name',['Active File','Endorsed','For Interview','Hired']);
                      $query->orderBy('name','asc');
                    }])
                    ->take(30)
                    ->get(): collect([]);
        $job_count = $this->jobs->whereStatus('open')->count();
        $applied_count = (count(auth()->user()->resume)>0)?$this->appliedJobs
                            ->whereApplicantId(auth()->user()->resume->id)
                            ->count():0;
        $forinterview_count = (count(auth()->user()->resume)>0)?$this->appliedJobs
                            ->whereApplicantId(auth()->user()->resume->id)
                            ->where('applicant_status_id','=',3)
                            ->count():0;

    	return view('Applicant::index')
                  ->with(compact('appliedJobs','job_count','applied_count','forinterview_count'));
    }

    public function appliedJobs()
    {
        if(count(auth()->user()->resume)>0){
          $appliedJobs = $this->appliedJobs
                       ->with('job')
                       ->whereHas('job',function($query){
                         $query->where('job_title','like','%'.\Request::get('q').'%');
                       })
                       ->whereApplicantId(auth()->user()->resume->id)
                       ->orderBy('created_at','desc')
                       ->with(['applicant_status' => function($query){
                         // $query->whereIn('name',['Active File','Endorsed','For Interview','Hired']);
                         $query->orderBy('name','asc');
                       }])
                       ->paginate(50);
        }else{
          $appliedJobs = collect([]);
        }

        return view('Applicant::jobs.applied-jobs')
                    ->with(compact('appliedJobs'));
    }

    public function listJob()
    {
        $jobs = $this->jobs
                    ->whereStatus('open')
                    ->orderBy('created_at','desc')
                    ->paginate(50);

        $industries = $this->jindustries->orderBy('id','asc')->get();
        return view('Applicant::jobs.list')
                ->with(compact('jobs','industries'));
    }

    public function applyJob(ApplyJobRequest $request)
    {
        if($request->ajax()){
            $job =  $this->jobs->whereJobCode($request->get('job_code'))->first();
            if(count($job)<1){
                return response()->json(['error' => 'Can\'t find your data'],401);
            }
            $check = $this->jobs->applied()
                             ->whereJobCode($request->get('job_code'))
                             ->first();
            if($check){
                return response()->json(['error' => 'You have already applied'],401);
            }
            $inputs = $request->only('applicant_cover_letter','expected_salary');
            $inputs['applicant_cover_letter'] = br2nl($request->get('applicant_cover_letter'));
            $inputs['applicant_id'] =  (auth()->user()->resume)?auth()->user()->resume->id:0;
            $inputs['job_id'] = $job->id;
            $inputs['employer_id'] = $job->employer->id;
            // $inputs['referred_by_id'] = 0;
            // $inputs['referred_start_date'] = null;
            // $inputs['referred_expired_date'] = null;
            //HF assigned
            /*
            // remove as per convo
            if($job->employer->is_premium){
              //choose random admin & internal recruiter 1,4
                $internal = $this->irecruiter
                                ->orderBy('task_on_hand','asc')
                                ->first();
                $inputs['hf_assigned'] = $internal->id;
                $inc = (int)$internal->task_on_hand + 1;
                $internal->update(['task_on_hand'=> $inc]);
            }else{
                $inputs['hf_assigned'] = 0;
            }
            */
            $inputs['applicant_status_id'] = 1; //Active File
            $inputs['is_visible_to_employer'] = 0;
            $data = $this->appliedJobs->create($inputs);
            $email_inputs['firstname'] = auth()->user()->firstname;
            $email_inputs['lastname'] = auth()->user()->lastname;
            $email_inputs['job_title'] =  $job->job_title;
            $email_inputs['job_title_slug'] =  $job->slug;
            $email_inputs['job_code'] = $job->job_code;
            $email_inputs['email'] = auth()->user()->email;
            // later
              $when = \Carbon\Carbon::now()->addMinutes(2);
            \Mail::later($when,new ApplicantQuickApply($email_inputs));
            $email_inputs['employer_email'] = $job->employer->user->email;
            $email_inputs['company_name'] = $job->employer->company_name;
            \Mail::later($when,new EmployerNotifyApplication($email_inputs));
            return response()->json(['success' => true,'data' => $data],200);
        }

    }

    public function viewJob($code)
    {
        $job = $this->jobs->whereJobCode($code)->first();
        if(count($job)<1){
            return abort(404);
        }
        return view('Applicant::jobs.details')
                    ->with(compact('job'));
    }

    public function settings()
    {
        return view('Applicant::settings');
    }

    public function updateSettings(Request $request)
    {
        $inputs = $request->only('firstname','lastname');
        if($request->has('password')){
          $inputs['password'] = \Hash::make($request->get('password'));
        }
        if($request->hasFile('avatar')){
            $files = [];
            $files[] = $request->file('avatar');
            $arr_files = $this->imagesUpload($files, 300, 300);
            if(strlen(auth()->user()->avatar)>0){
                $this->deleteImage(auth()->user()->avatar);
            }
            $inputs['avatar'] = $arr_files['uploaded_file'];
        }
        $check = auth()->user()->update($inputs);
        if(!($check)){
          return redirect()->back()->withErrors(['message'=>'Opps! Something went wrong']);
        }
        return redirect()->back()->with(['successMessage' => 'Updated Profile!']);
    }
}
