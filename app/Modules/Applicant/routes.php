<?php


Route::group(['module' => 'Applicant',
              'namespace' => 'App\Modules\Applicant\Controllers',
              'middleware' => ['web','auth','ApplicantOnly'],
              'prefix' => 'applicant'
          ],
            function() {
  // Route::get('quotes',function(){


  //   $json = json_decode(file_get_contents('http://quotes.rest/qod.json'), true);
  //   \Request::cookie('quotes_json',$json);
  //   return response()->json($json)->withCookie(cookie('quotes_json', $json));
  //   // return dd(count(auth()->user()->resume->attached_resume)<1);
  // });
  Route::post('settings',['as' => 'applicant.updateSettings','uses' => 'ApplicantController@updateSettings']);
  Route::get('account-settings',['as' => 'applicant.settings','uses' => 'ApplicantController@settings']);
  Route::get('resume-profile/edit',['as' => 'resume-profile.edit','uses' => 'ResumeController@editResume']);
  Route::post('resume-profile/edit',['as' => 'resume-profile.update','uses' => 'ResumeController@updateResume']);

  Route::get('resume-profile',['as' => 'applicant.resume.view','uses' => 'ResumeController@viewResume']);
  Route::get('resume-profile-create',['as' => 'applicant.resume.create','uses' => 'ResumeController@createResume']);
  Route::post('resume-profile-create',['as' => 'applicant.resume.store','uses' => 'ResumeController@storeResume']);
  Route::get('applied-jobs',['as' => 'applicant.job.applied','uses' => 'ApplicantController@appliedJobs']);
  Route::get('applied-jobs/{code}',['as' => 'applicant.job.view','uses' => 'ApplicantController@viewJob']);
  Route::get('jobs',['as' => 'applicant.job.list','uses' => 'ApplicantController@listJob']);
  Route::post('apply-job',['as' => 'applicant.job.apply','uses'=>'ApplicantController@applyJob']);
  Route::get('fetch-cities/{provcode}',['uses' => 'ResumeController@fetchCities']);
  Route::get('fetch-province/{regcode}',['uses' => 'ResumeController@fetchProvince']);
  Route::get('/',['as' => 'applicant.home','uses' => 'ApplicantController@index']);

});
