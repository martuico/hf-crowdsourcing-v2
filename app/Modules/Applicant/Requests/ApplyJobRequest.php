<?php

namespace App\Modules\Applicant\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ApplyJobRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'expected_salary' => 'nullable|max:30',
            'applicant_cover_letter' => 'required|max:1000',
        ];
    }
}
