<?php

namespace App\Modules\Applicant\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ApplicantProfileRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'firstname' => 'required',
            'lastname' => 'required',
            'current_address' => 'required',
            // 'current_city' => 'required',
            // 'current_province' => 'required',
            'email' => (auth()->user()->email !== \Request::get('email'))?'required|unique:users|email':'required|email',
            'number_mobile' => 'required',
            'attached_resume' => 'file|mimetypes:application/pdf,application/msword,application/vnd.openxmlformats-officedocument.wordprocessingml.document',
        ];
    }
}
