@extends('Dashboard::template.main')

@section('content')
  <!-- Page Header -->
  <div class="page-header page-header-block">
      <div class="page-header-section">
          <h4 class="title semibold">Applied Jobs</h4>
      </div>
      <div class="page-header-section">
          <!-- Toolbar -->
          <div class="toolbar clearfix">
            {{ \Form::open(['method' => 'GET']) }}
              <div class="input-group">
                <input type="text" name="q" class="form-control" placeholder="Search for Jobs" value="{{ \Request::get('q') }}">
                <span class="input-group-btn">
                  <button class="btn btn-default" type="submit"><i class="ico-search"></i> Search</button>
                  @if(\Request::has('q'))
                  <a href="{{route('applicant.job.applied')}}" class="btn btn-danger"><i class="ico-close"></i> Clear Search</a>
                  @endif
                </span>
              </div><!-- /input-group -->
            {{ \Form::close() }}
          </div>
          <!--/ Toolbar -->
      </div>
  </div>
  <!-- Page Header -->
  <!-- Browser Breakpoint -->
  <div class="row">
      <div class="col-lg-12">
          <!-- START panel -->
          <div class="panel panel-default">
              <!-- panel heading/header -->
              <div class="panel-heading">
                  <h3 class="panel-title ellipsis"><i class="ico-blog2 mr5"></i>Latest Applied Jobs</h3>
                  <!-- panel toolbar -->
                  <div class="panel-toolbar text-right">
                  </div>
                  <!--/ panel toolbar -->
              </div>
              <!--/ panel heading/header -->
              @if(count($appliedJobs)>0)
              <!-- panel body with collapse capabale -->
              <div class="table-responsive table-bordered table-hover table-condensed panel-collapse pull out">
                  <table class="table">
                      <thead>
                          <tr>
                              <th>Job Title</th>
                              <th width="20%">Job Location</th>
                              <th>Posted Date</th>
                              <th>Appointments</th>
                              <th>Humanfactor Feedback</th>
                              <th>Employer Feedback</th>
                          </tr>
                      </thead>
                      <tbody>
                        @foreach($appliedJobs as $appliedjob)
                          <tr>
                              <td>
                                <a href="{{ route('jobs.details',['slug' => $appliedjob->job->slug,'code' => $appliedjob->job->job_code]) }}" class="semibold text-accent" target="_blank"> {{ $appliedjob->job->job_title }} </a>
                                <br> <small class="text-muted">CODE: {{ $appliedjob->job->job_code }}</small>
                                <br>
                                @if(count($appliedjob->endorsement_slip)>0)
                                <p>
                                  <a href="{{ route('download.endorsemmentslip',['application_code' => $appliedjob->application_code]) }}"> <i class="fa fa-file"></i> Download Edorsement Slip  </a>
                                </p>
                                @endif
                              </td>
                              <td>{{ $appliedjob->job->full_address}}</td>
                              <td>{{ $appliedjob->job->created_at }}</td>
                              <td>{{ ($appliedjob->employer_appointment_sched)?:'No Appointments' }}</td>
                              <td class="stages" data-status="{{ $appliedjob->applicant_status_id }}">
                                <p><strong>Current Status :</strong> {{ $appliedjob->applicant_status->name }} </p>
                                @if($appliedjob->employer_appointment_type)
                                <p class="app_type"><strong>Appointments :</strong> {{ ucwords(str_replace('-',' ',$appliedjob->employer_appointment_type)) }} | {{ $appliedjob->employer_appointment_sched }} </p>
                                @endif
                                @if($appliedjob->endorsement_slip)
                                <p class="attachment"><strong>Edorsement Slip :</strong> <i class="fa fa-file"></i> <a href="{{ route('download.endorsemmentslip',['application_code' => $appliedjob->application_code]) }}"> Download here</a> </p>
                                @endif
                                @if($appliedjob->hire_date && $appliedjob->hire_date !== '0000-00-00')
                                <p class="hire_date"><strong>Hire Date :</strong> {{ $appliedjob->hire_date }} </p>
                                @endif
                                @if($appliedjob->hire_account)
                                <p class="hire_account"><strong>Account :</strong> {{ $appliedjob->hire_account }} </p>
                                @endif
                                @if($appliedjob->reason)
                                <p class="reason"><strong>Reason :</strong> {{ $appliedjob->reason }} </p>
                                @endif
                                @if($appliedjob->note)
                                <p class="notes"><strong>Note :</strong> {{ $appliedjob->note }} </p>
                                @endif
                              </td>
                              <td class="employerFb" data-employerfb="{{ $appliedjob->employer_applicant_status_id }}">
                                <p><strong>Applicant Feedback Status :</strong>  {{ ($appliedjob->employer_applicant_status)? $appliedjob->employer_applicant_status->name:'No Status' }} </p>
                                @if($appliedjob->employerFeedback)
                                  @if($sched_final = $appliedjob->employerFeedback->schedule_final_interview)
                                  <p class="sched_finalinterview"><strong>Final Interview :</strong>  {{ $sched_final }} </p>
                                  @endif
                                  @if($appliedjob->employerFeedback->reason)
                                  <p class="employer_reason"><strong>Reason :</strong> {{ $appliedjob->employerFeedback->reason }}</p>
                                  @endif
                                  @if($appliedjob->employerFeedback->notes)
                                  <p class="employer_notes"><strong>Notes :</strong> {{ $appliedjob->employerFeedback->notes }}</p>
                                  @endif
                                  @if($sched_valid = $appliedjob->employerFeedback->schedule_account_validation)
                                  <p class="sched_validation"><strong>Account Validation :</strong>  {{ $sched_valid }}</p>
                                  @endif
                                  @if($sched_offer = $appliedjob->employerFeedback->schedule_job_offer)
                                  <p class="sched_joboffer"><strong>Job Offer :</strong>  {{ $sched_offer }}</p>
                                  @endif
                                  @if($sched_training = $appliedjob->date_of_training)
                                  <p class="date_of_training"><strong>Date Training :</strong>  {{ $sched_training }}</p>
                                  @endif
                                  @if($hire_acc = $appliedjob->hire_account)
                                  <p class="hire_account"><strong>Hire Account :</strong>  {{ $hire_acc }}</p>
                                  @endif
                                  @if($hire_date = $appliedjob->hire_date)
                                  <p class="hire_date"><strong>Hire Date :</strong>  {{ $hire_date }}</p>
                                  @endif
                                @endif
                              </td>
                          </tr>
                        @endforeach
                      </tbody>
                  </table>
              </div>
              {{ $appliedJobs->links() }}
              <!--/ panel body with collapse capabale -->
              @else
              <div class="panel-body">
                <div class="alert alert-info">
                  <strong>No Data!</strong>
                </div>
              </div>
              @endif
          </div>
          <!--/ END panel -->
      </div>
  </div>
  <!-- Browser Breakpoint -->
@stop
