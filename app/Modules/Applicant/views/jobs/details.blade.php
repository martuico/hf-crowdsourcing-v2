@extends('Dashboard::template.main')

@section('content')
  <!-- Page Header -->
  <div class="page-header page-header-block">
      <div class="page-header-section">
          <h4 class="title semibold">Applied Job</h4>
      </div>
      <div class="page-header-section">
          <!-- Toolbar -->
          <div class="toolbar clearfix">
              <a href="{{ \URL::previous() }}" class="btn btn-default btn-sm"><i class="ico-chevron-left"></i></a>
          </div>
          <!--/ Toolbar -->
      </div>
  </div>

  <div class="row">
    <div class="col-md-8">
      <div class="widget panel">
        <div class="panel-body">
            <div class="page-header">
              <div class="page-title">
                <h2> {{$job->job_title }} <span class="label label-inverse">{{ $job->applicant_reward}}</span></h2>
                <strong><i class="ico-cogs"></i> {{ $job->job_industry->name }} </strong>
                <span><i class="ico-map-marker"></i>{{ $job->work_location }}</span><br>
              </div>
            </div>
            <ul class="list-inline">
              <li><span class="label label-default">{{ $job->type->name}}</span>
              </li>
              <li><span class="label label-default">{{ $job->job_level->name }}</span>
              </li>
              <li><span class="label label-default">{{ $job->education_level->name }}</span>
              </li>
            </ul>
            <div class="desc">
              {!! $job->description !!}
            </div>
        </div><!-- body -->
      </div>
    </div>
    <div class="col-md-4">
      <div class="widget panel">
        <div class="panel-body">
          <ul class="list-unstyled">
              <li class="text-center">
                  <img class="img-circle img-bordered-success" src="{{ $job->employer->logo }}" alt="" width="75px" height="75px">
                  <br>
                  <h5 class="semibold mb0">{{ $job->employer->company_name }}</h5>
                  @if($job->employer->is_premium)
                  <p class="nm"><span class="label label-success"><i class="fa fa-shield"></i> Premium Employer</span></p>
                  @endif
                  @if($job->employer->is_verified)
                  <p class="nm"><span class="label label-info"><i class="ico-check"></i> Verified</span></p>
                  @endif
              </li>
          </ul>
          <hr>
          <h4>About the company</h4>
          <div>{!! $job->employer->description !!}</div>
        </div>
      </div>
    </div>
  </div>

@stop