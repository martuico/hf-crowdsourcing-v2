@extends('Dashboard::template.main')

@section('content')
  <!-- Page Header -->
  <div class="page-header page-header-block">
      <div class="page-header-section">
          <h4 class="title semibold">Applicant Dashboard</h4>
      </div>
      <div class="page-header-section">
          <!-- Toolbar -->
          <div class="toolbar clearfix">
              @if(auth()->user()->type->name == 'Applicant' AND count(auth()->user()->resume)<1)
                <div class="alert alert-warning text-left">
                  <strong><i class="ico-warning-sign"></i> Create resume profile first before you can apply</strong>
                </div>
              @else
              <div class="col-xs-8">
                  <select class="form-control text-left" id="selectize-customselect">
                      <option value="0">Display metrics...</option>
                      <option value="1">Last 6 month</option>
                      <option value="2">Last 3 month</option>
                      <option value="3">Last month</option>
                  </select>
              </div>
              <div class="col-xs-4">
                  <button class="btn btn-primary pull-right"><i class="ico-loop4 mr5"></i>Update</button>
              </div>
              @endif
          </div>
          <!--/ Toolbar -->
      </div>
  </div>
  <!-- Page Header -->

  <div class="row">
      <!-- START Left Side -->
      <div class="col-md-9">
          <!-- Top Stats -->
          <div class="row">
              <div class="col-sm-4">
                  <!-- START Statistic Widget -->
                  <div class="table-layout animation delay animating fadeInDown">
                      <div class="col-xs-4 panel bgcolor-info text-center">
                          <div class="ico-list-alt fsize24"></div>
                      </div>
                      <div class="col-xs-8 panel">
                          <div class="panel-body text-center">
                              <h4 class="semibold nm">{{ $job_count }}</h4>
                              <p class="semibold text-muted mb0 mt5 ellipsis">Jobs</p>
                          </div>
                      </div>
                  </div>
                  <!--/ END Statistic Widget -->
              </div>
              <div class="col-sm-4">
                  <!-- START Statistic Widget -->
                  <div class="table-layout animation delay animating fadeInUp">
                      <div class="col-xs-4 panel bgcolor-teal text-center">
                          <div class="ico-crown fsize24"></div>
                      </div>
                      <div class="col-xs-8 panel">
                          <div class="panel-body text-center">
                              <h4 class="semibold nm">{{ $applied_count }}</h4>
                              <p class="semibold text-muted mb0 mt5 ellipsis">Applied Jobs</p>
                          </div>
                      </div>
                  </div>
                  <!--/ END Statistic Widget -->
              </div>
              <div class="col-sm-4">
                  <!-- START Statistic Widget -->
                  <div class="table-layout animation delay animating fadeInDown">
                      <div class="col-xs-4 panel bgcolor-primary text-center">
                          <div class="ico-box-add fsize24"></div>
                      </div>
                      <div class="col-xs-8 panel">
                          <div class="panel-body text-center">
                              <h4 class="semibold nm">{{ $forinterview_count }}</h4>
                              <p class="semibold text-muted mb0 mt5 ellipsis">For Interview</p>
                          </div>
                      </div>
                  </div>
                  <!--/ END Statistic Widget -->
              </div>
          </div>
          <!--/ Top Stats -->
          <!-- Browser Breakpoint -->
          <div class="row">
              <div class="col-lg-12">
                  <!-- START panel -->
                  <div class="panel panel-default">
                      <!-- panel heading/header -->
                      <div class="panel-heading">
                          <h3 class="panel-title ellipsis"><i class="ico-blog2 mr5"></i>Latest Applied Jobs</h3>
                          <!-- panel toolbar -->
                          <div class="panel-toolbar text-right">
                          </div>
                          <!--/ panel toolbar -->
                      </div>
                      <!--/ panel heading/header -->
                      @if(count($appliedJobs)>0)
                      <!-- panel body with collapse capabale -->
                      <div class="table-responsive panel-collapse pull out">
                          <table class="table">
                              <thead>
                                  <tr>
                                      <th>Job Title</th>
                                      <th>Job Location</th>
                                      <th>Posted Date</th>
                                      <th>Due Date</th>
                                      <th>Humanfactor Feedback</th>
                                      <th>Employer Feedback</th>
                                  </tr>
                              </thead>
                              <tbody>
                                @foreach($appliedJobs as $appliedjob)
                                  <tr>
                                      <td><a href="" class="semibold text-accent"> {{ $appliedjob->job->job_title }} </a></td>
                                      <td>{{ $appliedjob->job->work_location }}</td>
                                      <td>{{ $appliedjob->job->created_at }}</td>
                                      <td>{{ $appliedjob->job->valid_until }}</td>
                                      <td class="stages" data-status="{{ $appliedjob->applicant_status_id }}">
                                        <p><strong>Current Status :</strong> {{ $appliedjob->applicant_status->name }} </p>
                                        @if($appliedjob->employer_appointment_type)
                                        <p class="app_type"><strong>Appointments :</strong> {{ ucwords(str_replace('-',' ',$appliedjob->employer_appointment_type)) }} | {{ $appliedjob->employer_appointment_sched }} </p>
                                        @endif
                                        @if($appliedjob->endorsement_slip)
                                        <p class="attachment"><strong>Edorsement Slip :</strong> <i class="fa fa-file"></i> <a href="{{ route('download.endorsemmentslip',['application_code' => $appliedjob->application_code]) }}"> Download here</a> </p>
                                        @endif
                                        @if($appliedjob->hire_date && $appliedjob->hire_date !== '0000-00-00')
                                        <p class="hire_date"><strong>Hire Date :</strong> {{ $appliedjob->hire_date }} </p>
                                        @endif
                                        @if($appliedjob->hire_account)
                                        <p class="hire_account"><strong>Account :</strong> {{ $appliedjob->hire_account }} </p>
                                        @endif
                                        @if($appliedjob->reason)
                                        <p class="reason"><strong>Reason :</strong> {{ $appliedjob->reason }} </p>
                                        @endif
                                        @if($appliedjob->note)
                                        <p class="notes"><strong>Note :</strong> {{ $appliedjob->note }} </p>
                                        @endif
                                      </td>
                                      <td class="employerFb" data-employerfb="{{ $appliedjob->employer_applicant_status_id }}">
                                        <p><strong>Applicant Feedback Status :</strong>  {{ ($appliedjob->employer_applicant_status)? $appliedjob->employer_applicant_status->name:'No Status' }} </p>
                                        @if($appliedjob->employerFeedback)
                                          @if($sched_final = $appliedjob->employerFeedback->schedule_final_interview)
                                          <p class="sched_finalinterview"><strong>Final Interview :</strong>  {{ $sched_final }} </p>
                                          @endif
                                          @if($appliedjob->employerFeedback->reason)
                                          <p class="employer_reason"><strong>Reason :</strong> {{ $appliedjob->employerFeedback->reason }}</p>
                                          @endif
                                          @if($appliedjob->employerFeedback->notes)
                                          <p class="employer_notes"><strong>Notes :</strong> {{ $appliedjob->employerFeedback->notes }}</p>
                                          @endif
                                          @if($sched_valid = $appliedjob->employerFeedback->schedule_account_validation)
                                          <p class="sched_validation"><strong>Account Validation :</strong>  {{ $sched_valid }}</p>
                                          @endif
                                          @if($sched_offer = $appliedjob->employerFeedback->schedule_job_offer)
                                          <p class="sched_joboffer"><strong>Job Offer :</strong>  {{ $sched_offer }}</p>
                                          @endif
                                          @if($sched_training = $appliedjob->date_of_training)
                                          <p class="date_of_training"><strong>Date Training :</strong>  {{ $sched_training }}</p>
                                          @endif
                                          @if($hire_acc = $appliedjob->hire_account)
                                          <p class="hire_account"><strong>Hire Account :</strong>  {{ $hire_acc }}</p>
                                          @endif
                                          @if($hire_date = $appliedjob->hire_date)
                                          <p class="hire_date"><strong>Hire Date :</strong>  {{ $hire_date }}</p>
                                          @endif
                                        @endif
                                      </td>
                                  </tr>
                                @endforeach
                              </tbody>
                          </table>
                      </div>
                      <!--/ panel body with collapse capabale -->
                      @else
                      <div class="panel-body">
                        <div class="alert alert-info">
                          <strong>No Data!</strong>
                        </div>
                      </div>
                      @endif
                  </div>
                  <!--/ END panel -->
              </div>
          </div>
          <!-- Browser Breakpoint -->
      </div><!-- col-md-9 -->
      <!-- START Right Side -->
      <div class="col-md-3">
          <div class="panel panel-minimal">
              <div class="panel-heading"><h5 class="panel-title"><i class="ico-health mr5"></i>Upcoming Appointments</h5></div>

              <!-- Media list feed -->
              <ul class="media-list media-list-feed nm">
                  <li class="media">
                      <div class="media-object pull-left">
                          <i class="ico-pencil bgcolor-success"></i>
                      </div>
                      <div class="media-body">
                          <p class="media-heading">EDIT EXISTING PAGE</p>
                          <p class="media-text"><span class="text-primary semibold">Service Page</span> has been edited by Tamara Moon.</p>
                          <p class="media-meta">Just Now</p>
                      </div>
                  </li>
                  <li class="media">
                      <div class="media-object pull-left">
                          <i class="ico-link"></i>
                      </div>
                      <div class="media-body">
                          <a href="javascript:void(0);" class="media-heading text-primary">View Appointments</a>
                      </div>
                  </li>
              </ul>
              <!--/ Media list feed -->
          </div>
      </div>
      <!--/ END Right Side -->
  </div><!-- row -->
@stop
