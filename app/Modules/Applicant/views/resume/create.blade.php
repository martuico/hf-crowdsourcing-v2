@extends('Dashboard::template.main')

@section('content')
  <!-- Page Header -->
  <div class="page-header page-header-block">
      <div class="page-header-section">
          <h4 class="title semibold">Create Resume Profile</h4>
      </div>
      <div class="page-header-section">
          <!-- Toolbar -->
          <div class="toolbar clearfix text-right">
              <button class="btn btn-sm btn-defaut"  data-toggle="tooltip" data-placement="bottom" title="Check Resume Profile"><i class="ico-eye"></i></button>
          </div>
          <!--/ Toolbar -->
      </div>
  </div>
    <!-- START row -->
  <div class="row">
      <!-- Left / Top Side -->
      <div class="col-lg-12">
        <!-- form profile -->
        {!! \Form::open(['class' => 'panel','name' => 'form-profile','data-parsley-validate'=> '' ,'files' => true]) !!}
            <div class="panel-body">
                @if (count($errors) > 0)
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <div class="form-group">
                    <div class="row">
                        <div class="col-md-6">
                            <label class="control-label">Profile Pic<span class="text-danger">*</span></label>
                            <input id="input-id" type="file" class="file" data-preview-file-type="text" name="logo_url" data-parsley-errors-container="#errorBlockLogo"  data-parsley-error-message="Please add your picture" value="{{auth()->user()->avatar}}">
                            <div id="errorBlockLogo"></div>
                            <p class="text-default nm">Image file size should not exceed 2MB. Suggested image dimension 300px by 300px</p>
                        </div>
                        <div class="col-md-3">

                        </div>
                        <div class="col-md-3">

                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <div class="col-sm-9">
                            <label class="control-label">Your Name</label>
                        </div>
                        <div class="col-sm-3">
                            <label class="control-label">Date of Birth</label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-3">
                            <input type="text" class="form-control" value="{{ (old('firstname'))?:auth()->user()->firstname }}" name="firstname" required="" placeholder="First Name">
                        </div>
                        <div class="col-md-3">
                            <input type="text" class="form-control" value="{{ (old('middlename'))?:auth()->user()->middlename }}" name="middlename" placeholder="Middle Name">
                        </div>
                        <div class="col-md-3">
                            <input type="text" class="form-control" value="{{ (old('lastname'))?:auth()->user()->lastname }}" name="lastname" required="" placeholder="Last Name">
                        </div>
                        <div class="col-md-3">
                          <input type="text" class="form-control datepicker" value="" name="birthday" required="">
                        </div>
                    </div>
                </div>
                <div class="form-group">
                  <div class="row">
                  <div class="col-md-3">
                    <label class="control-label">Current Position</label>
                    <input type="text" class="form-control" name="current_position" value="{{ old('current_position') }}" required="">
                    <p class="text-muted">ex. OJT Account Manager, Supervisor, Designer and etc.</p>
                  </div>
                  <div class="col-md-3">
                    <label class="control-label">Availability to Start Working</label>
                    <select name="availability" class="form-control selc" required="">
                        <option value="">-- Select Availability--</option>
                        <option value="asap">ASAP</option>
                        <option value="within_a_month">With in a month</option>
                        <option value="others">Others</option>
                    </select>
                  </div>
                  <div class="col-md-3">
                    <label class="control-label">Expected Salary</label>
                    <input type="text" class="form-control" name="expected_salary">
                  </div>
                  <div class="col-md-3">
                    <label class="control-label">Preferred Work Location</label>
                    <select name="prefered_work_location" class="form-control selc" required="">
                        <option value="">-- Select Availability--</option>
                        <option value="0722">CEBU</option>
                        <option value="1124">DAVAO DEL SUR</option>
                        <option value="1339">CITY OF MANILA</option>
                    </select>
                  </div>
                  </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="row">
                                <div class="col-sm-12">
                                    <label class="control-label">Current Address Residing</label>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12 mb10">
                                    <input name="current_address" type="text" value="{{ old('current_address') }}" class="form-control" required=""  placeholder="Street Address">
                                </div>
                            </div>
                            <div class="row">
                              <div class="col-sm-12 mb10">
                              <select name="location_region" class="select2">
                                <option value="">--  Select Region--</option>
                                @foreach($location_region as $region)
                                <option value="{{ $region->regCode }}">{{ $region->regDesc }}</option>
                                @endforeach
                              </select>
                              </div>
                            </div>
                            <div class="row">
                              <div class="col-sm-6 mb10">
                                  <select name="location_province" class="select2"  disabled="">
                                  </select>
                              </div>
                              <div class="col-sm-6 mb10">
                                  <select name="location_city" class="select2"  disabled="">
                                  </select>
                              </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12">
                                    <label class="control-label">Contact Information</label>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12 mb10">
                                    <input name="email" type="text" required="" value="{{auth()->user()->email}}"  class="form-control" placeholder="Email">
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12 mb10">
                                    <input name="number_mobile" type="text" data-mask="(9999) 999-9999" required="" value="{{ old('number_mobile') }}" class="form-control" placeholder="Mobile Number">
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12 mb10">
                                    <input name="number_landline" type="text"  data-mask="(999) 999-9999"value="{{ old('number_landline') }}" class="form-control" placeholder="House Number">
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">

                            <div class="row">
                              <div class="col-sm-12">
                                  <label class="control-label">Provincial Address</label>
                              </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12 mb10">
                                    <input name="hometown_address" type="text" value="{{ old('hometown_address') }}" class="form-control" required=""  placeholder="Street Address">
                                </div>
                            </div>
                            <div class="row">
                              <div class="col-sm-12 mb10">
                              <select name="hometown_region" class="select2">
                                <option value="">--  Select Region--</option>
                                @foreach($location_region as $region)
                                <option value="{{ $region->regCode }}">{{ $region->regDesc }}</option>
                                @endforeach
                              </select>
                              </div>
                            </div>
                            <div class="row">
                              <div class="col-sm-6 mb10">
                                  <select name="location_province" class="select2"  disabled="">
                                  </select>
                              </div>
                              <div class="col-sm-6 mb10">
                                  <select name="location_city" class="select2"  disabled="">
                                  </select>
                              </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <label class="control-label">Skills</label>
                                    <input type="text" class="form-control skills" name="skills" value="{{old('skills')}}" placeholder="ex. Web Design, Trainable, Leadership skills" required="">
                                    <p class="text-muted">ex. MSWORD, Leardership, Excel, Photoshop</p>
                                </div>
                                <div class="col-sm-6">
                                    <label class="control-label">Languages</label>
                                    <input type="text" class="form-control languages" name="languages" value="{{old('languages')}}" placeholder="ex. English, Tagalog, Spanish" required="">
                                    <p class="text-muted">ex. English, Tagalog, Spanish</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <div class="col-sm-12">
                            <label class="control-label">Biography</label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <textarea class="form-control summernote" rows="3" name="biography" placeholder="Describe about yourself" required="" data-parsley-error-message="Please tell us more about yourself">{{old('biography')}}</textarea>
                            <p class="help-block">Tell us more about yourself in not more than 500 characters or less.</p>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <div class="col-sm-12">
                            <label class="control-label">Educational Attainment</label>
                        </div>
                    </div>
                    <div class="row">
                      <div class="col-md-3">
                        <label>Highest Educational Attainment</label>
                        <select name="education_level_id" class="form-control selc" required="">
                            <option value="">-- Select Industry--</option>
                            @foreach($levels as $lev)
                                <option value="{{$lev->id}}" {{ (old('education_level_id'))?'selected="selected"':'' }}>{{$lev->name}}</option>
                            @endforeach
                        </select>
                      </div>
                      <div class="col-md-4">
                        <label>Last School Attended</label>
                        <input type="text" class="form-control" required="" name="last_school">
                      </div>
                      <div class="col-md-3">
                        <label>Course/Field of Study</label>
                        <select name="job_industry_id" class="form-control selc" required="">
                            <option value="">-- Select Industry--</option>
                            @foreach($industries as $ind)
                                <option value="{{$ind->id}}" {{ (old('job_industry_id'))?'selected="selected"':'' }}>{{$ind->name}}</option>
                            @endforeach
                        </select>
                      </div>
                      <div class="col-md-2">
                        <label class="control-label">Professional License</label>
                        <select name="has_professional_license" class="form-control">
                          <option value="1">Yes</option>
                          <option value="0" selected="">No</option>
                        </select>
                      </div>
                    </div>
                    <?php /*
                    <div class="educations">
                        <div class="row" id="edu_1">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <input type="text" class="form-control" value="{{ old('school_name_1') }}"  name="school_name_1" placeholder="School Name">
                                        </div>
                                        <div class="col-md-6">
                                            <input type="text" class="form-control" value="{{ old('attainment_1') }}"   name="attainment_1" placeholder="Course">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <input type="text" class="form-control datepicker" value="{{ old('start_date_1') }}" name="start_date_1" placeholder="Date From">
                                        </div>
                                        <div class="col-md-6">
                                            <input type="text" class="form-control datepicker" value="{{ old('end_date_1') }}" name="end_date_1" placeholder="Date To">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <textarea class="form-control" rows="3" name="awards_1" placeholder="Tell us about your achievements or learning" data-parsley-error-message="Tell us about your achievements or learning">{{old('awards_1')}}</textarea>
                                            <p class="help-block">Tell us more about achievements in not more than 300 characters or less.</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12 text-right">
                            <button type="button" class="btn btn-success btn-sm" id="addEducation">Add More</button>
                        </div>
                    </div>
                    */
                   ?>
                </div><!--- education -->
                <hr>
                <div class="form-group">
                    <div class="row">
                        <div class="col-sm-12">
                            <label class="control-label">Experience</label>
                        </div>
                    </div>
                    @php
                      $year_arr = range(1950,date('Y'));
                      rsort($year_arr);
                    @endphp
                    <div class="experiences">
                        <div class="row" id="edu_1">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                      <div class="col-md-6">
                                        <label>Position</label>
                                        <input type="text" class="form-control" value="{{ old('position_1') }}"  name="position_1" placeholder="Postion">
                                      </div>
                                      <div class="col-md-6">
                                        <label>Company</label>
                                        <input type="text" class="form-control" value="{{ old('company_name_1') }}"  name="company_name_1" placeholder="Company Name">
                                      </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-md-6">
                                          <label>From</label>
                                          <select name="startMonth" class="form-control">
                                            <option value="">Month</option>
                                            <option value="1">January</option>
                                            <option value="2">February</option>
                                            <option value="3">March</option>
                                            <option value="4">April</option>
                                            <option value="5">May</option>
                                            <option value="6">June</option>
                                            <option value="7">July</option>
                                            <option value="8">August</option>
                                            <option value="9">September</option>
                                            <option value="10">October</option>
                                            <option value="11">November</option>
                                            <option value="12">December</option>
                                          </select>
                                          <br>
                                          <select name="startYear" class="form-control">
                                            <option value="">Year</option>
                                            @foreach($year_arr as $startYear)
                                              <option value="{{ $startYear }}">{{ $startYear }}</option>
                                            @endforeach
                                          </select>
                                        </div>
                                        <div class="col-md-6">
                                          <label>To</label>
                                          <select name="toMonth" class="form-control">
                                            <option value="">Month</option>
                                            <option value="1">January</option>
                                            <option value="2">February</option>
                                            <option value="3">March</option>
                                            <option value="4">April</option>
                                            <option value="5">May</option>
                                            <option value="6">June</option>
                                            <option value="7">July</option>
                                            <option value="8">August</option>
                                            <option value="9">September</option>
                                            <option value="10">October</option>
                                            <option value="11">November</option>
                                            <option value="12">December</option>
                                          </select>
                                          <br>
                                          <select name="toYear" class="form-control">
                                            <option value="">Year</option>
                                            @foreach($year_arr as $startYear)
                                              <option value="{{ $startYear }}">{{ $startYear }}</option>
                                            @endforeach
                                          </select>
                                          <br>
                                          <div class="checkbox custom-checkbox">
                                              <input type="checkbox" name="toCurrent" value="1" id="toCurrent">
                                              <label for="toCurrent">&nbsp;&nbsp;I currently work here</label>
                                          </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                  <div class="col-md-12">
                                    <label>Reason for leaving</label>
                                      <textarea class="form-control" rows="3" name="resposibilities_1" placeholder="Tell us about your responsibilities" data-parsley-error-message="Reason for leaving">{{old('resposibilities_1')}}</textarea>
                                  </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12 text-right">
                            <button type="button" class="btn btn-success btn-sm" id="addExperience">Add More</button>
                        </div>
                    </div>
                </div><!--- exp -->
                <hr>
                <div class="form-group">
                    <div class="row">
                        <div class="col-md-6">
                            <label class="control-label">Attach Resume:</label>
                            <input type="file" class="form-control" name="attached_resume" value="{{ old('attached_resume') }}" required="" parsley-filemaxsize="2">
                            <p class="text-muted">Note: Only MS-WORD (.doc) or PDF File (.pdf) are accepted 2 MB</p>
                        </div>
                        <div class="col-md-3">
                            <label class="control-label">&nbsp;</label>
                            <div class="checkbox custom-checkbox">
                                <input type="checkbox" name="willing_to_relocate" id="willing_to_relocate" {{ old('willing_to_relocate')?'checked="checked"':'' }} value="1">
                                <label for="willing_to_relocate">&nbsp;&nbsp;Willing to Relocate</label>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <label class="control-label">Current Employment Status</label>
                            <select name="current_employment_status" class="form-control selc" required="">
                                <option value="">-- Select Industry--</option>
                                @foreach(['actively_seeking' => 'Actively Seeing','currently_employed' => 'Currently Employed'] as $curid => $cur)
                                    <option value="{{$curid}}" {{ old('current_employment_status') == $curid ?'selected="selected"':'' }}>{{$cur}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>
            </div>
            <div class="panel-footer text-right">
                <button type="submit" class="btn btn-primary"><i class="ico-save"></i> Save</button>
            </div>
        </form>
        <!--/ form profile -->
      </div>
  </div>
@stop

@section('styles')
    <link href="{{asset('hfv1/plugins/jquery-ui/css/jquery-ui.css')}}" media="all" rel="stylesheet" type="text/css" />
    <link href="{{asset('hfv1/plugins/bootstrap-fileinput/css/fileinput.min.css')}}" media="all" rel="stylesheet" type="text/css" />
    <link href="{{asset('hfv1/plugins/summernote/dist/summernote.css')}}" media="all" rel="stylesheet" type="text/css" />
    <link href="{{asset('hfv1/plugins/selectize/css/selectize.css')}}" media="all" rel="stylesheet" type="text/css" />
    <style>
        div.file-thumbnail-footer {
            display: none;
        }
    </style>
@stop

@section('scripts')
    <script src="{{asset('hfv1/plugins/jquery-ui/js/jquery-ui.js')}}"></script>
    <script src="{{asset('hfv1/plugins/jquery-ui/js/addon/timepicker/jquery-ui-timepicker.js')}}"></script>
    <script src="{{asset('hfv1/plugins/summernote/dist/summernote.js')}}"></script>
    <script src="{{asset('hfv1/plugins/summernote/dist/summernote-cleaner.js')}}"></script>
    <script src="{{asset('hfv1/plugins/bootstrap-fileinput/js/plugins/purify.min.js')}}"></script>
    <script src="{{asset('hfv1/plugins/bootstrap-fileinput/js/fileinput.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('hfv1/plugins/inputmask/js/inputmask.js')}}"></script>
    <script type="text/javascript" src="{{asset('hfv1/plugins/selectize/js/selectize.js')}}"></script>
    <script>
        var ResumeProfile = {
            init : function(){
                this.misc();
                this.addExperience();
                this.addEducation();
                this.initDatePicker($('.datepicker'));
                var skills = $('.skills').selectize({
                                valueField: 'id',
                                labelField: 'name',
                                searchField: 'name',
                                create: true,
                                options: [],
                                maxItems: 20
                            });
                @if(strlen(old('skills'))>0)
                skills[0].selectize.setValue('{{old('skills')?:$applicant->skills}}')
                @endif

                var languages = $('.languages').selectize({
                                valueField: 'id',
                                labelField: 'name',
                                searchField: 'name',
                                create: true,
                                options: [],
                                maxItems: 20
                            });
                @if(strlen(old('languages'))>0)
                languages[0].selectize.setValue('{{old('languages')?:$applicant->languages}}')
                @endif
            },
            addExperience : function(){
                $('#addExperience').click(function(){
                    let countExp = $('.experiences').children().length + 1;
                    $('.experiences').append(ResumeProfile.experienceForm(countExp));
                    ResumeProfile.initDatePicker($('.datepicker'));
                });
            },
            addEducation : function(){
                $('#addEducation').click(function(){
                    let countExp = $('.educations').children().length + 1;
                    $('.educations').append(ResumeProfile.educationForm(countExp));
                    ResumeProfile.initDatePicker($('.datepicker'));
                });
            },
            educationForm : function(countExp){
                return `
                    <div class="row" id="edu_${countExp}">
                        <div class="col-md-6">
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-6">
                                        <input type="text" class="form-control" name="school_name_${countExp}" placeholder="School Name">
                                    </div>
                                    <div class="col-md-6">
                                        <input type="text" class="form-control" name="attainment_${countExp}" placeholder="Course">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-6">
                                        <input type="text" class="form-control datepicker" name="start_date_${countExp}" placeholder="Date From">
                                    </div>
                                    <div class="col-md-6">
                                        <input type="text" class="form-control datepicker" name="end_date_${countExp}" placeholder="Date To">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-12">
                                        <textarea class="form-control" rows="3" name="awards_${countExp}" placeholder="Tell us about your achievements or learning" data-parsley-error-message="Tell us about your achievements or learning"></textarea>
                                        <p class="help-block">Tell us more about achievements in not more than 300 characters or less.</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                `;
            },
            experienceForm : function(countExp) {
                return `
                    <div class="row" id="edu_${countExp}">
                        <div class="col-md-6">
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-6">
                                        <input type="text" class="form-control"  name="company_name_${countExp}" placeholder="Company Name">
                                    </div>
                                    <div class="col-md-6">
                                        <input type="text" class="form-control" name="position_${countExp}" placeholder="Job Title">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-6">
                                        <input type="text" class="form-control datepicker" name="start_date_exp_${countExp}" placeholder="Date From">
                                    </div>
                                    <div class="col-md-6">
                                        <input type="text" class="form-control datepicker" name="end_date_exp_${countExp}" placeholder="Date To">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-12">
                                        <textarea class="form-control" rows="3" name="resposibilities_${countExp}" placeholder="Tell us about your responsibilities" data-parsley-error-message="Please tell us about your responsibilities"></textarea>
                                        <p class="help-block">Tell us more about responsibilities in not more than 300 characters or less.</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                `;
            },
            initDatePicker: function(elem){
                elem.datepicker({ dateFormat: 'yy-mm-dd'});
            },
            misc : function(){

                window.ParsleyValidator
                  .addValidator('filemaxsize', function (val, req) {
                      var reqs = req.split('|');
                      var input = $('input[type="file"][name="'+reqs[0]+'"]');
                      var maxsize = reqs[1];
                      var max_bytes = maxsize * 1000000, file = input.files[0];

                      return file.size <= max_bytes;

                  }, 32).addMessage('en', 'filemaxsize', 'The File size is too big.')

                $('.selc').selectize({
                    sortField: 'text'
                });
                $('.select2').select2({
                  allowClear : true
                });
                $('[name="location_region"]').change(function(e){
                  var regcode = $(this).val();
                  console.log(regcode);
                  $.ajax({
                    url : window.location.origin + '/applicant/fetch-province/' + regcode,
                    type : 'get',
                    success : function(res){
                      if(res.success){
                        var x = '';
                        $('[name="location_province" ]').children().remove();
                        $('[name="location_city" ]').children().remove();
                        $('[name="location_city" ]').prop('disabled','disabled');
                        $('[name="location_province" ]').select2('val',null);
                        $('[name="location_city" ]').select2('val',null);
                        $.each(res.provinces,function(k,v){
                          x  +='<option value="'+v.provCode+'">'+v.provDesc+'</option>';
                        });
                        $('[name="location_province" ]').append($(x));
                        $('[name="location_province" ]').removeAttr('disabled');
                      }
                    },error : function(res){

                    }
                  });
                  e.stopImmediatePropagation();
                  e.preventDefault();
                });

                $('[name="location_province" ]').change(function(e){
                  var provcode = $(this).val();
                  $.ajax({
                    url : window.location.origin + '/applicant/fetch-cities/' + provcode,
                    type : 'get',
                    success : function(res){
                      if(res.success){
                        var x = '';
                        $('[name="location_city" ]').children().remove();
                        $('[name="location_city" ]').select2('val',null);
                        $.each(res.cities,function(k,v){
                          x  +='<option value="'+v.citymunCode+'">'+v.citymunDesc+'</option>';
                        });
                        $('[name="location_city" ]').append($(x));
                        $('[name="location_city" ]').removeAttr('disabled');
                      }
                    },error : function(res){

                    }
                  });
                  e.preventDefault();
                  e.stopImmediatePropagation();
                });

                $('.summernote').summernote({
                    height: 150,
                    toolbar:[
                            ['style',['style']],
                            ['font',['bold','italic','underline','clear']],
                            ['fontname',['fontname']],
                            ['color',['color']],
                            ['para',['ul','ol','paragraph']],
                            ['height',['height']],
                            ['view',['fullscreen']],
                            ['help',['help']]
                    ],
                    disableDragAndDrop: true,
                    cleaner:{
                               notTime:2400, // Time to display Notifications.
                               action:'both', // both|button|paste 'button' only cleans via toolbar button, 'paste' only clean when pasting content, both does both options.
                               newline:'<br>', // Summernote's default is to use '<p><br></p>'
                               notStyle:'position:absolute;top:0;left:0;right:0', // Position of Notification
                               keepHtml: false, //Remove all Html formats
                               keepClasses: false, //Remove Classes
                               badTags: ['style','script','applet','embed','noframes','noscript', 'html'], //Remove full tags with contents
                               badAttributes: ['style','start'] //Remove attributes from remaining tags
                    }
                })
            }
        }
        $(function(){
            ResumeProfile.init();
        });
        var settter = '{{ (strlen(auth()->user()->avatar)>0)?0:1 }}';

        $("#input-id").fileinput({
            showUpload :false,
            maxFileCount: 1,
            minFileCount: parseInt(settter),
            showRemove: false,
            allowedFileTypes: ['image'],
            maxFileSize: 2000,
            maxImageWidth: 150,
            maxImageHeight: 150,
            resizeImage: true,
            validateInitialCount: false,
            overwriteInitial: true,
            @if(strlen(auth()->user()->avatar)>0)
            initialPreview: [
                "<img src='{{auth()->user()->profile_pic}}' style='max-width:150px;max-height:150px;' class='file-preview-image img-responsive'>"
            ],
            initialPreviewConfig: [
                    {width: "150px"},
            ],
            initialPreviewShowDelete: false,
            fileActionSettings: {
                showZoom: false
            }
            @endif
        });
    </script>
@stop
