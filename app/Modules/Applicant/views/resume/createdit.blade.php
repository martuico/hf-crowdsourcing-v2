@extends('Dashboard::template.main')

@section('content')
  <!-- Page Header -->
  <div class="page-header page-header-block">
      <div class="page-header-section">
        @if(\Route::currentRouteName() == 'applicant.resume.create')
          <h4 class="title semibold">Create Resume Profile</h4>
        @else
          <h4 class="title semibold">Edit Resume Profile</h4>
        @endif
      </div>
      <div class="page-header-section">
          <!-- Toolbar -->
          <div class="toolbar clearfix text-right">
            @if(count($applicant)>0)
              <a href="{{ route('applicant.resume.view') }}" class="btn btn-default btn-sm"><i class="fa fa-drivers-license-o"></i> VIEW RESUME</a>
            @endif
          </div>
          <!--/ Toolbar -->
      </div>
  </div>
    <!-- START row -->
  <div class="row">
      <!-- Left / Top Side -->
      <div class="col-lg-12">
        <!-- form profile -->
        @if(\Route::currentRouteName() == 'applicant.resume.create')
          {!! \Form::open(['class' => 'panel','name' => 'form-profile',
                           'route' => 'applicant.resume.store',
                          'data-parsley-validate'=> '' ,'files' => true]) !!}
        @else
          {!! \Form::open(['class' => 'panel','name' => 'form-profile',
                           'route' => 'resume-profile.update',
                          'data-parsley-validate'=> '' ,'files' => true]) !!}
        @endif
            <div class="panel-body">
                @if (count($errors) > 0)
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <div class="form-group">
                    <div class="row">
                        <div class="col-md-6">
                            <label class="control-label">Profile Pic<span class="text-danger">*</span></label>
                            <input id="input-id" type="file" class="file" data-preview-file-type="text" name="logo_url" data-parsley-errors-container="#errorBlockLogo"  data-parsley-error-message="Please add your picture" value="{{(count($applicant)>0)?$applicant->user->avatar:''}}">
                            <div id="errorBlockLogo"></div>
                            <p class="text-default nm">Image file size should not exceed 2MB. Suggested image dimension 300px by 300px</p>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <div class="col-sm-9">
                            <label class="control-label">Your Name</label>
                        </div>
                        <div class="col-sm-3">
                            <label class="control-label">Date of Birth</label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-3">
                            <input type="text" class="form-control" value="{{ (old('firstname'))?:(count($applicant)>0 AND $applicant->user)?$applicant->user->firstname:'' }}" name="firstname" required="" placeholder="First Name">
                        </div>
                        <div class="col-md-3">
                            <input type="text" class="form-control" value="{{ (old('middlename'))?:(count($applicant)>0 AND $applicant->user)?$applicant->user->middlename:'' }}" name="middlename" placeholder="Middle Name">
                        </div>
                        <div class="col-md-3">
                            <input type="text" class="form-control" value="{{ (old('lastname'))?:(count($applicant)>0 AND $applicant->user)?$applicant->user->lastname:'' }}" name="lastname" required="" placeholder="Last Name">
                        </div>
                        <div class="col-md-3">
                          <input type="text" class="form-control datepicker" value="{{  (old('date_of_birth'))?:($applicant)?$applicant->date_of_birth:'' }}" name="date_of_birth" required="">
                        </div>
                    </div>
                </div>
                <div class="form-group">
                  <div class="row">
                  <div class="col-md-3">
                    <label class="control-label">Current Position</label>
                    <input type="text" class="form-control" name="current_position" value="{{ old('current_position')?:($applicant)?$applicant->position:'' }}" required="">
                    <p class="text-muted">ex. OJT Account Manager, Supervisor, Designer and etc.</p>
                  </div>
                  <div class="col-md-3">
                    <label class="control-label">Availability to Start Working</label>
                    <select name="availability" class="form-control" required="">
                        <option value="">-- Select Availability--</option>
                        <option value="asap" {{ (old('availability') == 'asap')?'selected="selected"':(count($applicant)>0 AND $applicant->availability == 'asap')?'selected="selected"':'' }}>ASAP</option>
                        <option value="within_a_month" {{ (old('availability') == 'within_a_month')?'selected="selected"':(count($applicant)>0 AND $applicant->availability == 'within_a_month')?'selected="selected"':'' }}>With in a month</option>
                        <option value="others" {{ (old('availability') == 'within_a_month')?'selected="selected"':(count($applicant)>0 AND $applicant->availability == 'within_a_month')?'selected="selected"':'' }}>Others</option>
                    </select>
                  </div>
                  <div class="col-md-3">
                    <label class="control-label">Expected Salary</label>
                    <input class="form-control moneymask" name="expected_salary"  value="{{ old('expected_salary')?:($applicant)?$applicant->expected_salary:'' }}" required="">
                  </div>
                  <div class="col-md-3">
                    <label class="control-label">Preferred Work Location</label>
                    <select name="prefered_work_location" class="form-control selc" required="">
                        <option value="">-- Select Availability--</option>
                        <option value="064501" {{ (old('prefered_work_location') == '064501')?'selected="selected"':(count($applicant)>0 AND $applicant->prefered_work_location == '064501')?'selected="selected"':'' }}>BACOLOD CITY (Capital)</option>
                        <option value="072217" {{ (old('prefered_work_location') == '072217')?'selected="selected"':(count($applicant)>0 AND $applicant->prefered_work_location == '072217')?'selected="selected"':'' }}>CEBU CITY (Capital)</option>
                        <option value="104305" {{ (old('prefered_work_location') == '104305')?'selected="selected"':(count($applicant)>0 AND $applicant->prefered_work_location == '104305')?'selected="selected"':'' }}>CAGAYAN DE ORO CITY (Capital)</option>
                        <option value="112402" {{ (old('prefered_work_location') == '112402')?'selected="selected"':(count($applicant)>0 AND $applicant->prefered_work_location == '112402')?'selected="selected"':'' }}>DAVAO CITY</option>
                        <option value="063022" {{ (old('prefered_work_location') == '063022')?'selected="selected"':(count($applicant)>0 AND $applicant->prefered_work_location == '063022')?'selected="selected"':'' }}>ILOILO CITY (Capital)</option>
                        <option value="137602" {{ (old('prefered_work_location') == '137602')?'selected="selected"':(count($applicant)>0 AND $applicant->prefered_work_location == '137602')?'selected="selected"':'' }}>CITY OF MAKATI</option>
                        <option value="137401" {{ (old('prefered_work_location') == '137401')?'selected="selected"':(count($applicant)>0 AND $applicant->prefered_work_location == '137401')?'selected="selected"':'' }}>CITY OF MANDALUYONG</option>
                        <option value="137603" {{ (old('prefered_work_location') == '137603')?'selected="selected"':(count($applicant)>0 AND $applicant->prefered_work_location == '137603')?'selected="selected"':'' }}>CITY OF MUNTINLUPA</option>
                        <option value="137604" {{ (old('prefered_work_location') == '137604')?'selected="selected"':(count($applicant)>0 AND $applicant->prefered_work_location == '137604')?'selected="selected"':'' }}>CITY OF PARAÑAQUE</option>
                        <option value="137605" {{ (old('prefered_work_location') == '137605')?'selected="selected"':(count($applicant)>0 AND $applicant->prefered_work_location == '137605')?'selected="selected"':'' }}>PASAY CITY</option>
                        <option value="137404" {{ (old('prefered_work_location') == '137404')?'selected="selected"':(count($applicant)>0 AND $applicant->prefered_work_location == '137404')?'selected="selected"':'' }}>QUEZON CITY</option>
                        <option value="137607" {{ (old('prefered_work_location') == '137607')?'selected="selected"':(count($applicant)>0 AND $applicant->prefered_work_location == '137607')?'selected="selected"':'' }}>TAGUIG CITY</option>
                    </select>
                  </div>
                  </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="row">
                                <div class="col-sm-12">
                                    <label class="control-label">Current Address Residing</label>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12 mb10">
                                    <input name="current_address" type="text" value="{{ old('current_address')?:(count($applicant)>0)?$applicant->current_address:'' }}" class="form-control" required=""  placeholder="Street Address">
                                </div>
                            </div>
                            <div class="row">
                              <div class="col-sm-12 mb10">
                              <select name="current_region" class="select2">
                                <option value="">--  Select Region--</option>
                                @foreach($location_region as $region)
                                <option value="{{ $region->regCode }}" {{ (old('current_region') == $region->regCode )?'selected="selected"':(count($applicant)>0 AND $applicant->current_region == $region->regCode )?'selected="selected"':'' }}>{{ $region->regDesc }}</option>
                                @endforeach
                              </select>
                              </div>
                            </div>
                            <div class="row">
                              <div class="col-sm-6 mb10">
                                  <select name="current_province" class="select2"
                                  @if(count($applicant)<1)
                                  disabled=""
                                  @endif>
                                    @if(count($applicant)>0 AND count($applicant->c_province)>0)
                                      <option value="{{ $applicant->current_province }}">{{ $applicant->c_province->provDesc }}</option>
                                    @endif
                                  </select>
                              </div>
                              <div class="col-sm-6 mb10">
                                  <select name="current_city" class="select2"
                                  @if(count($applicant)<1)
                                  disabled=""
                                  @endif>
                                    @if(count($applicant)>0 AND count($applicant->c_city)>0)
                                      <option value="{{ $applicant->current_city }}">{{ $applicant->c_city->citymunDesc }}</option>
                                    @endif
                                  </select>
                              </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12">
                                    <label class="control-label">Contact Information</label>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12 mb10">
                                    <input name="email" type="text" required="" value="{{ old('email')?:(count($applicant)>0)?$applicant->user->email:''}}"  class="form-control" placeholder="Email">
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12 mb10">
                                    <input name="number_mobile" type="text" required="" value="{{ old('number_mobile')?:($applicant)?$applicant->mobile_number_1:'' }}" class="form-control numbercode" placeholder="Mobile Number">
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12 mb10">
                                    <input name="number_landline" type="text" value="{{ old('number_landline')?:($applicant)?$applicant->house_number_1:'' }}" class="form-control numbercode" placeholder="House Number">
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">

                            <div class="row">
                              <div class="col-sm-12">
                                  <label class="control-label">Provincial Address</label>
                              </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12 mb10">
                                    <input name="hometown_address" type="text" value="{{ old('hometown_address')?:($applicant)?$applicant->hometown_address:'' }}" class="form-control" required=""  placeholder="Street Address">
                                </div>
                            </div>
                            <div class="row">
                              <div class="col-sm-12 mb10">
                              <select name="hometown_region" class="select2">
                                <option value="">--  Select Region--</option>
                                @foreach($location_region as $region)
                                <option value="{{ $region->regCode }}"  {{ (old('hometown_region') == $region->regCode )?'selected="selected"':(count($applicant)>0 AND $applicant->hometown_region == $region->regCode )?'selected="selected"':'' }}>{{ $region->regDesc }}</option>
                                @endforeach
                              </select>
                              </div>
                            </div>
                            <div class="row">
                              <div class="col-sm-6 mb10">
                                  <select name="hometown_province" class="select2"
                                    @if(count($applicant)<1)
                                    disabled=""
                                    @endif>
                                      @if(count($applicant)>0 AND count($applicant->h_province)>0)
                                        <option value="{{ $applicant->hometown_province }}">{{ ($applicant->h_province)?$applicant->h_province->provDesc:'' }}</option>
                                      @endif
                                  </select>
                              </div>
                              <div class="col-sm-6 mb10">
                                  <select name="hometown_city" class="select2"
                                    @if(count($applicant)<1)
                                    disabled=""
                                    @endif>
                                      @if(count($applicant)>0 AND count($applicant->h_city)>0)
                                        <option value="{{ $applicant->hometown_city }}">{{ ($applicant->h_city)?$applicant->h_city->citymunDesc:'' }}</option>
                                      @endif
                                  </select>
                              </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <label class="control-label">Skills</label>
                                    <input type="text" class="form-control skills" name="skills" value="{{old('skills')?:($applicant)?$applicant->skills:''}}" placeholder="ex. Web Design, Trainable, Leadership skills">
                                    <p class="text-muted">ex. MSWORD, Leardership, Excel, Photoshop</p>
                                </div>
                                <div class="col-sm-6">
                                    <label class="control-label">Languages</label>
                                    <input type="text" class="form-control languages" name="languages" value="{{old('languages')?:($applicant)?$applicant->languages:''}}" placeholder="ex. English, Tagalog, Spanish">
                                    <p class="text-muted">ex. English, Tagalog, Spanish</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <div class="col-sm-12">
                            <label class="control-label">Education Section</label>
                        </div>
                    </div>
                    <div class="row">
                      <div class="col-md-3">
                        <label>Highest Educational Attainment</label>
                        <select name="education_level_id" class="form-control selc" required="">
                            <option value="">-- Select Industry--</option>
                            @foreach($levels as $lev)
                                <option value="{{$lev->id}}" {{ (old('education_level_id') == $lev->id)?'selected="selected"':($applicant AND $applicant->education_level_id == $lev->id)?'selected="selected"':'' }}>{{$lev->name}}</option>
                            @endforeach
                        </select>
                      </div>
                      <div class="col-md-4">
                        <label>Last School Attended</label>
                        <input type="text" class="form-control" required="" name="last_school" value="{{ ($applicant)?$applicant->last_school:'' }}">
                      </div>
                      <div class="col-md-3">
                        <label>Field of interest</label>
                        <select name="job_industry_id" class="form-control selc" required="">
                            <option value="">-- Select Industry--</option>
                            @foreach($industries as $ind)
                                <option value="{{$ind->id}}" {{ (old('job_industry_id') == $ind->id)?'selected="selected"':($applicant AND $applicant->job_industry_id == $ind->id)?'selected="selected"':'' }}>{{$ind->name}}</option>
                            @endforeach
                        </select>
                      </div>
                      <div class="col-md-2">
                        <label class="control-label">Professional License</label>
                        <select name="has_professional_license" class="form-control">
                          <option value="1" {{ (old('has_professional_license') == '1')?'selected="selected"':($applicant AND $applicant->has_professional_license == '1')?'selected="selected"':'' }}>I have</option>
                          <option value="0" {{ (old('has_professional_license') == '0')?'selected="selected"':($applicant AND $applicant->has_professional_license == '0')?'selected="selected"':'' }}>I don't have</option>
                        </select>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-md-4">
                        <label>Course or Degree Earned</label>
                        <input type="text" class="form-control" name="field_of_study" value="{{ old('field_of_study')?:($applicant)?$applicant->field_of_study:'' }}">
                      </div>
                    </div>
                </div>
                <hr>
                <div class="form-group">
                    <div class="row">
                        <div class="col-sm-6">
                            <label>Work Experience Section</label>
                        </div>
                        <div class="col-md-6 text-right">
                            <button type="button" class="btn btn-success btn-sm" id="addExperience">Add Experience</button>
                        </div>
                    </div>
                    @php
                      $year_arr = range(1950,date('Y'));
                      rsort($year_arr);
                    @endphp
                    <div class="experiences">

                      @if(count($applicant)> 0 AND count($applicant->experiences)>0)
                        @php
                          $ctr_exp = 1;
                        @endphp
                        @foreach($applicant->experiences as $exp)
                        <div class="row" id="exp_{{$ctr_exp}}">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                      <div class="col-md-6">
                                        <label>Position</label>
                                        <input type="text" class="form-control" value="{{ $exp->position }}"  name="position_{{$ctr_exp}}" placeholder="Postion">
                                      </div>
                                      <div class="col-md-6">
                                        <label>Company</label>
                                        <input type="text" class="form-control" value="{{ $exp->company_name }}"  name="company_name_{{$ctr_exp}}" placeholder="Company Name">
                                      </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-md-6">
                                          <label>From</label>
                                          <select name="startMonth_{{$ctr_exp}}" class="form-control">
                                            <option value="">Month</option>
                                            <option value="1" {{ $exp->startMonth == '1'?'selected="selected"':'' }}>January</option>
                                            <option value="2" {{ $exp->startMonth == '2'?'selected="selected"':'' }}>February</option>
                                            <option value="3" {{ $exp->startMonth == '3'?'selected="selected"':'' }}>March</option>
                                            <option value="4" {{ $exp->startMonth == '4'?'selected="selected"':'' }}>April</option>
                                            <option value="5" {{ $exp->startMonth == '5'?'selected="selected"':'' }}>May</option>
                                            <option value="6" {{ $exp->startMonth == '6'?'selected="selected"':'' }}>June</option>
                                            <option value="7" {{ $exp->startMonth == '7'?'selected="selected"':'' }}>July</option>
                                            <option value="8" {{ $exp->startMonth == '8'?'selected="selected"':'' }}>August</option>
                                            <option value="9" {{ $exp->startMonth == '9'?'selected="selected"':'' }}>September</option>
                                            <option value="10" {{ $exp->startMonth == '10'?'selected="selected"':'' }}>October</option>
                                            <option value="11" {{ $exp->startMonth == '11'?'selected="selected"':'' }}>November</option>
                                            <option value="12" {{ $exp->startMonth == '12'?'selected="selected"':'' }}>December</option>
                                          </select>
                                          <br>
                                          <select name="startYear_{{$ctr_exp}}" class="form-control">
                                            <option value="">Year</option>
                                            @foreach($year_arr as $startYear)
                                              <option value="{{ $startYear }}" {{ $exp->startYear == $startYear?'selected="selected"':'' }}>{{ $startYear }}</option>
                                            @endforeach
                                          </select>
                                        </div>
                                        <div class="col-md-6">
                                          <div class="endDropdown">
                                            <label>To</label>
                                            <select name="endMonth_{{$ctr_exp}}" class="form-control">
                                              <option value="">Month</option>
                                              <option value="1" {{ $exp->endMonth == '1'?'selected="selected"':'' }}>January</option>
                                              <option value="2" {{ $exp->endMonth == '2'?'selected="selected"':'' }}>February</option>
                                              <option value="3" {{ $exp->endMonth == '3'?'selected="selected"':'' }}>March</option>
                                              <option value="4" {{ $exp->endMonth == '4'?'selected="selected"':'' }}>April</option>
                                              <option value="5" {{ $exp->endMonth == '5'?'selected="selected"':'' }}>May</option>
                                              <option value="6" {{ $exp->endMonth == '6'?'selected="selected"':'' }}>June</option>
                                              <option value="7" {{ $exp->endMonth == '7'?'selected="selected"':'' }}>July</option>
                                              <option value="8" {{ $exp->endMonth == '8'?'selected="selected"':'' }}>August</option>
                                              <option value="9" {{ $exp->endMonth == '9'?'selected="selected"':'' }}>September</option>
                                              <option value="10" {{ $exp->endMonth == '10'?'selected="selected"':'' }}>October</option>
                                              <option value="11" {{ $exp->endMonth == '11'?'selected="selected"':'' }}>November</option>
                                              <option value="12" {{ $exp->endMonth == '12'?'selected="selected"':'' }}>December</option>
                                            </select>
                                            <br>
                                            <select name="endYear_{{$ctr_exp}}" class="form-control">
                                              <option value="">Year</option>
                                              @foreach($year_arr as $startYear)
                                                <option value="{{ $startYear }}" {{ $exp->endYear == $startYear?'selected="selected"':'' }}>{{ $startYear }}</option>
                                              @endforeach
                                            </select>
                                          </div>
                                          <br>
                                          <input type="hidden" name="currently_work_here_{{$ctr_exp}}" value="{{ $exp->currently_work_here }}">
                                          <div class="checkbox custom-checkbox checkCurrent">
                                              <input type="checkbox" name="toCurrent_{{$ctr_exp}}" value="1" id="toCurrent_{{$ctr_exp}}" {{ $exp->currently_work_here == '1'?'checked="checked"':'' }}>
                                              <label for="toCurrent_{{$ctr_exp}}">&nbsp;&nbsp;I currently work here</label>
                                          </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                  <div class="col-md-12">
                                    <label>Reason for leaving</label>
                                      <textarea class="form-control" rows="3" name="resposibilities_{{$ctr_exp}}" placeholder="Tell us about your responsibilities" data-parsley-error-message="Reason for leaving">{{ $exp->resposibilities }}</textarea>
                                  </div>
                                </div>
                            </div>
                        </div>
                        @endforeach
                      @endif
                    </div>

                </div><!--- exp -->
                <hr>
                <div class="form-group">
                    <div class="row">
                        <div class="col-md-6">
                            <label class="control-label">Attach Resume:</label>
                            <input type="file" class="form-control" name="attached_resume" value="{{ old('attached_resume') }}" parsley-filemaxsize="2">
                            <p class="text-muted">Note: Only MS-WORD (.doc) or PDF File (.pdf) are accepted 2 MB</p>
                            @if(count($applicant)>0)
                            <p>You have uploaded resume - {{ str_replace(url('upload-resume').'/', '', $applicant->attached_resume) }}</p>
                            @endif
                        </div>
                        <div class="col-md-3">
                            <label class="control-label">&nbsp;</label>
                            <div class="checkbox custom-checkbox">
                                <input type="checkbox" name="willing_to_relocate" id="willing_to_relocate" {{ old('willing_to_relocate')?'checked="checked"':($applicant AND $applicant->willing_to_relocate == '1')?'checked="checked"':''}} value="1">
                                <label for="willing_to_relocate">&nbsp;&nbsp;Willing to Relocate</label>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <label class="control-label">Current Employment Status</label>
                            <select name="current_employment_status" class="form-control" required="">
                                <option value="">-- Select Industry--</option>
                                @foreach(['actively_seeking' => 'Actively Seeking','currently_employed' => 'Currently Employed'] as $curid => $cur)
                                    <option value="{{$curid}}" {{ old('current_employment_status') == $curid ?'selected="selected"':(count($applicant)>1 AND $applicant->current_employment_status == $curid)?'selected="selected"':'' }}>{{$cur}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>
            </div>
            <div class="panel-footer text-right">
                <button type="submit" class="btn btn-primary"><i class="ico-save"></i> Save</button>
            </div>
        </form>
        <!--/ form profile -->
      </div>
  </div>
@stop

@section('styles')
    <link href="{{asset('hfv1/plugins/bootstrap-datepicker/css/datepicker.css')}}" media="all" rel="stylesheet" type="text/css" />
    <link href="{{asset('hfv1/plugins/bootstrap-datepicker/css/datepicker3.css')}}" media="all" rel="stylesheet" type="text/css" />
    <link href="{{asset('hfv1/plugins/bootstrap-fileinput/css/fileinput.min.css')}}" media="all" rel="stylesheet" type="text/css" />
    <link href="{{asset('hfv1/plugins/summernote/dist/summernote.css')}}" media="all" rel="stylesheet" type="text/css" />
    <link href="{{asset('hfv1/plugins/selectize/css/selectize.css')}}" media="all" rel="stylesheet" type="text/css" />
    <style>
        div.file-thumbnail-footer {
            display: none;
        }
        .experiences {
          padding-top: 10px;
          padding-bottom: 10px;
        }
        .experiences > div:nth-of-type(even)
        {
          background: #ccc;
        }
    </style>
@stop

@section('scripts')
    <script src="{{asset('hfv1/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js')}}"></script>
    <script src="{{asset('hfv1/plugins/summernote/dist/summernote.js')}}"></script>
    <script src="{{asset('hfv1/plugins/summernote/dist/summernote-cleaner.js')}}"></script>
    <script src="{{asset('hfv1/plugins/bootstrap-fileinput/js/plugins/purify.min.js')}}"></script>
    <script src="{{asset('hfv1/plugins/bootstrap-fileinput/js/fileinput.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('hfv1/plugins/jquery-inputmask/dist/jquery.inputmask.bundle.js')}}"></script>
    <script type="text/javascript" src="{{asset('hfv1/plugins/selectize/js/selectize.js')}}"></script>
    <script>
        var ResumeProfile = {
            init : function(){
                this.misc();
                this.addExperience();
                this.initDatePicker($('.datepicker'));
                $('.moneymask').inputmask({
                    'alias' : 'numeric',
                    'groupSeparator' : ',',
                    'autoGroup' : true,
                    'digits' : 2,
                    'digitsOptional' : false,
                    'prefix' : 'Php ',
                    'placeholder' : '0'
                });
                $('.numbermask').inputmask({
                    'alias' : 'integer',
                    'digitsOptional' : false,
                    'placeholder' : '0'
                });
                $('.numbercode').inputmask('(9999) 999-9999');
                $('.skills').selectize({
                                create: true,
                                maxItems: 20
                            });

                $('.languages').selectize({
                                create: true,
                                maxItems: 20
                            });
                this.checkCurrent()
                this.yearGenerate()
                this.currentWork();
            },
            checkCurrent : function(){
              $('.checkCurrent').click(function(){
                var checkBoxDiv = $('.checkCurrent');
                // console.log($(this).find('input').is(':checked'))
                if($(this).find('input').is(':checked')){
                    checkBoxDiv.parent().find('.endDropdown').hide();
                    checkBoxDiv.parent().find('[name^="currently_work_here_"]').val('1');
                    checkBoxDiv.parent().prepend($('<h3 class="currentText">Currently work here</h3>'));
                    $('[name="current_employment_status"]').val('currently_employed')
                }else{
                  checkBoxDiv.parent().find('.endDropdown').show();
                  checkBoxDiv.parent().find('[name="currently_work_here"]').val('0');
                  checkBoxDiv.parent().find('select').val('');
                  $('.currentText').remove()
                  $('[name="current_employment_status"]').val('actively_seeking')
                }
              });
            },
            currentWork : function(){
              var checkBoxDiv = $('.checkCurrent');
              // console.log(checkBoxDiv.find('input').is(':checked'))
              if(checkBoxDiv.find('input').is(':checked')){
                  checkBoxDiv.parent().find('.endDropdown').hide();
                  checkBoxDiv.parent().find('[name^="currently_work_here_"]').val('1');
                  checkBoxDiv.parent().prepend($('<h3 class="currentText">Currently work here</h3>'));
                  $('[name="current_employment_status"]').val('currently_employed')
              }else{
                checkBoxDiv.parent().find('.endDropdown').show();
                checkBoxDiv.parent().find('[name="currently_work_here"]').val('0');
                $('.currentText').remove()
                $('[name="current_employment_status"]').val('actively_seeking')
              }
            },
            addExperience : function(){
                $('#addExperience').click(function(){
                    let countExp = $('.experiences').children().length + 1;
                    $('.experiences').append(ResumeProfile.experienceForm(countExp));
                    ResumeProfile.yearGenerate();
                    ResumeProfile.checkCurrent();
                    ResumeProfile.removeExp();
                });
            },
            yearGenerate : function(){
              var gen = {!! json_encode($year_arr) !!};
              console.log(gen);
              $.each(gen,function(k,v){
                $('.yeargen').append($('<option value="'+v+'">'+v+'</option>'))
              })
            },
            removeExp : function(){
              $('.removableexp').click(function(){
                $(this).parent().parent().remove();
              });
            },
            experienceForm : function(countExp) {
                return `
                <div class="row" id="exp_${countExp}">
                    <div class="col-md-6">
                        <div class="form-group">
                            <div class="row">
                              <div class="col-md-6">
                                <label>Position</label>
                                <input type="text" class="form-control" value=""  name="position_${countExp}" placeholder="Postion">
                              </div>
                              <div class="col-md-6">
                                <label>Company</label>
                                <input type="text" class="form-control" value=""  name="company_name_${countExp}" placeholder="Company Name">
                              </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-6">
                                  <label>From</label>
                                  <select name="startMonth_${countExp}" class="form-control">
                                    <option value="">Month</option>
                                    <option value="1">January</option>
                                    <option value="2">February</option>
                                    <option value="3">March</option>
                                    <option value="4">April</option>
                                    <option value="5">May</option>
                                    <option value="6">June</option>
                                    <option value="7">July</option>
                                    <option value="8">August</option>
                                    <option value="9">September</option>
                                    <option value="10">October</option>
                                    <option value="11">November</option>
                                    <option value="12">December</option>
                                  </select>
                                  <br>
                                  <select name="startYear_${countExp}" class="form-control yeargen">
                                    <option value="">Year</option>
                                  </select>
                                </div>
                                <div class="col-md-6">
                                  <div class="endDropdown">
                                    <label>To</label>
                                    <select name="endMonth_${countExp}" class="form-control">
                                      <option value="">Month</option>
                                      <option value="1">January</option>
                                      <option value="2">February</option>
                                      <option value="3">March</option>
                                      <option value="4">April</option>
                                      <option value="5">May</option>
                                      <option value="6">June</option>
                                      <option value="7">July</option>
                                      <option value="8">August</option>
                                      <option value="9">September</option>
                                      <option value="10">October</option>
                                      <option value="11">November</option>
                                      <option value="12">December</option>
                                    </select>
                                    <br>
                                    <select name="endYear_${countExp}" class="form-control yeargen">
                                      <option value="">Year</option>
                                    </select>
                                  </div>
                                  <br>
                                  <input type="hidden" name="currently_work_here_${countExp}">
                                  <div class="checkbox custom-checkbox checkCurrent">
                                      <input type="checkbox" name="toCurrent_${countExp}" value="1" id="toCurrent_${countExp}">
                                      <label for="toCurrent_${countExp}">&nbsp;&nbsp;I currently work here</label>
                                  </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <button class="btn btn-danger btn-xs removableexp pull-right"><i class="fa fa-times-circle"></i></button>
                        <div class="form-group">
                          <div class="col-md-12">
                            <label>Reason for leaving</label>
                              <textarea class="form-control" rows="3" name="resposibilities_1" placeholder="Tell us about your responsibilities" data-parsley-error-message="Reason for leaving">{{old('resposibilities_1')}}</textarea>
                          </div>
                        </div>
                    </div>
                  </div>
                `;
            },
            initDatePicker: function(elem){
                elem.datepicker({ format: 'yyyy-mm-dd'});
            },
            misc : function(){

                window.ParsleyValidator
                  .addValidator('filemaxsize', function (val, req) {
                      var reqs = req.split('|');
                      var input = $('input[type="file"][name="'+reqs[0]+'"]');
                      var maxsize = reqs[1];
                      var max_bytes = maxsize * 1000000, file = input.files[0];

                      return file.size <= max_bytes;

                  }, 32).addMessage('en', 'filemaxsize', 'The File size is too big.')

                $('.selc').selectize({
                    sortField: 'text'
                });
                $('.select2').select2({
                  allowClear : true
                });
                $('[name="current_region"]').change(function(e){
                  var regcode = $(this).val();
                  console.log('current_region',regcode);
                  $.ajax({
                    url : window.location.origin + '/applicant/fetch-province/' + regcode,
                    type : 'get',
                    success : function(res){
                      if(res.success){
                        var x = '';
                        $('[name="current_province" ]').children().remove();
                        $('[name="current_city" ]').children().remove();
                        $('[name="current_city" ]').prop('disabled','disabled');
                        $('[name="current_province" ]').select2('val',null);
                        $('[name="current_city" ]').select2('val',null);
                        $.each(res.provinces,function(k,v){
                          x  +='<option value="'+v.provCode+'">'+v.provDesc+'</option>';
                        });
                        $('[name="current_province" ]').append($(x));
                        $('[name="current_province" ]').removeAttr('disabled');
                      }
                    },error : function(res){

                    }
                  });
                  e.stopImmediatePropagation();
                  e.preventDefault();
                });

                $('[name="current_province" ]').change(function(e){
                  var provcode = $(this).val();
                  $.ajax({
                    url : window.location.origin + '/applicant/fetch-cities/' + provcode,
                    type : 'get',
                    success : function(res){
                      if(res.success){
                        var x = '';
                        $('[name="current_city" ]').children().remove();
                        $('[name="current_city" ]').select2('val',null);
                        $.each(res.cities,function(k,v){
                          x  +='<option value="'+v.citymunCode+'">'+v.citymunDesc+'</option>';
                        });
                        $('[name="current_city" ]').append($(x));
                        $('[name="current_city" ]').removeAttr('disabled');
                      }
                    },error : function(res){

                    }
                  });
                  e.preventDefault();
                  e.stopImmediatePropagation();
                });

                $('[name="hometown_region"]').change(function(e){
                  var regcode = $(this).val();
                  console.log('hometown_region',regcode);
                  $.ajax({
                    url : window.location.origin + '/applicant/fetch-province/' + regcode,
                    type : 'get',
                    success : function(res){
                      if(res.success){
                        var x = '';
                        $('[name="hometown_province" ]').children().remove();
                        $('[name="hometown_city" ]').children().remove();
                        $('[name="hometown_city" ]').prop('disabled','disabled');
                        $('[name="hometown_province" ]').select2('val',null);
                        $('[name="hometown_city" ]').select2('val',null);
                        $.each(res.provinces,function(k,v){
                          x  +='<option value="'+v.provCode+'">'+v.provDesc+'</option>';
                        });
                        $('[name="hometown_province" ]').append($(x));
                        $('[name="hometown_province" ]').removeAttr('disabled');
                      }
                    },error : function(res){

                    }
                  });
                  e.stopImmediatePropagation();
                  e.preventDefault();
                });

                $('[name="hometown_province" ]').change(function(e){
                  var provcode = $(this).val();
                  $.ajax({
                    url : window.location.origin + '/applicant/fetch-cities/' + provcode,
                    type : 'get',
                    success : function(res){
                      if(res.success){
                        var x = '';
                        $('[name="hometown_city" ]').children().remove();
                        $('[name="hometown_city" ]').select2('val',null);
                        $.each(res.cities,function(k,v){
                          x  +='<option value="'+v.citymunCode+'">'+v.citymunDesc+'</option>';
                        });
                        $('[name="hometown_city" ]').append($(x));
                        $('[name="hometown_city" ]').removeAttr('disabled');
                      }
                    },error : function(res){

                    }
                  });
                  e.preventDefault();
                  e.stopImmediatePropagation();
                });

                $('.summernote').summernote({
                    height: 150,
                    toolbar:[
                            ['style',['style']],
                            ['font',['bold','italic','underline','clear']],
                            ['fontname',['fontname']],
                            ['color',['color']],
                            ['para',['ul','ol','paragraph']],
                            ['height',['height']],
                            ['view',['fullscreen']],
                            ['help',['help']]
                    ],
                    disableDragAndDrop: true,
                    cleaner:{
                               notTime:2400, // Time to display Notifications.
                               action:'both', // both|button|paste 'button' only cleans via toolbar button, 'paste' only clean when pasting content, both does both options.
                               newline:'<br>', // Summernote's default is to use '<p><br></p>'
                               notStyle:'position:absolute;top:0;left:0;right:0', // Position of Notification
                               keepHtml: false, //Remove all Html formats
                               keepClasses: false, //Remove Classes
                               badTags: ['style','script','applet','embed','noframes','noscript', 'html'], //Remove full tags with contents
                               badAttributes: ['style','start'] //Remove attributes from remaining tags
                    }
                })


            }
        }
        $(function(){
            ResumeProfile.init();
        });
        var settter = '{{ (count($applicant)>0 AND strlen($applicant->user->avatar)>0)?0:1 }}';

        $("#input-id").fileinput({
            showUpload :false,
            maxFileCount: 1,
            minFileCount: parseInt(settter),
            showRemove: false,
            allowedFileTypes: ['image'],
            maxFileSize: 2000,
            maxImageWidth: 150,
            maxImageHeight: 150,
            resizeImage: true,
            validateInitialCount: false,
            overwriteInitial: true,
            @if(count($applicant)>0 AND strlen($applicant->user->avatar)>0)
            initialPreview: [
                "<img src='{{$applicant->user->profile_pic}}' style='max-width:150px;max-height:150px;' class='file-preview-image img-responsive'>"
            ],
            initialPreviewConfig: [
                    {width: "150px"},
            ],
            initialPreviewShowDelete: false,
            fileActionSettings: {
                showZoom: false
            }
            @endif
        });

    </script>
@stop
