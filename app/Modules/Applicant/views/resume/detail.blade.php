@extends('Dashboard::template.main')

@section('content')
  <!-- Page Header -->
  <div class="page-header page-header-block">
      <div class="page-header-section">
          <h4 class="title semibold">Resume {{auth()->user()->my_name}}</h4>
      </div>
      <div class="page-header-section">
          <!-- Toolbar -->
          <div class="toolbar clearfix text-right">
              <a href="{{route('resume-profile.edit')}}" class="btn btn-sm btn-warning"  data-toggle="tooltip" data-placement="bottom" title="Edit Resume Profile"><i class="fa fa-edit"></i> EDIT RESUME</a>
          </div>
          <!--/ Toolbar -->
      </div>
  </div>
  <div class="row">
    <div class="col-md-4">
      <div class="widget panel">
          <!-- panel body -->
          <div class="panel-body">
              <ul class="list-unstyled">
                  <li class="text-center">
                      <img class="img-circle img-bordered-success" src="{{ (count($applicant)>0 AND count($applicant->user)>0 AND $applicant->user->profile_pic)?$applicant->user->profile_pic:'' }}" alt="" width="75px" height="75px">
                      <br>
                      <h5 class="semibold mb0">{{ (count($applicant)>0 AND count($applicant->user)>0)?$applicant->user->my_name:'' }}</h5>
                      <p class="nm text-muted">{{ $applicant->position }}</p>
                      <a href="{{ route('download.resume',['code' => $applicant->auid]) }}" class="nm text-teal"><i class="fa fa-file"></i> Download Resume</a>
                  </li>
              </ul>
              <hr>
              <div class="row">
                <div class="col-md-12">
                  <p class="nm">Availability : <strong>{{ (count($applicant)>1 AND $applicant->availability)?strtoupper($applicant->availability):'' }}</strong></p>
                  <p class="nm">Expected Salary : <strong>{{ (count($applicant)>1 AND $applicant->expected_salary)?strtoupper($applicant->expected_salary):'' }}</strong></p>
                  <p class="nm">Preferred Work Location : <strong>{{ (count($applicant)>1 AND $applicant->prefered_location)?strtoupper($applicant->prefered_location->citymunDesc):'' }}</strong></p>
                </div>
              </div>
          </div>
          <!--/ panel body -->
      </div>

      <div class="widget panel">
        <div class="panel-body">
          <div class="col-md-12">
            <div class="row">
              <h4><i class="ico-bubble-dots3"></i> Languages</h4>
              @if(strlen($applicant->languages)>0)
              <ul class="list-inline">
                @foreach(explode(',',$applicant->languages) as $lang)
                <li><span class="label label-default">{{ $lang }}</span></li>
                @endforeach
              </ul>
              @endif
            </div>
            <div class="row">
              <h4><i class="ico-glasses3"></i> Skills</h4>
              @if(strlen($applicant->skills)>0)
              <ul class="list-inline">
                @foreach(explode(',',$applicant->skills) as $skil)
                <li><span class="label label-default">{{ $skil }}</span></li>
                @endforeach
              </ul>
              @endif
            </div>
          </div>
        </div>
      </div>
    </div><!-- col-md-4 -->
    <div class="col-md-8">
      <div class="widget panel">
        <div class="panel-body">
          <div class="table-layout">
            <div class="row">
              <div class="col-md-6">
                <h5 class="panel-title"><i class="ico-vcard"></i> Personal Information</h5>
                <p class="mb5">Full name : <strong class="text-uppercase">{{ $applicant->user->my_name }}</strong></p>
                <p class="mb5">D.O.B : <strong class="text-uppercase">{{ $applicant->date_of_birth?:'None' }}</strong></p>
                <p class="mb5">Email : <strong class="text-uppercase"><a href="mailto:{{ $applicant->user->email }}?Subject=From%20Human%20Factor">{{ $applicant->user->email }}</a></strong></p>
                <p class="mb5">Employment Status: <strong class="text-uppercase">{{ $applicant->employment_status?:'None' }}</strong></p>
                <p class="mb5">Mobile Number: <strong class="text-uppercase">{{ $applicant->mobile_number_1?:'None' }}</strong></p>
                <p class="mb5">House Number: <strong class="text-uppercase">{{ $applicant->house_number_1?:'None' }}</strong></p>
              </div>
              <div class="col-md-6">
                <div class="row">
                  <h5 class="panel-title"><i class="ico-map-marker"></i>Current Address</h5 >
                  <p>{!! nl2br($applicant->current_fulladdress) !!}</p>
                </div>
                <div class="row">
                  <h5 class="panel-title"><i class="ico-map-marker"></i>Provincial Addres</h5 >
                  <p>{!! nl2br($applicant->hometown_fulladdress) !!}</p>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-12">
                <h5 class="panel-title"><i class="fa fa-graduation-cap"></i> Educational Attainment</h5>
                <p class="nm">Highest Education Attainment: <strong>{{ $applicant->education_level->name }}</strong></p>
                <p class="nm">Last School Attended: <strong>{{ $applicant->last_school }}</strong></p>
                <p class="nm">Industry of study: <strong>{{ $applicant->job_industry->name }}</strong></p>
              </div>
            </div><!-- row -->
            <hr>
            <div class="row">
              <div class="col-md-12">
                <h4>Experiences</h4>
                @if(count($applicant->experiences )>0)
                @foreach($applicant->experiences as $exp)
                  @if(strlen($exp->company_name )>0 AND strlen($exp->position)>0)
                  <h5 class="text-uppercase nm">{{ $exp->position }}</h5>
                  <p class="nm">Company : <strong>{{ $exp->company_name }}</strong></p>
                  @endif
                  <p class="nm"><strong>{{  makeMonthName($exp->startMonth).' '.$exp->startYear }} - {{ ($exp->currently_work_here)?'Present':makeMonthName($exp->endMonth) .' '.$exp->endYear}}</strong></p>
                  @if(strlen($exp->resposibilities)>0)
                  <p class="nm">Reason for leaving : <strong>{{ $exp->resposibilities }}</strong></p>
                  @endif
                  <hr style="border-style: dashed;">
                @endforeach
                @else
                <div class="alert alert-info">
                  No data
                </div>
                @endif
              </div>
            </div><!-- row -->




          </div>
        </div>
      </div>
    </div>
  </div>
@stop

@section('scripts')
  <script>
    $(function(){
      // $('.indicator').show();
      $('.indicator').remove();
      // $.ajax({
      //   url: window.location.origin +'/applicant/quotes',
      //   type: 'get',
      //   success:function(data){
      //     if(data.success){
      //       var quotes = data.contents.quotes[0];
      //       $('#cp').text(data.contents.copyright);
      //       $('#author').text(quotes.author);
      //       $('#quote').text(quotes.quote);
      //       $('.indicator').remove();
      //     }
      //   },
      //   error: function(data){
      //     console.log(data);
      //   }
      // });
    });
  </script>
@stop
