<?php
function hfStages($stage){
  switch ($stage) {
    case '2':
      return ['employer_appointment_type','employer_appointment_sched','endorsement_slip'];
      break;
    case '6':
      return ['employer_appointment_type','employer_appointment_sched','endorsement_slip'];
      break;
    case '7':
      return ['employer_appointment_type','employer_appointment_sched','endorsement_slip','note'];
      break;
    case '8':
      return ['hire_date','hire_account','note'];
      break;
    default:
      return [];
      break;
  }
}
function makeMonthName($num){
  return date('F', mktime(0, 0, 0, $num, 10));
}
if(!function_exists('categories')){
  function categories(){
    return [
    1=> "Accounting and Finance",
    2 => "Administrative and HR",
    3 => "Advertising & Media",
    4 => "Architecture and Construction",
    5 => "Arts and Entertainment",
    6 => "Banking / Finance",
    7 => "Beauty Care / Health",
    8 => "Building and Construction",
    9 => "Customer Service",
    10 => "Design and Media",
    11 => "Education and Training",
    12 => "Healthcare",
    13 => "Hospitality / F & B",
    14 => "Information Technology (IT) and Software",
    15 => "Insurance",
    16 => "Legal",
    17 => "Management and Consultancy",
    18 => "Manufacturing / Production",
    19 => "Media and Communications",
    20 => "Real estate and Property",
    21 => "Safety and Security",
    22 => "Sales and Marketing",
    23 => "Sciences",
    24 => "Supply Chain",
    25 => "Writing and Content",
    26 => "Others"
    ];
  }
}

if(!function_exists('br2nl')){
  function br2nl($value = ''){
    return preg_replace('/<br\\s*?\/??>/i','',$value);
  }
}

if(!function_exists('randString')){
  function randString($length) {
    $char = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
    $char = str_shuffle($char);
    for($i = 0, $rand = '', $l = strlen($char) - 1; $i < $length; $i ++) {
        $rand .= $char{mt_rand(0, $l)};
    }
    return $rand;
  }
}
if(!function_exists('limitString')){
    function limitString($string, $limit = 100) {
        // Return early if the string is already shorter than the limit
        if(strlen($string) < $limit) {return $string;}

        $regex = "/(.{1,$limit})\b/";
        preg_match($regex, $string, $matches);
        return $matches[1];
    }
}
if(!function_exists('textarea_input')){
  function textarea_input($value = ''){
    return nl2br(htmlentities($value, ENT_QUOTES, 'UTF-8'));
  }
}

if(!function_exists('random_str')){
  function random_str($length, $keyspace = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ')
  {
      $str = '';
      $max = mb_strlen($keyspace, '8bit') - 1;
      for ($i = 0; $i < $length; ++$i) {
          $str .= $keyspace[random_int(0, $max)];
      }
      return $str;
  }
}
if(!function_exists('avatar')){
  function avatar($str){
      return ($str)?:asset('image/avatar/avatar.png');
  }
}

if(!function_exists('remove_currency')){
  function remove_currency($str){
      $str = (float) str_replace(',','',str_replace('₱ ','',$str));
      return $str;
  }
}




if (!function_exists('in_range')) {
  function in_range($number, $min, $max, $inclusive = FALSE)
  {
      if (is_int($number) && is_int($min) && is_int($max))
      {
          return $inclusive
              ? ($number >= $min && $number <= $max)
              : ($number > $min && $number < $max) ;
      }

      return FALSE;
  }
}

if (!function_exists('diffInHours')) {
  function diffInHours($date)
  {
    return Carbon\Carbon::createFromFormat('Y-m-d H:i:s',$date)->diffInHours(Carbon\Carbon::now());
  }
}

if (!function_exists('dateForHumans')) {
  function dateForHumans($date)
  {
    return Carbon\Carbon::createFromFormat('Y-m-d H:i:s',$date)->diffForHumans();
  }
}

if (!function_exists('date_inputs')) {
  function date_inputs($date)
  {
    return Carbon\Carbon::createFromFormat('Y-m-d H:i:s',$date)->format('Y-m-d');
  }
}

if (!function_exists('toDayDateTimeString')) {
  function toDayDateTimeString($date)
  {
    return Carbon\Carbon::createFromFormat('Y-m-d H:i:s',$date)->toDayDateTimeString();
  }
}
if (!function_exists('fromDateString')) {
  function fromDateString($date)
  {
    return Carbon\Carbon::createFromFormat('m-d-Y',$date)->format('Y-m-d');
  }
}

if (!function_exists('toDateString2')) {
  function toDateString2($date)
  {
    return Carbon\Carbon::createFromFormat('Y-m-d',$date)->format('Y-m-d');
  }
}

if (!function_exists('toDayDateTimeString2')) {
  function toDayDateTimeString2($date)
  {
    return Carbon\Carbon::createFromFormat('Y-m-d',$date)->toDayDateTimeString();
  }
}

if (!function_exists('toDateString')) {
  function toDateString($date)
  {
    $date = Carbon\Carbon::parse($date);
    return Carbon\Carbon::createFromFormat('Y-m-d H:i:s',$date)->toDateString();
  }
}
if (!function_exists('toFormattedDateString')) {
  function toFormattedDateString($date)
  {
    return (strtotime($date)>0) ? Carbon\Carbon::createFromFormat('Y-m-d H:i:s',$date)->toFormattedDateString():'None';
  }
}
if (!function_exists('formatValue')) {
  // function formatNumberAbbreviation($number) {
  //       $abbrevs = array(12 => "T", 9 => "B", 6 => "M", 3 => "K", 0 => "");
  //
  //       foreach($abbrevs as $exponent => $abbrev) {
  //           if($number >= pow(10, $exponent)) {
  //               return intval($number / pow(10, $exponent)) . $abbrev;
  //           }
  //       }
  //   }
  function formatValue($size, $precision = 1)
  {
      static $suffixes = array('', 'k', 'm');
      $base = log($size) / log(1000);

      if ($base >= 5/3 && $base < 2) {
          return round(pow(1000, $base - floor($base)) / 100, $precision) . 'lakh';
      }

      return round(pow(1000, $base - floor($base)), $precision) . $suffixes[floor($base)];
  }
}
if (!function_exists('progress_style')) {
    function progress_style($data = 0){
        // [1=> 'danger','danger','info','warning','success']
          switch ($data) {
            case in_range($data, 1, 20, TRUE):
              return 'warning';
              break;
            case in_range($data, 21, 40, TRUE):
                return 'info';
                break;
            case in_range($data, 41, 60, TRUE):
                return 'primary';
                break;
            case in_range($data, 61, 80, TRUE):
                return 'teal';
                break;
            case in_range($data, 81, 100, TRUE):
                return 'success';
                break;
            default:
              return 'danger';
              break;
          }
    }
}

if (!function_exists('remove_col')) {
    function remove_id($data = []){
          return array_except($data, ['id','created_at','updated_at']);
    }
}

if(!function_exists('selected')){
    function selected($request, $should){
        return ($request == $should) ? 'selected' : '';
    }
}

if(!function_exists('activeClass')){
    function activeClass($request, $should){
        return ($request == $should) ? 'active' : '';
    }
}

if(!function_exists('contact_format')){
  function contact_format($phoneNumber,$format ='ph'){
    $phoneNumber = preg_replace('/[^0-9]/','',$phoneNumber);
    if($format == 'ph'){
      if(strlen($phoneNumber) == 10) {
          $areaCode = substr($phoneNumber, 0, 3);
          $nextThree = substr($phoneNumber, 3, 3);
          $lastFour = substr($phoneNumber, 6, 4);

          $phoneNumber = '('.$areaCode.') '.$nextThree.'-'.$lastFour;
      }
      if(strlen($phoneNumber) == 11) {
        $areaCode = substr($phoneNumber, 0, 4);
        $nextThree = substr($phoneNumber, 4, 3);
        $lastFour = substr($phoneNumber, 7, 4);
          $phoneNumber = '('.$areaCode.') '.$nextThree.'-'.$lastFour;
      }
    }

    if($format != 'ph'){
     if(strlen($phoneNumber) > 10) {
         $countryCode = substr($phoneNumber, 0, strlen($phoneNumber)-10);
         $areaCode = substr($phoneNumber, -10, 3);
         $nextThree = substr($phoneNumber, -7, 3);
         $lastFour = substr($phoneNumber, -4, 4);

         $phoneNumber = '+'.$countryCode.' ('.$areaCode.') '.$nextThree.'-'.$lastFour;
     }
     else if(strlen($phoneNumber) == 10) {
         $areaCode = substr($phoneNumber, 0, 3);
         $nextThree = substr($phoneNumber, 3, 3);
         $lastFour = substr($phoneNumber, 6, 4);

         $phoneNumber = '('.$areaCode.') '.$nextThree.'-'.$lastFour;
     }
     else if(strlen($phoneNumber) == 7) {
         $nextThree = substr($phoneNumber, 0, 3);
         $lastFour = substr($phoneNumber, 3, 4);

         $phoneNumber = $nextThree.'-'.$lastFour;
     }
   }
   return $phoneNumber;
  }
}
