<?php namespace App\Modules\Landing\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Modules\Applicant\Requests\ApplyJobRequest;
use App\Modules\Landing\Requests\QuickApplyRequest;

use App\Mail\ApplicantQuickApply;
use App\Mail\EmployerNotifyApplication;

use App\Job,
    App\ApplicantEducation,
    App\ApplicantEducationLevel,
    App\ApplicantExperience,
    App\Applicant,
    App\ApplicantJob,
    App\JobIndustry,
    App\IRecruiter,
    App\Referral,
    App\Region,
    App\User;

use App\Traits\ImageUpload;

class JobController extends Controller
{

  use ImageUpload;
  protected $jobs;
  protected $users;
  protected $appliedJobs;
  protected $jindustries;
  protected $applicant;
  protected $irecruiter;
  protected $referral;
  protected $location_region;

  public function __construct(Job $jobs,
                              ApplicantJob $appliedJobs,
                              JobIndustry $jindustries,
                              Applicant $applicant,
                              ApplicantEducation $appEducation,
                              ApplicantEducationLevel $appEduLevel,
                              ApplicantExperience $appExperience,
                              IRecruiter $irecruiter,
                              Referral $referral,
                              Region $location_region,
                              User $users)
  {
      $this->users = $users;
      $this->jobs = $jobs;
      $this->appEducation = $appEducation;
      $this->appEduLevel = $appEduLevel;
      $this->appExperience = $appExperience;
      $this->applicant = $applicant;
      $this->appliedJobs = $appliedJobs;
      $this->jindustries = $jindustries;
      $this->irecruiter = $irecruiter;
      $this->referral = $referral;
      $this->location_region = $location_region;
  }


    public function index()
    {
        $req_premiumjobs = (\Request::has('t') AND \Request::get('t') == 'reg')?0:1;
        $industries_q = '';
        $q = \Request::has('q')?\Request::get('q'):'';
        $reg = \Request::has('reg')?\Request::get('reg'):'';
        $prov = \Request::has('prov')?\Request::get('prov'):'';
        $city = \Request::has('city')?\Request::get('city'):'';
        if(\Request::has('category')){
          $industries_q = $this->jindustries->where('slug','=',\Request::get('category'))->first();
        }
        // return $industries_q;
        $jobs = $this->jobs->query();
        $jobs->SearchPremiumJobs($req_premiumjobs);
        if($industries_q){
          $jobs->whereRaw('(job_industry_id = ?)',[$industries_q->id]);
        }
        if($q){
          $jobs->where('job_title','like',['%'.$q.'%']);
        }
        if($reg){
          $jobs->where('location_region','like',[$reg]);
        }
        if($prov){
          $jobs->where('location_province','like',[$prov]);
        }
        if($city){
          $jobs->where('location_city','like',[$city]);
        }

        $jobs->where('valid_until','>',\Carbon\Carbon::now())
          ->whereStatus('open')
          ->orderBy('created_at','desc');
        $jobs = $jobs->paginate(50);
        // return dd(\DB::getQueryLog());
        $industries = $this->jindustries->orderBy('id','asc')->get();
        $location_region = $this->location_region->orderBy('regCode','asc')->get();
        return view('Landing::jobs-v2.index')
                    ->with(compact('jobs','industries','location_region'));
    }

    public function show($job_title,$job_code)
    {
        $job = $this->jobs
                    ->where('job_code','=',$job_code)
                    ->whereStatus('open')
                    ->first();

        $levels = $this->appEduLevel->orderBy('name','asc')->get();

        $industries = $this->jindustries->orderBy('id','asc')->get();
        if(count($job)<1){
          return abort(404);
        }
        if(\Session::has('referral_code')){
          $referral = $this->referral
                          ->where('uniq_link','=',\Session::get('referral_code'))
                          ->where('job_code','=',$job_code)
                          ->first();
        }
        $location_region = $this->location_region->orderBy('regCode','asc')->get();
        return view('Landing::jobs-v2.details')
                      ->with(compact('job','industries','location_region','referral','levels'));
    }

    public function fetchMyreferrals(Request $request)
    {
      $q = $request->get('q');
      if(strlen($q)>0){
          $referrals = \App\User::whereHas('resume',function($query){
                    $query->where('referred_by_id','=',auth()->user()->id);
                })->whereRaw('(users.firstname like ? OR users.lastname like ? OR users.email like ?)',['%'.$q.'%','%'.$q.'%','%'.$q.'%'])
                  ->limit(15)
                  ->get();
          $referrals = $referrals->pluck('my_name','email');
          $y = [];
          $x = [];
          foreach($referrals as $rk => $rv){
              $x['email'] = $rk;
              $x['my_name'] = $rv;
              $y[] = $x;
          }
          $referrals = $y;
          return response()->json(['referrals' => $referrals],200);
      }
    }

    public function referralShow($job_title,$job_code,$referral_link)
    {
        $job = $this->jobs
                    ->where('job_code','=',$job_code)
                    ->whereStatus('open')
                    ->first();
        $industries = $this->jindustries->orderBy('id','asc')->get();
        $referral = $this->referral->where('uniq_link','=',$referral_link)->first();
        if(count($referral)<1){
          // show($job_title,$job_code)
          return redirect('jobs.details')->route('',['job_title' => $job_title,'job_code'=>$job_code]);
        }
        \Session::put('referral_code',$referral_link);
        return view('Landing::jobs.details')
                    ->with(compact('job','industries','referral'));
    }

    public function applyJob(ApplyJobRequest $request)
    {
        if($request->ajax()){
            $job =  $this->jobs->whereJobCode($request->get('job_code'))->first();
            if(count($job)<1){
                return response()->json(['error' => 'Can\'t find your data'],401);
            }
            $check = $this->jobs->applied()
                             ->whereJobCode($request->get('job_code'))
                             ->first();
            if($check){
                return response()->json(['error' => 'You have already applied'],401);
            }
            $inputs = $request->only('applicant_cover_letter','expected_salary');
            $inputs['applicant_cover_letter'] = br2nl($request->get('applicant_cover_letter'));
            $inputs['applicant_id'] =  (auth()->user()->resume)?auth()->user()->resume->id:0;
            $inputs['job_id'] = $job->id;
            $inputs['employer_id'] = $job->employer->id;
            $inputs['applicant_status_id'] = 1; //Active File
            $inputs['is_visible_to_employer'] = 0;
            $data = $this->appliedJobs->create($inputs);
            return response()->json(['success' => true,'data' => $data],200);
        }

    }

    public function quickApply(QuickApplyRequest $request)
    {
      $job = $this->jobs->where('job_code','=',$request->get('job_code'))->first();
      if(count($job)<1){
          return abort(404);
      }
      //Create user
      $user_inputs = $request->only('firstname','lastname','email');
      $user_inputs['user_type_id'] = 5;
      $user_inputs['is_active'] = 1;
      $user_inputs['password']  = \Hash::make($request->get('password'));
      $user = $this->users->create($user_inputs);
      \Auth::login($user);
      if($request->hasFile('attached_resume')){
          $files = [];
          $file = $request->file('attached_resume');
          $filename = str_random(5).'-eq-'.str_slug($user->my_name).'.'.$file->getClientOriginalExtension();
          $destination= public_path('upload-resume').'/'.$filename;
          $file->move(public_path('upload-resume'),$filename);
          if(count($user->resume)>0 AND strlen($user->resume->attached_resume)>0){
              $this->deleteImage($user->resume->attached_resume);
          }
          $applicant_inputs['attached_resume'] = url('upload-resume').'/'.$filename;
      }

      $applicant_inputs = $request->only('job_industry_id','current_address','date_of_birth',
                                        'current_region','current_province','current_city',
                                        'hometown_region','hometown_province','hometown_city',
                                        'hometown_address','skills','languages','prefered_work_location',
                                        'position','willing_to_relocate','education_level_id','last_school',
                                      'field_of_study','has_professional_license');
      $applicant_inputs['user_id'] = $user->id;
      $applicant_inputs['mobile_number_1'] = $request->get('number_mobile');
      $applicant_inputs['house_number_1'] = $request->get('number_landline');
      $applicant_inputs['expected_salary'] = $request->get('current_salary');
      $internal = $this->irecruiter
                        ->where('status','active')
                        ->orderBy('task_on_hand','asc')
                        ->limit(1)
                        ->first();

      $applicant_inputs['hf_assigned'] = $internal->user_id;
      $inc = (int)$internal->task_on_hand + 1;
      $this->irecruiter->whereId($internal->id)->update(['task_on_hand'=> $inc]);
      //Create resume
      $applicant_profile = $this->applicant->create($applicant_inputs);
      $inputs = array_except($request->all(),['job_industry_id','current_address','date_of_birth',
                                        'current_region','current_province','current_city',
                                        'hometown_region','hometown_province','hometown_city',
                                        'hometown_address','skills','languages','firstname','lastname','email',
                                      'attached_resume','user_id','number_mobile','number_landline']);
      $countExp = $this->countExp($inputs);
      $arr_runExp = $this->runExp($countExp,$inputs,$applicant_profile->id);
      //Apply for this job
      $appliedJobs = $this->appliedJobs->create(['applicant_id' => $applicant_profile->id,
                                  'job_id' => $job->id,
                                  'employer_id' => $job->employer_id,
                                  'applicant_cover_letter' => br2nl($request->get('applicant_cover_letter')),
                                  'expected_salary' => $request->get('expected_salary'),
                                  'applicant_status_id' => 1,
                                  'is_visible_to_employer' => 0]);
      //Send email
      $email_inputs['firstname'] = $user->firstname;
      $email_inputs['lastname'] = $user->lastname;
      $email_inputs['job_title'] =  $job->job_title;
      $email_inputs['job_title_slug'] =  $job->slug;
      $email_inputs['job_code'] = $job->job_code;
      $email_inputs['email'] = $user->email;
      $when = \Carbon\Carbon::now()->addMinutes(2);
      // \Mail::later($when,new NotifyEmployerHFstatus($email_inputs));
      \Mail::later($when,new ApplicantQuickApply($email_inputs));
      // if($job->employer->is_premium < 1){
          $email_inputs['employer_email'] = $job->employer->user->email;
          $email_inputs['company_name'] = $job->employer->company_name;
          $email_inputs['applicant_email'] = $user->email;
          \Mail::later($when,new EmployerNotifyApplication($email_inputs));
      // }

      return redirect()->back()
                    ->with(['quickApplyMessage' => 'You have succefully applied for this job']);
    }


    protected function runExp($countExp,$inputs,$applicant_id)
    {   $experience = [];
        $check = [];
        if(!$countExp){
            return $check;
        }
        foreach(range(1,$countExp) as $expIndex => $expVal){
            $experience['applicant_id'] = $applicant_id;
            $experience['company_name'] = isset($inputs['company_name_'.$expVal])?$inputs['company_name_'.$expVal]:null;
            $experience['position'] = isset($inputs['position_'.$expVal])?$inputs['position_'.$expVal]:null;
            $experience['resposibilities'] = isset($inputs['resposibilities_'.$expVal])?$inputs['resposibilities_'.$expVal]:null;
            $experience['startMonth'] = isset($inputs['startMonth_'.$expVal])?$inputs['startMonth_'.$expVal]:null;
            $experience['startYear'] = isset($inputs['startYear_'.$expVal])?$inputs['startYear_'.$expVal]:null;
            $experience['endMonth'] = isset($inputs['endMonth_'.$expVal])?$inputs['endMonth_'.$expVal]:null;
            $experience['endYear'] = isset($inputs['endYear_'.$expVal])?$inputs['endYear_'.$expVal]:null;
            $experience['currently_work_here'] = isset($inputs['currently_work_here_'.$expVal])?$inputs['currently_work_here_'.$expVal]:null;
            $check[] = $this->appExperience->create($experience);
        }
        return $check;
    }

    protected function countExp($inputs)
    {
        $count_expcompany_name = count(preg_grep("/^company_name_(\d)+$/",array_keys($inputs)));
        $count_expposition = count(preg_grep("/^position_(\d)+$/",array_keys($inputs)));
        $sumexp = ($count_expcompany_name + $count_expposition) % 2;
        if($sumexp > 0){
            return redirect()->back()->withErrors(['message' => ' Please information on Experience section such as Company Name and Position']);
        }
        return $count_expcompany_name;
    }
}
