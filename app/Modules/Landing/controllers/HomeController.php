<?php namespace App\Modules\Landing\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests;

class HomeController extends Controller
{


    public function index()
    {
      return redirect()->route('jobs');
      return view('Landing::home');
    }

    public function aboutUs()
    {
        return redirect()->route('jobs');
        return view('Landing::about-us');
    }

    public function faq()
    {
        return redirect()->route('jobs');
        return view('Landing::faq');
    }

    public function pricing()
    {
        return redirect()->route('jobs');
        return view('Landing::pricing');
    }

    public function howItWorks()
    {
        return redirect()->route('jobs');
        return view('Landing::howItWorks');
    }

    public function register()
    {
        return view('Landing::signup');
    }

    public function login()
    {
        return view('Landing::login');
    }

    public function activity()
    {
        return redirect()->route('jobs');
        return view('Landing::create-resume');
    }
}
