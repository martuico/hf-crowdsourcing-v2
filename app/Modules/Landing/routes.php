<?php


Route::group(['module' => 'Landing',
              'namespace' => 'App\Modules\Landing\Controllers',
              'middleware' => 'web'
          ],
            function() {

    Route::post('quick-apply',['as' => 'quickapply','uses' => 'JobController@quickApply']);
    Route::post('jobs/applied',['as' => 'jobs.applied','uses' => 'JobController@applyJob']);
    Route::get('fetch-myreferrals',['uses' => 'JobController@fetchMyreferrals']);
    Route::get('jobs/{job_title}/{job_code}/r/{referral_link}',['as' => 'jobs.referral_link','uses' => 'JobController@referralShow']);
    Route::get('jobs/{job_title}/{job_code}',['as' => 'jobs.details','uses' => 'JobController@show']);
    Route::get('jobs',['as' => 'jobs','uses' => 'JobController@index']);
    Route::get('special-recruiment-activity',['as' => 'recruitment.activity','uses' => 'HomeController@activity']);
    Route::get('about-us',['as' => 'aboutUs','uses'=>'HomeController@aboutUs']);
    Route::get('how-it-works',['as' => 'howItWorks','uses' => 'HomeController@howItWorks']);
    Route::get('pricing',['as' => 'pricing','uses' => 'HomeController@pricing']);
    Route::get('faq',['as' => 'faq','uses' => 'HomeController@faq']);
    Route::get('/',['as' => 'home','uses' => 'HomeController@index']);
});
