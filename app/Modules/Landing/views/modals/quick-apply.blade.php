<div class="modal fade" id="quickApplyWizard" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Quick Application</h4>
      </div>
      <div class="modal-body">
        <!-- START Form Wizard -->
        {{ \Form::open(['route' => 'quickapply','class' => 'form-horizontal','id' => 'wizard-validate','files' => true]) }}
            <!-- Wizard Container 2 -->
            <div class="wizard-title">Information</div>
            <div class="wizard-container">
                <div class="form-group">
                    <div class="col-md-12">
                        <h5 class="semibold text-primary nm">Provide your account details.</h5>
                    </div>
                </div>
                <input type="hidden" name="job_code">
                <div class="form-group">
                    <label class="col-sm-2 control-label">Name <span class="text-danger">*</span></label>
                    <div class="col-sm-4">
                        <input type="text" name="firstname" class="form-control" placeholder="First Name" data-parsley-group="information" data-parsley-required>
                    </div>
                    <div class="col-sm-4">
                        <input type="text" name="lastname" class="form-control" placeholder="Last Name" data-parsley-group="information" data-parsley-required>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">Email <span class="text-danger">*</span></label>
                    <div class="col-sm-5">
                        <input type="email" name="email" onblur="ResumeProfile.checkEmail()" class="form-control" data-parsley-group="information" data-parsley-required data-parsley-type="email">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">Birthday <span class="text-danger">*</span></label>
                    <div class="col-sm-5">
                        <input type="text" name="date_of_birth" class="form-control datepicker" data-parsley-group="information" data-parsley-required>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">Password <span class="text-danger">*</span></label>
                    <div class="col-sm-4">
                        <input type="password" name="password" class="form-control" placeholder="Password" data-parsley-group="information" data-parsley-required>
                    </div>
                    <div class="col-sm-4">
                        <input type="password" name="password_confirmation" class="form-control" placeholder="Confirm Password" data-parsley-group="information" data-parsley-error-message="Confirm Password must be the same with Password" data-parsley-required data-parsley-equalto="input[name=password]">
                    </div>
                </div>

            </div>
            <!--/ Wizard Container 2 -->
            <div class="wizard-title">Contact</div>
            <div class="wizard-container">
              <div class="form-group">
                  <label class="col-sm-2 control-label">Current Address <i class="text-danger">*</i></label>
                  <div class="col-sm-10">
                      <div class="row">
                          <div class="col-sm-12">
                              <input type="text" name="current_address" class="form-control mb5" placeholder="Street Address" required="">
                          </div>
                          <div class="col-sm-6">
                            <select name="current_region" class="select2" required="">
                              <option value="">--  Select Region--</option>
                              @foreach($location_region as $region)
                              <option value="{{ $region->regCode }}">{{ $region->regDesc }}</option>
                              @endforeach
                            </select>
                          </div>
                          <div class="col-sm-6">
                              <select name="current_province" class="select2"
                                disabled="" placeholder="City">
                                <option value="">-- Select Province --</option>
                              </select>
                          </div>

                      </div>
                      <div class="row">
                        <div class="col-sm-6">
                          <select name="current_city" class="select2"
                          disabled="">
                            <option value="">-- Select City --</option>
                          </select>
                        </div>
                      </div>
                  </div>
              </div>
              <div class="form-group">
                  <label class="col-sm-2 control-label">Provincial Address</label>
                  <div class="col-sm-10">
                      <div class="row">
                          <div class="col-sm-12">
                              <input type="text" name="hometown_address" class="form-control mb5" placeholder="Street Address">
                          </div>
                          <div class="col-sm-6">
                            <select name="hometown_region" class="select2" placeholder="Region">
                              <option value="">--  Select Region--</option>
                              @foreach($location_region as $region)
                                <option value="{{ $region->regCode }}">{{ $region->regDesc }}</option>
                              @endforeach
                            </select>
                          </div>
                          <div class="col-sm-6">
                              <select name="hometown_province" class="select2"
                                disabled="" placeholder="Province">
                                <option value="">-- Select Province --</option>
                              </select>
                          </div>
                      </div>
                      <div class="row">
                        <div class="col-sm-6">
                          <select name="hometown_city" class="select2"
                          disabled="">
                            <option value="">-- Select City --</option>
                          </select>
                        </div>
                      </div>
                  </div>
              </div>
              <div class="form-group">
                  <label class="col-sm-2 control-label">Mobile <br>Number <span class="text-danger">*</span></label>
                  <div class="col-sm-5">
                      <input type="text" name="number_mobile" class="form-control numbercode" data-parsley-group="contact" data-parsley-required>
                  </div>
              </div>
              <div class="form-group">
                  <label class="col-sm-2 control-label">Landline <br> Number </label>
                  <div class="col-sm-5">
                      <input type="text" name="number_landline" class="form-control numbercode" data-parsley-group="contact" >
                  </div>
              </div>
            </div>
            <!-- Wizard Container 3 -->
            <div class="wizard-title">Job Info</div>
            <div class="wizard-container">
                <div class="form-group">
                    <label class="col-sm-2 control-label">Title & Employment Status <span class="text-danger">*</span></label>
                    <div class="col-sm-5">
                        <input type="text" name="position" class="form-control" data-parsley-group="jobinfo" data-parsley-required >
                    </div>
                    <div class="col-sm-3">
                        <select name="current_employment_status" class="form-control" id="">
                          <option value="actively_seeking" selected="">Actively Seeking</option>
                          <option value="currently_employed" id="">Currently Employed</option>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">Availability <span class="text-danger">*</span></label>
                    <div class="col-sm-6">
                      <select name="availability" class="form-control" required="" data-parsley-group="jobinfo">
                        <option value="">-- Select Availability--</option>
                        <option value="asap">ASAP</option>
                        <option value="within_a_month">With in a month</option>
                        <option value="others">Others</option>
                    </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">Current Salary <span class="text-danger">*</span></label>
                    <div class="col-sm-5">
                        <input type="text" name="current_salary" class="form-control moneymask" data-parsley-group="jobinfo" data-parsley-required>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">Willing to relocate <span class="text-danger">*</span></label>
                    <div class="col-sm-5">
                      <select name="willing_to_relocate" class="form-control" id="">
                        <option value="1" id="">Yes</option>
                        <option value="0" id="">No</option>
                      </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">Prefered Work Location <span class="text-danger">*</span></label>
                    <div class="col-sm-4">
                        <select name="prefered_work_location" class="form-control" data-parsley-group="payment" data-parsley-required>
                          <option value="064501">BACOLOD CITY (Capital)</option>
                          <option value="072217">CEBU CITY (Capital)</option>
                          <option value="104305">CAGAYAN DE ORO CITY (Capital)</option>
                          <option value="112402">DAVAO CITY</option>
                          <option value="063022">ILOILO CITY (Capital)</option>
                          <option value="137602">CITY OF MAKATI</option>
                          <option value="137401">CITY OF MANDALUYONG</option>
                          <option value="137603">CITY OF MUNTINLUPA</option>
                          <option value="137604">CITY OF PARAÑAQUE</option>
                          <option value="137605">PASAY CITY</option>
                          <option value="137404">QUEZON CITY</option>
                          <option value="137607">TAGUIG CITY</option>
                        </select>
                    </div>
                </div>
            </div>
            <!--/ Wizard Container 3 -->
            <div class="wizard-title">Education</div>
            <div class="wizard-container">
              <div class="row">
                  <div class="col-sm-6">
                      <label class="control-label">Skills</label>
                      <input type="text" class="form-control skills" name="skills" value="" placeholder="ex. Web Design, Trainable, Leadership skills">
                      <p class="text-muted">ex. MSWORD, Leardership, Excel, Photoshop</p>
                  </div>
                  <div class="col-sm-6">
                      <label class="control-label">Languages</label>
                      <input type="text" class="form-control languages" name="languages" value="" placeholder="ex. English, Tagalog, Spanish">
                      <p class="text-muted">ex. English, Tagalog, Spanish</p>
                  </div>
              </div>
              <div class="form-group">
                  <label class="col-sm-2 control-label">Highest Educational Attainment <span class="text-danger">*</span></label>
                  <div class="col-sm-5">
                    <select name="education_level_id" class="form-control">
                        <option value="">-- Select Industry--</option>
                        @foreach($levels as $lev)
                            <option value="{{$lev->id}}">{{$lev->name}}</option>
                        @endforeach
                    </select>
                  </div>
              </div>
              <div class="form-group">
                  <label class="col-sm-2 control-label">Last School Attended <span class="text-danger">*</span></label>
                  <div class="col-sm-5">
                      <input type="text" name="last_school" class="form-control" data-parsley-group="education" data-parsley-required>
                  </div>
              </div>
              <div class="form-group">
                  <label class="col-sm-2 control-label">Title/Degree Finised <span class="text-danger">*</span></label>
                  <div class="col-sm-5">
                      <input type="text" name="field_of_study" class="form-control" data-parsley-group="education" data-parsley-required>
                      <p class="text-muted">ex. Bachelor of Science in I.T., High School Graduate, 2 Years in Business Administration</p>
                  </div>
              </div>
              <div class="form-group">
                  <label class="col-sm-2 control-label">Field of interest <span class="text-danger">*</span></label>
                  <div class="col-sm-5">
                    <select name="job_industry_id" class="form-control">
                        <option value="">-- Select Industry--</option>
                        @foreach($industries as $ind)
                            <option value="{{$ind->id}}">{{$ind->name}}</option>
                        @endforeach
                    </select>
                  </div>
              </div>
              <div class="form-group">
                  <label class="col-sm-2 control-label">Professional License <span class="text-danger">*</span></label>
                  <div class="col-sm-5">
                    <select name="has_professional_license" class="form-control">
                      <option value="1">I have</option>
                      <option value="0">I don't have</option>
                    </select>
                  </div>
              </div>
            </div>
            <!-- Wizard Container 4 -->
            <div class="wizard-title">Work Experience</div>
            <div class="wizard-container">
                <div class="form-group">
                    <div class="col-md-12">
                        <h5 class="semibold text-primary nm">Work Experiences</h5>
                    </div>
                </div>
                @php
                  $year_arr = range(1950,date('Y'));
                  rsort($year_arr);
                @endphp
                <input type="hidden" class="year" data-years="{{ json_encode($year_arr) }}">
                <div class="row">
                    <div class="col-sm-6">
                        <label>Work Experience Section</label>
                    </div>
                    <div class="col-md-6 text-right">
                        <button type="button" class="btn btn-success btn-sm" id="addExperience">Add Experience</button>
                    </div>
                </div>
                <div class="experiences">
                </div>
            </div>
            <!-- Wizard Container 4 -->
            <div class="wizard-title">Job Application</div>
            <div class="wizard-container">
              <div class="form-group">
                <label class="col-sm-2 control-label">Job Expected Salary <span class="text-danger">*</span></label>
                <div class="col-md-5">
                  <input type="text" class="form-control moneymask" required="" name="expected_salary" placeholder=""  data-parsley-group="terms">
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-2 control-label">Application / Cover Letter <span class="text-danger">*</span></label>
                <div class="col-sm-7">
                  <textarea name="applicant_cover_letter" id="cover_letter" style="min-height: 250px;" class="form-control" required="" data-parsley-group="terms"></textarea>
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-2 control-label">Attached Latest CV</label>
                <div class="col-sm-5">
                  <input type="file" class="form-control" name="attached_resume">
                </div>
              </div>
              <div class="form-group">
                  <div class="col-md-10 col-md-offset-1">
                      <span class="checkbox custom-checkbox">
                          <input type="checkbox" name="checkbox-tos" id="checkbox-tos" required="" data-parsley-group="terms">
                          <label for="checkbox-tos">&nbsp;&nbsp;I agree with this site <a href="javascript:void(0);">Term Of Services</a></label>
                      </span>
                  </div>
              </div>
            </div>
        </form>
        <!--/ END Form Wizard -->
      </div>
      <!--div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Save changes</button>
      </div-->
    </div>
  </div>
</div>
