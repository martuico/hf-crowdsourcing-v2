<!DOCTYPE html>
<html class="frontend">
    <!-- START Head -->
    <head>
        <!-- START META SECTION -->
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Humanfactor - Recruitment Crowdsourcing</title>
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <meta name="author" content="Alphanum.ph">
        <meta name="description" content="Humanfactor Recruitment is reinventing your job-seeking experience">
        <meta name="keywords" content="Job Portal, Manila, Cebu, Davao, Hiring, Philippines, BPO">
        <meta property="og:url" content="http://humanfactor.alphanum.ph/" />
        <meta property="og:image" content="http://humanfactor.alphanum.ph/hfv1/image/background/cover_page.jpg" />
        <meta property="og:title" content="Humanfactor - It's all about people" />
        <meta property="og:description" content="Humanfactor is reinventing your job-seeking experience. An easier, faster, and better experience awaits all applicants with its new career portal." />

        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">

        <link rel="apple-touch-icon-precomposed" sizes="144x144" href="{{asset('hfv1/image/touch/apple-touch-icon-144x144-precomposed.png')}}">
        <link rel="apple-touch-icon-precomposed" sizes="114x114" href="{{asset('hfv1/image/touch/apple-touch-icon-114x114-precomposed.png')}}">
        <link rel="apple-touch-icon-precomposed" sizes="72x72" href="{{asset('hfv1/image/touch/apple-touch-icon-72x72-precomposed.png')}}">
        <link rel="apple-touch-icon-precomposed" href="{{asset('hfv1/image/touch/apple-touch-icon-57x57-precomposed.png')}}">
        <link rel="shortcut icon" href="{{asset('hfv1/image/favicon.ico')}}">
        <!--/ END META SECTION -->

        <!-- START STYLESHEETS -->
        <!-- Plugins stylesheet : optional -->

        <link rel="stylesheet" href="{{asset('hfv1/plugins/select2/css/select2.css')}}">
        <link rel="stylesheet" href="{{asset('hfv1/plugins/owl/css/owl-carousel.css')}}">
        <link rel="stylesheet" href="{{asset('hfv1/plugins/font-awesome/css/font-awesome.min.css')}}">
        <!--/ Plugins stylesheet : optional -->

        <!-- Application stylesheet : mandatory -->
        <link rel="stylesheet" href="{{asset('hfv1/stylesheet/bootstrap.css')}}">
        <link rel="stylesheet" href="{{asset('hfv1/stylesheet/layout.css')}}">
        <link rel="stylesheet" href="{{asset('hfv1/stylesheet/uielement.css')}}">
        <link rel="stylesheet" href="{{asset('hfv1/stylesheet/custom.css')}}">
        <link rel="stylesheet" href="{{asset('hfv1/stylesheet/themes/layouts/fixed-header.css')}}">
        <!--/ Application stylesheet -->

        <!-- modernizr script -->
        <script type="text/javascript" src="{{asset('hfv1/plugins/modernizr/js/modernizr.js')}}"></script>
        <!--/ modernizr script -->
        @yield('styles')
    </head>
    <!--/ END Head -->

    <!-- START Body -->
    <body>
       @include('Landing::template.header')

        <!-- START Template Main -->
        <section id="main" role="main">
            @yield('content')
            <!-- START To Top Scroller -->
            <a href="#" class="totop animation" data-toggle="waypoints totop" data-showanim="bounceIn" data-hideanim="bounceOut" data-offset="50%"><i class="ico-angle-up"></i></a>
            <!--/ END To Top Scroller -->
        </section>
        <!--/ END Template Main -->

        @include('Landing::template.footer')

        <!-- START JAVASCRIPT SECTION (Load javascripts at bottom to reduce load time) -->
        <!-- Application and vendor script : mandatory -->
        <script type="text/javascript" src="{{asset('hfv1/javascript/vendor.js')}}"></script>
        <script type="text/javascript" src="{{asset('hfv1/javascript/core.js')}}"></script>
        <script type="text/javascript" src="{{asset('hfv1/javascript/frontend/app.js')}}"></script>
        <!--/ Application and vendor script : mandatory -->

        <!-- Plugins and page level script : optional -->
        <script type="text/javascript" src="{{asset('hfv1/plugins/smoothscroll/js/smoothscroll.js')}}"></script>
        <script type="text/javascript" src="{{asset('hfv1/plugins/owl/js/owl.carousel.js')}}"></script>
        <script type="text/javascript" src="{{asset('hfv1/plugins/stellar/js/jquery.stellar.js')}}"></script>
        <script type="text/javascript" src="{{asset('hfv1/javascript/frontend/home/home-v2.js')}}"></script>
        <script type="text/javascript" src="{{asset('hfv1/plugins/owl/js/owl.carousel.js')}}"></script>
        <script type="text/javascript" src="{{asset('hfv1/javascript/backend/components/carousel.js')}}"></script>
        <script type="text/javascript" src="{{asset('hfv1/plugins/parsley/js/parsley.js')}}"></script>
        <script src="{{asset('hfv1/plugins/select2/js/select2.js')}}"></script>
        <script>
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            setInterval(function(){
                $.get(window.location.origin + '/epa/zz_session')
                  .success(function(res){
                    $('meta[name="csrf-token"]').attr('content',res);
                  });
            },5000);
        </script>
        <!--/ Plugins and page level script : optional -->
        <!--/ END JAVASCRIPT SECTION -->
        @yield('scripts')
    </body>
    <!--/ END Body -->
</html>
