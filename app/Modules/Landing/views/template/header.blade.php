<!-- START Template Header -->
<header id="header" class="navbar">
    <div class="container">
        <!-- START navbar header -->
        <div class="navbar-header navbar-header-transparent navbar-header-mar">
            <!-- Brand -->
            <a class="navbar-brand" href="{{route('home')}}">
                <img alt="Brand" src="{{asset('hfv1/image/logo/logo.png')}}">
                <!--span class="logo-figure" style="margin-left:-4px;"></span>
                <span class="logo-text" style="margin-left:-4px;"></span-->
            </a>
            <!--/ Brand -->
        </div>
        <!--/ END navbar header -->

        <!-- START Toolbar -->
        <div class="navbar-toolbar clearfix">
            <!-- START Left nav -->
            <ul class="nav navbar-nav">
                <!-- Navbar collapse: This menu will take position at the top of template header (mobile only). Make sure that only #header have the `position: relative`, or it may cause unwanted behavior -->
                <li class="navbar-main navbar-toggle">
                    <a href="javascript:void(0);" data-toggle="collapse" data-target="#navbar-collapse">
                        <span class="meta">
                            <span class="icon"><i class="ico-paragraph-justify3"></i></span>
                        </span>
                    </a>
                </li>
                <!--/ Navbar collapse -->
            </ul>
            <!--/ END Left nav -->
            @if(\Auth::check())
            <a href="{{route('register')}}" class="btn btn-info navbar-btn navbar-right navbar-btn-myy">MY ACCOUNT</a>
            @else
            <a href="{{route('register')}}" class="btn btn-success navbar-btn navbar-right navbar-btn-my">SIGN UP</a>
            <a href="{{route('login')}}" class="btn btn-default navbar-btn navbar-right navbar-btn-myy" style="">LOGIN</a>
            @endif
            <!-- START nav collapse -->
            <div class="collapse navbar-collapse navbar-collapse-alt" id="navbar-collapse">
                <ul class="nav navbar-nav navbar-right">
                  <?php /*
                    <li class="{{activeClass(\Route::currentRouteName(),'home')}}">
                        <a href="{{route('home')}}">
                            <span class="meta">
                                <span class="text">HOME</span>
                            </span>
                        </a>
                    </li>
                    <li class="{{activeClass(\Route::currentRouteName(),'jobs')}}">
                        <a href="{{route('jobs')}}">
                            <span class="meta">
                                <span class="text">JOBS</span>
                            </span>
                        </a>
                    </li>
                    <li class="{{activeClass(\Route::currentRouteName(),'aboutUs')}}">
                        <a href="{{route('aboutUs')}}">
                            <span class="meta">
                                <span class="text">ABOUT US</span>
                            </span>
                        </a>
                    </li>
                    <li class="{{activeClass(\Route::currentRouteName(),'howItWorks')}}">
                        <a href="{{route('howItWorks')}}">
                            <span class="meta">
                                <span class="text">HOW IT WORKS</span>
                            </span>
                        </a>
                    </li>
                    <li class="{{activeClass(\Route::currentRouteName(),'faq')}}">
                        <a href="{{route('faq')}}">
                            <span class="meta">
                                <span class="text">FAQ's</span>
                            </span>
                        </a>
                    </li>
                    */
                   ?>
                </ul>
            </div>
            <!--/ END nav collapse -->
        </div>
        <!--/ END Toolbar -->
    </div>
</header>
<!--/ END Template Header -->
