@extends('Landing::template.main')

@section('content')
  <!-- START page header -->
  <section class="page-header page-header-block nm">
      <!-- pattern -->
      <div class="pattern pattern9"></div>
      <!--/ pattern -->
      <div class="container pt15 pb15">
          <div class="page-header-section">
              <h4 class="title">Jobs</h4>
          </div>
          <div class="page-header-section">
              <!-- Toolbar -->
              <div class="toolbar">
                  <ol class="breadcrumb breadcrumb-transparent nm">
                      <li><a href="{{route('home')}}">HumanFactor</a></li>
                      <li class="active">Jobs</li>
                  </ol>
              </div>
              <!--/ Toolbar -->
          </div>
      </div>
  </section>
  <div class="clearfix"></div>
  <!--/ END page header -->
  <section class="section bgcolor-white">
      <div class="container">
        <div class="row">
          <div class="col-lg-3"></div>
          <div class="col-lg-9">
            {{ \Form::open(['class' => '']) }}
            <div class="row">
              <div class="col-md-3">
                <div class="form-group">
                  <label class="sr-only" for="exampleInputEmail3">Search Job Title</label>
                  <input type="text" name="q" class="form-control" placeholder="Search Job Title">
                </div>
              </div>
              <div class="col-md-3">
                <div class="form-group">
                  <label class="sr-only" for="exampleInputEmail3">By Region</label>
                  <select name="reg" class="select2">
                    <option value="">--  Select Region--</option>
                    @foreach($location_region as $region)
                    <option value="{{ $region->regCode }}">{{ $region->regDesc }}</option>
                    @endforeach
                  </select>
                </div>
              </div>
              <div class="col-md-3">
                <div class="form-group">
                  <select name="prov" class="select2" disabled="">
                    <option value="">-- Select Province -- </option>
                  </select>
                </div>
              </div>
              <div class="col-md-3">
                <div class="form-group">
                  <select name="city" class="select2" disabled="">
                    <option value="">-- Select City -- </option>
                  </select>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-6">
                <a href="{{ route('jobs') }}" class="btn {{(\Request::has('t') == false)?'btn-success':'btn-default'}}">Premium Jobs</a>
                <a href="{{ route('jobs',['t' => 'reg']) }}" class="btn {{(\Request::has('t') == true AND \Request::get('t') == 'reg')?'btn-success':'btn-default'}}">Regular Jobs</a>
              </div>
              <div class="col-md-6 text-right">
                <button class="btn btn-default"><i class="ico-search"></i> Filter</button>
              </div>
            </div>
            {{ \Form::close() }}
          </div>
        </div>
        <!-- Browser Breakpoint -->
        <div class="row mt30">
          <div class="col-lg-3">
            <div class="list-group">
              <div href="" class="list-group-item">
                <h4 class="list-group-item-heading">Categories</h4>
              </div>
              @foreach($industries as $industry)
              <a href="#" class="list-group-item">{{ $industry->name }} <span class="badge">{{ $industry->jobs->count() }}</span></a>
              @endforeach
            </div>
          </div>
            <div class="col-lg-9">
              @if(count($jobs)>0)
                <div class="list-group joblist">
                  @foreach($jobs as $job)
                  @php
                    $arr = array_except($job->toArray(),['id','created_at','status','updated_at','deleted_at','referral_reward','finders_fee','order_level','valid_until','job_type_id','job_industry_id','work_shift_id','education_level_id','job_level_id','employer_id','work_shift']);
                    $arr['job_type'] = $job->type->name;
                    $arr['job_industry'] = $job->job_industry->name;
                    $arr['work_shift'] = $job->shift->name;
                    $arr['education_level'] = $job->education_level->name;
                    $arr['job_level'] = $job->job_level->name;
                    $arr['employer'] = array_only($job->employer->toArray(),['company_name','logo','slug','description','is_premium','is_verified']);
                    $arr = json_encode($arr);
                  @endphp
                  <a href="{{ route('jobs.details',['job_title' => $job->slug,
                                                    'job_code' => $job->job_code]) }}"
                      class="list-group-item job-detail"
                      data-code="{{ $job->job_code}}" target="_blank">
                    <div class="media col-md-2">
                        <figure class="pull-left">
                            <img class="media-object img-rounded img-responsive"  src="{{ $job->employer->logo_url}}" alt="{{ $job->job_title }}" >
                        </figure>
                    </div>
                    <div class="col-md-6" style="padding:0px;">
                        <h4 class="list-group-item-heading"> {{ $job->job_title }} </h4>
                        <p class="list-group-item-text">
                          <i class="ico-building"></i> {{ $job->employer->company_name }}

                          @if($job->employer->is_premium)
                          <span class="label label-success" ><i class="fa fa-shield"></i> Premium Employer</span>
                          @endif
                          @if($job->employer->is_verified)
                            <span class="label label-info"><i class="ico-check"></i> Verified</span>
                          @else
                            <span class="label label-info"><i  class="ico-checkbox-partial"></i> Just Launched</span>
                          @endif
                        </p>
                        <p class="list-group-item-text"><i class="ico-map-marker"></i> {{ $job->full_address }}</p>
                    </div>
                    <div class="col-md-4 text-center mt20" style="padding:0px;">
                        <div class="btn-group" role="group" aria-label="Apply and get rewarded">
                          @if(auth()->check() AND(auth()->user()->resume AND $job->appliedjobs($job->id)->first()))
                            <button type="button" class="appliedbtn btn btn-sm btn-info btn-smallpad" data-hasApplied="true"  data-toggle="tooltip" data-placement="bottom" title="Good Luck!"><i class="ico-checkmark-circle"></i> Applied</button>
                          @else
                            @if($job->employer->is_premium)
                          <button type="button" class="rewardbtn btn btn-sm btn-success btn-smallpad"  data-toggle="tooltip" data-placement="bottom" title="Get Rewarded When you Get Hired">{{ $job->applicant_reward }}</button>
                          <button type="button" class="rewardbtn btn btn-sm btn-default btn-smallpad">Apply</button>
                            @else
                              <button type="button" class="rewardbtn btn btn-sm btn-default btn-smallpad">Apply</button>
                            @endif
                          @endif
                        </div>
                        <p> {{ $job->created_at }}</p>
                    </div>
                  </a>
                  @endforeach
                </div>
                  <hr>
                {{ $jobs->appends($_GET)->links() }}
              <!--/ panel body with collapse capabale -->
              @else
                <div class="alert alert-info">
                  <strong>No Data!</strong>
                </div>
              @endif
            </div>
        </div>
        <!-- Browser Breakpoint -->
      </div>
  </section>
@stop

@section('styles')
<link rel="stylesheet" href="{{asset('hfv1/plugins/sweetalert/dist/sweetalert.css')}}">
<link rel="stylesheet" href="{{asset('hfv1/plugins/select2/css/select2.css')}}">
<style>
  .joblist a.list-group-item {
      height:auto;
      min-height:111px;
  }
  .joblist a.list-group-item.active small {
      color:#fff;
  }
  .stars {
      margin:20px auto 1px;
  }
</style>
@stop

@section('scripts')
  <script src="{{asset('hfv1/plugins/select2/js/select2.min.js')}}"></script>
  <script>

    $(function(){
      $('.select2').select2({
        allowClear : true
      });

      $('[name="location_region"]').change(function(e){
        var regcode = $(this).val();
        console.log(regcode);
        $.ajax({
          url : window.location.origin + '/epa/fetch-province/' + regcode,
          type : 'get',
          success : function(res){
            if(res.success){
              console.log(res)
              var x = '';
              $('[name="location_province" ]').children().not(':first').remove();
              $('[name="location_city" ]').children().not(':first').remove();
              $('[name="location_city" ]').prop('disabled','disabled');
              $('[name="location_province" ]').select2('val',null);
              $('[name="location_city" ]').select2('val',null);
              $.each(res.provinces,function(k,v){
                x  +='<option value="'+v.provCode+'">'+v.provDesc+'</option>';
              });
              $('[name="location_province" ]').append($(x));
              $('[name="location_province" ]').removeAttr('disabled');
            }
          },error : function(res){

          }
        });
        e.stopImmediatePropagation();
        e.preventDefault();
      });

      $('[name="location_province" ]').change(function(e){
        var provcode = $(this).val();
        $.ajax({
          url : window.location.origin + '/epa/fetch-cities/' + provcode,
          type : 'get',
          success : function(res){
            if(res.success){
              var x = '';
              $('[name="location_city" ]').children().not(':first').remove();
              $('[name="location_city" ]').select2('val',null);
              $.each(res.cities,function(k,v){
                x  +='<option value="'+v.citymunCode+'">'+v.citymunDesc+'</option>';
              });
              $('[name="location_city" ]').append($(x));
              $('[name="location_city" ]').removeAttr('disabled');
            }
          },error : function(res){

          }
        });
        e.preventDefault();
        e.stopImmediatePropagation();
      });

    });
  </script>
@stop
