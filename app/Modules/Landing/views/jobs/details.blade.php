@extends('Landing::template.main')

@section('content')
  <!-- START page header -->
  <section class="page-header page-header-block nm">
      <!-- pattern -->
      <div class="pattern pattern9"></div>
      <!--/ pattern -->
      <div class="container pt15 pb15">
          <div class="page-header-section">
              <h4 class="title">{{ $job->job_title }}</h4>
          </div>
          <div class="page-header-section">
              <!-- Toolbar -->
              <div class="toolbar">
                  <ol class="breadcrumb breadcrumb-transparent nm">
                      <li><a href="{{route('home')}}">HumanFactor</a></li>
                      <li><a href="{{ route('jobs') }}">Jobs</a></li>
                      <li class="active">{{ $job->job_title }}</li>
                  </ol>
              </div>
              <!--/ Toolbar -->
          </div>
      </div>
  </section>
  <section class="section bgcolor-white">
      <div class="container">
        <div class="row mt30">
          <div class="col-lg-3">
            <div class="list-group">
              <div href="" class="list-group-item">
                <h4 class="list-group-item-heading">Categories</h4>
              </div>
              @foreach($industries as $industry)
              <a href="#" class="list-group-item">{{ $industry->name }} <span class="badge">{{ $industry->jobs->count() }}</span></a>
              @endforeach
            </div>
          </div>
          <div class="col-md-9">
            <div class="panel-transparent">
              <div class="panel-body">
                  <div class="page-header" style="border:0;">
                    <div class="col-md-12">
                      @if(\Session::has('referral_code'))
                        <div class="alert alert-info">
                          <strong>You have been referred. Please review the job details and complete your profile by providing your account password and adding your resume</strong>
                        </div>
                      @endif
                      @php
                        $arr = array_except($job->toArray(),['id','created_at','status','updated_at','deleted_at','referral_reward','finders_fee','order_level','valid_until','job_type_id','job_industry_id','work_shift_id','education_level_id','job_level_id','employer_id','work_shift']);
                        $arr['job_type'] = $job->type->name;
                        $arr['job_industry'] = $job->job_industry->name;
                        $arr['work_shift'] = $job->shift->name;
                        $arr['education_level'] = $job->education_level->name;
                        $arr['job_level'] = $job->job_level->name;
                        $arr['employer'] = array_only($job->employer->toArray(),['company_name','logo','slug','description','is_premium','is_verified']);
                        $arr = json_encode($arr);
                      @endphp
                      <input type="hidden" id="dat" data-job="{{ $arr }}">
                      <img class="img-bordered-success img-responsive" src="{{ $job->employer->logo }}" alt="" style="max-height:75px;" />
                      <div class="page-header" style="border:0;">
                        <h2 class="page-title"> {{$job->job_title }} <small>{{ $job->employer->company_name }}</small></h2>
                        <p class="nm">
                        @if($job->employer->is_premium)
                        <span class="label label-success"><i class="fa fa-shield"></i> Premium Employer</span>
                        @endif
                        @if($job->employer->is_verified)
                        <span class="label label-info"><i class="ico-check"></i> Verified</span>
                        @endif
                        </p>
                        <h4 class="panel-title">Tell your community</h4>
                        <ul class="list-inline">
                          <li><a href="#"  id="shareBtn" class="btn btn-fb btn-sm"><i class="fa fa-facebook"></i></a></li>
                          <li><a href="#"  class="btn btn-inverse btn-sm" data-jobcode="{{ $job->job_code }}" data-toggle="modal" data-target="#email"><i class="fa fa-envelope-o"></i></a></li>
                          <li><a href="#" class="btn btn-default btn-sm"><i class="fa fa-link"></i></a></li>
                        </ul>
                        <hr>
                          @if(auth()->check() AND auth()->user()->user_type_id == '6')
                            <span class="label label-inverse">Referral Reward : {{ $job->referral_reward}}</span>
                          @endif
                          @if(auth()->check() AND auth()->user()->user_type_id == '5')
                          <span class="label label-inverse">Hiring Reward : {{ $job->applicant_reward}}</span>
                          @endif
                          @if(auth()->guest())
                            <div class="btn-group" role="group" aria-label="...">
                              <button type="button" class="btn btn-default">Hiring Reward - {{ $job->applicant_reward}}</button>
                              <button type="button" class="btn btn-default">Referral Reward - {{ $job->referral_reward}}</button>
                            </div>
                          @endif
                        <p><strong><i class="ico-cogs"></i> {{ $job->job_industry->name }} </strong></p>
                        <span><i class="ico-map-marker"></i>{{ $job->full_address }}</span><br>
                      </div>
                    </div>
                    <ul class="list-inline">
                      <li><span class="label label-default">Work Type: {{ $job->type->name}}</span>
                      </li>
                      <li><span class="label label-default">Work Experience: {{ $job->job_level->name }}</span>
                      </li>
                      <li><span class="label label-default">Education Level: {{ $job->education_level->name }}</span>
                      </li>
                      @if($job->show_salary_range)
                      <li><span class="label label-default">Salary Range: {{ $job->salary_from .' - '.$job->salary_to }}</span>
                      </li>
                      @endif
                    </ul>
                  </div>
                  <h4>About the company</h4>
                  <div>{!! $job->employer->description !!}</div>
                  <h4>Job Description</h4>
                  <div class="desc">
                    {!! $job->description !!}
                  </div>
              </div><!-- body -->
            </div>
            <!-- Referral Section-->
            @if(\Session::has('referral_code') AND count($referral)>0)
              <div class="page-header" style="boder:0;">
                <h3 class="page-title">Referral Candidate Form</h3>
                <p>You may fill-up quick signup and create your resume to complete this application</p>
              </div>
            <div class="panel widget">
              <div class="panel-heading">
                <h3 class="panel-title">Get started by completing this form</h3>
              </div>
              <div class="panel-body">
                <form role="form"  method="POST" action="{{ route('register') }}" data-parsley-validate>
                {{ csrf_field() }}
                    <div class="row">
                      <div class="col-md-12">
                        <div id="error-container" class="mb10">
                        </div>
                      </div>
                    </div>
                    <div class="form-group">
                      <input type="email" disabled="" value="{{ $referral->email }}" class="form-control">
                    </div>
                    <div class="form-group">
                        <input type="password" class="form-control input-lg" name="password" id="signup_password" placeholder="Create a password" required="">
                    </div>
                    <div class="form-group">
                      <input type="password" class="form-control input-lg" name="password_confirmation" id="signup_password" placeholder="Retype password" required="">
                      <span class="help-block text-muted">Use at least one lowercase letter, one numeral, and seven characters.</span>
                    </div>
                    <button type="submit" class="btn btn-success btn-block btn-lg">Start Now</button>
                </form>
              </div>
            </div>
            @endif
            @if(!(\Session::has('referral_code')) AND auth()->guest())
              <div class="page-header" style="boder:0;">
                <h3 class="page-title">Quick and Easy Sign Up!</h3>
                <form role="form"  method="POST" action="{{ route('register') }}" data-parsley-validate>
                {{ csrf_field() }}
                      <div class="well" style="color: #444444;font-size: 11px;">
                        <div class="row">
                          <div class="col-md-12">
                            <div id="error-container" class="mb10">
                            </div>
                          </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                 <div class="radio">
                                    <label>
                                      <input type="radio" name="type" value="applicant" required="" data-parsley-errors-container="#error-container" data-parsley-error-message="Please select any of this choices"> Job Seeker
                                    </label>
                                  </div>
                            </div>
                            <div class="col-md-4">
                                <div class="radio">
                                    <label>
                                      <input type="radio"  name="type" value="recruiter"> Recruiter
                                    </label>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="radio">
                                  <label>
                                    <input type="radio"  name="type" value="employer"> Employer
                                  </label>
                                </div>
                            </div>

                        </div>
                      </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <input type="text" class="form-control input-lg" name="firstname" placeholder="First Name" required="" autofocus="">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <input type="text" class="form-control input-lg" name="lastname" placeholder="Last Name" required="">
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <input type="email" class="form-control input-lg" name="email" id="signup_email" placeholder="Your email" required="">
                    </div>
                    <div class="form-group">
                        <input type="password" class="form-control input-lg" name="password" id="signup_password" placeholder="Create a password" required="">
                    </div>
                    <div class="form-group">
                      <input type="password" class="form-control input-lg" name="password_confirmation" id="signup_password" placeholder="Retype password" required="">
                      <span class="help-block text-muted">Use at least one lowercase letter, one numeral, and seven characters.</span>
                    </div>
                    <button type="submit" class="btn btn-success btn-block btn-lg">Get Started Now</button>
                </form>
              </div>
            @endif
            <!-- End Referral Section-->
            @if(auth()->check() AND auth()->user()->type->slug == 'applicant')

              <div id="for-applicant">
                @if(auth()->user()->resume AND $job->appliedjobs($job->id)->first())
                  <button type="button" class="appliedbtn btn btn-sm btn-info btn-smallpad" data-hasApplied="true"  data-toggle="tooltip" data-placement="bottom" title="Good Luck!"><i class="ico-checkmark-circle"></i> Applied</button>
                @else
                <div id="application-form">
                <div class="page-header mb5 pb5">
                  <div class="page-title"><h3>Apply Here</h3></div>
                </div>

                <div class="row">
                  <div class="col-md-12">
                    @if(count(auth()->user()->resume)<1)
                      <p><a href="{{ route('applicant.resume.create')}}">Create Resume Here...</a></p>
                    @else
                    <div id="form" data-parsley-validate>
                      <div class="form-group">
                        <div class="row">
                          <div class="col-md-6">
                            <label>Expected Salary</label>
                            <input type="text" class="form-control moneymask" name="expected_salary" placeholder="">
                          </div>
                          <div class="col-md-6"></div>
                        </div>
                        <div class="row">
                          <div class="col-md-12">
                            <p class="text-muted">Be realistic. Employers may ask for proof of your latest salary. Pricing your salary too high can disqualify you from the position.</p>
                            <input type="hidden" name="job_code" value="{{ $job->job_code }}">
                          </div>
                        </div>
                      </div>
                      <div class="form-group">
                        <label>Application / Cover Letter <span class="text-danger">*</span></label>
                        <textarea name="applicant_cover_letter" id="cover_letter" style="min-height: 250px;" class="form-control" required="">Dear Hiring Manager,

Hello and good day!

This letter is in response to your {{ $job->job_title }} job advertisement in HumanFactor.ph on {{date('F d Y')}}.

[Short description of your achievements and contact information here]

Thank you very much.

Sincerely yours,

{{ auth()->user()->my_name }}
</textarea>
                      </div>
                      <div class="form-group">
                        <label>Your Resume</label>
                        <p><a href="javascript:void(0);" class="btn btn-link"><i class="ico-file"></i> {{ str_replace(url('upload-resume').'/', '', auth()->user()->resume->attached_resume) }}</a></p>
                      </div>
                      <button type="button" class="btn btn-success pull-right" id="applyJob">Submit Application</button>
                      @endif
                    </div>
                  </div>
                @endif
                  </div>
                </div><!-- row -->
              </div>
            @endif
          </div>
        </div>
      </div>
  </section>
  <!-- Modal -->
<div class="modal fade" id="email" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Email Invites</h4>
      </div>
      {!! \Form::open(['id' => 'emailform']) !!}
      <div class="modal-body">
        <div class="form-group">
          <label>Candidates will recieve an email inviting them to join and apply</label>
          <input type="email" class="form-control" id="select-email" value="">
          <input type="hidden" value="job_code">
          @if(auth()->check() AND auth()->user()->type->slug == 'recruiter')
            <p class="text-muted">You may pull emails from your referral candidates</p>
          @endif
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Send</button>
      </div>
      {!! \Form::close() !!}
    </div>
  </div>
</div>
@stop

@section('styles')
  <link rel="stylesheet" href="{{asset('hfv1/plugins/sweetalert/dist/sweetalert.css')}}">
  <link rel="stylesheet" href="{{ asset('hfv1/plugins/selectize/css/selectize.css') }}">
  <style>
  .btn-fb {
    color:#fff;
    background-color: #3B5998;
  }
  .btn-fb:hover {
    color:#fff;
    background-color: #324b82;
  }
  .btn-tw {
    background-color: #55ACEE;
  }
  .bg-primary:hover {
    background-color: #324b82;
    color: #fff;
  }
  </style>
@stop
@section('scripts')
  <script src="{{ asset('hfv1/plugins/selectize/js/selectize.js') }}"></script>
  <script type="text/javascript" src="{{asset('hfv1/plugins/jquery-inputmask/dist/jquery.inputmask.bundle.js')}}"></script>
  <script type="text/javascript" src="{{asset('hfv1/plugins/sweetalert/dist/sweetalert.min.js')}}"></script>
  <script>
  var jobDetail = {
    init : function(){
      this.misc();
      this.sendEmailInvite()
      this.selectizeEmail()
      $('.moneymask').inputmask({
          'alias' : 'numeric',
          'groupSeparator' : ',',
          'autoGroup' : true,
          'digits' : 2,
          'digitsOptional' : false,
          'prefix' : 'Php ',
          'placeholder' : '0'
      });
      this.apply();
      let job = $('#dat').data('job');
      $('#cover_letter').val(jobDetail.coverLetter(job).trim());
    },
    apply : function(data){
      $('#applyJob').click(function(e){
        e.preventDefault();
        e.stopImmediatePropagation();
        var x = {'applicant_cover_letter' : $('#application-form').find('#cover_letter').val(),
                'expected_salary': $('[name="expected_salary"]').val()};
        $.ajax({
          url : '{{ route('jobs.applied') }}',
          type: 'post',
          data : { 'job_code': $('#application-form').find('[name="job_code"]').val(),
                  'applicant_cover_letter' : $('#application-form').find('#cover_letter').val(),
                  'expected_salary': $('#application-form').find('[name="expected_salary"]').val()},
          success : function(result){
            if(result.success){
              $('#application-form').remove();
              $('#for-applicant').append($('<button type="button" class="appliedbtn btn btn-sm btn-info btn-smallpad" data-hasApplied="true"  data-toggle="tooltip" data-placement="bottom" title="Good Luck!"><i class="ico-checkmark-circle"></i> Applied</button>'))
              // applyJob.misc();
              swal(
                'Good job!',
                'You have applied for this job',
                'success'
              )
            }
          },
          error : function(result){
            swal(
              'Oops...',
              'Opps! Something went wrong!',
              'error'
            )
          }
        })
      });

    },
    coverLetter : function coverLetter(data) {
return `Dear ${data.employer.company_name},

Hello and good day!

This letter is in response to your ${data.job_title} job advertisement in HumanFactor.ph on {{date('F d Y')}}.

[Short description of your achievements and contact information here]

Thank you very much.

Sincerely yours,

{{ (auth()->check())?auth()->user()->my_name:'' }}
`;
    },
    sendEmailInvite : function(){

    },
    selectizeEmail : function(){
      var REGEX_EMAIL = '([a-z0-9!#$%&\'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&\'*+/=?^_`{|}~-]+)*@' +
                  '(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?)';

        $('#select-email').selectize({
            persist: true,
            valueField: 'email',
            labelField: 'my_name',
            searchField: ['my_name', 'email'],
            create: true,
            render: {
                item: function(item, escape) {
                    return '<div class="bg-primary">' +
                        (item.my_name ? '<span class="name">' + escape(item.my_name) + '</span>' : '') +
                        (item.email ? '<span class="email">' + escape(item.email) + '</span>' : '') +
                    '</div>';
                },
                option: function(item, escape) {
                    var label = item.my_name || item.email;
                    var caption = item.my_name ? item.email : null;
                    return '<div class="bg-primary">' +
                        '<span class="label">' + escape(label) + '</span>' +
                        (caption ? '<span class="caption">' + escape(caption) + '</span>' : '') +
                    '</div>';
                }
            },
            createFilter: function(input) {
                var match, regex;

                // email@address.com
                regex = new RegExp('^' + REGEX_EMAIL + '$', 'i');
                match = input.match(regex);
                if (match) return !this.options.hasOwnProperty(match[0]);

                // name <email@address.com>
                regex = new RegExp('^([^<]*)\<' + REGEX_EMAIL + '\>$', 'i');
                match = input.match(regex);
                if (match) return !this.options.hasOwnProperty(match[2]);

                return false;
            },
            create: function(input) {
                if ((new RegExp('^' + REGEX_EMAIL + '$', 'i')).test(input)) {
                    return {email: input};
                }
                var match = input.match(new RegExp('^([^<]*)\<' + REGEX_EMAIL + '\>$', 'i'));
                if (match) {
                    return {
                        email : match[2],
                        name  : $.trim(match[1])
                    };
                }
                alert('Invalid email address.');
                return false;
            },
            load: function(query, callback) {
                if (!query.length) return callback();
                $.ajax({
                    url: window.location.origin + '/fetch-myreferrals',
                    type: 'GET',
                    data : {
                      q: query
                    },
                    error: function() {
                        callback();
                    },
                    success: function(res) {
                      console.log(res);
                        callback(res.referrals);
                    }
                });
            }
        });
    },
    misc : function(){
      window.fbAsyncInit = function() {
        FB.init({
          @if(env('APP_ENV') == 'local')
          appId      : '117502128814779',
          @else
          appId      : '219282678573488',
          @endif
          xfbml      : true,
          version    : 'v2.9'
        });
        FB.AppEvents.logPageView();
      };

      (function(d, s, id){
         var js, fjs = d.getElementsByTagName(s)[0];
         if (d.getElementById(id)) {return;}
         js = d.createElement(s); js.id = id;
         js.src = "//connect.facebook.net/en_US/sdk.js";
         fjs.parentNode.insertBefore(js, fjs);
       }(document, 'script', 'facebook-jssdk'));

       document.getElementById('shareBtn').onclick = function() {
         FB.ui({
           method: 'share',
           display: 'popup',
           href: 'http://dev.hf.local/jobs/web-developer/ABQ447272',
         }, function(response){});
       }

       //selectize
       $('#email').on('show.bs.modal', function (event) {
         var button = $(event.relatedTarget) // Button that triggered the modal
         var jobcode = button.data('jobcode') // Extract info from data-* attributes
         // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
         // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
         var modal = $(this)
         modal.find('[name="job_code"]').val(jobcode);
       })
    }
  }

  $(function(){
      jobDetail.init()
  });

</script>
@stop
