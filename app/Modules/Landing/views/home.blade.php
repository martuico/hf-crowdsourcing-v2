@extends('Landing::template.main')

@section('content')
  <!-- START jumbotron -->
  <section class="jumbotron jumbotron-bg7 nm" data-stellar-background-ratio="0.4" style="min-height:486px;">
      <!-- pattern + overlay -->
      <div class="overlay pattern pattern2"></div>
      <!--/ pattern + overlay -->
      <?php /*
      <div class="container" style="padding-top:8%;padding-bottom:8%;">
          <div class="row">
              <div class="col-md-8">
                  <h1 class="thin text-white font-alt mt0 mb5">Recruiment Crowdsourcing</h1>
                  <h3 class="lead text-white font-alt mt0 mb5">Where you Apply for a Job or Refer a Candidate and Get Paid <i class="fa fa-usd"></i><i class="fa fa-usd"></i><i class="fa fa-usd"></i></h3>
                  <hr>
              </div>
              <div class="col-md-4">
                  <h1 class="thin text-white font-alt mt0 mb5">&nbsp;</h1>
                  <h3 class="lead text-white font-alt mt0 mb5">Quick and Easy Sign Up!</h3>
                  <hr>
              </div>
          </div>
          <div class="row">
              <div class="col-md-8">
                  <!-- example 4 -->
                  <div class="panel nm no-border">
                      <div class="panel-body owl-carousel" id="how-one-slide">
                          <!-- slide #1 -->
                          <div class="item table-layout">
                              <div class="row">
                                  <div class="col-md-12 text-center">
                                      <h3 style="color: #444444;">Employers</h3>
                                  </div>
                              </div>
                              <div class="row">
                                  <div class="col-sm-3 text-center">
                                      <p><span class="badge badge-success">1</span></p>
                                      <img src="{{asset('hfv1/image/icons/landingpageoptimization.png')}}" alt="affiliate-marketing" width="100">
                                      <h4 style="color: #444444;">Post A Job</h4>
                                      <p class="text-default">Post a job for free and set a finder's fee.</p>
                                  </div>
                                  <div class="col-sm-3 text-center">
                                      <p><span class="badge badge-success">2</span></p>
                                      <img src="{{asset('hfv1/image/icons/keywordgenerator.png')}}" alt="affiliate-marketing" width="100">
                                      <h4 style="color: #444444;">Receive Quality Resumes</h4>
                                      <p class="text-default">After a 2-step quality check by humanfactor, receive qualified resumes sourced by professional recruiters.</p>
                                  </div>
                                  <div class="col-sm-3 text-center">
                                      <p><span class="badge badge-success">3</span></p>
                                      <img src="{{asset('hfv1/image/icons/marketresearch.png')}}" alt="affiliate-marketing" width="100">
                                       <h4 style="color: #444444;">Review & Hire</h4>
                                      <p class="text-default">humanfactor’s dashboard will let you track communication and schedule interviews between you and the candidate.</p>
                                  </div>
                                  <div class="col-sm-3 text-center">
                                      <p><span class="badge badge-success">4</span></p>
                                      <img src="{{asset('hfv1/image/icons/conversionrateoptimization.png')}}" alt="affiliate-marketing" width="100">
                                       <h4 style="color: #444444;">Evaluate and Pay</h4>
                                      <p class="text-default">Evaluate your decision with 30 days of employment. After a successful hire, the employer then pays for placement.</p>
                                  </div>
                              </div>
                          </div>
                          <!--/ slide #1 -->
                          <!-- slide #2 -->
                          <div class="item table-layout">
                              <div class="row">
                                  <div class="col-md-12 text-center">
                                      <h3 style="color: #444444;">Recruiter</h3>
                                  </div>
                              </div>
                              <div class="row">
                                  <div class="col-sm-3 text-center">
                                      <p><span class="badge badge-success">1</span></p>
                                      <img src="{{asset('hfv1/image/icons/localseo.png')}}" alt="affiliate-marketing" width="100">
                                      <h4 style="color: #444444;">Browse Jobs</h4>
                                      <p class="text-default">Browse open full-time and contract positions posted by employers in the marketplace.</p>
                                  </div>
                                  <div class="col-sm-3 text-center">
                                      <p><span class="badge badge-success">2</span></p>
                                      <img src="{{asset('hfv1/image/icons/branddevelopment.png')}}" alt="affiliate-marketing" width="100">
                                      <h4 style="color: #444444;">Submit Candidates</h4>
                                      <p class="text-default">Source the most suitable candidates with their resume.</p>
                                  </div>
                                  <div class="col-sm-3 text-center">
                                      <p><span class="badge badge-success">3</span></p>
                                      <img src="{{asset('hfv1/image/icons/seoconsulting.png')}}" alt="affiliate-marketing" width="100">
                                       <h4 style="color: #444444;">Track Applicants</h4>
                                      <p class="text-default">Stay up-to-date through humanfactor's dashboard and get notified through email on each step.</p>
                                  </div>
                                  <div class="col-sm-3 text-center">
                                      <p><span class="badge badge-success">4</span></p>
                                      <img src="{{asset('hfv1/image/icons/payperclick.png')}}" alt="affiliate-marketing" width="100">
                                       <h4 style="color: #444444;">Get Paid</h4>
                                      <p class="text-default">Once the candidate is hired, humanfactor will compensate you on an 80-20 split fee structure.</p>
                                  </div>
                              </div>
                          </div>
                          <!--/ slide #2 -->
                          <!-- slide #3 -->
                          <div class="item table-layout">
                              <div class="row">
                                  <div class="col-md-12 text-center">
                                      <h3 style="color: #444444;">Job Seeker</h3>
                                  </div>
                              </div>
                              <div class="row">
                                  <div class="col-sm-3 text-center">
                                      <p><span class="badge badge-success">1</span></p>
                                      <img src="{{asset('hfv1/image/icons/landingpageoptimization.png')}}" alt="affiliate-marketing" width="100">
                                      <h4 style="color: #444444;">Create Resume</h4>
                                      <p class="text-default">Post a job for free and set a finder's fee.</p>
                                  </div>
                                  <div class="col-sm-3 text-center">
                                      <p><span class="badge badge-success">2</span></p>
                                      <img src="{{asset('hfv1/image/icons/keywordgenerator.png')}}" alt="affiliate-marketing" width="100">
                                      <h4 style="color: #444444;">Apply for a Job</h4>
                                      <p class="text-default">After a 2-step quality check by humanfactor, receive qualified resumes sourced by professional recruiters.</p>
                                  </div>
                                  <div class="col-sm-3 text-center">
                                      <p><span class="badge badge-success">3</span></p>
                                      <img src="{{asset('hfv1/image/icons/marketresearch.png')}}" alt="affiliate-marketing" width="100">
                                       <h4 style="color: #444444;">Appointment Interview</h4>
                                      <p class="text-default">humanfactor’s dashboard will let you track communication and schedule interviews between you and the candidate.</p>
                                  </div>
                                  <div class="col-sm-3 text-center">
                                      <p><span class="badge badge-success">4</span></p>
                                      <img src="{{asset('hfv1/image/icons/returnoninvestment.png')}}" alt="affiliate-marketing" width="100">
                                       <h4 style="color: #444444;">Hired and Reward</h4>
                                      <p class="text-default">Evaluate your decision with 30 days of employment. After a successful hire, the employer then pays for placement.</p>
                                  </div>
                              </div>
                          </div>
                          <!--/ slide #3 -->
                      </div>
                  </div>
                  <!--/ example 4 -->
              </div>
              <div class="col-md-4">
                  <form role="form"  method="POST" action="{{ route('register') }}" data-parsley-validate>
                  {{ csrf_field() }}
                        <div class="well" style="color: #444444;font-size: 11px;">
                          <div class="row">
                            <div class="col-md-12">
                              <div id="error-container" class="mb10">
                              </div>
                            </div>
                          </div>
                          <div class="row">
                              <div class="col-md-4">
                                   <div class="radio">
                                      <label>
                                        <input type="radio" name="type" value="applicant" required="" data-parsley-errors-container="#error-container" data-parsley-error-message="Please select any of this choices"> Job Seeker
                                      </label>
                                    </div>
                              </div>
                              <div class="col-md-4">
                                  <div class="radio">
                                      <label>
                                        <input type="radio"  name="type" value="recruiter"> Recruiter
                                      </label>
                                  </div>
                              </div>
                              <div class="col-md-4">
                                  <div class="radio">
                                    <label>
                                      <input type="radio"  name="type" value="employer"> Employer
                                    </label>
                                  </div>
                              </div>

                          </div>
                        </div>
                      <div class="row">
                          <div class="col-md-6">
                              <div class="form-group">
                                  <input type="text" class="form-control input-lg" name="firstname" placeholder="First Name" required="" autofocus="">
                              </div>
                          </div>
                          <div class="col-md-6">
                              <div class="form-group">
                                  <input type="text" class="form-control input-lg" name="lastname" placeholder="Last Name" required="">
                              </div>
                          </div>
                      </div>
                      <div class="form-group">
                          <input type="email" class="form-control input-lg" name="email" id="signup_email" placeholder="Your email" required="">
                      </div>
                      <div class="form-group">
                          <input type="password" class="form-control input-lg" name="password" id="signup_password" placeholder="Create a password" required="">
                      </div>
                      <div class="form-group">
                        <input type="password" class="form-control input-lg" name="password_confirmation" id="signup_password" placeholder="Retype password" required="">
                        <span class="help-block text-muted">Use at least one lowercase letter, one numeral, and seven characters.</span>
                      </div>
                      <button type="submit" class="btn btn-success btn-block btn-lg">Get Started Now</button>
                  </form>
              </div>
          </div>
      </div>
      */
      ?>
      <div class="container">
        <div class="col-md-12">
          <div class="row">
            <div class="col-md-12 text-center" style="padding-top:8%;padding-bottom:8%;">
              <h1 class="thin text-white font-alt mt0 mb5">Recruitment Marketplace</h1>
              <h3 class="thin text-white font-alt mt5 mb0">We are reinvinting the recruitment industry to make it faster, fairer and more effective for all.</h3>
              <!-- div class="row">
                <div class="col-md-6 col-md-offset-3">
                  <div class="input-group">
                    <input type="text" class="form-control input-lg" placeholder="Search Jobs">
                    <span class="input-group-btn">
                      <button class="btn btn-success btn-lg" type="button" style="    font-size: 24px;"><i class="ico-search"></i></button>
                    </span>
                  </div>
                </div>
              </div -->
              <div class="row mt30">
                <ul class="list-inline">
                  <li>
                    <button class="btn btn-block btn-default btn-lg">
                    Hire
                    </button>
                  </li>
                  <li>
                    <button class="btn btn-block btn-default btn-lg">
                    Apply
                    </button>
                  </li>
                  <li>
                    <button class="btn btn-block btn-default btn-lg">
                    Refer
                    </button>
                  </li>
                </ul>
              </div>
            </div>
          </div>

        </div>
      </div>
  </section>
  <!--/ END jumbotron -->

  <!-- START Features Section -->
  <section class="section bgcolor-white">
      <div class="container">
          <!-- START Section Header -->
          <div class="row">
              <div class="col-md-12">
                  <div class="section-header text-center">
                      <h1 class="section-title font-alt mb25">Why Start With Us?</h1>
                      <div class="row">
                          <div class="col-md-8 col-md-offset-2">
                              <h4 class="thin text-muted text-center">
                                  List of <span class="semibold text-success">Human Factor</span> feature sets that will blow up your mind.
                              </h4>
                          </div>
                      </div>
                  </div>
              </div>
          </div>
          <!--/ END Section Header -->

          <!-- START row -->
          <div class="table-layout mb0">
              <div class="col-md-4">
                  <div class="table-layout animation" data-toggle="waypoints" data-showanim="fadeInRight" data-trigger-once="true">
                      <div class="col-xs-2 valign-top"><img src="{{asset('hfv1/image/icons/targetaudience.png')}}" width="100%" alt=""></div>

                      <div class="col-xs-19 pl15">
                          <h4 class="font-alt">Only Interview Quality Candidates</h4>
                          <p class="nm">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                          tempor incididunt ut labore et dolore magna aliqua.</p>
                      </div>
                  </div>
                  <div class="table-layout animation" data-toggle="waypoints" data-showanim="fadeInRight" data-trigger-once="true">
                      <div class="col-xs-2 valign-top"><img src="{{asset('hfv1/image/icons/seoperfomance.png')}}" width="100%" alt=""></div>
                      <div class="col-xs-10 pl15">
                          <h4 class="font-alt">Control Costs & Reduce Time to Fill</h4>
                          <p class="nm">Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                          consequat.</p>
                      </div>
                  </div>
              </div>
              <div class="col-md-4 pt15 text-center animation" data-toggle="waypoints" data-showanim="flipInY" data-trigger-once="true" data-offset="40%">
                  <img src="hfv1/image/mockup/iphone-5s.png">
              </div>
              <div class="col-md-4">
                  <div class="table-layout text-right animation" data-toggle="waypoints" data-showanim="fadeInLeft" data-trigger-once="true">
                      <div class="col-xs-10 pr15">
                          <h4 class="font-alt">Human Factor free Applicant Tracking System</h4>
                          <p class="nm">Duis aute irure dolor in reprehenderit in voluptate velit esse
                          cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat.</p>
                      </div>
                      <div class="col-xs-2 valign-top"><img src="{{asset('hfv1/image/icons/responsivewebdesign.png')}}" width="99%" alt=""></div>
                  </div>
                  <div class="table-layout text-right animation mb0" data-toggle="waypoints" data-showanim="fadeInLeft" data-trigger-once="true">
                      <div class="col-xs-10 pr15">
                          <h4 class="font-alt">Human Factor Portal Built for All Parties</h4>
                          <p class="nm">Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                      </div>
                      <div class="col-xs-2 valign-top"><img src="{{asset('hfv1/image/icons/brandprotection.png')}}" width="100%" alt=""></div>
                  </div>
              </div>
          </div>
          <!--/ END row -->
      </div>
  </section>
  <!--/ END Features Section -->


  <!-- START Testimonials section + jumbotron -->
  <section class="section jumbotron jumbotron-bg6 nm" data-stellar-background-ratio="0.4">
      <div class="pb35">
          <!-- pattern + overlay -->
          <div class="overlay pattern pattern10"></div>
          <!-- pattern + overlay -->

          <div class="container">
              <!-- START Section Header -->
              <div class="row">
                  <div class="col-md-12">
                      <div class="section-header no-border text-center">
                          <h1 class="section-title font-alt">Testimonials</h1>
                          <div class="row">
                              <div class="col-md-8 col-md-offset-2">
                                  <h4 class="thin text-muted text-center">Some of our satisfied partners, recruiters and job seekers review</h4>
                              </div>
                          </div>
                      </div>
                  </div>
              </div>
              <!--/ END Section Header -->

              <!-- START row -->
              <div class="row">
                  <!-- example 1 -->
                  <div class="owl-carousel" id="customer-reviews">
                      <!-- review #1 -->
                      <div class="item text-center pt5">
                          <blockquote class="nm">
                              <p>Best admin panel on themeforest, thanks for the amazing work!!!</p>
                              <footer class="text-white">Zebdi</footer>
                          </blockquote>
                      </div>
                      <!--/ review #1 -->
                      <!-- review #2 -->
                      <div class="item text-center pt5">
                          <blockquote class="nm">
                              <p>Very stylish theme and very easy to implement. Works perfectly in different browsers and on different devices and has frequent updates.</p>
                              <footer class="text-white">Jacko120</footer>
                          </blockquote>
                      </div>
                      <!--/ review #2 -->
                      <!-- review #3 -->
                      <div class="item text-center pt5">
                          <blockquote class="nm">
                              <p>Well commented code, easy to work with it. Great work!</p>
                              <footer class="text-white">indexto</footer>
                          </blockquote>
                      </div>
                      <!--/ review #3 -->
                      <!-- review #4 -->
                      <div class="item text-center pt5">
                          <blockquote class="nm">
                              <p>Lightweight and mobile focused, neat, clean and well organized code. Please keep up the good work!</p>
                              <footer class="text-white">chakkit</footer>
                          </blockquote>
                      </div>
                      <!--/ review #4 -->
                      <!-- review #5 -->
                      <div class="item text-center pt5">
                          <blockquote class="nm">
                              <p>Overall a great product guys! Everything is great, the design, the code, the feel. Thank you!</p>
                              <footer class="text-white">Anblick1010</footer>
                          </blockquote>
                      </div>
                      <!--/ review #5 -->
                  </div>
                  <!--/ example 1 -->
              </div>
              <!--/ END row -->
          </div>
      </div>
  </section>
  <!--/ END Testimonials section + jumbotron -->

@stop
