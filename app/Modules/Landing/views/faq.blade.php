@extends('Landing::template.main')

@section('content')
  <!-- START page header -->
  <section class="page-header page-header-block nm">
      <!-- pattern -->
      <div class="pattern pattern9"></div>
      <!--/ pattern -->
      <div class="container pt15 pb15">
          <div class="page-header-section">
              <h4 class="title">Frequently Ask Question</h4>
          </div>
          <div class="page-header-section">
              <!-- Toolbar -->
              <div class="toolbar">
                  <ol class="breadcrumb breadcrumb-transparent nm">
                      <li><a href="{{route('home')}}">Human Factor</a></li>
                      <li class="active">FAQ</li>
                  </ol>
              </div>
              <!--/ Toolbar -->
          </div>
      </div>
  </section>
  <div class="clearfix"></div>
  <!--/ END page header -->
@stop