<?php

namespace App\Modules\Landing\Requests;

use Illuminate\Foundation\Http\FormRequest;

class QuickApplyRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email' => 'required|email|unique:users',
            'firstname' => 'required',
            'lastname' => 'required',
            'date_of_birth' => 'required',
            'password' => 'required|confirmed',
            'password_confirmation' => 'required',
            'expected_salary' => 'nullable|max:30',
            'applicant_cover_letter' => 'max:1000',
        ];
    }
}
