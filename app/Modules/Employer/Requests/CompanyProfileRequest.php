<?php

namespace App\Modules\Employer\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CompanyProfileRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'logo_url' => (count(auth()->user()->employer)>0 AND  auth()->user()->employer->logo_url)?'nullable|image':'required|image|max:2000',
            'company_name' => 'required|min:3|max:191',
            'contact_designation' => 'required|min:3|max:191',
            'office_address' => 'required|min:3|max:191',
            'office_number_1' => 'required|min:3|max:191',
            'office_number_2' => 'nullable|min:3|max:191',
            'website' => 'nullable|url|max:191',
            'sm_facebook' => 'nullable|url|max:191',
            'sm_linkedin' => 'nullable|url|max:191',
            'sm_twitter' => 'nullable|url|max:191',
            'description' => 'max:1000'
        ];
    }
}
