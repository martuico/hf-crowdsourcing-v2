<?php

namespace App\Modules\Employer\Requests;

use Illuminate\Foundation\Http\FormRequest;

class JobCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'job_title' => 'required|min:3|max:191',
            'job_type_id' => 'required',
            // 'provide_relocation' => 'required',
            // 'finders_fee' => 'required',
            'description' => 'required',
            'work_shift_id' => 'required',
            // 'work_location' => 'required',
            'education_level_id' => 'required',
            'job_industry_id' => 'required',
            'job_type_id' => 'required',
            'skills' => 'required',
            'salary_from' => 'required',
            'salary_to' => 'required'
        ];
    }
}
