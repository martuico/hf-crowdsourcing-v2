<?php namespace App\Modules\Employer\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Traits\ImageUpload,
    App\Traits\ShortNumberFormat;
use App\Modules\Employer\Requests\CompanyProfileRequest;

use App\Employer,
    App\Job,
    App\ApplicantJobActivity,
    App\User,
    App\CompanyRecruiter,
    App\Applicant;

class EmployerController extends Controller
{
    use ImageUpload,ShortNumberFormat;
    protected $employer;
    protected $job;
    protected $applicant;
    protected $application_activities;
    protected $user;
    protected $company_recruiter;

    public function __construct(Employer $employer,
                                Job $job,
                                Applicant $applicant,
                                ApplicantJobActivity $application_activities,
                                User $user,
                                CompanyRecruiter $company_recruiter)
    {
        $this->employer = $employer;
        $this->job = $job;
        $this->applicant = $applicant;
        $this->application_activities = $application_activities;
        $this->user = $user;
        $this->company_recruiter = $company_recruiter;
    }

    public function index()
    {
       $jobs = (count(auth()->user()->employer)>0)?auth()->user()->employer->jobs()
                     ->orderBy('created_at','desc')
                     ->take(15)
                     ->get(): collect([]);
        $applicants = (count(auth()->user()->employer)>0)?auth()->user()->employer->applicants()
                        ->where('applicant_status_id','>',1)
                        ->orderBy('created_at','desc')
                        ->take(15)
                        ->get():collect([]);
        $applicants_acc = (auth()->user()->employer)?auth()->user()->employer->applicants()
                        ->orderBy('created_at','desc')
                        ->take(15)
                        ->pluck('id'):[0];
        $activities = $this->application_activities
                           ->whereIn('applicant_job_id',$applicants_acc)
                           ->take(15)
                           ->get();
        $total_jobs = (count(auth()->user()->employer)>0)?
                                $this->number_format_short(auth()->user()->employer->jobs()->count()):0;

        $total_applicants = (count(auth()->user()->employer)>0)?
                            $this->number_format_short(auth()->user()->employer->applicants()->count()):0;

        $total_hired =  (count(auth()->user()->employer)>0)?
                            $this->number_format_short(auth()->user()->employer->applicants()
                                                ->where('applicant_status_id','=',6)
                                                ->count()):0;

        return view('Employer::index')
                    ->with(compact('jobs',
                                    'applicants',
                                    'activities',
                                    'total_jobs',
                                    'total_jobs',
                                    'total_applicants',
                                    'total_hired'));
    }

    public function pipeline()
    {
        return view('Employer::applicant-pipeline-v2');
    }

    public function appointments()
    {
        $applicant_appointments = auth()->user()->employer->applicants()
                                       ->where('applicant_status_id','=','2')
                                       ->orWhere('applicant_status_id','=','3')
                                       ->orWhere('applicant_status_id','=','7')
                                       ->orWhere('employer_applicant_status_id','=','2')
                                       ->orWhere('employer_applicant_status_id','=','4')
                                       ->orWhere('employer_applicant_status_id','=','6')
                                       ->orderBy('created_at','desc')
                                       ->paginate(50);

        return view('Employer::appointments')
                    ->with(compact('applicant_appointments'));
    }

    public function profile()
    {
        if(auth()->user()->type->name == 'Employer' AND count(auth()->user()->employer)<1){
            return redirect()->route('employer.create.profile');
        }
        $employer = auth()->user()->employer;
        $jobs = $employer->jobs()->orderBy('created_at','desc')->paginate(25);
        return view('Employer::company-profile')
                    ->with(compact('employer','jobs'));
    }

    public function createProfile()
    {
        return view('Employer::create-company-profile');
    }

    public function editProfile()
    {
        $employer = $this->employer->whereUserId(auth()->user()->id)->first();
        return view('Employer::edit-company-profile')
                     ->with(compact('employer'));
    }

    public function storeProfile(CompanyProfileRequest $request)
    {
        $inputs = $request->only('company_name',
                                'contact_designation',
                                'contact_person_mobile',
                                'contact_person_mobil_2',
                                'office_address',
                                'office_number_1',
                                'office_number_2',
                                'website',
                                'description',
                                'sm_facebook',
                                'sm_linkedin',
                                'sm_twitter');

        if($request->hasFile('logo_url')){
            $files = [];
            $files[] = $request->file('logo_url');
            $arr_files = $this->imagesUpload($files, 300, 300);
            if(count(auth()->user()->employer)>0 AND strlen(auth()->user()->employer->logo_url)>0){
                $this->deleteImage(auth()->user()->employer->logo_url);
            }
            $inputs['logo_url'] = $arr_files['uploaded_file'];
        }

        $inputs['user_id'] = auth()->user()->id;
        $check = $this->employer->create($inputs);
        if(!$check){
            return redirect()->back()->withErrors(['message' => 'Opps! Something went wrong']);
        }
        return redirect()->route('employer.profile');
    }

    public function updateProfile(CompanyProfileRequest $request)
    {
        $inputs = $request->only('company_name',
                                'contact_designation',
                                'contact_person_mobile',
                                'contact_person_mobil_2',
                                'office_address',
                                'office_number_1',
                                'office_number_2',
                                'website',
                                'description',
                                'sm_facebook',
                                'sm_linkedin',
                                'sm_twitter');

        if($request->hasFile('logo_url')){
            $files = [];
            $files[] = $request->file('logo_url');
            $arr_files = $this->imagesUpload($files, 300, 300);
            if(count(auth()->user()->employer)>0 AND strlen(auth()->user()->employer->logo_url)>0){
                $this->deleteImage(auth()->user()->employer->logo_url);
            }
            $inputs['logo_url'] = $arr_files['uploaded_file'];
        }

        // $inputs['user_id'] = auth()->user()->id;
        $check = $this->employer->where('user_id','=',auth()->user()->id)->update($inputs);
        if(!$check){
            return redirect()->back()->withErrors(['message' => 'Opps! Something went wrong']);
        }
        return redirect()->route('employer.profile');
    }

    public function jobapplicants($param)
    {
        $job = (object) ['title' => 'Web Developer'];
        return view('Employer::career-applicants')
                    ->with(compact('job'));
    }

    public function applicants(Request $request)
    {
        $stat = auth()->user()->employer->is_premium ? 1:0;
        $query = $request->has('q')?$request->get('q'):'';
        $query_status = $request->has('status')?$request->get('status'):null;
        if($query OR $query_status){
            $aoo_stat = \App\ApplicantStatus::where('slug','=',$query_status)->first();
            $applicants = auth()->user()
                                  ->employer
                                  ->applicants()
                                  ->whereHas('applicant.user',function($q) use ($query){
                                      $q->whereRaw("(firstname LIKE ? OR lastname LIKE ? OR email LIKE ?)",['%'.$query.'%','%'.$query.'%','%'.$query.'%']);
                                  })
                                  ->where('applicant_status_id',($query_status)?'=':'>',($aoo_stat)?$aoo_stat->id:1)
                                  ->orderBy('created_at','desc')
                                  ->paginate(25);
        }else{
            $applicants = auth()->user()
                                  ->employer
                                  ->applicants()
                                  ->where('applicant_status_id','>',$stat)
                                  ->orderBy('created_at','desc')
                                  ->paginate(25);
        }



        return view('Employer::jobs.applicants')
                        ->with(compact('applicants'));
    }

    public function updateApplicantStatus(Request $request)
    {
        if($request->ajax()){
            $applicant = auth()->user()->employer
                                ->applicants()
                                ->whereApplicationCode($request->get('applicant_code'))
                                ->first();
            if(count($applicant)<1){
                return response()->json(['error' => true],403);
            }
            if($request->has('status')){
              $up = $applicant->update(['applicant_status_id' => $request->get('status')]);
            }
            $status = $applicant->applicant_status->name;
            return response()->json(['success' => $up,
                                      'status' => $status],200);
        }
    }

    public function applicantDetails($code)
    {
        $applicant = auth()->user()
                          ->employer
                          ->applicants()
                          ->whereApplicationCode($code)
                          ->first();
        // return $applicant;
        return view('Employer::jobs.applicant')
                        ->with(compact('applicant'));
    }

    public function inviteRecruiters()
    {

        $employer = auth()->user()->employer;
        $recruiters = $employer->employer_recruiters()->paginate(15);
        return view('Employer::invite-recruiters')
                    ->with(compact('employer','recruiters'));
    }

    public function addRecruiter(Request $request)
    {
        $inputs = $request->only('email','firstname','lastname');
        $inputs['password'] = \Hash::make($request->get('password'));
        $inputs['user_type_id'] = 3;
        $inputs['is_active'] = 1;
        $user = $this->user->create($inputs);
        $this->company_recruiter->create(['employer_id' => auth()->user()->employer->id,
                                        'recruiter_id' => $user->id]);
        return redirect()->back()->with(['successMessage' => 'Added Recruiter']);
    }

    public function billings()
    {
        return view('Employer::billings');
    }

    public function settings()
    {
        return view('Employer::settings');
    }
}
