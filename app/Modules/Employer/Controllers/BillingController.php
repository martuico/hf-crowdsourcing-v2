<?php namespace App\Modules\Employer\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Traits\ImageUpload;
use App\Modules\Employer\Requests\JobCreateRequest;

use App\Employer,
    App\Job,
    App\JobIndustry,
    App\JobType,
    App\Skill,
    App\Shift,
    App\ApplicantEducationLevel,
    App\JobLevel,
    App\ApplicantJob,
    App\ApplicantJobActivity,
    App\Region,
    App\Province,
    App\ApplicantEmployerFeedback,
    App\City;

class BillingController extends Controller
{
    use ImageUpload;
    protected $employer;
    protected $job;
    protected $jtype;
    protected $jindustry;
    protected $jskill;
    protected $shift;
    protected $edu_level;
    protected $applicantJob;
    protected $applicantJobActivity;
    protected $appEmployerFeedback;
    protected $location_region, $location_province, $location_city;

    public function __construct(Employer $employer,
                                Job $job,
                                JobIndustry $jindustry,
                                JobType $jtype,
                                Skill $skill,
                                Shift $shift,
                                ApplicantEducationLevel $edu_level,
                                JobLevel $job_level,
                                ApplicantJob $applicantJob,
                                ApplicantJobActivity $applicantJobActivity,
                                Region $location_region,
                                Province $location_province,
                                City $location_city,
                                ApplicantEmployerFeedback $appEmployerFeedback)
    {
        $this->employer = $employer;
        $this->job = $job;
        $this->jindustry = $jindustry;
        $this->jtype = $jtype;
        $this->skill = $skill;
        $this->shift = $shift;
        $this->edu_level = $edu_level;
        $this->job_level = $job_level;
        $this->applicantJob = $applicantJob;
        $this->applicantJobActivity = $applicantJobActivity;
        $this->location_region = $location_region;
        $this->location_province = $location_province;
        $this->location_city = $location_city;
        $this->appEmployerFeedback = $appEmployerFeedback;
        $this->middleware('hasCompanyProfile');
    }

    public function index()
    {
        return view('Employer::billings.index');
    }
}
