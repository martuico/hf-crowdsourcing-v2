<?php namespace App\Modules\Employer\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Traits\ImageUpload;
use App\Modules\Employer\Requests\JobCreateRequest;

use App\Employer,
    App\Job,
    App\JobIndustry,
    App\JobType,
    App\Skill,
    App\Shift,
    App\ApplicantEducationLevel,
    App\JobLevel,
    App\ApplicantJob,
    App\ApplicantJobActivity,
    App\Region,
    App\Province,
    App\ApplicantEmployerFeedback,
    App\City;

class JobController extends Controller
{
    use ImageUpload;
    protected $employer;
    protected $job;
    protected $jtype;
    protected $jindustry;
    protected $jskill;
    protected $shift;
    protected $edu_level;
    protected $applicantJob;
    protected $applicantJobActivity;
    protected $appEmployerFeedback;
    protected $location_region, $location_province, $location_city;

    public function __construct(Employer $employer,
                                Job $job,
                                JobIndustry $jindustry,
                                JobType $jtype,
                                Skill $skill,
                                Shift $shift,
                                ApplicantEducationLevel $edu_level,
                                JobLevel $job_level,
                                ApplicantJob $applicantJob,
                                ApplicantJobActivity $applicantJobActivity,
                                Region $location_region,
                                Province $location_province,
                                City $location_city,
                                ApplicantEmployerFeedback $appEmployerFeedback)
    {
        $this->employer = $employer;
        $this->job = $job;
        $this->jindustry = $jindustry;
        $this->jtype = $jtype;
        $this->skill = $skill;
        $this->shift = $shift;
        $this->edu_level = $edu_level;
        $this->job_level = $job_level;
        $this->applicantJob = $applicantJob;
        $this->applicantJobActivity = $applicantJobActivity;
        $this->location_region = $location_region;
        $this->location_province = $location_province;
        $this->location_city = $location_city;
        $this->appEmployerFeedback = $appEmployerFeedback;
        $this->middleware('hasCompanyProfile');
    }

    public function index()
    {
        $employer = $this->employer->where('user_id','=',auth()->user()->id)
                                    ->first();
        $jobs = $this->job->where('employer_id','=',$employer->id)
                        ->orderBy('created_at','desc')
                        ->paginate(30);
        return view('Employer::jobs.careers')
                    ->with(compact('jobs'));
    }

    public function show($jobCode)
    {
        $employer = $this->employer->where('user_id','=',auth()->user()->id)
                                    ->first();
        $job = $this->job
                    ->where('employer_id','=',$employer->id)
                    ->whereJobCode($jobCode)
                    ->first();
        if(count($job)<1){
            return abort(404);
        }
        $applicants = $this->applicantJob
                            ->where('job_id','=',$job->id)
                            ->where('employer_id','=',$employer->id)
                            ->orderBy('created_at','asc')
                            ->paginate(50);
        return view('Employer::jobs.details')
                        ->with(compact('employer','job','applicants'));
    }

    public function pipeline($jobCode)
    {
        $employer = $this->employer->where('user_id','=',auth()->user()->id)
                                    ->first();
        $job = $this->job
                    ->where('employer_id','=',$employer->id)
                    ->whereJobCode($jobCode)
                    ->first();
        if(count($job)<1){
            return abort(404);
        }
        $applicants = $this->applicantJob
                            ->where('job_id','=',$job->id)
                            ->where('employer_id','=',$employer->id)
                            ->where('applicant_status_id','<>',1)
                            ->orderBy('created_at','asc')
                            ->paginate(50);
        // return $job;
        return view('Employer::jobs.pipeline')
                        ->with(compact('employer','job','applicants'));
    }

    public function create()
    {
        $industries = $this->jindustry->orderBy('name','asc')->get();
        $types =  $this->jtype->orderBy('name','asc')->get();
        $jskill = $this->skill->orderBy('name','asc')->get();
        $shifts = $this->shift->orderBy('name','asc')->get();
        $edulevels = $this->edu_level->orderBy('name','asc')->get();
        $job_levels = $this->job_level->orderBy('name','asc')->get();
        $location_region = $this->location_region->orderBy('regCode','asc')->get();
        return view('Employer::jobs.create')
                    ->with(compact('jskill','types','industries','shifts','location_region','edulevels','job_levels'));
    }

    public function fetchProvince($regcode)
    {

        $provinces = $this->location_province
                        ->where('regCode','=',$regcode)
                        ->orderBy('provDesc','asc')
                        ->get();
        if(count($provinces)< 1){
            return abort(404);
        }

        return response()->json(['success' => true,
                                 'provinces' => $provinces],200);
    }

    public function fetchCities($provcode)
    {
        $cities = $this->location_city
                        ->where('provCode','=',$provcode)
                        ->orderBy('citymunDesc','asc')
                        ->get();

        if(count($cities)< 1){
            return abort(404);
        }

        return response()->json(['success' => true,
                                 'cities' => $cities],200);
    }

    public function store(JobCreateRequest $request)
    {
        // return dd(hash_equals($request->session()->token(), $request->input('_token')));
        \Session::put('_token', sha1(microtime()));
        $inputs = $request->only('job_title','job_type_id','job_industry_id','education_level_id','work_location','skills','salary_from','salary_to','work_shift_id','description','finders_fee','provide_relocation','designation','job_level_id','location_region','location_province','location_city');

        $inputs['provide_relocation'] = $request->has('provide_relocation')?$request->get('provide_relocation'):0;
        $inputs['show_salary_range'] = $request->has('show_salary_range')?$request->get('show_salary_range'):0;
        $check = $this->job->create($inputs);

        if(!$check){
            return redirect()->back()->withErrros(['Opps! Something went wrong!']);
        }
        return redirect()->route('employer.careers');
    }

    public function edit($jobCode)
    {
        $employer = $this->employer->where('user_id','=',auth()->user()->id)
                                    ->first();
        $job = $this->job->where('employer_id','=',$employer->id)
                        ->whereJobCode($jobCode)
                        ->first();
        if(count($job)<1){
            abort(404);
        }

        $industries = $this->jindustry->orderBy('name','asc')->get();
        $types =  $this->jtype->orderBy('name','asc')->get();
        $jskill = $this->skill->orderBy('name','asc')->get();
        $shifts = $this->shift->orderBy('name','asc')->get();
        $edulevels = $this->edu_level->orderBy('name','asc')->get();
        $job_levels = $this->job_level->orderBy('name','asc')->get();
        $location_region = $this->location_region->orderBy('regCode','asc')->get();
        return view('Employer::jobs.edit')
                    ->with(compact('jskill','types','industries','shifts','jlevels','job','edulevels','job_levels','location_region'));
    }

    public function update(JobCreateRequest $request,$jobcode)
    {
        $inputs = $request->only('job_title','job_type_id','job_industry_id','education_level_id','work_location','skills','salary_from','salary_to','work_shift_id','description','finders_fee','provide_relocation','designation','job_level_id');
        $inputs['show_salary_range'] = $request->has('show_salary_range')?$request->get('show_salary_range'):0;
        $inputs['provide_relocation'] = $request->has('provide_relocation')?$request->get('provide_relocation'):0;
        $check = $this->job->whereJobCode($jobcode)->update($inputs);
        if(!$check){
            return redirect()->back()->withErrros(['Opps! Something went wrong!']);
        }
        return redirect()->route('employer.careers');
    }

    public function applicantJob($job, $applicant)
    {

        $application = $this->applicantJob
                            ->whereApplicationCode($applicant)
                            ->first();

        if(count($application->applicant)<1){
            return abort(404);
        }
        $applicant = $application->applicant;
        if(\Session::get('viewing-application') !== $application->application_code){
            $this->applicantJobActivity->create(['applicant_job_id' => $application->id,
                                         'user_id' => auth()->user()->id,
                                         'icon' => '<i class="ico-eye-open bgcolor-success"></i>',
                                         'title' => 'Viewing Application',
                                         'description' => 'Viewed applicant profile ('.$application->application_code.')']);
            \Session::put('viewing-application',$application->application_code);
        }

        return view('Employer::jobs.applicant-detailv2')
                        ->with(compact('application','applicant'));
    }

    public function applicantActivityComment(Request $request)
    {
        if($request->ajax()){
            $application = auth()->user()->employer
                                ->applicants()
                                ->whereApplicationCode($request->get('application_code'))
                                ->first();

            if(count($application)<1){
                return abort(404);
            }
            $activity = $this->applicantJobActivity
                             ->create(['applicant_job_id' => $application->id,
                                     'user_id' => auth()->user()->id,
                                     'icon' => '<i class="ico-mail bgcolor-success"></i>',
                                     'title' => 'Commented Application',
                                      'description' =>  $request->get('comment').' ('.$application->application_code.')']);
            $activity->created_by = auth()->user()->my_name;
            return response()->json(['success' => true,
                                     'activity' => $activity],200);
        }

    }

    public function applicantAppoiments($job, $applicant)
    {
        $application = auth()->user()->employer
                          ->applicants()
                          ->whereApplicationCode($applicant)
                          ->first();
        if(count($application)<1){
          return abort(404);
        }
        return view('Employer::jobs.applicant-detail')
                      ->with(compact('application'));
    }

    public function setAppoiment(Request $request)
    {
        if($request->ajax()){
            $application = auth()->user()->employer
                              ->applicants()
                              ->whereApplicationCode($request->get('applicant_code'))
                              ->first();

            if(count($application)<1){
                return response()->json(['error' => 'no data'],404);
            }

            $inputs = ['employer_appointment_type' => $request->get('employer_appointment_type'),
                       'employer_appointment_sched' => $request->get('employer_appointment_sched')
                      ];

            $ch = $application->update($inputs);
            $activity = $this->applicantJobActivity
                             ->create(['applicant_job_id' => $application->id,
                                     'user_id' => auth()->user()->id,
                                     'icon' => '<i class="ico-mail bgcolor-success"></i>',
                                     'title' => 'Set Appointment',
                                      'description' => 'Added '.$request->get('employer_appointment_type').' appointment on '. $request->get('employer_appointment_sched') .' ('.$application->application_code.')']);
            return response()->json(['success' => $ch],200);
        }
    }

    public function updateStage(Request $request,$applicant)
    {
        // return $request->all();
        $inputs = array_filter($request->only('employer_applicant_status_id',
                                              'schedule_final_interview',
                                              'schedule_account_validation',
                                              'reason',
                                              'notes',
                                              'schedule_job_offer'
                                            ));
        $application = $this->applicantJob->whereApplicationCode($applicant)
                    ->first();
        if(count($application)<1){
            return abort(404);
        }
        if($application->employer_applicant_status_id == 8){
            return redirect()->back()->with(['successMessage' => 'Can\'t update once applicant is hired']);
        }

        $fb = $this->appEmployerFeedback->where('applicant_job_id','=',$application->id)->first();
        if(count($fb)<1){
            $inputs1 = ['applicant_job_id' => $application->id,
                        'applicant_id' => $application->applicant_id,
                        'employer_id' => $application->employer_id,
                        'job_id' => $application->job_id
            ];
            $new_inputs = array_merge($inputs,$inputs1);
            $this->appEmployerFeedback->create($new_inputs);
            $application->update(['employer_applicant_status_id' => $request->get('employer_applicant_status_id')]);
        }else{
            $fb->update($inputs);
        }

        if($request->get('employer_applicant_status_id') == '8'){
            $application->update(['applicant_status_id' => 8,
                                  'employer_applicant_status_id' => 8,
                                  'hire_account' => $request->get('hire_account'),
                                  'hire_date' => $request->get('hire_date'),
                                  'date_of_training' => $request->get('date_of_training')
                                ]);
        }

        return redirect()->back()->with(['successMessage' => 'Updated Status']);
    }
}
