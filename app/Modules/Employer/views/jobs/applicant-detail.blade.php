@extends('Dashboard::template.main')

@section('content')
  <!-- Page Header -->
  <div class="page-header page-header-block">
      <div class="page-header-section">
          <h4 class="title semibold">Applicantion - {{ $application->applicant->user->my_name }} for {{ $application->job->job_title}} <br>
            <small>CODE: {{ $application->application_code }}</small>
          </h4>
      </div>
      <div class="page-header-section">
          <!-- Toolbar -->
          <div class="toolbar clearfix text-right">
              <a href="{{ \URL::previous() }}" class="btn btn-sm btn-default"  data-toggle="tooltip" data-placement="bottom" title="Create Job Post"><i class="ico-chevron-left"></i></a>
          </div>
          <!--/ Toolbar -->
      </div>
  </div>

  <div class="row">
    <div class="col-md-6">
      <div class="widget panel">
          <!-- panel body -->
          <div class="panel-body">
              <ul class="list-unstyled">
                  <li class="text-center">
                      <img class="img-circle img-bordered-success" src="{{ $application->applicant->user->profile_pic }}" alt="" width="75px" height="75px">
                      <br>
                      <h5 class="semibold mb0">{{ $application->applicant->user->my_name }}</h5>
                      <p class="nm text-muted">{{ $application->applicant->position }}</p>
                      <a href="javscript:void(0);" class="nm text-teal"><i class="fa fa-link"></i> {{ str_replace(url('upload-resume').'/', '', $application->applicant->attached_resume) }} </a>
                  </li>
              </ul>
              <hr>
              <div class="row">
                <div class="col-md-6">
                  <h4><i class="ico-bubble-dots3"></i> Languages</h4>


                  @if(strlen($application->applicant->languages)>0)
                    <ul class="list-inline">
                    @foreach(explode(',',$application->applicant->languages) as $language)
                    <li><span class="label label-default">{{ $language }}</span></li>
                    @endforeach
                    </ul>
                  @endif

                </div>
                <div class="col-md-6">
                  <h4><i class="ico-bubble-dots3"></i> Skills</h4>
                  @if(strlen($application->applicant->skills)>0)
                  <ul class="list-inline">
                    @foreach(explode(',',$application->applicant->skills) as $skill)
                    <li><span class="label label-default">{{ $skill }}</span></li>
                    @endforeach
                  </ul>
                  @endif
                </div>
              </div>
          </div>
          <!--/ panel body -->
      </div>

      <div class="widget panel">
        <div class="panel-body">
          <div class="table-layout">
            <h4>Biography</h4>
            {!! $application->applicant->biography !!}
            <hr>

            <div class="row">
              <div class="col-md-6">
                <h4><i class="ico-vcard"></i> Personal Information</h4>
                <p class="mb5">Full name : <strong class="text-uppercase">{{ $application->applicant->user->my_name }}</strong></p>
                <p class="mb5">D.O.B : <strong class="text-uppercase">{{ $application->applicant->date_of_birth?:'None' }}</strong></p>
                <p class="mb5">Email : <strong class="text-uppercase"><a href="mailto:{{ $application->applicant->user->email }}?Subject=From%20Human%20Factor">{{ $application->applicant->user->email }}</a></strong></p>
                <p class="mb5">Employment Status: <strong class="text-uppercase">{{ $application->applicant->employment_status?:'None' }}</strong></p>
              </div>
              <div class="col-md-6">
                <div class="row">
                  <h4><i class="ico-map-marker"></i> Address</h4>
                  <p>{{ $application->applicant->full_address }}</p>
                </div>
                <div class="row">
                  <p class="mb5">Mobile Number: <strong class="text-uppercase">{{ $application->applicant->mobile_number_1?:'None' }}</strong></p>
                  <p class="mb5">House Number: <strong class="text-uppercase">{{ $application->applicant->house_number_1?:'None' }}</strong></p>
                </div>
              </div>
            </div>

            <hr>
            <div class="row">
              <div class="col-md-12">
                <h4>Experiences</h4>
                @if(count($application->applicant->experiences )>0)
                @foreach($application->applicant->experiences as $exp)
                  @if(strlen($exp->company_name )>0 AND strlen($exp->position)>0)
                  <h5 class="text-uppercase">{{ $exp->position }} @ {{ $exp->company_name }}</h5>
                  @endif
                  @if(strlen($exp->from)>0 AND strlen($exp->to)>0)
                  <p class="text-muted">{{ $exp->from }} - {{ $exp->to }}</p>
                  @endif
                  @if(strlen($exp->resposibilities)>0)
                  <p>{{ $exp->resposibilities }}</p>
                  @endif
                @endforeach
                @else
                <div class="alert alert-info">
                  No data
                </div>
                @endif
              </div>
            </div><!-- row -->

            <hr>
            <div class="row">
              <div class="col-md-12">
                <h4>Educations</h4>
                @if(count($application->applicant->educations )>0)
                @foreach($application->applicant->educations as $edu)
                  @if(strlen($edu->school_name)>0 AND strlen($edu->attainment)>0)
                  <h5 class="text-uppercase">{{ $edu->attainment }} @ {{ $edu->school_name }}</h5>
                  @endif
                  @if(strlen($edu->from_to)>0 AND strlen($edu->to)>0)
                  <p class="text-muted">{{ $edu->from }} - {{ $edu->to }}</p>
                  @endif
                  @if(strlen($edu->awards)>0)
                  <p>{{ $edu->awards }}</p>
                  @endif
                @endforeach
                @else
                <div class="alert alert-info">
                  No data
                </div>
                @endif
              </div>
            </div><!-- row -->

          </div>
        </div>
      </div>
    </div>
    <div class="col-md-6">
      <div class="widget panel">
        <div class="panel-header">
          <!-- Nav tabs -->
          <ul class="nav nav-tabs" role="tablist">
            <li role="presentation" class="{{activeClass(\Route::currentRouteName(),'employer.careers.applicant.details')}}"><a href="{{ route('employer.careers.applicant.details',['job_code' => $application->job->job_code,'applicant' => $application->application_code]) }}">STATUS/ACTIVITIES</a></li>
            <li role="presentation" class="{{activeClass(\Route::currentRouteName(),'employer.careers.applicant.appointments')}}"><a href="{{ route('employer.careers.applicant.appointments',['job_code' => $application->job->job_code,'applicant' => $application->application_code]) }}">APPOINTMENTS</a></li>
          </ul>
        </div>
        <div class="panel-body">
          <div class="tab-content">
            <div role="tabpanel" class="tab-pane {{activeClass(\Route::currentRouteName(),'employer.careers.applicant.details')}}" id="status">
              @php
                $stat = [1 => 'Activite File',
                         2 => 'Endorsed',
                         3 => 'For Final Interview',
                         4 => 'No Show',
                         5 => 'Rejected',
                         6 => 'Hired'
                        ];

              @endphp

              <div class="panel-heading">
                <h5 class="panel-title" id="statustext">Current Status: {{ $stat[$application->applicant_status_id] }}</h5>
              </div>
              <div class="btn-group" data-toggle="buttons" id="statuses">
                @if($application->applicant_status_id == 1)
                  <label class="btn btn-primary status hasChoices">
                    <input type="radio" name="options2" id="option2" autocomplete="off" data-applicantcode="{{ $application->application_code }}" data-status="2"> Endorsed
                  </label>
                  <label class="btn btn-primary status hasChoices">
                    <input type="radio" name="options2" id="option2" autocomplete="off" data-applicantcode="{{ $application->application_code }}" data-status="3"> For Final Interview
                  </label>
                  <label class="btn btn-primary status lastChoice">
                    <input type="radio" name="options2" id="option2" autocomplete="off" data-applicantcode="{{ $application->application_code }}" data-status="5"> Rejected
                  </label>
                @elseif($application->applicant_status_id == 2 OR $application->applicant_status_id == 3)
                  <label class="btn btn-primary status lastChoice">
                    <input type="radio" name="options4" id="option4" autocomplete="off" data-applicantcode="{{ $application->application_code }}" data-status="4"> No Show
                  </label>
                  <label class="btn btn-primary status lastChoice">
                    <input type="radio" name="options5" id="option5" autocomplete="off" data-applicantcode="{{ $application->application_code }}" data-status="5"> Rejected
                  </label>
                  <label class="btn btn-primary status lastChoice">
                    <input type="radio" name="options6" id="option6" autocomplete="off" data-applicantcode="{{ $application->application_code }}" data-status="6"> Hired
                  </label>
                @else
                @endif
              </div>

              <div class="form-group mt10">
                <div class="has-icon mb10">
                    <input type="text" class="form-control" id="comment">
                    <i class="ico-paper-plane form-control-icon"></i>
                </div>
              </div>
              <div class="panel-heading"><h5 class="panel-title"><i class="ico-health mr5"></i>Latest Activity  {{ date('F-d')}}</h5></div>
              <!-- Media list feed -->
              @php
                $activities = $application->activities()->paginate(15);
              @endphp
              @if(count($activities)>0)

              <ul class="media-list media-list-feed nm">
                  @foreach($activities as $activity)
                  <li class="media">
                      <div class="media-object pull-left">
                          {!! $activity->icon !!}
                      </div>
                      <div class="media-body">
                          <p class="media-heading">{{ $activity->title }}</p>
                          <p class="media-text"><span class="text-primary semibold">{{ $activity->description }}</span> by {{ $activity->user->my_name }}</p>
                          <p class="media-meta">{{ $activity->created_at }}</p>
                      </div>
                  </li>
                  @endforeach
              </ul>
              @endif
              {{ $activities->links() }}
            </div>
            <div role="tabpanel" class="tab-pane {{activeClass(\Route::currentRouteName(),'employer.careers.applicant.appointments')}}" id="appointments">
              <h4 class="panel-title">Select appointment type</h4>
              <div class="btn-group" data-toggle="buttons" id="appointment_status">
                <label class="btn btn-primary appointment lastChoice">
                  <input type="radio" name="options5" id="option5" autocomplete="off" data-applicantcode="{{ $application->application_code }}" data-appointment="call"><i class="ico-phone"></i> Call
                </label>
                <label class="btn btn-primary appointment lastChoice">
                  <input type="radio" name="options6" id="option6" autocomplete="off" data-applicantcode="{{ $application->application_code }}" data-appointment="walk-in"><i class="fa fa-wheelchair-alt"></i> Walk-in
                </label>
              </div>
              <div class="row" style="margin-top:20px;">
                <div class="col-md-12">
                    <h4 class="panel-title">Schedule date and time</h4>
                   <div id="datetimepicker1"></div>
                </div>
              </div>
              <div class="row mt-20">
                  <div class="panel-heading">
                  <h4 class="panel-title fsize13">Date & Time: <span id="hidden-val"></span></h4>
                  <input type="hidden" id="my_hidden_input">
                  <input type="hidden" id="my_hidden_type">
                  <h4 class="panel-title fsize13">Type: <span id="appointment-type"></span></h4>
                  </div>
              </div>
              <div class="form-group">
                <button type="button" id="setAppoiment" class="btn btn-sm btn-info"><i class="ico-clock"></i> Set Schedule</button>
              </div>
              @if(strlen($application->employer_appointment_sched)>0)
              <div class="alert alert-info">
                <p><strong>Appointment Schedule: {{$application->employer_appointment_sched}}</strong></p>
                <p><strong>Appointment Type: {{$application->employer_appointment_type}}</strong></p>
              </div>
              @endif
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
@stop


@section('styles')
  <link rel="stylesheet" href="{{ asset('hfv1/plugins/sweetalert/dist/sweetalert.css') }}">
  <link rel="stylesheet" href="{{asset('bower_components/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css')}}" />
@stop
@section('scripts')
  <script src="{{ asset('hfv1/plugins/sweetalert/dist/sweetalert.min.js') }}"></script>
  <script type="text/javascript" src="{{asset('bower_components/moment/min/moment.min.js')}}"></script>
  <script type="text/javascript" src="{{asset('bower_components/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js')}}"></script>
  <script>
  var ApplicationDetail = {
      init : function(){
        this.changeStatus();
        this.addComment();
        this.datePick();
        this.appointmentType();
        this.setAppoiment();
      },
      setAppoiment: function(){
        $('#setAppoiment').click(function(e){

          var atype = $('#my_hidden_type').val();
          var datetime = $('#my_hidden_input').val();
          var txtdatetime = $('#hidden-val').text();
          if(atype == '' && datetime == ''){
            swal('Error!','Please set appointment schedule','error');
            return false;
          }

          var data = {
            'employer_appointment_sched' : datetime,
            'employer_appointment_type' : atype,
            'applicant_code' : '{{ $application->application_code }}'
          }

          $.ajax({
            url : window.location.origin + '/employer/set-appointment',
            type : 'post',
            data : data,
            success : function(res){
              console.log(res);
              if($('.alert-info').length > 0){
                $('.alert-info').remove();
              }
              if(res.success){
                var temp = `<div class="alert alert-info">
                  <p><strong>Appointment Schedule: ${txtdatetime}</strong></p>
                  <p><strong>Appointment Type: ${atype}</strong></p>
                </div>`
                $('#appointments').append($(temp));
                return true;
              }
              swal('Error!','Opps! Something went wrong','error');
            },error : function(res){
              console.log(res);
              swal('Error!','Opps! Something went wrong','error');
            }
          })
          e.preventDefault();
          e.stopImmediatePropagation();
        });
      },
      appointmentType : function(){
        $('.appointment').click(function(e){
          var txt = $(this).find('input').attr('data-appointment')
          $('#appointment-type').text(txt.toUpperCase());
          $('#my_hidden_type').val(txt);
        });
      },
      datePick : function(){
        $('#datetimepicker1').datetimepicker({
          inline: true,
          sideBySide: true
        });
        $('#datetimepicker1').on('dp.change', function(event) {
          //console.log(moment(event.date).format('MM/DD/YYYY h:mm a'));
          //console.log(event.date.format('MM/DD/YYYY h:mm a'));
          $('#selected-date').text(event.date);
          var formatted_date = event.date.format('MM/DD/YYYY h:mm a');
          var dt = moment(formatted_date, ["MM/DD/YYYY h:mm A"]).format("YYYY-MM-DD HH:mm:ss");
          $('#my_hidden_input').val(dt);
          $('#hidden-val').text(formatted_date);
        });
      },
      addComment : function(){
        $('#comment').keypress(function(e){
          var box = $(this);
          var comment =  box.val();
          var application_code = '{{ $application->application_code }}';
          if(e.which == 13 && comment !='' && comment.length > 0){
            $.ajax({
              url : window.location.origin + '/employer/applicant-comment',
              type : 'post',
              data : { 'comment': comment, 'application_code' : application_code},
              success : function(res){
                console.log(res);
                $('.media-list-feed').prepend($(ApplicationDetail.boxTemplate(res.activity)));
                swal('Sent!','Commented added in activities','success');
                box.val('');
              },error : function(res){
                console.log(res);
                swal('Opps!','Something went wrong','error');
              }
            });

          }
        });
      },
      boxTemplate: function(activity){
        return `<li class="media">
            <div class="media-object pull-left">
                ${activity.icon}
            </div>
            <div class="media-body">
                <p class="media-heading">${activity.title}</p>
                <p class="media-text">
                <span class="text-primary semibold">${activity.description}</span>
                by ${activity.created_by}</p>
                <p class="media-meta">${activity.created_at}</p>
            </div>
        </li>`
      },
      changeStatus: function(){
        $('.status').click(function(e){
          var option = $(this);
          var status = option.find('input').attr('data-status');
          var applicant_code = option.find('input').attr('data-applicantcode');
          console.log(applicant_code,status);
          $.ajax({
            url : window.location.origin + '/employer/update-applicant-status',
            type : 'post',
            data : {'applicant_code' : applicant_code,
                    'status' : status},
            success : function(res){
              if(res.success){
                if(option.hasClass('hasChoices')){
                  $('#statuses').children().remove();
                  $('#statuses').append(ApplicationDetail.statusTemplate(applicant_code));
                  $('#statustext').text('Current Status:'+res.status);
                }
                if(option.hasClass('lastChoice')){
                  $('#statuses').children().remove();
                  $('#statustext').text('Current Status:'+res.status);
                }
                swal('Updated!','You have change the status','success');
              }
            },
            error : function(res){
              console.log(res);
              swal('Opps!','Something went wrong','error');
            }
          });
          e.stopImmediatePropagation();
          e.preventDefault();
        });
      },
      statusTemplate: function(applicant_code){
        return  `
          <label class="btn btn-primary status lastChoice">
            <input type="radio" name="options4" id="option4" autocomplete="off" data-applicantcode="${applicant_code}" data-status="4"> No Show
          </label>
          <label class="btn btn-primary status lastChoice">
            <input type="radio" name="options5" id="option5" autocomplete="off" data-applicantcode="${applicant_code}" data-status="5"> Rejected
          </label>
          <label class="btn btn-primary status lastChoice">
            <input type="radio" name="options6" id="option6" autocomplete="off" data-applicantcode="${applicant_code}" data-status="6"> Hired
          </label>
        `;
      }
  }
  $(function(){
    ApplicationDetail.init()
  });
  </script>
@stop
