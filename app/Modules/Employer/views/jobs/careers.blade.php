@extends('Dashboard::template.main')

@section('content')
  <!-- Page Header -->
  <div class="page-header page-header-block">
      <div class="page-header-section">
          <h4 class="title semibold">Careers</h4>
      </div>
      <div class="page-header-section">
          <!-- Toolbar -->
          <div class="toolbar clearfix text-right">
              <a href="{{route('employer.job.create')}}" class="btn btn-sm btn-default"  data-toggle="tooltip" data-placement="bottom" title="Create Job Post"><i class="ico-file-plus"></i> CREATE JOB POST</a>
          </div>
          <!--/ Toolbar -->
      </div>
  </div>

  @if(auth()->user()->type->name == 'Employer' AND count(auth()->user()->employer)<1)
  <div class="row">
    <div class="col-lg-12">
      <div class="alert alert-info">
        <strong>Note:  You need to set up your 'Company Profile' first</strong>
      </div>
    </div>
  </div>
  @else

  <div class="row">
      <div class="col-lg-12">
          <!-- START panel -->
          <div class="panel panel-default">
            @if( count($jobs)>0 )
              <!-- panel heading/header -->
              <div class="panel-heading">
                  <h3 class="panel-title ellipsis"><i class="ico-blog2 mr5"></i>Jobs</h3>
                  <!-- panel toolbar -->
                  <div class="panel-toolbar text-right">
                  </div>
                  <!--/ panel toolbar -->
              </div>
              <!--/ panel heading/header -->
              <!-- panel body with collapse capabale -->
              <div class="table-responsive panel-collapse pull out">
                  <table class="table">
                      <thead>
                          <tr>
                              <th>Job Title</th>
                              <th>Job Designation</th>
                              <th>Work Shift</th>
                              <th>Posted Date</th>
                              <th>Due Date</th>
                              <th>Status</th>
                              <th class="text-center" width="10%">Total Applicants</th>
                              <th></th>
                          </tr>
                      </thead>
                      <tbody>
                          @foreach($jobs as $job)
                          <tr>
                              <td><span class="semibold text-accent">{{ $job->job_title }}</span><br>
                                <span class="text-muted">CODE: {{ $job->job_code }}</span>
                              </td>
                              <td>{{ $job->designation }}</td>
                              <td>{{ $job->shift->name }}</td>
                              <td>{{ $job->created_at }}</td>
                              <td>{{ $job->valid_until }}</td>
                              <td><span class="label label-success">{{ strtoupper($job->status) }}</span></td>
                              <td class="text-center">
                                <a href="{{route('employer.careers.pipeline',['jobcode' => $job->job_code])}}" class="semibold text-accent">
                                  <span class="label label-inverse">
                                    {{ $job->applicant_premium_count }}
                                  </span>
                                </a>
                              </td>
                              <td>
                                <a href="{{route('employer.careers.details',['jobcode' => $job->job_code])}}" class="btn btn-info btn-xs"><i class="fa fa-eye"></i></a>
                                <a href="{{route('employer.job.edit',['jobcode' => $job->job_code])}}" class="btn btn-warning btn-xs"><i class="fa fa-edit"></i></a>
                              </td>
                          </tr>
                          @endforeach
                      </tbody>
                  </table>
              </div>
              <!--/ panel body with collapse capabale -->
          </div>
          {{ $jobs->links() }}
          @else
          <div class="panel widget">
            <div class="panel-body">
              <div class="alert alert-info">
                <strong>No jobs posted yet!</strong>
              </div>
            </div>
          </div>
          @endif
          <!--/ END panel -->
      </div>
  </div>
  @endif
@stop

@section('styles')
  <style>
    tbody > tr > td:last-child {
      text-align:center;
    }
  </style>
@stop
