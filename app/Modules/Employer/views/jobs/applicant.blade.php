<div class="container-fluid">
  <div class="col-md-12">
  <div class="row">
    <div class="col-md-4">
      <div class="widget panel">
          <!-- panel body -->
          <div class="panel-body">
              <ul class="list-unstyled">
                  <li class="text-center">
                      <img class="img-circle img-bordered-success" src="{{ $applicant->applicant->user->profile_pic }}" alt="" width="75px" height="75px">
                      <br>
                      <h5 class="semibold mb0">{{ $applicant->applicant->user->my_name }}</h5>
                      <p class="nm text-muted">{{ $applicant->applicant->position }}</p>
                      <a href="javscript:void(0);" class="nm text-teal"><i class="fa fa-link"></i> {{ str_replace(url('upload-resume').'/', '', $applicant->applicant->attached_resume) }} </a>
                  </li>
              </ul>
          </div>
          <!--/ panel body -->
      </div>

      <div class="widget panel">
        <div class="panel-body">
          <div class="col-md-12">
            <div class="row">
              <h4><i class="ico-bubble-dots3"></i> Languages</h4>
              @if(strlen($applicant->applicant->languages)>0)
              <ul class="list-inline">
                @foreach(explode(',',$applicant->applicant->languages) as $lang)
                <li><span class="label label-default">{{ $lang }}</span></li>
                @endforeach
              </ul>
              @endif
            </div>
            <div class="row">
              <h4><i class="ico-glasses3"></i> Skills</h4>
              @if(strlen($applicant->applicant->skills)>0)
              <ul class="list-inline">
                @foreach(explode(',',$applicant->applicant->skills) as $skil)
                <li><span class="label label-default">{{ $skil }}</span></li>
                @endforeach
              </ul>
              @endif
            </div>
            <div class="row">
              <h4>Status</h4>
              <select name="status" class="form-control status" data-applicantcode="{{ $applicant->application_code }}">
                <option value="1" {{ ($applicant->applicant_status_id == 1)? 'selected':'' }}>Active File</option>
                <option value="2" {{ ($applicant->applicant_status_id == 2)? 'selected':'' }}>Endorsed</option>
                <option value="3" {{ ($applicant->applicant_status_id == 3)? 'selected':'' }}>For Final Interview</option>
                <option value="4" {{ ($applicant->applicant_status_id == 4)? 'selected':'' }}>No Show</option>
                <option value="5" {{ ($applicant->applicant_status_id == 5)? 'selected':'' }}>Rejected</option>
                <option value="6" {{ ($applicant->applicant_status_id == 6)? 'selected':'' }}>Hired</option>
              </select>

            </div>
          </div>
        </div>
      </div>
    </div><!-- col-md-4 -->
    <div class="col-md-8">
      <div class="widget panel">
        <div class="panel-body">
          <div class="table-layout">
            <h4>Biography</h4>
            {!! $applicant->applicant->biography !!}
            <hr>

            <div class="row">
              <div class="col-md-6">
                <h4><i class="ico-vcard"></i> Personal Information</h4>
                <p class="mb5">Full name : <strong class="text-uppercase">{{ $applicant->applicant->user->my_name }}</strong></p>
                <p class="mb5">D.O.B : <strong class="text-uppercase">{{ $applicant->applicant->date_of_birth?:'None' }}</strong></p>
                <p class="mb5">Email : <strong class="text-uppercase"><a href="mailto:{{ $applicant->applicant->user->email }}?Subject=From%20Human%20Factor">{{ $applicant->applicant->user->email }}</a></strong></p>
                <p class="mb5">Employment Status: <strong class="text-uppercase">{{ $applicant->applicant->employment_status?:'None' }}</strong></p>
              </div>
              <div class="col-md-6">
                <div class="row">
                  <h4><i class="ico-map-marker"></i> Address</h4>
                  <p>{{ $applicant->applicant->full_address }}</p>
                </div>
                <div class="row">
                  <p class="mb5">Mobile Number: <strong class="text-uppercase">{{ $applicant->applicant->mobile_number_1?:'None' }}</strong></p>
                  <p class="mb5">House Number: <strong class="text-uppercase">{{ $applicant->applicant->house_number_1?:'None' }}</strong></p>
                </div>
              </div>
            </div>

            <hr>
            <div class="row">
              <div class="col-md-12">
                <h4>Experiences</h4>
                @if(count($applicant->applicant->experiences )>0)
                @foreach($applicant->applicant->experiences as $exp)
                  @if(strlen($exp->company_name )>0 AND strlen($exp->position)>0)
                  <h5 class="text-uppercase">{{ $exp->position }} @ {{ $exp->company_name }}</h5>
                  @endif
                  @if(strlen($exp->from)>0 AND strlen($exp->to)>0)
                  <p class="text-muted">{{ $exp->from }} - {{ $exp->to }}</p>
                  @endif
                  @if(strlen($exp->resposibilities)>0)
                  <p>{{ $exp->resposibilities }}</p>
                  @endif
                @endforeach
                @else
                <div class="alert alert-info">
                  No data
                </div>
                @endif
              </div>
            </div><!-- row -->

            <hr>
            <div class="row">
              <div class="col-md-12">
                <h4>Educations</h4>
                @if(count($applicant->applicant->educations )>0)
                @foreach($applicant->applicant->educations as $edu)
                  @if(strlen($edu->school_name)>0 AND strlen($edu->attainment)>0)
                  <h5 class="text-uppercase">{{ $edu->attainment }} @ {{ $edu->school_name }}</h5>
                  @endif
                  @if(strlen($edu->from_to)>0 AND strlen($edu->to)>0)
                  <p class="text-muted">{{ $edu->from }} - {{ $edu->to }}</p>
                  @endif
                  @if(strlen($edu->awards)>0)
                  <p>{{ $edu->awards }}</p>
                  @endif
                @endforeach
                @else
                <div class="alert alert-info">
                  No data
                </div>
                @endif
              </div>
            </div><!-- row -->

          </div>
        </div>
      </div>
    </div>
  </div>
  </div>
</div>
