@extends('Dashboard::template.main')

@section('content')
  <!-- Page Header -->
  <div class="page-header page-header-block">
      <div class="page-header-section">
          <h4 class="title semibold">Applicant Pipeline</h4>
      </div>
      <div class="page-header-section">
          <!-- Toolbar -->
          <div class="toolbar clearfix text-right">
            {{ \Form::open(['method' => 'GET']) }}
              <div class="input-group">
                    <input type="text" name="q" class="form-control" placeholder="Search Applicant">
                    <span class="input-group-btn">
                      <button class="btn btn-default" type="submit">Go!</button>
                      <a class="btn btn-danger" href="{{route('employer.applicants')}}">Clear Filter</a>
                    </span>
              </div><!-- /input-group -->
            {{ \Form::close() }}
          </div>
          <!--/ Toolbar -->
      </div>
  </div>
  <!-- Page Header -->
  <!-- START row -->
  <div class="row">
      <div class="col-md-12">
          <!-- START panel -->

          <div class="panel panel-primary">
            @if(count($applicants)>0)
              <!-- panel heading/header -->
              <div class="panel-heading">
                  <h3 class="panel-title"><span class="panel-icon mr5"><i class="ico-table22"></i></span> Applicants</h3>
                  <!-- panel toolbar -->
                  <div class="panel-toolbar text-right">
                  </div>
                  <!--/ panel toolbar -->
              </div>
              <!--/ panel heading/header -->
              <!-- panel toolbar wrapper -->

              <div class="panel-toolbar-wrapper pl0 pt5 pb5">
                  @if(auth()->user()->type->slug == 'employer' AND auth()->user()->employer->is_premium)
                  <div class="panel-toolbar text-right">
                      <a href="{{route('employer.applicants')}}?{{isset($_GET['q'])?'q='.$_GET['q'].'&':''}}status=endorsed" class="btn btn-sm btn-default">Endorsed</a>
                      <a href="{{route('employer.applicants')}}?{{isset($_GET['q'])?'q='.$_GET['q'].'&':''}}status=re-profile-to-premium-job-post" class="btn btn-sm btn-default">Re profile to Premium Job post</a>
                      <a href="{{route('employer.applicants')}}?{{isset($_GET['q'])?'q='.$_GET['q'].'&':''}}status=active-file" class="btn btn-sm btn-default">Active File</a>
                      <a href="{{route('employer.applicants')}}?{{isset($_GET['q'])?'q='.$_GET['q'].'&':''}}status=refer-to-regular-job-post" class="btn btn-sm btn-default">Refer to Regular Job Post</a>
                      <a href="{{route('employer.applicants')}}?{{isset($_GET['q'])?'q='.$_GET['q'].'&':''}}status=no-show" class="btn btn-sm btn-default">No Show</a>
                      <a href="{{route('employer.applicants')}}?{{isset($_GET['q'])?'q='.$_GET['q'].'&':''}}status=re-schedule" class="btn btn-sm btn-default">Re schedule</a>
                      <a href="{{route('employer.applicants')}}?{{isset($_GET['q'])?'q='.$_GET['q'].'&':''}}status=hired" class="btn btn-sm btn-default">Hired</a>
                  </div>
                  @endif
              </div>
              <!--/ panel toolbar wrapper -->

              <!-- panel body with collapse capabale -->
              <div class="table-responsive pull out">
                  <table class="table table-bordered table-hover" id="table1">
                      <thead>
                          <tr>
                              <th width="5%"></th>
                              <th width="20%">Name</th>
                              <th>Applied</th>
                              <th width="25%">HF Status</th>
                              <th width="25%">Employer Feedback</th>
                              <th width="3%"></th>
                          </tr>
                      </thead>
                      <tbody>
                        @foreach($applicants as $applicant)
                          <tr>
                              <td><div class="media-object"><img src="{{ $applicant->applicant->user->profile_pic}}" alt="" class="img-circle"></div>
                              </td>
                              <td>{{ $applicant->applicant->user->myname }} <br>
                                  <p class="no-margin text-muted"><small>CODE: {{ $applicant->application_code }}</small></p>
                                  <small>{{ $applicant->applicant->full_address}}</small>
                              </td>
                              <td>
                                {{ $applicant->job->job_title}}
                                <br>
                                {{ $applicant->job->designation}}
                              </td>
                              <td class="stages" data-status="{{ $applicant->applicant_status_id }}">
                                <p><strong>Current Status :</strong> {{ $applicant->applicant_status->name }} </p>
                                @if($applicant->employer_appointment_type)
                                <p class="app_type"><strong>Appointments :</strong> {{ ucwords(str_replace('-',' ',$applicant->employer_appointment_type)) }} | {{ $applicant->employer_appointment_sched }} </p>
                                @endif
                                @if($applicant->endorsement_slip)
                                <p class="attachment"><strong>Edorsement Slip :</strong> <i class="fa fa-file"></i> {{ str_replace(url('/').'/endorsement-slips-media/','',$applicant->endorsement_slip) }} </p>
                                @endif
                                @if($applicant->hire_date && $applicant->hire_date !== '0000-00-00')
                                <p class="hire_date"><strong>Hire Date :</strong> {{ $applicant->hire_date }} </p>
                                @endif
                                @if($applicant->hire_account)
                                <p class="hire_account"><strong>Account :</strong> {{ $applicant->hire_account }} </p>
                                @endif
                                @if($applicant->reason)
                                <p class="reason"><strong>Reason :</strong> {{ $applicant->reason }} </p>
                                @endif
                                @if($applicant->note)
                                <p class="notes"><strong>Note :</strong> {{ $applicant->note }} </p>
                                @endif
                              </td>
                              <td class="employerFb" data-employerfb="{{ $applicant->employer_applicant_status_id }}">
                                <p><strong>Applicant Feedback Status :</strong>  {{ ($applicant->employer_applicant_status)? $applicant->employer_applicant_status->name:'No Status' }} </p>
                                @if($applicant->employerFeedback)
                                  @if($sched_final = $applicant->employerFeedback->schedule_final_interview)
                                  <p class="sched_finalinterview"><strong>Final Interview :</strong>  {{ $sched_final }} </p>
                                  @endif
                                  @if($applicant->employerFeedback->reason)
                                  <p class="employer_reason"><strong>Reason :</strong> {{ $applicant->employerFeedback->reason }}</p>
                                  @endif
                                  @if($applicant->employerFeedback->notes)
                                  <p class="employer_notes"><strong>Notes :</strong> {{ $applicant->employerFeedback->notes }}</p>
                                  @endif
                                  @if($sched_valid = $applicant->employerFeedback->schedule_account_validation)
                                  <p class="sched_validation"><strong>Account Validation :</strong>  {{ $sched_valid }}</p>
                                  @endif
                                  @if($sched_offer = $applicant->employerFeedback->schedule_job_offer)
                                  <p class="sched_joboffer"><strong>Job Offer :</strong>  {{ $sched_offer }}</p>
                                  @endif
                                  @if($sched_training = $applicant->date_of_training)
                                  <p class="date_of_training"><strong>Date Training :</strong>  {{ $sched_training }}</p>
                                  @endif
                                  @if($hire_acc = $applicant->hire_account)
                                  <p class="hire_account"><strong>Hire Account :</strong>  {{ $hire_acc }}</p>
                                  @endif
                                  @if($hire_date = $applicant->hire_date)
                                  <p class="hire_date"><strong>Hire Date :</strong>  {{ $hire_date }}</p>
                                  @endif
                                @endif
                              </td>
                              <td class="text-center">
                                @if(auth()->user()->type->slug == 'employer' AND auth()->user()->employer->is_premium)
                                  <a href="{{ route('employer.careers.applicant.details',['job_code' => $applicant->job->job_code,'applicant' => $applicant->application_code]) }}" class="btn btn-sm btn-default"target="_blank"><i class="ico-profile"></i></a>
                                @else
                                  <button class="btn btn-sm btn-default" title="View Resume" data-toggle="modal" data-target=".resume" data-applicant="{{ $applicant->application_code }}"><i class="ico-profile"></i></button>
                                @endif
                              </td>
                          </tr>
                          @endforeach
                      </tbody>
                  </table>
              </div>
              <!--/ panel body with collapse capabale -->
              {{ $applicants->appends($_GET)->links() }}
              @else
                <div class="panel-footer">
                  <div class="alert alert-info">
                    <strong>No data</strong>
                  </div>
                </div>
              @endif
          </div>
      </div>
  </div>
  <!--/ END row -->
@if(auth()->user()->type->slug == 'employer' AND auth()->user()->employer->is_premium == 0)
  <!-- Modal -->
<div class="modal left fade resume" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Resume</h4>
      </div>
      <div class="modal-body"></div>
      <!--div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Save changes</button>
      </div-->
    </div>
  </div>
</div>
@endif
@stop

@section('styles')
  <link rel="stylesheet" href="{{ asset('hfv1/plugins/sweetalert/dist/sweetalert.css') }}">
@stop
@section('scripts')
  <script src="{{ asset('hfv1/plugins/sweetalert/dist/sweetalert.min.js') }}"></script>
  <script>
    var EmployerApplicant = {
        init : function(){
          this.viewResume();
          @if(auth()->user()->type->slug == 'employer' AND auth()->user()->employer->is_premium)
          this.changeStatus();
          @endif
          this.elemStageShow($('.stages').attr('data-status'),$('.stages'))
          this.elemStageShow($('.employerFb').attr('data-status'),$('.employerFb'))
        },
        elemEmpHide : function(td){
          td.find('.sched_finalinterview').hide();
          td.find('.sched_validation').hide();
          td.find('.employer_reason').hide();
          td.find('.employer_notes').hide();
          td.find('.sched_joboffer').hide();
          td.find('.date_of_training').hide();
          td.find('.hire_date').hide();
          td.find('.hire_account').hide();
        },
        elemEmpStageShow: function(stage,td){
          switch (stage) {
            case '1':
              EmployerApplicant.elemEmpHide(td)
              td.find('.employer_notes').show();
            break;
            case '2':
              EmployerApplicant.elemEmpHide(td)
              td.find('.sched_finalinterview').show();
              td.find('.employer_notes').show();
              break;
            case '3':
              EmployerApplicant.elemEmpHide(td)
              td.find('.employer_reason').show();
              break;
            case '4':
              EmployerApplicant.elemEmpHide(td)
              td.find('.sched_validation').show();
              td.find('.employer_notes').show();
              break;
            case '5':
              EmployerApplicant.elemEmpHide(td)
              td.find('.employer_reason').show();
              break;
            case '6':
              EmployerApplicant.elemEmpHide(td)
              td.find('.sched_joboffer').show();
              td.find('.employer_notes').show();
              break;
            case '7':
              EmployerApplicant.elemEmpHide(td)
              td.find('.employer_reason').show();
              break;
            case '8':
              EmployerApplicant.elemEmpHide(td)
              td.find('.date_of_training').show();
              td.find('.hire_date').show();
              td.find('.hire_account').show();
              td.find('.notes').show();
              break;
            default:
              EmployerApplicant.elemEmpHide(td)
            break;
          }
        },
        elemHide : function(td){
          td.find('.app_type').hide();
          td.find('.pick_datetime').hide();
          td.find('.attachment').hide();
          td.find('.reason').hide();
          td.find('.refer_premium').hide();
          td.find('.refer_regular').hide();
          td.find('.notes').hide();
          td.find('.hire_date').hide();
          td.find('.hire_account').hide();
        },
        elemStageShow: function(stage,td){
          switch (stage) {
            case '2':
              EmployerApplicant.elemHide(td)
              td.find('.app_type').show();
              td.find('.pick_datetime').show();
              td.find('.attachment').show();
              break;
            case '3':
              EmployerApplicant.elemHide(td)
              td.find('.app_type').show();
              td.find('.pick_datetime').show();
              td.find('.attachment').show();
              td.find('.refer_premium').show();
              break;
            case '4':
              EmployerApplicant.elemHide(td)
              td.find('.reason').show();
              break;
            case '5':
              EmployerApplicant.elemHide(td)
              td.find('.refer_regular').show();
              td.find('.reason').show();
              break;
            case '6':
              EmployerApplicant.elemHide(td)
              td.find('.notes').show();
              break;
            case '7':
              EmployerApplicant.elemHide(td)
              td.find('.app_type').show();
              td.find('.pick_datetime').show();
              td.find('.attachment').show();
              td.find('.notes').show();
              break;
            case '8':
              EmployerApplicant.elemHide(td)
              td.find('.hire_date').show();
              td.find('.hire_account').show();
              td.find('.notes').show();
              break;
            default:
              EmployerApplicant.elemHide(td)
            break;
          }
        },
        viewResume : function(){
          $('.resume').on('show.bs.modal' ,function(e){

            var button = $(e.relatedTarget)
            var applicant = button.data('applicant')
            var modal = $(this)
            modal.find('.modal-body').children().remove()
            $.ajax({
              url : window.location.origin + '/employer/fetch-applicant-details/' + applicant,
              type : 'get',
              success : function(res){
                modal.find('.modal-body').append($(res))
                EmployerApplicant.changeStatus()
              },error : function(res){
                console.log(res);
              }
            })
            // e.preventDefault();
            // e.stopImmediatePropagation();
          });
        },
        changeStatus: function(){
          $('.status').change(function(e){
            var option = $(this);
            var status = option.val();
            var applicant_code = option.data('applicantcode');
            // console.log(status,applicant_code);
            $.ajax({
              url : window.location.origin + '/employer/update-applicant-status',
              type : 'post',
              data : {'applicant_code' : applicant_code,
                      'status' : status},
              success : function(res){
                if(res.success){
                  swal('Updated!','You have change the status','success');
                  $('[data-applicant="'+applicant_code+'"]')
                      .parents()
                      .closest('tr')
                      .find('td')
                      .eq(5)
                      .find('span')
                      .text(res.status)
                }
              },
              error : function(res){
                console.log(res);
                swal('Opps!','Something went wrong','error');
              }
            });
            e.stopImmediatePropagation();
            e.preventDefault();
          });
        }
    }
    $(function(){
      EmployerApplicant.init();
    });
  </script>
@stop
