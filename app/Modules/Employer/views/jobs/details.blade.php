@extends('Dashboard::template.main')

@section('content')
  <!-- Page Header -->
  <div class="page-header page-header-block">
      <div class="page-header-section">
          <h4 class="title semibold">Job Details</h4>
      </div>
      <div class="page-header-section">
          <!-- Toolbar -->
          <div class="toolbar clearfix">
              <a href="{{route('employer.careers')}}" class="btn btn-default btn-sm"><i class="ico-chevron-left"></i> Back to Listings</a>
              <a href="{{ route('employer.careers.pipeline',['job_code' => $job->job_code]) }}" class="btn btn-default btn-sm">Pipeline</a>
              <a href="{{ route('employer.job.edit',['job_code' => $job->job_code]) }}" class="btn btn-default btn-sm">Edit</a>
          </div>
          <!--/ Toolbar -->
      </div>
  </div>

  <div class="row">
    <div class="col-md-8">
      <div class="widget panel">
        <div class="panel-body">
            <div class="page-header" style="border:0;">
            <div class="col-md-12">
              @php
                $arr = array_except($job->toArray(),['id','created_at','status','updated_at','deleted_at','referral_reward','finders_fee','order_level','valid_until','job_type_id','job_industry_id','work_shift_id','education_level_id','job_level_id','employer_id','work_shift']);
                $arr['job_type'] = $job->type->name;
                $arr['job_industry'] = $job->job_industry->name;
                $arr['work_shift'] = $job->shift->name;
                $arr['education_level'] = $job->education_level->name;
                $arr['job_level'] = $job->job_level->name;
                $arr['employer'] = array_only($job->employer->toArray(),['company_name','logo','slug','description','is_premium','is_verified']);
                $arr = json_encode($arr);
              @endphp
              <input type="hidden" id="dat" data-job="{{ $arr }}">
              <img class="img-bordered-success img-responsive" src="{{ $job->employer->logo }}" alt="" style="max-height:75px;" />
              <div class="page-header" style="border:0;">
                <h2 class="page-title"> {{$job->job_title }} <small>{{ $job->employer->company_name }}</small></h2>
                <p class="nm">
                @if($job->employer->is_premium)
                <span class="label label-success"><i class="fa fa-shield"></i> Premium Employer</span>
                @endif
                @if($job->employer->is_verified)
                <span class="label label-info"><i class="ico-check"></i> Verified</span>
                @endif
                </p>
                <hr>
                  @if(auth()->check() AND auth()->user()->user_type_id == '6')
                    <span class="label label-inverse">Referral Reward : {{ $job->referral_reward}}</span>
                  @endif
                  @if(auth()->check() AND auth()->user()->user_type_id == '5')
                  <span class="label label-inverse">Hiring Reward : {{ $job->applicant_reward}}</span>
                  @endif
                  @if(auth()->guest())
                    <div class="btn-group" role="group" aria-label="...">
                      <button type="button" class="btn btn-default">Hiring Reward - {{ $job->applicant_reward}}</button>
                      <button type="button" class="btn btn-default">Referral Reward - {{ $job->referral_reward}}</button>
                    </div>
                  @endif
                <p><strong><i class="ico-cogs"></i> {{ $job->job_industry->name }} </strong></p>
                <span><i class="ico-map-marker"></i>{{ $job->full_address }}</span><br>
              </div>
            </div>
          </div>
            <ul class="list-inline">
              <li><span class="label label-default">Work Type: {{ $job->type->name}}</span>
              </li>
              <li><span class="label label-default">Work Experience: {{ $job->job_level->name }}</span>
              </li>
              <li><span class="label label-default">Education Level: {{ $job->education_level->name }}</span>
              </li>
              @if($job->show_salary_range)
              <li><span class="label label-default">Salary Range: {{ $job->salary_from .' - '.$job->salary_to }}</span>
              </li>
              @endif
            </ul>
            <h4>About the company</h4>
            <div>{!! $job->employer->description !!}</div>
            <h4>Job Description</h4>
            <div class="desc">
              {!! $job->description !!}
            </div>
        </div><!-- body -->
      </div>
    </div>
  </div>
@stop
