@extends('Dashboard::template.main')

@section('content')
  <!-- Page Header -->
  <div class="page-header page-header-block">
      <div class="page-header-section">
          <h4 class="title semibold">Create Job Post</h4>
      </div>
      <div class="page-header-section">
          <!-- Toolbar -->
          <div class="toolbar clearfix text-right">
              <a href="{{route('employer.careers')}}" class="btn btn-sm btn-default"  data-toggle="tooltip" data-placement="bottom" title="Back to careers"><i class="ico-caret-left"></i></a>
          </div>
          <!--/ Toolbar -->
      </div>
  </div>
    <!-- START row -->
  <div class="row">
      <!-- Left / Top Side -->
      <div class="col-lg-12">
        <!-- form profile -->
        {!! \Form::open(['class' => 'panel form-horizontal form-bordered','name' => 'form-profile','data-parsley-validate'=> '' ,'files' => true]) !!}
            @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif

            <div class="panel-body pt0 pb0">
                <div class="form-group header bgcolor-default">
                    <div class="col-md-12">
                        <h4 class="semibold text-primary mt0 mb5">Job Post</h4>
                        <p class="text-default nm">This information appears on your public profile, search results, and beyond.</p>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label">Job Title <span class="text-danger">*</span></label>
                    <div class="col-sm-6">
                        <input type="text" class="form-control" name="job_title" value="{{old('job_title')}}" placeholder="Job Title ex. (Customer Service Representative)" required=""  data-parsley-error-message="Please add your job title" data-parsley-length="[3, 191]">
                        <p class="help-block">Enter your job title.</p>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label">Job Type <span class="text-danger">*</span></label>
                    <div class="col-sm-6">
                        <select name="job_type_id" class="form-control" required="" data-parsley-type="number">
                            <option value="">--- Types ---</option>
                            @foreach($types as $type)
                                <option value="{{$type->id}}" {{ (old('job_type_id') == $type->id)?'selected="selected"':'""' }}>{{$type->name}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label">Job Industry <span class="text-danger">*</span></label>
                    <div class="col-sm-6">
                        <select name="job_industry_id" class="selc" required="" data-parsley-type="number">
                            <option value="">--- Industries ---</option>
                            @foreach($industries as $industry)
                                <option value="{{$industry->id}}" {{ (old('job_industry_id') == $industry->id)?'selected="selected"':'""' }}>{{$industry->name}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label">Job Level <span class="text-danger">*</span></label>
                    <div class="col-sm-6">
                        <select name="job_level_id" class="form-control" required="" data-parsley-type="number">
                            <option value="">--- Industries ---</option>
                            @foreach($job_levels as $jlev)
                                <option value="{{$jlev->id}}" {{ (old('job_level_id') == $jlev->id)?'selected="selected"':'""' }}>{{$jlev->name}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label">Education Level <span class="text-danger">*</span></label>
                    <div class="col-sm-6">
                        <select name="education_level_id" class="form-control" required="" data-parsley-type="number">
                            <option value="">--- Education Level ---</option>
                            @foreach($edulevels as $jlevel)
                                <option value="{{$jlevel->id}}" {{ (old('education_level_id') == $jlevel->id)?'selected="selected"':'""' }}>{{$jlevel->name}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <?php /*
                <div class="form-group">
                    <label class="col-sm-3 control-label">Job Designation / Account/ Department <span class="text-danger">*</span></label>
                    <div class="col-sm-6">
                        <input type="text" class="form-control" name="designation" value="{{old('designation')}}" placeholder="Dell Account" required="">
                    </div>
                </div>
                */
               ?>
                <div class="form-group">
                  <label class="col-sm-3 control-label">Work Location <span class="text-danger">*</span></label>
                  <div class="col-sm-6">
                      <input type="text" class="form-control" name="work_location" value="{{old('work_location') }}"
                      placeholder="Street Address" required="">
                  </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label">Region<span class="text-danger">*</span></label>
                    <div class="col-sm-6">
                      <select name="location_region" class="select2">
                        <option value="">--  Select Region--</option>
                      @foreach($location_region as $region)
                        <option value="{{ $region->regCode }}">{{ $region->regDesc }}</option>
                      @endforeach
                      </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label">Province / City <span class="text-danger">*</span></label>
                    <div class="col-sm-3">
                      <select name="location_province" class="select2"  disabled="">
                      </select>
                    </div>
                    <div class="col-sm-3">
                      <select name="location_city" class="select2"  disabled="">
                      </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label">Skills <span class="text-danger">*</span></label>
                    <div class="col-sm-6">
                        <input type="text" class="form-control skills" name="skills" value="{{old('skills')}}" placeholder="ex. Web Design, Trainable, Leadership skills" required="">
                        <p class="help-block"></p>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label">Salary</label>
                    <div class="col-sm-2">
                        <input type="text" class="form-control moneymask" name="salary_from" value="{{old('salary_from')}}" placeholder="Salary range from">
                    </div>
                    <div class="col-sm-2">
                        <input class="form-control moneymask" name="salary_to"  value="{{old('salary_to')}}"  placeholder="Salary range to">
                    </div>
                    <div class="col-sm-2">
                      <div class="checkbox custom-checkbox">
                          <input type="checkbox" name="show_salary_range" id="checkboxSalaryRange" value="1">
                          <label for="checkboxSalaryRange">&nbsp;&nbsp;Show Salary Range</label>
                      </div>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label">Work Shift <span class="text-danger">*</span></label>
                    <div class="col-sm-6">
                        <select name="work_shift_id" class="selc" required="" data-parsley-type="number">
                            <option value="">-- Select Work Shift --</option>
                            @foreach($shifts as $shift)
                                <option value="{{$shift->id}}" {{ (old('work_shift_id') == $shift->id)?'selected="selected"':'""' }}>{{$shift->name}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-3 control-label">Job Description and Responsibility <span class="text-danger">*</span></label>
                    <div class="col-sm-9">
                        <textarea class="form-control summernote" rows="3" name="description" placeholder="Describe about yourself" required="" data-parsley-error-message="Please tell us about the Company">@if(strlen(old('description'))>0) {{old('description')}} @else @include('Employer::jobs.jobdesc-template') @endif</textarea>
                        <p class="help-block">About your company in 500 characters or less.</p>
                    </div>
                </div>

                @if(auth()->user()->type->slug == 'employer' AND auth()->user()->employer->is_premium)
                <div class="form-group header bgcolor-default">
                    <div class="col-md-12">
                        <h4 class="semibold text-primary nm">For Human Factor and Partners Section</h4>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-3 control-label">Search Fee <span class="text-danger">*</span></label>
                    <div class="col-sm-6">
                        <input type="text" class="form-control moneymask" name="finders_fee" value="{{old('finders_fee')}}" placeholder="ex. (Php 17,000)" required="">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label">Provide Relocation <span class="text-danger">*</span></label>
                    <div class="col-sm-6">
                      <select name="provide_relocation" class="form-control" id="" required="">
                          <option value="">--- Select Here... ---</option>
                          @foreach([0=>'No',1=>'Yes'] as $ch => $cch)
                          <option value="1" {{ (old('provide_relocation') == $ch)?'selected="selected"':'""' }}> {{$cch}} </option>
                          @endforeach
                      </select>
                    </div>
                </div>

                @endif
            </div>
            <div class="panel-footer text-right">
                <button type="submit" class="btn btn-primary"><i class="ico-save"></i> Save</button>
            </div>
        </form>
        <!--/ form profile -->
      </div>
  </div>
@stop

@section('styles')
    <link href="{{asset('hfv1/plugins/bootstrap-fileinput/css/fileinput.min.css')}}" media="all" rel="stylesheet" type="text/css" />
    <link href="{{asset('hfv1/plugins/summernote/dist/summernote.css')}}" media="all" rel="stylesheet" type="text/css" />
    <link href="{{asset('hfv1/plugins/selectize/css/selectize.css')}}" media="all" rel="stylesheet" type="text/css" />

@stop

@section('scripts')
    <script src="{{asset('hfv1/plugins/summernote/dist/summernote.js')}}"></script>
    <script src="{{asset('hfv1/plugins/summernote/dist/summernote-cleaner.js')}}"></script>
    <script src="{{asset('hfv1/plugins/bootstrap-fileinput/js/plugins/purify.min.js')}}"></script>
    <script src="{{asset('hfv1/plugins/bootstrap-fileinput/js/fileinput.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('hfv1/plugins/jquery-inputmask/dist/jquery.inputmask.bundle.js')}}"></script>
    <script type="text/javascript" src="{{asset('hfv1/plugins/selectize/js/selectize.js')}}"></script>
    <script>
        $('.selc').selectize({
            sortField: 'text'
        });
        $('.select2').select2({
          allowClear : true
        });
        $('[name="location_region"]').change(function(e){
          var regcode = $(this).val();
          console.log(regcode);
          $.ajax({
            url : window.location.origin + '/employer/fetch-province/' + regcode,
            type : 'get',
            success : function(res){
              if(res.success){
                var x = '';
                $('[name="location_province" ]').children().remove();
                $('[name="location_city" ]').children().remove();
                $('[name="location_city" ]').prop('disabled','disabled');
                $('[name="location_province" ]').select2('val',null);
                $('[name="location_city" ]').select2('val',null);
                $.each(res.provinces,function(k,v){
                  x  +='<option value="'+v.provCode+'">'+v.provDesc+'</option>';
                });
                $('[name="location_province" ]').append($(x));
                $('[name="location_province" ]').removeAttr('disabled');
              }
            },error : function(res){

            }
          });
          e.stopImmediatePropagation();
          e.preventDefault();
        });

        $('[name="location_province" ]').change(function(e){
          var provcode = $(this).val();
          $.ajax({
            url : window.location.origin + '/employer/fetch-cities/' + provcode,
            type : 'get',
            success : function(res){
              if(res.success){
                var x = '';
                $('[name="location_city" ]').children().remove();
                $('[name="location_city" ]').select2('val',null);
                $.each(res.cities,function(k,v){
                  x  +='<option value="'+v.citymunCode+'">'+v.citymunDesc+'</option>';
                });
                $('[name="location_city" ]').append($(x));
                $('[name="location_city" ]').removeAttr('disabled');
              }
            },error : function(res){

            }
          });
          e.preventDefault();
          e.stopImmediatePropagation();
        });

        $('.moneymask').inputmask({
            'alias' : 'numeric',
            'groupSeparator' : ',',
            'autoGroup' : true,
            'digits' : 2,
            'digitsOptional' : false,
            'prefix' : 'Php ',
            'placeholder' : '0'
        });

        $('.numbermask').inputmask({
            'alias' : 'integer',
            'digitsOptional' : false,
            'placeholder' : '0'
        });

        var skills = $('.skills').selectize({
                        valueField: 'id',
                        labelField: 'name',
                        searchField: 'name',
                        create: true,
                        options: [],
                        maxItems: 20
                    });
        @if(strlen(old('skills'))>0)
        skills[0].selectize.setValue('{{old('skills')}}')
        @endif
        $('.summernote').summernote({
            height: 450,
            toolbar:[
                    ['style',['style']],
                    ['font',['bold','italic','underline','clear']],
                    ['fontname',['fontname']],
                    ['color',['color']],
                    ['para',['ul','ol','paragraph']],
                    ['height',['height']],
                    ['view',['fullscreen']],
                    ['help',['help']]
            ],
            disableDragAndDrop: true,
            cleaner:{
                       notTime:2400, // Time to display Notifications.
                       action:'both', // both|button|paste 'button' only cleans via toolbar button, 'paste' only clean when pasting content, both does both options.
                       newline:'<br>', // Summernote's default is to use '<p><br></p>'
                       notStyle:'position:absolute;top:0;left:0;right:0', // Position of Notification
                       keepHtml: false, //Remove all Html formats
                       keepClasses: false, //Remove Classes
                       badTags: ['style','script','applet','embed','noframes','noscript', 'html'], //Remove full tags with contents
                       badAttributes: ['style','start'] //Remove attributes from remaining tags
            }
        })
    </script>
@stop
