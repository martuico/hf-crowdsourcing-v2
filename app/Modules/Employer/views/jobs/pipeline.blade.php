@extends('Dashboard::template.main')

@section('content')
  <!-- Page Header -->
  <div class="page-header page-header-block">
      <div class="page-header-section">
          <h4 class="title semibold">Job Detail - {{ $job->job_code }}</h4>
      </div>
      <div class="page-header-section">
          <!-- Toolbar -->
          <div class="toolbar clearfix text-right">
              <a href="{{route('employer.careers')}}" class="btn btn-sm btn-default"  data-toggle="tooltip" data-placement="bottom" title="Back to careers"><i class="fa fa-chevron-left"></i> Back to Listings</a>
              <a href="{{ route('employer.careers.details',['job_code' => $job->job_code]) }}" class="btn btn-default btn-sm"><i class="fa fa-list"></i> Details</a>
          </div>
          <!--/ Toolbar -->
      </div>
  </div>
  <div class="row">
    <div class="col-md-12">
      <div class="widget panel">
          <!-- panel body -->
          <div class="panel-body">
              <ul class="list-table">
                  <li class="text-left">
                      <h5 class="semibold ellipsis">
                          {{ $job->job_title }}<br>
                          <small class="text-muted">Created {{ $job->created_at}} - Valid Until: {{ $job->valid_until }}</small>
                      </h5>
                  </li>
                  <li class="text-right"><button type="button" class="btn btn-info">{{ strtoupper($job->status) }}</button></li>
              </ul>
              <!-- Nav section -->
              <ul class="nav nav-section nav-justified mt15">
                  <li>
                      <div class="section">
                        @if(auth()->user()->employer->is_premium)
                          <h4 class="nm">{{ $job->applicant_premium_count }}</h4>
                        @else
                          <h4 class="nm">{{ $job->applicant_count }}</h4>
                        @endif
                          <p class="nm text-muted">Applicants</p>
                      </div>
                  </li>
                  <li>
                      <div class="section">
                          <h4 class="nm">{{ $job->forinterview_count }}</h4>
                          <p class="nm text-muted">For Interview</p>
                      </div>
                  </li>
                  <li>
                      <div class="section">
                          <h4 class="nm">{{ $job->hired_count }}</h4>
                          <p class="nm text-muted">Hired</p>
                      </div>
                  </li>
              </ul>
              <!--/ Nav section -->
          </div>
          <!--/ panel body -->
      </div>
    </div><!-- col-md-12 -->
  </div><!-- row -->

  <div class="row">
    <div class="col-md-12">
      <div class="panel panel-default">
        <!-- panel heading/header -->
        <div class="panel-heading">
            <h3 class="panel-title">Applicants</h3>
            <!-- panel toolbar -->
            <div class="panel-toolbar text-right">
                <!-- option -->
                <div class="option">
                    <button class="btn up" data-toggle="panelcollapse"><i class="arrow"></i></button>
                    <button class="btn" data-toggle="panelremove" data-parent=".col-md-12"><i class="remove"></i></button>
                </div>
                <!--/ option -->
            </div>
            <!--/ panel toolbar -->
        </div>
        <!--/ panel heading/header -->
        <!-- panel body with collapse capabale -->
        <div class="table-responsive panel-collapse pull out">
          @if(count($applicants)>0)
            <table class="table table-bordered table-condensed table-hover">
                <thead>
                    <tr>
                        <th>Name</th>
                        <th>Status</th>
                        <th>Appointment Type</th>
                        <th>Appointment Schedule</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($applicants as $applicant)
                    <tr>
                        <td>{{ ($applicant->applicant)?$applicant->applicant->user->my_name:'Err' }}</td>
                        <td><span class="label label-warning">{{ strtoupper($applicant->applicant_status->name) }}</span></td>
                        <td>{{ strtoupper($applicant->employer_appointment_type)?:'None'  }}</td>
                        <td>{{ $applicant->employer_appoiment_sched?:'None' }}</td>
                        <td>
                          <a href="{{ route('employer.careers.applicant.details',['job_code' => $job->job_code,'applicant' => $applicant->application_code])}}" class="btn btn-sm btn-default"><i class="ico-eye"></i></a>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
            {{ $applicants->links() }}
            @else
            <div class="panel-body">
              <div class="alert alert-info">
                <strong>No Data</strong>
              </div>
            </div>
            @endif
        </div>
        <!--/ panel body with collapse capabale -->
    </div>
    </div>
  </div>
@stop
