<p><b>What We Offer:</b></p>
<ul>
	<li>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</li>
	<li>Omnis quis ipsum enim libero.</li>
	<li>Laudantium cumque aperiam natus dolorem, tempora, porro maiores.</li>
</ul>

<p><b>Qualifications:</b></p>
<ul>
	<li>Graduate of any Bachelor’s Degree or Undergraduates</li>
	<li>Must Have Good English Communication Skills</li>
	<li>Willing to work on Holidays, Weekends, and Shifting Schedules</li>
</ul>
<p><b>Responsibilities:</b></p>
<ul>
	<li>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nulla iste ex rem.</li>
	<li>Similique vitae neque eos officia, hic voluptatibus accusantium, debitis tempore a ullam</li>
</ul>
<p><b>Please prepare the following documents when you apply:</b></p>
<ul>
<li>Updated Resume</li>
<li>2 valid IDs</li>
</ul>