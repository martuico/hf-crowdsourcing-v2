<li class="{{activeClass(\Route::currentRouteName(),'employer.home')}}">
    <a href="{{route('employer.home')}}">
        <span class="figure"><i class="ico-dashboard2"></i></span>
        <span class="text">Dashboard</span>
</li>
@if((auth()->user()->type->name == 'Employer' AND count(auth()->user()->employer)<1) == false)
<li class="{{activeClass(\Route::currentRouteName(),'employer.pipeline')}}">
    <a href="{{route('employer.pipeline')}}">
        <span class="figure"><i class="ico-filter22"></i></span>
        <span class="text">Applicant Pipleline</span>
    </a>
</li>
<?php
/*
<li class="{{activeClass(\Route::currentRouteName(),'employer.appointments')}}">
    <a href="{{route('employer.appointments')}}">
        <span class="figure"><i class="ico-calendar2"></i></span>
        <span class="text">My Appointments</span>
    </a>
</li>
*/
?>
<li class="{{activeClass(\Route::currentRouteName(),'employer.careers')}}">
    <a href="{{route('employer.careers')}}">
        <span class="figure"><i class="ico-archive"></i></span>
        <span class="text">Careers</span>
    </a>
</li>
<li class="{{activeClass(\Route::currentRouteName(),'employer.billings')}}">
    <a href="{{route('employer.billings')}}" data-toggle="submenu" data-target="#form" data-parent=".topmenu">
        <span class="figure"><i class="ico-coins"></i></span>
        <span class="text">Billing</span>
    </a>
</li>
@endif
<li class="{{activeClass(\Route::currentRouteName(),'employer.profile')}}">
    <a href="{{route('employer.profile')}}">
        <span class="figure"><i class="ico-library"></i></span>
        <span class="text">Company Profile</span>
</li>

@if(auth()->user()->type->slug == 'employer' AND auth()->user()->employer->is_premium == 1)
<li class="{{activeClass(\Route::currentRouteName(),'employer.inviteRecruiters')}}">
    <a href="{{route('employer.inviteRecruiters')}}" data-toggle="submenu" data-target="#form" data-parent=".topmenu">
        <span class="figure"><i class="ico-user-plus"></i></span>
        <span class="text">Recruiters Access</span>
    </a>
</li>
@endif
