@extends('Dashboard::template.main')

@section('content')
  <!-- Page Header -->
  <div class="page-header page-header-block">
      <div class="page-header-section">
          <h4 class="title semibold">My Appointments</h4>
      </div>
      <div class="page-header-section">
          <!-- Toolbar -->
          <div class="toolbar clearfix text-right">
              <div class="input-group">
                    <input type="text" class="form-control" placeholder="Search Applicants">
                    <span class="input-group-btn">
                      <button class="btn btn-default" type="button">Go!</button>
                    </span>
                  </div><!-- /input-group -->
          </div>
          <!--/ Toolbar -->
      </div>
  </div>
  <!-- Page Header -->

  <!-- START row -->
  <div class="row">
      <div class="col-md-12">
          @if(count($applicant_appointments)>0)
          <!-- START panel -->
          <div class="panel panel-primary">
              <!-- panel heading/header -->
              <div class="panel-heading">
                  <h3 class="panel-title"><span class="panel-icon mr5"><i class="ico-table22"></i></span> Applicants</h3>
                  <!-- panel toolbar -->
                  <div class="panel-toolbar text-right">
                  </div>
                  <!--/ panel toolbar -->
              </div>
              <!--/ panel heading/header -->

              <!-- panel body with collapse capabale -->
              <div class="table-responsive pull out">
                  <table class="table table-bordered table-hover" id="table1">
                      <thead>
                          <tr>
                              <th width="5%"></th>
                              <th>Name</th>
                              <th>Date Applied</th>
                              <th>Appointment</th>
                              <th>Designation</th>
                              <th>HF Feedback</th>
                              <th>Status Feedback</th>
                              <th></th>
                          </tr>
                      </thead>
                      <tbody>
                          @foreach($applicant_appointments as $appointment)
                          <tr>
                              <td><div class="media-object"><img src="{{ $appointment->applicant->user->profile_pic }}" alt="" class="img-circle"></div>
                              </td>
                              <td>{{$appointment->applicant->user->my_name }}</td>
                              <td>{{$appointment->job->created_at}}</td>
                              <td>{{$appointment->job->job_title}} <br>
                                <small class="text-muted">{{$appointment->job->designation}}</small>
                              </td>
                              <td>
                                TYPE: {{ strtoupper(str_replace('-','',$appointment->employer_appointment_type)) }}
                                <br>
                                DATETIME: {{ $appointment->employer_appointment_sched }}
                                @if($appointment->endorsement_slip)
                                <p class="attachment"><strong>Edorsement Slip :</strong> <i class="fa fa-file"></i> {{ str_replace(url('/').'/endorsement-slips-media/','',$appointment->endorsement_slip) }} </p>
                                @endif
                              </td>
                              <td>
                                @if($appointment->employer_appointment_type)
                                <p class="app_type"><strong>Appointments :</strong> {{ ucwords(str_replace('-',' ',$appointment->employer_appointment_type)) }} | {{ $appointment->employer_appointment_sched }} </p>
                                @endif

                              </td>
                              <td>
                                @if($appointment->employerFeedback)
                                  @if($sched_final = $appointment->employerFeedback->schedule_final_interview)
                                  <p class="sched_finalinterview"><strong>Final Interview :</strong>  {{ $sched_final }} </p>
                                  @endif
                                  @if($appointment->employerFeedback->reason)
                                  <p class="employer_reason"><strong>Reason :</strong> {{ $appointment->employerFeedback->reason }}</p>
                                  @endif
                                  @if($appointment->employerFeedback->notes)
                                  <p class="employer_notes"><strong>Notes :</strong> {{ $appointment->employerFeedback->notes }}</p>
                                  @endif
                                  @if($sched_valid = $appointment->employerFeedback->schedule_account_validation)
                                  <p class="sched_validation"><strong>Account Validation :</strong>  {{ $sched_valid }}</p>
                                  @endif
                                  @if($sched_offer = $appointment->employerFeedback->schedule_job_offer)
                                  <p class="sched_joboffer"><strong>Job Offer :</strong>  {{ $sched_offer }}</p>
                                  @endif
                                  @if($sched_training = $appointment->date_of_training)
                                  <p class="date_of_training"><strong>Date Training :</strong>  {{ $sched_training }}</p>
                                  @endif
                                  @if($hire_acc = $appointment->hire_account)
                                  <p class="hire_account"><strong>Hire Account :</strong>  {{ $hire_acc }}</p>
                                  @endif
                                  @if($hire_date = $appointment->hire_date)
                                  <p class="hire_date"><strong>Hire Date :</strong>  {{ $hire_date }}</p>
                                  @endif
                                @else
                                  <p>No Status</p>
                                @endif
                              </td>
                              <!--td>
                                 <select name="" class="form-control" id="">
                                   <option value="pending">Pending</option>
                                   <option value="confirmed">Confirmed</option>
                                   <option value="completed">Completed</option>
                                 </select>
                              </td-->
                              <td class="text-center">
                                  <a href="{{ route('employer.careers.applicant.details',['job_code' => $appointment->job->job_code,'applicant' => $appointment->application_code]) }}" class="btn btn-sm btn-default" data-toggle="tooltip" data-placement="bottom" title="View Resume"><i class="ico-profile"></i></a>
                              </td>
                          </tr>
                          @endforeach
                      </tbody>
                  </table>
              </div>
              <!--/ panel body with collapse capabale -->
          </div>
          @else
            <div class="alert alert-info">
              No appointments
            </div>
          @endif
      </div>
  </div>
  <!--/ END row -->
@stop
