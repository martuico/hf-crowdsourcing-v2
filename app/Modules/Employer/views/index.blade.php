@extends('Dashboard::template.main')
@section('styles')
  <style>
    tbody > tr > td:last-child {
      text-align:center;
    }
  </style>
@stop
@section('content')
  <!-- Page Header -->
  <div class="page-header page-header-block">
      <div class="page-header-section">
          <h4 class="title semibold">Employer Dashboard</h4>
      </div>
      <div class="page-header-section">
          <!-- Toolbar -->
          <div class="toolbar clearfix">
              @if(auth()->user()->type->name == 'Employer' AND count(auth()->user()->employer)<1)
              <div class="alert alert-warning text-left">
                <strong><i class="ico-warning-sign"></i> Create Company Profile First</strong>
              </div>
              @endif
              <!--div class="col-xs-8">
                  <select class="form-control text-left" id="selectize-customselect">
                      <option value="0">Display metrics...</option>
                      <option value="1">Last 6 month</option>
                      <option value="2">Last 3 month</option>
                      <option value="3">Last month</option>
                  </select>
              </div>
              <div class="col-xs-4">
                  <button class="btn btn-primary pull-right"><i class="ico-loop4 mr5"></i>Update</button>
              </div-->
          </div>
          <!--/ Toolbar -->
      </div>
  </div>
  <!-- Page Header -->

  <div class="row">
      <!-- START Left Side -->
      <div class="col-md-9">
          <!-- Top Stats -->
          <div class="row">
              <div class="col-sm-4">
                  <!-- START Statistic Widget -->
                  <div class="table-layout animation delay animating fadeInDown">
                      <div class="col-xs-4 panel bgcolor-info text-center">
                          <div class="ico-users3 fsize24"></div>
                      </div>
                      <div class="col-xs-8 panel">
                          <div class="panel-body text-center">
                              <h4 class="semibold nm">{{ $total_jobs }}</h4>
                              <p class="semibold text-muted mb0 mt5 ellipsis text-uppercase">POSTED JOBS</p>
                          </div>
                      </div>
                  </div>
                  <!--/ END Statistic Widget -->
              </div>
              <div class="col-sm-4">
                  <!-- START Statistic Widget -->
                  <div class="table-layout animation delay animating fadeInUp">
                      <div class="col-xs-4 panel bgcolor-teal text-center">
                          <div class="ico-crown fsize24"></div>
                      </div>
                      <div class="col-xs-8 panel">
                          <div class="panel-body text-center">
                              <h4 class="semibold nm">{{ $total_applicants }}</h4>
                              <p class="semibold text-muted mb0 mt5 ellipsis text-uppercase">Applicants</p>
                          </div>
                      </div>
                  </div>
                  <!--/ END Statistic Widget -->
              </div>
              <div class="col-sm-4">
                  <!-- START Statistic Widget -->
                  <div class="table-layout animation delay animating fadeInDown">
                      <div class="col-xs-4 panel bgcolor-primary text-center">
                          <div class="ico-box-add fsize24"></div>
                      </div>
                      <div class="col-xs-8 panel">
                          <div class="panel-body text-center">
                              <h4 class="semibold nm">{{ $total_hired }}</h4>
                              <p class="semibold text-muted mb0 mt5 ellipsis text-uppercase">Hired</p>
                          </div>
                      </div>
                  </div>
                  <!--/ END Statistic Widget -->
              </div>
          </div>
          <!--/ Top Stats -->
          <!-- Browser Breakpoint -->
          <div class="row">
              <div class="col-lg-12">
                  <!-- START panel -->
                  <div class="panel panel-default">
                      <!-- panel heading/header -->
                      <div class="panel-heading">
                          <h3 class="panel-title ellipsis"><i class="ico-blog2 mr5"></i>Latest Jobs Activities</h3>
                          <!-- panel toolbar -->
                          <div class="panel-toolbar text-right">
                          </div>
                          <!--/ panel toolbar -->
                      </div>
                      <!--/ panel heading/header -->
                      @if(count($jobs)>0)
                      <!-- panel body with collapse capabale -->
                      <div class="table-responsive panel-collapse pull out">
                          <table class="table">
                              <thead>
                                  <tr>
                                      <th>Job Title</th>
                                      <th width="20%">Job Location</th>
                                      <th>Posted Date</th>
                                      <th>Due Date</th>
                                      <th>Status</th>
                                      <th class="text-center">Total Applicants</th>
                                  </tr>
                              </thead>
                              <tbody>
                                @foreach($jobs as $job)
                                  <tr>
                                      <td><a href="" class="semibold text-accent"> {{ $job->job_title }} </a></td>
                                      <td><small>{{ $job->full_address }}</small></td>
                                      <td>{{ $job->created_at }}</td>
                                      <td>{{ $job->valid_until }}</td>
                                      <td><span class="label label-success">{{ ucwords($job->status) }}</span></td>
                                      <td><a href="" class="semibold text-accent">{{ $job->countEndorsedApplicants->count() }}</a></td>
                                  </tr>
                                @endforeach
                              </tbody>
                          </table>
                      </div>
                      <!--/ panel body with collapse capabale -->
                      @else
                      <div class="panel-body">
                        <div class="alert alert-info">
                          <strong>No Data!</strong>
                        </div>
                      </div>
                      @endif
                  </div>
                  <!--/ END panel -->
              </div>
          </div>
          <!-- Browser Breakpoint -->
          <!-- Browser Breakpoint -->
          <div class="row">
              <div class="col-lg-12">
                  <!-- START panel -->
                  <div class="panel panel-default">
                      <!-- panel heading/header -->
                      <div class="panel-heading">
                          <h3 class="panel-title ellipsis"><i class="ico-reading mr5"></i>Latest Applicants</h3>
                          <!-- panel toolbar -->
                          <div class="panel-toolbar text-right">
                          </div>
                          <!--/ panel toolbar -->
                      </div>
                      <!--/ panel heading/header -->
                      <!-- panel body with collapse capabale -->
                      @if(count($applicants)>0)
                      <div class="table-responsive panel-collapse pull out">
                          <table class="table">
                              <thead>
                                  <tr>
                                      <th></th>
                                      <th>Name</th>
                                      <th>Date Applied</th>
                                      <th>Job Applied For</th>
                                      <th>Designation</th>
                                      <th>Application Status</th>
                                  </tr>
                              </thead>
                              <tbody>
                                  @foreach($applicants as $applicant)
                                  <tr>
                                      <td>
                                        <div class="media-object">
                                          <img src="{{ $applicant->applicant->user->profile_pic }}" alt="" class="img-circle">
                                        </div>
                                      </td>
                                      <td>{{ $applicant->applicant->user->my_name }}</td>
                                      <td>{{ $applicant->created_at }}</td>
                                      <td><a href="{{ route('employer.careers.details',['code' => $applicant->job->job_code]) }}" class="semibold text-accent">{{ $applicant->job->job_title }}</a></td>
                                      <td>{{ $applicant->job->designation }}</td>
                                      <td><span class="label label-success">{{ $applicant->applicant_status->name }}</span></td>
                                  </tr>
                                  @endforeach
                              </tbody>
                          </table>
                      </div>
                      @else
                      <div class="panel-body">
                        <div class="alert alert-info">
                          <strong>No Data!</strong>
                        </div>
                      </div>
                      @endif
                      <!--/ panel body with collapse capabale -->
                  </div>
                  <!--/ END panel -->
              </div>
          </div>
          <!-- Browser Breakpoint -->
      </div><!-- col-md-9 -->
      <!-- START Right Side -->
      <div class="col-md-3">
          <div class="panel panel-minimal">
              <div class="panel-heading"><h5 class="panel-title"><i class="ico-health mr5"></i>Application Activities</h5></div>
              @if(count($activities)>0)
              <!-- Media list feed -->
              <ul class="media-list media-list-feed nm">
                @foreach($activities as $activity)
                  <li class="media">
                      <div class="media-object pull-left">
                          {!! $activity->icon !!}
                      </div>
                      <div class="media-body">
                          <p class="media-heading">{{ $activity->title }}</p>
                          <p class="media-text"><span class="text-primary semibold">{{ $activity->description }}</span> by {{$activity->user->my_name}}</p>
                          <p class="media-meta">{{ $activity->created_at }}</p>
                      </div>
                  </li>
                @endforeach
              </ul>
              <!--/ Media list feed -->
              @endif
          </div>
      </div>
      <!--/ END Right Side -->
  </div><!-- row -->
@stop
