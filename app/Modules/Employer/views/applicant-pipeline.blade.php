@extends('Dashboard::template.main')

@section('content')
  <!-- Page Header -->
  <div class="page-header page-header-block">
      <div class="page-header-section">
          <h4 class="title semibold">Applicant Pipeline - 50 Latest applicants from your job post</h4>
      </div>
      <div class="page-header-section">
          <!-- Toolbar -->
          <div class="toolbar clearfix text-right">
              <div class="input-group">
                    <input type="text" class="form-control" placeholder="Search Job Title">
                    <span class="input-group-btn">
                      <button class="btn btn-default" type="button">Go!</button>
                    </span>
                  </div><!-- /input-group -->
          </div>
          <!--/ Toolbar -->
      </div>
  </div>
  <!-- Page Header -->
  <div class="row">
     <!-- START Left Side -->
    <div class="col-md-12">
      <!-- START row -->
      <div class="row" id="applicants-panel">
          <div class="col-md-3 sortable">
              <div class="panel-heading bg-success">
                <div class="panel-title">
                  Endorsed
                </div>
              </div>
              <div class="panel"></div>
              <!-- START panel -->
              <div class="panel widget">
                  <div class="table-layout nm">
                      <div class="col-xs-4 text-center"><img src="{{asset('hfv1/image/avatar/avatar.png')}}" width="100%"></div>
                      <div class="col-xs-8 valign-middle">
                          <div class="panel-body">
                              <h5 class="semibold mt0 mb5"><a href="javascript:void(0);">Violet Kidd</a></h5>
                              <p class="ellipsis text-muted mb5"><i class="ico-envelop mr5"></i> libero.Integer@turpis.org</p>
                              <p class="text-muted nm"><i class="ico-location2 mr5"></i> Bahamas</p>
                          </div>
                      </div>
                  </div>
              </div>
              <!--/ END panel -->

              <!-- START panel -->
              <div class="panel widget">
                  <div class="table-layout nm">
                      <div class="col-xs-4 text-center"><img src="{{asset('hfv1/image/avatar/avatar.png')}}" width="100%"></div>
                      <div class="col-xs-8 valign-middle">
                          <div class="panel-body">
                              <h5 class="semibold mt0 mb5"><a href="javascript:void(0);">Violet Kidd</a></h5>
                              <p class="ellipsis text-muted mb5"><i class="ico-envelop mr5"></i> libero.Integer@turpis.org</p>
                              <p class="text-muted nm"><i class="ico-location2 mr5"></i> Bahamas</p>
                          </div>
                      </div>
                  </div>
              </div>
              <!--/ END panel -->
          </div>
          <div class="col-md-3 sortable">
              <div class="panel-heading bg-danger">
                <div class="panel-title">
                  Rejected
                </div>
              </div>
              <div class="panel"></div>
               <!-- START panel -->
              <div class="panel widget">
                  <div class="table-layout nm">
                      <div class="col-xs-4 text-center"><img src="{{asset('hfv1/image/avatar/avatar.png')}}" width="100%"></div>
                      <div class="col-xs-8 valign-middle">
                          <div class="panel-body">
                              <h5 class="semibold mt0 mb5"><a href="javascript:void(0);">Violet Kidd</a></h5>
                              <p class="ellipsis text-muted mb5"><i class="ico-envelop mr5"></i> libero.Integer@turpis.org</p>
                              <p class="text-muted nm"><i class="ico-location2 mr5"></i> Bahamas</p>
                          </div>
                      </div>
                  </div>
              </div>
              <!--/ END panel -->
               <!-- START panel -->
              <div class="panel widget">
                  <div class="table-layout nm">
                      <div class="col-xs-4 text-center"><img src="{{asset('hfv1/image/avatar/avatar.png')}}" width="100%"></div>
                      <div class="col-xs-8 valign-middle">
                          <div class="panel-body">
                              <h5 class="semibold mt0 mb5"><a href="javascript:void(0);">Violet Kidd</a></h5>
                              <p class="ellipsis text-muted mb5"><i class="ico-envelop mr5"></i> libero.Integer@turpis.org</p>
                              <p class="text-muted nm"><i class="ico-location2 mr5"></i> Bahamas</p>
                          </div>
                      </div>
                  </div>
              </div>
              <!--/ END panel -->
          </div>
          <div class="col-md-3 sortable">
              <div class="panel-heading bg-primary">
                <div class="panel-title">
                  Hired
                </div>
              </div>
              <div class="panel"></div>
               <!-- START panel -->
              <div class="panel widget">
                  <div class="table-layout nm">
                      <div class="col-xs-4 text-center"><img src="{{asset('hfv1/image/avatar/avatar.png')}}" width="100%"></div>
                      <div class="col-xs-8 valign-middle">
                          <div class="panel-body">
                              <h5 class="semibold mt0 mb5"><a href="javascript:void(0);">Violet Kidd</a></h5>
                              <p class="ellipsis text-muted mb5"><i class="ico-envelop mr5"></i> libero.Integer@turpis.org</p>
                              <p class="text-muted nm"><i class="ico-location2 mr5"></i> Bahamas</p>
                          </div>
                      </div>
                  </div>
              </div>
              <!--/ END panel -->
               <!-- START panel -->
              <div class="panel widget">
                  <div class="table-layout nm">
                      <div class="col-xs-4 text-center"><img src="{{asset('hfv1/image/avatar/avatar.png')}}" width="100%"></div>
                      <div class="col-xs-8 valign-middle">
                          <div class="panel-body">
                              <h5 class="semibold mt0 mb5"><a href="javascript:void(0);">Violet Kidd</a></h5>
                              <p class="ellipsis text-muted mb5"><i class="ico-envelop mr5"></i> libero.Integer@turpis.org</p>
                              <p class="text-muted nm"><i class="ico-location2 mr5"></i> Bahamas</p>
                          </div>
                      </div>
                  </div>
              </div>
              <!--/ END panel -->
          </div>
          <div class="col-md-3 sortable">
              <div class="panel-heading bg-inverse">
                <div class="panel-title">
                  No Show Final Interview
                </div>
              </div>
              <div class="panel"></div>
               <!-- START panel -->
              <div class="panel widget">
                  <div class="table-layout nm">
                      <div class="col-xs-4 text-center"><img src="{{asset('hfv1/image/avatar/avatar.png')}}" width="100%"></div>
                      <div class="col-xs-8 valign-middle">
                          <div class="panel-body">
                              <h5 class="semibold mt0 mb5"><a href="javascript:void(0);">Violet Kidd</a></h5>
                              <p class="ellipsis text-muted mb5"><i class="ico-envelop mr5"></i> libero.Integer@turpis.org</p>
                              <p class="text-muted nm"><i class="ico-location2 mr5"></i> Bahamas</p>
                          </div>
                      </div>
                  </div>
              </div>
              <!--/ END panel -->
               <!-- START panel -->
              <div class="panel widget">
                  <div class="table-layout nm">
                      <div class="col-xs-4 text-center"><img src="{{asset('hfv1/image/avatar/avatar.png')}}" width="100%"></div>
                      <div class="col-xs-8 valign-middle">
                          <div class="panel-body">
                              <h5 class="semibold mt0 mb5"><a href="javascript:void(0);">Violet Kidd</a></h5>
                              <p class="ellipsis text-muted mb5"><i class="ico-envelop mr5"></i> libero.Integer@turpis.org</p>
                              <p class="text-muted nm"><i class="ico-location2 mr5"></i> Bahamas</p>
                          </div>
                      </div>
                  </div>
              </div>
              <!--/ END panel -->
          </div>
      </div>
      <!--/ END row -->
    </div>
  </div>
@stop

@push('sidebar')
   @include('Employer::sidebar')
@endpush
@section('styles')
<link rel="stylesheet" href="{{asset('hfv1/plugins/jquery-ui/css/jquery-ui.css')}}">
<style>
  #applicants-panel > .sortable > .panel {
    margin: 0px;
  }
  #applicants-panel > .sortable >.panel {
    border-left: 2px #ccc dashed;
    border-right: 2px #ccc dashed;
    border-bottom: 2px #ccc dashed;
    /*padding: 0px;
    margin:0px 5px;*/
  }
  .bg-success {
    background-color: #60af3f;
    color: #fff;
  }
  .bg-danger {
    background-color: #a53e3e;
    color: #fff;
  }
  .bg-inverse {
    background-color: #383838;
    color: #fff;
  }
</style>
@stop
@section('scripts')
  <script type="text/javascript" src="{{asset('hfv1/plugins/jquery-ui/js/jquery-ui.js')}}"></script>
  <script type="text/javascript" src="{{asset('hfv1/javascript/backend/components/sortable.js')}}"></script>
  <script>
    $(function(){
      // Draggable portlets are rendered using jQuery Sortable plugin
      if (!jQuery().sortable) {
          return;
      }

      // $(".sortable .sortable-column").sortable({
      //     connectWith: ".sortable .col-md-3 .sortable-column",
      //     handle: ".list-group-item-heading",
      //     cancel: ".portlet-close",
      //     placeholder: "sortable-box-placeholder round-all",
      //     forcePlaceholderSize: true,
      //     tolerance: 'pointer',
      //     forceHelperSize: true,
      //     revert: true,
      //     helper: 'original',
      //     opacity: 0.8,
      //     iframeFix: false
      // });

      // Sortable grid
        // ================================
        $('#applicants-panel').sortable({
            connectWith: '.sortable',
            items: '.panel',
            opacity: 0.8,
            coneHelperSize: true,
            placeholder: 'ui-sortable-placeholder',
            forcePlaceholderSize: true,
            tolerance: 'pointer',
            helper: 'clone',
            cancel: '.panel-sortable-empty',
            revert: true,
            iframeFix: false,
            update: function(b, c) {
                if (c.item.prev().hasClass('panel-sortable-empty')) {
                    c.item.prev().before(c.item);
                }
            }
        });
    });
  </script>
@stop