@extends('Dashboard::template.main')

@section('content')
  <!-- Page Header -->
  <div class="page-header page-header-block">
      <div class="page-header-section">
          <h4 class="title semibold">Applicant Pipeline</h4>
      </div>
      <div class="page-header-section">
          <!-- Toolbar -->
          <div class="toolbar clearfix text-right">
              <div class="input-group">
                    <input type="text" class="form-control" placeholder="Search Job Title">
                    <span class="input-group-btn">
                      <button class="btn btn-default" type="button">Go!</button>
                    </span>
                  </div><!-- /input-group -->
          </div>
          <!--/ Toolbar -->
      </div>
  </div>
  <!-- Page Header -->
  <!-- START row -->
  <div class="row">
      <div class="col-md-12">
          <!-- START panel -->
          <div class="panel panel-primary">
              <!-- panel heading/header -->
              <div class="panel-heading">
                  <h3 class="panel-title"><span class="panel-icon mr5"><i class="ico-table22"></i></span> Applicants</h3>
                  <!-- panel toolbar -->
                  <div class="panel-toolbar text-right">
                      <!-- option -->
                      <div class="option">
                          <button class="btn up" data-toggle="panelcollapse"><i class="reload"></i></button>
                      </div>
                      <!--/ option -->
                  </div>
                  <!--/ panel toolbar -->
              </div>
              <!--/ panel heading/header -->
              <!-- panel toolbar wrapper -->
              <div class="panel-toolbar-wrapper pl0 pt5 pb5">
                  <div class="panel-toolbar pl10">
                      <div class="col-md-4">
                          <form class="form-inline">
                            <div class="form-group">
                              <label for="exampleInputName2">Show: </label>
                              <select name="" class="form-control" id="">
                                     <option value="">30</option>
                                     <option value="">50</option>
                                     <option value="">100</option>
                              </select>
                            </div>
                          </form>
                      </div>
                  </div>
                  <div class="panel-toolbar text-right">

                      <button type="button" class="btn btn-sm btn-default">Endorsed</button>
                      <button type="button" class="btn btn-sm btn-default">Rejected</button>
                      <button type="button" class="btn btn-sm btn-default">No Show</button>
                      <button type="button" class="btn btn-sm btn-default">Hired</button>
                  </div>
              </div>
              <!--/ panel toolbar wrapper -->

              <!-- panel body with collapse capabale -->
              <div class="table-responsive pull out">
                  <table class="table table-bordered table-hover" id="table1">
                      <thead>
                          <tr>
                              <th width="5%"></th>
                              <th>Name</th>
                              <th>Date Applied</th>
                              <th>Applied For</th>
                              <th>Designation</th>
                              <th width="13%">Status</th>
                              <th></th>
                          </tr>
                      </thead>
                      <tbody>
                          <tr>
                              <td><div class="media-object"><img src="{{asset('hfv1/image/avatar/avatar.png')}}" alt="" class="img-circle"></div>
                              </td>
                              <td>Short, Cheyenne E.</td>
                              <td>Bangladesh</td>
                              <td>quis.diam@gravida.net</td>
                              <td>Tellus Non Corp.</td>
                              <td>
                                 <select name="" class="form-control" id="">
                                   <option value="">Endorsed</option>
                                   <option value="">Rejected</option>
                                   <option value="">No Show</option>
                                   <option value="">Hired</option>
                                 </select>
                              </td>
                              <td class="text-center">
                                  <button class="btn btn-sm btn-default" data-toggle="tooltip" data-placement="bottom" title="View Resume"><i class="ico-profile"></i></button>
                                  <button class="btn btn-sm btn-default" data-toggle="tooltip" data-placement="bottom" title="View Appointments"><i class="ico-clock"></i></button>
                              </td>
                          </tr>
                      </tbody>
                  </table>
              </div>
              <!--/ panel body with collapse capabale -->
          </div>
      </div>
  </div>
  <!--/ END row -->
@stop