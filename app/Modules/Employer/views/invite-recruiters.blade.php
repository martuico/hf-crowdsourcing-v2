@extends('Dashboard::template.main')

@section('content')
  <!-- Page Header -->
  <div class="page-header page-header-block">
      <div class="page-header-section">
          <h4 class="title semibold">Recruiters</h4>
      </div>
      <div class="page-header-section">
          <!-- Toolbar -->
          <div class="toolbar clearfix text-right">
              <button class="btn btn-sm btn-defaut"  data-toggle="collapse" data-target="#form-recruiter" data-placement="bottom" title="Check Company Profile"><i class="ico-user-plus"></i> Add Recruiter</button>
          </div>
          <!--/ Toolbar -->
      </div>
      <form method="post" class="collapse" id="form-recruiter" data-parsley-validate="" novalidate="" aria-expanded="false" style="height: 0px;">
        {{ csrf_field() }}
        <div class="form-group">
            <div class="row">
                <div class="col-sm-3">
                    <label class="control-label">First Name</label>
                    <input type="text" name="firstname" class="form-control" placeholder="First name" required="">
                </div>
                <div class="col-sm-3">
                  <label class="control-label">Last Name</label>
                  <input type="text" name="lastname" class="form-control" placeholder="Last name" required="">
                </div>
                <div class="col-sm-3">
                  <label class="control-label">Email</label>
                  <input type="email" name="email" class="form-control" placeholder="email@company.com" required="">
                </div>
                <div class="col-sm-3">
                  <label class="control-label">Password</label>
                  <input type="password" name="password" class="form-control" placeholder="Password" required="">
                </div>
            </div>
        </div>
        <div class="form-group">
            <button type="reset" class="btn btn-default">Reset</button>
            <button type="submit" class="btn btn-success">Add Recruiter</button>
        </div>
    </form>
  </div>
  @if(\Session::get('successMessage'))
  <div class="row">
    <div class="col-md-12">
      <div class="alert alert-info">
        <strong>{{ \Session::get('successMessage') }}</strong>
      </div>
    </div>
  </div>
  @endif
  @if(count($recruiters)>0)
  <!-- START row -->
  <div class="row">
      <div class="col-md-12">
          <!-- START panel -->
          <div class="panel panel-primary">
              <!-- panel heading/header -->
              <div class="panel-heading">
                  <h3 class="panel-title"><span class="panel-icon mr5"><i class="ico-table22"></i></span> Recruiters</h3>
                  <!-- panel toolbar -->
                  <div class="panel-toolbar text-right">
                      <!-- option -->
                      <div class="option">
                          <button class="btn up" data-toggle="panelcollapse"><i class="reload"></i></button>
                      </div>
                      <!--/ option -->
                  </div>
                  <!--/ panel toolbar -->
              </div>
              <!--/ panel heading/header -->
              <!-- panel toolbar wrapper -->
              <div class="panel-toolbar-wrapper pl0 pt5 pb5">
                  <div class="panel-toolbar pl10">
                      <div class="col-md-4">
                      </div>
                  </div>
                  <div class="panel-toolbar text-right">
                      <form class="form-inline">
                        <div class="form-group">
                          <div class="input-group">
                                <input type="text" class="form-control" placeholder="Search for...">
                                <span class="input-group-btn">
                                  <button class="btn btn-default" type="button"><i class="ico-search"></i> Search Recruiter</button>
                                </span>
                          </div><!-- /input-group -->
                        </div>
                      </form>
                  </div>
              </div>
              <!--/ panel toolbar wrapper -->

              <!-- panel body with collapse capabale -->
              <div class="table-responsive pull out">
                  <table class="table table-bordered table-hover" id="table1">
                      <thead>
                          <tr>
                              <th width="5%"></th>
                              <th>Name</th>
                              <th>Email</th>
                              <th>Password</th>
                              <th width="10%"></th>
                          </tr>
                      </thead>
                      <tbody>
                        @foreach($recruiters as $recruiter)
                          <tr>
                              <td><div class="media-object"><img src="{{asset('hfv1/image/avatar/avatar.png')}}" alt="" class="img-circle"></div>
                              </td>
                              <td>{{ $recruiter->recruiter->my_name }}</td>
                              <td>{{ $recruiter->recruiter->email }}</td>
                              <td><span class="label label-success">Complete</span></td>
                              <td class="text-center">
                                  <button class="btn btn-sm btn-default" data-toggle="tooltip" data-placement="bottom" title="Edit Account"><i class="ico-edit"></i></button>
                                  <button class="btn btn-sm btn-default" data-toggle="tooltip" data-placement="bottom" title="View Profile"><i class="ico-profile"></i></button>
                              </td>
                          </tr>
                        @endforeach
                      </tbody>
                  </table>
              </div>
              <!--/ panel body with collapse capabale -->
          </div>
      </div>
  </div>
  <!--/ END row -->
  @else
    <div class="row">
      <div class="col-md-12">
        <div class="alert alert-info">
          <strong>No Data!</strong>
        </div>
      </div>
    </div>
  @endif
@endsection
