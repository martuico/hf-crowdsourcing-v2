@extends('emailLayout.main')

@section('content')
  <body class="body" style="padding:0; margin:0; display:block; background:#ffffff; -webkit-text-size-adjust:none" bgcolor="#ffffff">
  <table align="center" cellpadding="0" cellspacing="0" width="100%" height="100%" >
    <tr>
      <td align="center" valign="top" bgcolor="#ffffff"  width="100%">

      <table cellspacing="0" cellpadding="0" width="100%">
        <tr>
          <td style="border-bottom: 3px solid #3bcdc3;" width="100%">
            <center>
              <table cellspacing="0" cellpadding="0" width="500" class="w320">
                <tr>
                  <td valign="top" style="padding:10px 0; text-align:left;" class="mobile-center">
                    <img width="250" height="62" src="http://hf.martuico.me/hfv1/image/logo/logo.png" style="width:100%;">
                  </td>
                </tr>
              </table>
            </center>
          </td>
        </tr>
        <tr>
          <td background="https://www.filepicker.io/api/file/zLBr1W6UT6qZP4jI2yRz" bgcolor="#64594b" valign="top" style="background: url(https://www.filepicker.io/api/file/zLBr1W6UT6qZP4jI2yRz) no-repeat center; background-color: #64594b; background-position: center;">
            <!--[if gte mso 9]>
            <v:rect xmlns:v="urn:schemas-microsoft-com:vml" fill="true" stroke="false" style="mso-width-percent:1000;height:303px;">
              <v:fill type="tile" src="https://www.filepicker.io/api/file/ewEXNrLlTneFGtlB5ryy" color="#64594b" />
              <v:textbox inset="0,0,0,0">
            <![endif]-->
            <div>
              <center>
                <table cellspacing="0" cellpadding="0" width="530" height="303" class="w320">
                  <tr>
                    <td valign="middle" style="vertical-align:middle; padding-right: 15px; padding-left: 15px; text-align:left; color:#fff;" class="mobile-center" height="303">

                      <h1>CANDIDATE HAS SUBMITTED A APPLICATION</h1><br>
                      <h2>You may review his CV and account profile</h2>

                    </td>
                  </tr>
                </table>
              </center>
            </div>
            <!--[if gte mso 9]>
              </v:textbox>
            </v:rect>
            <![endif]-->
          </td>
        </tr>
        <tr>
          <td valign="top">
            <center>
              <table cellspacing="0" cellpadding="0" width="500" class="w320">
                <tr>
                  <td>

                    <table cellspacing="0" cellpadding="0" width="100%">
                      <tr>
                        <td class="mobile-padding" style="text-align:left;">
                        <br>
                        Good day!
                        <br><br>
                        Applicant {{ $inputs['firstname']. ' '.$inputs['lastname'] }} has submitted an application on {{ $inputs['job_title'] }} with a {{ $inputs['job_code'] }}.
                          <br>
                        You may check the application on your <a href="{{ url('/').'/employer' }}">dashboard.</a>
                          <br><br>
                        Good luck on your application!
                          <br><br>
                        Regards, <br>
                        Human Factor Support Team.
                        </td>
                      </tr>
                    </table>
                  </td>
                </tr>
                <tr>
                  <td>
                    <table cellspacing="0" cellpadding="25" width="100%">
                      <tr>
                        <td>
                          &nbsp;
                        </td>
                      </tr>
                    </table>
                  </td>
                </tr>
              </table>
            </center>
          </td>
        </tr>
        <tr>
          <td style="background-color:#c2c2c2;">

            <center>
              <table cellspacing="0" cellpadding="0" width="500" class="w320">
                <tr>
                  <td>
                    <table cellspacing="0" cellpadding="30" width="100%">
                      <tr>
                        <td style="text-align:center;">
                          <a href="#">
                            <img width="61" height="51" src="https://www.filepicker.io/api/file/vkoOlof0QX6YCDF9cCFV" alt="twitter" />
                          </a>
                          <a href="#">
                            <img width="61" height="51" src="https://www.filepicker.io/api/file/fZaNDx7cSPaE23OX2LbB" alt="google plus" />
                          </a>
                          <a href="#">
                            <img width="61" height="51" src="https://www.filepicker.io/api/file/b3iHzECrTvCPEAcpRKPp" alt="facebook" />
                          </a>
                        </td>
                      </tr>
                    </table>
                  </td>
                </tr>
                <tr>
                  <td>
                    <center>
                      <table style="margin:0 auto;" cellspacing="0" cellpadding="5" width="100%">
                        <tr>
                          <td style="text-align:center; margin:0 auto;" width="100%">
                             <a href="#" style="text-align:center;color:#fff;">
                               HUMANFACTOR.PH
                             </a>
                          </td>
                        </tr>
                      </table>
                    </center>
                  </td>
                </tr>
              </table>
            </center>

          </td>
        </tr>
      </table>

      </td>
    </tr>
  </table>
  </body>
@stop
