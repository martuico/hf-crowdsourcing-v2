@extends('Dashboard::template.main')

@section('content')
  <!-- Page Header -->
  <div class="page-header page-header-block">
      <div class="page-header-section">
          <h4 class="title semibold">{{$employer->company_name}} Profile</h4>
      </div>
      <div class="page-header-section">
          <!-- Toolbar -->
          <div class="toolbar clearfix text-right">
              <a href="{{route('employer.edit.profile')}}" class="btn btn-sm btn-default"  data-toggle="tooltip" data-placement="bottom" title="Update Company Profile"><i class="ico-edit"></i></a>
          </div>
          <!--/ Toolbar -->
      </div>
  </div>

  <!-- START row -->
  <div class="row">
      <!-- Left / Top Side -->
      <div class="col-lg-4">
        <div class="widget panel">
            <!-- panel body -->
            <div class="panel-body bgcolor-success">
                <ul class="list-unstyled mt15 mb15">
                    <li class="text-center">
                        <img class="img-circle img-bordered" src="{{$employer->logo}}" alt="" width="65px" height="65px">
                    </li>
                    <li class="text-center">
                        <h5 class="semibold mb0">{{$employer->company_name}}</h5>
                        <span><i class="ico-map-marker"></i> {{$employer->office_address}}</span>
                    </li>
                </ul>
            </div>
            <!--/ panel body -->
            <!-- panel body -->
            <div class="panel-body">
                <!-- Nav section -->
                <ul class="nav nav-section nav-justified">
                    <li>
                        <div class="section">
                            <h4 class="nm"><i class="ico-earth"></i></h4>
                            <p class="nm">Website</p>
                        </div>
                    </li>
                    <li>
                        <div class="section">
                            <h4 class="nm">{{ $employer->hiredApplicants->count() }}</h4>
                            <p class="nm">Hired</p>
                        </div>
                    </li>
                    <li>
                        <div class="section">
                            <h4 class="nm">{{ $employer->jobs->count() }}</h4>
                            <p class="nm">Job Opening</p>
                        </div>
                    </li>
                </ul>
                <!--/ Nav section -->
            </div>
            <!--/ panel body -->
        </div>

        <div class="widget panel">
          <div class="panel-body">
                <p><span class="label label-info">
                @if($employer->is_verified == '1')
                  <i class="ico-checkmark-circle2"></i> Verified
                @else
                  <i class="ico-minus-circle2"></i> Not Verified
                @endif
                </span>
                </p>
                <p>
                <span class="label label-success">
                @if($employer->is_premium == '1')
                  <i class="ico-checkmark-circle2"></i> Verified
                @else
                  <i class="ico-minus-circle2"></i> Not Verified</span>
                @endif
                </span>
                </p>
              <h5 class="semibold mb0">About Our Company</h5>
              <p class="text-default mb0">{!! $employer->description !!}</p>
              <hr>
              <h5 class="semibold mb0">Contacts</h5>
              <p class="text-default mb0">Office Number: <span class="semibold text-accent">{{$employer->office_number_1}}</span></p>
              <p class="text-default mb0">Mobile Number: <span class="semibold text-accent">{{$employer->office_number_2 ?:'None'}}</span></p>
              <h5 class="semibold mb0">Social Media Page</h5>
              <p class="text-default mb0">Facebook: <span class="semibold text-accent">{{$employer->sm_facebook ?:'None'}}</span></p>
              <p class="text-default mb0">Twitter: <span class="semibold text-accent">{{$employer->sm_linkedin ?:'None'}}</span></p>
              <p class="text-default mb0">Linkedin: <span class="semibold text-accent">{{$employer->sm_twitter ?:'None'}}</span></p>
          </div>
        </div>
      </div>
      <!--/ Left / Top Side -->

      <!-- Left / Bottom Side -->
      <div class="col-lg-8">
        @if(auth()->user()->type->name == 'Employer' AND count(auth()->user()->employer)>0 AND $employer->is_verified < 1)
        <div class="alert alert-warning text-left">
          <strong><i class="ico-warning-sign"></i> You're account has not been verified yet but you can start adding job post! </strong>
        </div>
        @endif

        <div class="widget panel mt20">
          <!--/ panel body -->
          @if(count($jobs)>0)
          <!-- List group -->
          <ul class="list-group">
            @foreach($jobs as $job)
              <li class="list-group-item">
                  <div class="row">
                    <div class="col-md-8">
                      <h4>{{$job->job_title}}</h4>
                    </div>
                    <div class="col-md-4">
                      <p class="mt10"><span class="label label-info">{{ strtoupper($job->status) }}</span></p>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-md-8">
                      <p class="nm"><i class="ico-map-marker"></i>{{ $job->work_location }}</p>
                    </div>
                    <div class="col-md-4">
                      <p class="nm"><span class="label label-default"><i class="fa fa-check-square"></i> Hired : {{ $job->hiredApplicants->count() }}</span></p>
                    </div>
                  </div>
                  <small class="text-muted">{{ $job->created_at }}</small>
              </li>
            @endforeach
          </ul>
          <!-- List group -->
        </div>
        {{ $jobs->links() }}
        @else
        <div class="panel widget">
          <div class="panel-body">
            <div class="alert alert-info">
              <strong>No data for careers!</strong>
            </div>
          </div>
        </div>
        @endif
      </div>
      <!--/ Left / Bottom Side -->
  </div>
  <!--/ END row -->
@stop
