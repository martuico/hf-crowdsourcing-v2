@extends('Dashboard::template.main')

@section('content')
  <!-- Page Header -->
  <div class="page-header page-header-block">
      <div class="page-header-section">
          <h4 class="title semibold">Create Company</h4>
      </div>
      <div class="page-header-section">
      </div>
  </div>
    <!-- START row -->
  <div class="row">
      <!-- Left / Top Side -->
      <div class="col-lg-12">
        <!-- form profile -->
        {!! \Form::open(['class' => 'panel form-horizontal form-bordered','name' => 'form-profile','data-parsley-validate'=> '' ,'files' => true]) !!}
            <div class="panel-body pt0 pb0">
                @if (count($errors) > 0)
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif

                <div class="form-group header bgcolor-default">
                    <div class="col-md-12">
                        <h4 class="semibold text-primary mt0 mb5">Company Profile</h4>
                        <p class="text-default nm">This information appears on your public profile, search results, and beyond.</p>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label">Company Logo <span class="text-danger">*</span></label>
                    <div class="col-sm-9">
                        <input id="input-id" type="file" class="file" data-preview-file-type="text" name="logo_url" data-parsley-errors-container="#errorBlockLogo"  data-parsley-error-message="Please add your logo" data-parsley-required>
                        <div id="errorBlockLogo"></div>
                        <p class="text-default nm">Image file size should not exceed 2MB. Suggested image dimension 300px by 300px</p>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label">Company Name <span class="text-danger">*</span></label>
                    <div class="col-sm-6">
                        <input type="text" class="form-control" name="company_name" value="{{old('company_name')}}" placeholder="Company Name" required=""  data-parsley-error-message="Please add your company name" data-parsley-length="[3, 191]">
                        <p class="help-block">Enter your company name.</p>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label">Office Address <span class="text-danger">*</span></label>
                    <div class="col-sm-6">
                        <input type="text" class="form-control" name="office_address" value="{{old('office_address')}}" placeholder="Office Address" required="" data-parsley-error-message="Please add your office address" data-parsley-length="[3, 191]">
                        <p class="help-block">Where in the world are you?</p>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label">Office Landline Number <span class="text-danger">*</span></label>
                    <div class="col-sm-5">
                        <input type="text" class="form-control" name="office_number_1" value="{{old('office_number_1')}}" placeholder="contact number 1" required="" data-parsley-error-message="Please add your office mumber" data-parsley-length="[3, 191]" data-mask="(999) 999-9999" >
                        <p class="help-block"></p>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label">Office Mobile Number</label>
                    <div class="col-sm-5">
                        <input type="text" class="form-control" name="office_number_2" value="{{old('office_number_2')}}"  data-mask="(9999) 999-9999" placeholder="contact number 1" data-parsley-length="[3, 191]">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label">Website</label>
                    <div class="col-sm-6">
                        <input type="text" class="form-control" data-parsley-type="url" name="website" value="{{old('website')}}" placeholder="http://">
                        <p class="help-block">Have a homepage or a blog? Put the address here.</p>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label">About the Company <span class="text-danger">*</span></label>
                    <div class="col-sm-9">
                        <textarea class="form-control summernote" rows="3" name="description" placeholder="Describe about yourself" required="" data-parsley-error-message="Please tell us about the Company">{{old('description')}}</textarea>
                        <p class="help-block">About your company in 500 characters or less.</p>
                    </div>
                </div>
                <div class="form-group header bgcolor-default">
                    <div class="col-md-12">
                        <h4 class="semibold text-primary nm">Contact Person Contacts</h4>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label">{{auth()->user()->myname}} Company's Position <span class="text-danger">*</span></label>
                    <div class="col-sm-6">
                        <input type="text" class="form-control" name="contact_designation" value="{{old('contact_designation')}}" placeholder="Your Position (ex. CEO, HR Manager)" required="" data-parsley-error-message="Please add your designation for this company" data-parsley-length="[3, 191]">
                        <p class="help-block">Please enter your designation for this company.</p>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label"> Mobile Number 1 <span class="text-danger">*</span></label>
                    <div class="col-sm-6">
                        <input type="text" class="form-control" data-parsley-required="" name="contact_person_mobile" value="{{old('contact_person_mobile')}}" data-mask="(9999) 999-9999" placeholder="contact number 1" data-parsley-length="[3, 191]">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label"> Mobile Number 2</label>
                    <div class="col-sm-6">
                        <input type="text" class="form-control" name="contact_person_mobil_2" value="{{old('contact_person_mobil_2')}}" data-mask="(9999) 999-9999" placeholder="contact number 1" data-parsley-length="[3, 191]">
                    </div>
                </div>
                <div class="form-group header bgcolor-default">
                    <div class="col-md-12">
                        <h4 class="semibold text-primary nm">Social Media Page</h4>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label"><i class="fa fa-facebook"></i> Facebook</label>
                    <div class="col-sm-6">
                        <input type="text" class="form-control" data-parsley-type="url" name="sm_facebook" value="{{old('sm_facebook')}}" placeholder="http://">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label"><i class="fa fa-linkedin"></i> LinkedIn</label>
                    <div class="col-sm-6">
                        <input type="text" class="form-control" data-parsley-type="url" name="sm_linkedin" value="{{old('sm_linkedin')}}" placeholder="http://">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label"><i class="fa fa-twitter"></i> Twitter</label>
                    <div class="col-sm-6">
                        <input type="text" class="form-control" data-parsley-type="url" name="sm_twitter" value="{{old('sm_twitter')}}" placeholder="http://">
                    </div>
                </div>
            </div>
            <div class="panel-footer text-right">
                <button type="submit" class="btn btn-primary"><i class="ico-save"></i> Save</button>
            </div>
        </form>
        <!--/ form profile -->
      </div>
  </div>
@stop

@section('styles')
    <link href="{{asset('hfv1/plugins/bootstrap-fileinput/css/fileinput.min.css')}}" media="all" rel="stylesheet" type="text/css" />
    <link href="{{asset('hfv1/plugins/summernote/dist/summernote.css')}}" media="all" rel="stylesheet" type="text/css" />

@stop

@section('scripts')

    <script src="{{asset('hfv1/plugins/summernote/dist/summernote.js')}}"></script>
    <script src="{{asset('hfv1/plugins/summernote/dist/summernote-cleaner.js')}}"></script>
    <script src="{{asset('hfv1/plugins/bootstrap-fileinput/js/plugins/purify.min.js')}}"></script>
    <script src="{{asset('hfv1/plugins/bootstrap-fileinput/js/fileinput.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('hfv1/plugins/inputmask/js/inputmask.js')}}"></script>
    <script>
        $("#input-id").fileinput({
            showUpload :false,
            maxFileCount: 1,
            showRemove: false,
            allowedFileTypes: ['image'],
            maxFileSize: 2000,
            validateInitialCount: true
        });

        $('.summernote').summernote({
            height: 150,
            toolbar:[
                    ['style',['style']],
                    ['font',['bold','italic','underline','clear']],
                    ['fontname',['fontname']],
                    ['color',['color']],
                    ['para',['ul','ol','paragraph']],
                    ['height',['height']],
                    ['view',['fullscreen']],
                    ['help',['help']]
            ],
            disableDragAndDrop: true,
            cleaner:{
                       notTime:2400, // Time to display Notifications.
                       action:'both', // both|button|paste 'button' only cleans via toolbar button, 'paste' only clean when pasting content, both does both options.
                       newline:'<br>', // Summernote's default is to use '<p><br></p>'
                       notStyle:'position:absolute;top:0;left:0;right:0', // Position of Notification
                       keepHtml: false, //Remove all Html formats
                       keepClasses: false, //Remove Classes
                       badTags: ['style','script','applet','embed','noframes','noscript', 'html'], //Remove full tags with contents
                       badAttributes: ['style','start'] //Remove attributes from remaining tags
            }
        })
    </script>
@stop