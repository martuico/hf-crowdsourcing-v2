@extends('Dashboard::template.main')

@section('content')
  <!-- Page Header -->
  <div class="page-header page-header-block">
      <div class="page-header-section">
          <h4 class="title semibold">Company Profile</h4>
      </div>
      <div class="page-header-section">
          <!-- Toolbar -->
          <div class="toolbar clearfix text-right">
              <button class="btn btn-sm btn-defaut"  data-toggle="tooltip" data-placement="bottom" title="Update Company Profile"><i class="ico-edit"></i></button> &nbsp;
              <button class="btn btn-sm btn-defaut"  data-toggle="tooltip" data-placement="bottom" title="Check Company Profile"><i class="ico-eye"></i></button>
          </div>
          <!--/ Toolbar -->
      </div>
  </div>

  <!-- START row -->
  <div class="row">
      <!-- Left / Top Side -->
      <div class="col-lg-3">
          <!-- tab menu -->
          <ul class="list-group list-group-tabs">
              <li class="list-group-item active"><a href="#profile" data-toggle="tab"><i class="ico-user2 mr5"></i> Company Profile</a></li>
              <li class="list-group-item"><a href="#account" data-toggle="tab"><i class="ico-archive2 mr5"></i> Account</a></li>
              <li class="list-group-item"><a href="#password" data-toggle="tab"><i class="ico-key2 mr5"></i> Password</a></li>
          </ul>
          <!-- tab menu -->

          <hr><!-- horizontal line -->

          <!-- figure with progress -->
          <ul class="list-table">
              <li style="width:70px;">
                  <img class="img-circle img-bordered" src="{{asset('hfv1/image/avatar/avatar7.jpg')}}" alt="" width="65px">
              </li>
              <li class="text-left">
                  <h5 class="semibold ellipsis mt0">Erich Reyes</h5>
                  <div style="max-width:200px;">
                      <div class="progress progress-xs mb5">
                          <div class="progress-bar progress-bar-warning" style="width:70%"></div>
                      </div>
                      <p class="text-muted clearfix nm">
                          <span class="pull-left">Profile complete</span>
                          <span class="pull-right">70%</span>
                      </p>
                  </div>
              </li>
          </ul>
          <!--/ figure with progress -->

      </div>
      <!--/ Left / Top Side -->

      <!-- Left / Bottom Side -->
      <div class="col-lg-9">
          <!-- START Tab-content -->
          <div class="tab-content">
              <!-- tab-pane: profile -->
              <div class="tab-pane active" id="profile">
                  <!-- form profile -->
                  <form class="panel form-horizontal form-bordered" name="form-profile">
                      <div class="panel-body pt0 pb0">
                          <div class="form-group header bgcolor-default">
                              <div class="col-md-12">
                                  <h4 class="semibold text-primary mt0 mb5">Company Profile</h4>
                                  <p class="text-default nm">This information appears on your public profile, search results, and beyond.</p>
                              </div>
                          </div>
                          <div class="form-group">
                              <label class="col-sm-3 control-label">Company Logo</label>
                              <div class="col-sm-9">
                                  <div class="btn-group pr5">
                                      <img class="img-circle img-bordered" src="{{asset('hfv1/image/avatar/avatar7.jpg')}}" alt="" width="34px">
                                  </div>
                                  <div class="btn-group">
                                      <button type="button" class="btn btn-default">Change photo</button>
                                      <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                                          <span class="caret"></span>
                                      </button>
                                      <ul class="dropdown-menu" role="menu">
                                          <li><a href="#">Upload photo</a></li>
                                          <li><a href="#">Remove</a></li>
                                      </ul>
                                  </div>
                              </div>
                          </div>
                          <div class="form-group">
                              <label class="col-sm-3 control-label">Company Name</label>
                              <div class="col-sm-6">
                                  <input type="text" class="form-control" name="name" value="Erich Reyes">
                                  <p class="help-block">Enter your real name.</p>
                              </div>
                          </div>
                          <div class="form-group">
                              <label class="col-sm-3 control-label">Office Address</label>
                              <div class="col-sm-5">
                                  <input type="text" class="form-control" name="location">
                                  <p class="help-block">Where in the world are you?</p>
                              </div>
                          </div>
                          <div class="form-group">
                              <label class="col-sm-3 control-label">Office Number</label>
                              <div class="col-sm-5">
                                  <input type="text" class="form-control" name="location" placeholder="contact number 1">
                                  <p class="help-block"></p>
                              </div>
                          </div>
                          <div class="form-group">
                              <label class="col-sm-3 control-label">&nbsp;</label>
                              <div class="col-sm-5">
                                  <input type="text" class="form-control" name="location" placeholder="contact number 1">
                              </div>
                          </div>
                          <div class="form-group">
                              <label class="col-sm-3 control-label">Website</label>
                              <div class="col-sm-6">
                                  <input type="text" class="form-control" name="website" value="http://">
                                  <p class="help-block">Have a homepage or a blog? Put the address here.</p>
                              </div>
                          </div>
                          <div class="form-group">
                              <label class="col-sm-3 control-label">About the Company</label>
                              <div class="col-sm-6">
                                  <textarea class="form-control" rows="3" placeholder="Describe about yourself"></textarea>
                                  <p class="help-block">About yourself in 160 characters or less.</p>
                              </div>
                          </div>
                          <div class="form-group header bgcolor-default">
                              <div class="col-md-12">
                                  <h4 class="semibold text-primary nm">Social Media Page</h4>
                              </div>
                          </div>
                          <div class="form-group">
                              <label class="col-sm-3 control-label"><i class="fa fa-facebook"></i> Facebook</label>
                              <div class="col-sm-6">
                                  <input type="text" class="form-control" name="website" value="http://">
                              </div>
                          </div>
                          <div class="form-group">
                              <label class="col-sm-3 control-label"><i class="fa fa-linkedin"></i> LinkedIn</label>
                              <div class="col-sm-6">
                                  <input type="text" class="form-control" name="website" value="http://">
                              </div>
                          </div>
                          <div class="form-group">
                              <label class="col-sm-3 control-label"><i class="fa fa-twitter"></i> Twitter</label>
                              <div class="col-sm-6">
                                  <input type="text" class="form-control" name="website" value="http://">
                              </div>
                          </div>
                      </div>
                      <div class="panel-footer">
                          <button type="reset" class="btn btn-default">Reset</button>
                          <button type="submit" class="btn btn-primary">Save change</button>
                      </div>
                  </form>
                  <!--/ form profile -->
              </div>
              <!--/ tab-pane: profile -->

              <!-- tab-pane: account -->
              <div class="tab-pane" id="account">
                  <!-- form account -->
                  <form class="panel form-horizontal form-bordered" name="form-account">
                      <div class="panel-body pt0 pb0">
                          <div class="form-group header bgcolor-default">
                              <div class="col-md-12">
                                  <h4 class="semibold text-primary mt0 mb5">Account</h4>
                                  <p class="text-default nm">Change your basic account and language settings.</p>
                              </div>
                          </div>
                          <div class="form-group">
                              <label class="col-sm-3 control-label">Profile Picture</label>
                              <div class="col-sm-9">
                                  <div class="btn-group pr5">
                                      <img class="img-circle img-bordered" src="{{asset('hfv1/image/avatar/avatar7.jpg')}}" alt="" width="34px">
                                  </div>
                                  <div class="btn-group">
                                      <button type="button" class="btn btn-default">Change photo</button>
                                      <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                                          <span class="caret"></span>
                                      </button>
                                      <ul class="dropdown-menu" role="menu">
                                          <li><a href="#">Upload photo</a></li>
                                          <li><a href="#">Remove</a></li>
                                      </ul>
                                  </div>
                              </div>
                          </div>
                          <div class="form-group">
                              <label class="col-sm-3 control-label">Full Name</label>
                              <div class="col-sm-5">
                                  <input type="text" class="form-control" name="email" placeholder="First Name">
                              </div>
                          </div>
                          <div class="form-group">
                              <label class="col-sm-3 control-label">&nbsp;</label>
                              <div class="col-sm-5">
                                  <input type="text" class="form-control" name="email" placeholder="Middle Name">
                              </div>
                          </div>
                          <div class="form-group">
                              <label class="col-sm-3 control-label">&nbsp;</label>
                              <div class="col-sm-5">
                                  <input type="text" class="form-control" name="email" placeholder="Last Name">
                              </div>
                          </div>
                          <div class="form-group">
                              <label class="col-sm-3 control-label">Email</label>
                              <div class="col-sm-5">
                                  <input type="text" class="form-control" name="email" placeholder="mail@company.ph">
                              </div>
                          </div>
                      </div>
                      <div class="panel-footer">
                          <button type="reset" class="btn btn-default">Reset</button>
                          <button type="submit" class="btn btn-primary">Save change</button>
                      </div>
                  </form>
                  <!--/ form account -->
              </div>
              <!--/ tab-pane: account -->

              <!-- tab-pane: security -->
              <div class="tab-pane" id="security">
                  <!-- form security -->
                  <form class="panel form-horizontal form-bordered" name="form-security">
                      <div class="panel-body pt0 pb0">
                          <div class="form-group header bgcolor-default">
                              <div class="col-md-12">
                                  <h4 class="semibold text-primary mt0 mb5">Security</h4>
                                  <p class="text-default nm">Change your security settings.</p>
                              </div>
                          </div>
                          <div class="form-group">
                              <label class="col-sm-3 control-label">Login verification</label>
                              <div class="col-sm-9">
                                  <div class="row">
                                      <div class="col-sm-12">
                                          <span class="radio custom-radio">
                                              <input type="radio" name="loginverification" id="loginverification1">
                                              <label for="loginverification1">&nbsp;&nbsp;Send login verification requests to my phone</label>
                                          </span>
                                      </div>
                                  </div>
                                  <div class="row">
                                      <div class="col-sm-12">
                                          <span class="radio custom-radio">
                                              <input type="radio" name="loginverification" id="loginverification2">
                                              <label for="loginverification2">&nbsp;&nbsp;Send login verification requests to my phone</label>
                                          </span>
                                      </div>
                                  </div>
                              </div>
                          </div>
                          <div class="form-group">
                              <label class="col-sm-3 control-label">Password reset</label>
                              <div class="col-sm-9">
                                  <span class="checkbox custom-checkbox">
                                      <input type="checkbox" name="passwordreset" id="passwordreset">
                                      <label for="passwordreset">&nbsp;&nbsp;Require personal information to reset my password</label>
                                  </span>
                                  <p class="help-block">By default, you can initiate a password reset by entering only your @username. If you check this box, you will be prompted to enter your email address or phone number if you forget your password.</p>
                              </div>
                          </div>
                          <div class="form-group header bgcolor-default">
                              <div class="col-md-12">
                                  <h4 class="semibold text-primary mt0 mb5">Privacy</h4>
                                  <p class="text-default nm">Change your privacy settings.</p>
                              </div>
                          </div>
                          <div class="form-group">
                              <label class="col-sm-3 control-label">Photo tagging</label>
                              <div class="col-sm-9">
                                  <div class="row">
                                      <div class="col-sm-12">
                                          <span class="radio custom-radio">
                                              <input type="radio" name="phototagging" id="phototagging1">
                                              <label for="phototagging1">&nbsp;&nbsp;Allow anyone to tag me in photos</label>
                                          </span>
                                      </div>
                                  </div>
                                  <div class="row">
                                      <div class="col-sm-12">
                                          <span class="radio custom-radio">
                                              <input type="radio" name="phototagging" id="phototagging2">
                                              <label for="phototagging2">&nbsp;&nbsp;Only allow people I follow to tag me in photos</label>
                                          </span>
                                      </div>
                                  </div>
                                  <div class="row">
                                      <div class="col-sm-12">
                                          <span class="radio custom-radio">
                                              <input type="radio" name="phototagging" id="phototagging3">
                                              <label for="phototagging3">&nbsp;&nbsp;Do not allow anyone to tag me in photos</label>
                                          </span>
                                      </div>
                                  </div>
                              </div>
                          </div>
                          <div class="form-group">
                              <label class="col-sm-3 control-label">Discoverability</label>
                              <div class="col-sm-9">
                                  <span class="checkbox custom-checkbox">
                                      <input type="checkbox" name="discoverability" id="discoverability">
                                      <label for="discoverability">&nbsp;&nbsp;Let others find me by my email address</label>
                                  </span>
                              </div>
                          </div>
                          <div class="form-group">
                              <label class="col-sm-3 control-label">Personalization</label>
                              <div class="col-sm-9">
                                  <span class="checkbox custom-checkbox">
                                      <input type="checkbox" name="personalization" id="personalization">
                                      <label for="personalization">&nbsp;&nbsp;Tailor content based on my recent website visits</label>
                                  </span>
                                  <p class="help-block"><a href="javascript:void(0);">Learn more</a> about how this works and your additional privacy controls.</p>
                              </div>
                          </div>
                          <div class="form-group">
                              <label class="col-sm-3 control-label">Promoted content</label>
                              <div class="col-sm-9">
                                  <span class="checkbox custom-checkbox">
                                      <input type="checkbox" name="ads" id="ads">
                                      <label for="ads">&nbsp;&nbsp;Tailor ads based on information shared by ad partners.</label>
                                  </span>
                                  <p class="help-block"><a href="javascript:void(0);">Learn more</a> about how this works and your additional privacy controls.</p>
                              </div>
                          </div>
                      </div>
                      <div class="panel-footer">
                          <button type="reset" class="btn btn-default">Reset</button>
                          <button type="submit" class="btn btn-primary">Save change</button>
                      </div>
                  </form>
              </div>
              <!--/ tab-pane: security -->

              <!-- tab-pane: password -->
              <div class="tab-pane" id="password">
                  <!-- form password -->
                  <form class="panel form-horizontal form-bordered" name="form-password">
                      <div class="panel-body pt0 pb0">
                          <div class="form-group header bgcolor-default">
                              <div class="col-md-12">
                                  <h4 class="semibold text-primary mt0 mb5">Password</h4>
                                  <p class="text-default nm">Change your password or recover your current one.</p>
                              </div>
                          </div>
                          <div class="form-group">
                              <label class="col-sm-3 control-label">Current password</label>
                              <div class="col-sm-5">
                                  <input type="text" class="form-control" name="currentpass">
                                  <p class="help-block"><a href="javascript:void(0);">Forgot password?</a></p>
                              </div>
                          </div>
                          <div class="form-group">
                              <label class="col-sm-3 control-label">New password</label>
                              <div class="col-sm-5">
                                  <input type="text" class="form-control" name="newpass">
                              </div>
                          </div>
                          <div class="form-group">
                              <label class="col-sm-3 control-label">Verify password</label>
                              <div class="col-sm-5">
                                  <input type="text" class="form-control" name="verifypass">
                              </div>
                          </div>
                      </div>
                      <div class="panel-footer">
                          <button type="reset" class="btn btn-default">Reset</button>
                          <button type="submit" class="btn btn-primary">Save change</button>
                      </div>
                  </form>
              </div>
              <!--/ tab-pane: password -->
          </div>
          <!--/ END Tab-content -->
      </div>
      <!--/ Left / Bottom Side -->
  </div>
  <!--/ END row -->
@stop