<?php


Route::group(['module' => 'Employer',
              'namespace' => 'App\Modules\Employer\Controllers',
              'middleware' => ['web','auth','EmployerOnly'],
              'prefix' => 'employer'
          ],
            function() {

    Route::get('billings',['as' => 'employer.billings','uses' => 'BillingController@index']);
    Route::post('set-appointment',['as' => 'employer.applicant.appointment','uses' => 'JobController@setAppoiment']);

    Route::get('settings',['as' => 'employer.settings','uses' => 'EmployerController@settings']);
    // Route::get('billings',['as' => 'employer.billings','uses' => 'EmployerController@billings']);

    Route::post('invite-recruiters',['as' => 'employer.recruiter.store', 'uses' => 'EmployerController@addRecruiter']);
    Route::get('invite-recruiters',['as' => 'employer.inviteRecruiters','uses' => 'EmployerController@inviteRecruiters']);
    Route::get('careers/{jobpost}/applicants',['as' => 'employer.jobapplicants','uses' => 'EmployerController@jobapplicants']);


    Route::get('careers/create',['as' => 'employer.job.create','uses' => 'JobController@create']);
    Route::post('careers/create',['as' => 'employer.job.create','uses' => 'JobController@store']);
    Route::get('careers/{job_code}/applicant/{applicant}/appointments',['as' => 'employer.careers.applicant.appointments','uses' => 'JobController@applicantAppoiments']);

    Route::get('careers/{job_code}/applicant/{applicant}',['as' => 'employer.careers.applicant.details','uses' => 'JobController@applicantJob']);
    Route::post('careers/applicant/{applicant}/employer-update-stage',['as' => 'employer.application.employerstages','uses' => 'JobController@updateStage']);

    Route::get('careers/{job_code}/pipeline',['as' => 'employer.careers.pipeline','uses' => 'JobController@pipeline']);
    Route::get('careers/{job_code}/details',['as' => 'employer.careers.details','uses' => 'JobController@show']);
    Route::get('careers/{jobcode}/edit',['as' => 'employer.job.edit','uses' => 'JobController@edit']);
    Route::post('careers/{jobcode}/edit',['uses' => 'JobController@update']);
    Route::get('careers',['as' => 'employer.careers','uses' => 'JobController@index']);

    Route::get('company-profile/create',['as' => 'employer.create.profile','uses' => 'EmployerController@createProfile']);
    Route::post('company-profile/create',['middleware' => 'anitmultiple','as' => 'employer.store.profile','uses' => 'EmployerController@storeProfile']);

    Route::get('company-profile/edit',['as' => 'employer.edit.profile','uses' => 'EmployerController@editProfile']);
    Route::post('company-profile/edit',['as' => 'employer.update.profile','uses' => 'EmployerController@updateProfile']);


    Route::get('company-profile',['as' => 'employer.profile','uses' => 'EmployerController@profile']);
    Route::get('appointments',['as' => 'employer.appointments','uses' => 'EmployerController@appointments']);

    Route::post('applicant-comment',['uses' => 'JobController@applicantActivityComment']);
    Route::post('update-applicant-status',['uses' => 'EmployerController@updateApplicantStatus']);
    Route::get('fetch-applicant-details/{code}',['as' => 'employer.applicant.details','uses' => 'EmployerController@applicantDetails']);
    Route::get('applicants',['as' => 'employer.applicants','uses' => 'EmployerController@applicants']);
    Route::get('fetch-cities/{provcode}',['uses' => 'JobController@fetchCities']);
    Route::get('fetch-province/{regcode}',['uses' => 'JobController@fetchProvince']);
    Route::get('test',function(){
        // return \App\Region::where('regCode','=','07')
        //                 ->with('provinces')
        //                 ->get();

    });
    Route::get('/',['as' => 'employer.home','uses' => 'EmployerController@index']);
});
