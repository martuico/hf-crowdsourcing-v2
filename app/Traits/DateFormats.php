<?php

namespace App\Traits;
// use Carbon\Cabon as Carbon;

trait DateFormats
{
  public function fromTo($date)
  {
      if($date)
        return \Carbon\Carbon::createFromFormat('Y-m-d',$date)->format('F Y');
      return $date;
  }

  public function diffInHours($date)
  {
      if(!$date)
        return $date;
      return \Carbon\Carbon::createFromFormat('Y-m-d H:i:s',$date)
            ->diffInHours(\Carbon\Carbon::now());
  }

  public function dateForHumans($date)
  {
      if(!$date)
        return $date;
      return \Carbon\Carbon::createFromFormat('Y-m-d H:i:s',$date)->diffForHumans();
  }

  public function toDayDateTimeString($date)
  {
      if(!$date)
        return $date;
      return \Carbon\Carbon::createFromFormat('Y-m-d H:i:s',$date)->toDayDateTimeString();
  }

  public function validUnitFormat($date)
  {
      if(!$date)
        return $date;
      return \Carbon\Carbon::createFromFormat('Y-m-d',$date)->toFormattedDateString();
  }
}