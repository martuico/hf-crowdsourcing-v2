<?php

namespace App\Traits;

use league\filesystem\src\File;
use intervention\image\src\Intervention\Image\Image;

trait ImageUpload
{

    public function imagesUploadWithThumbNail($requestImages, $maxwidth, $minwidth, $minheight, $maxheight)
    {
          reset($requestImages);
          $first = key($requestImages);
          $check = [];
          foreach($requestImages as $key_image => $image){
                  $filename = str_random(5).'-eq.'.$image->getClientOriginalExtension();
                  $thumbnail_destination = public_path('upload-media/eq/thumbnail').'/'.$filename;
                  $thumb_img = \Image::make($image->getRealPath());
                  // $thumb_img->resize(78,22, function($constraint){
                  //     $constraint->aspectRatio();
                  // })->save($thumbnail_destination);
                  $thumb_img->fit($maxwidth,$maxheight, function($constraint){
                      $constraint->aspectRatio();
                  })->save($thumbnail_destination);

                  $original_url = public_path('upload-media/eq/big').'/'.$filename;
                  $big_size = \Image::make($image->getRealPath());
                  $big_size->fit($minwidth,$minheight, function($constraint){
                      $constraint->aspectRatio();
                  })->resizeCanvas($minwidth,$minheight,'center',false,[0,0,0,0])->save($original_url);
                  $check['big_image'] = url('upload-media/eq/big').'/'.$filename;
                  $check['thumb_image'] =  url('upload-media/eq/thumbnail').'/'.$filename;
          }
          return $check;
    }

    public function imagesUpload($requestImages, $width, $height)
    {
          reset($requestImages);
          $first = key($requestImages);
          $check = [];
          foreach($requestImages as $key_image => $image){
                  $filename = str_random(5).'-eq.'.$image->getClientOriginalExtension();
                  $original_url =  public_path('upload-media').'/'.$filename;

                  $big_size = \Image::make($image->getRealPath());
                  $big_size->resize($width,$height, function($constraint){
                      $constraint->aspectRatio();
                      $constraint->upsize();
                  })->resizeCanvas($width,$height,'center',false,[0,0,0,0])->save($original_url);
                  $check['uploaded_file'] = url('upload-media').'/'.$filename;
          }
          return $check;
    }

    public function deleteImagesWithThumbnail($check,$colOrig,$colThumb)
    {
        $orig_image = str_replace(url('/'),'',$check->$colOrig);
        $thumb_image = str_replace(url('/'),'',$check->$colThumb);
        $orig_image = public_path().$orig_image;
        $thumb_image = public_path().$thumb_image;

        if(\File::exists($orig_image)){
            \File::delete($orig_image);
        }

        if(\File::exists($thumb_image)){
            \File::delete($thumb_image);
        }

        return true;
    }

    public function deleteImage($link)
    {
        $orig_image = str_replace(url('/'),'',$link);
        $orig_image = public_path().$orig_image;

        if(\File::exists($orig_image)){
            \File::delete($orig_image);
        }

        return true;
    }
}