<?php namespace App\Traits;

use App\Job;
use App\Applicant;
use App\ApplicantJob;
use App\Referral;

trait Generator
{
  public static function generateApplicationcodeNumber() {
    $number = mt_rand(10000000, 99999999); // better than rand()

    // call the same function if the barcode exists already
    if (self::codeNumberExists($number)) {
        return self::codeApplicantionCodeExists();
    }
    $str = random_str(5, 'abcdefghijklmnopqrstuvwxyz');
    // otherwise, it's valid and can be used
    return strtoupper($str).'-'.$number;
  }

  public static function generateLinkNumber() {
    $number = mt_rand(10000000, 99999999); // better than rand()

    // call the same function if the barcode exists already
    if (self::codeNumberExists($number)) {
        return self::codeReferredCodeExists();
    }
    $str = random_str(5, 'abcdefghijklmnopqrstuvwxyz');
    // otherwise, it's valid and can be used
    return strtoupper($str).'-'.$number;
  }

  public static function generateApplicantNumber() {
    $number = mt_rand(10000000, 99999999); // better than rand()

    // call the same function if the barcode exists already
    if (self::codeApplicantNumberExists($number)) {
        return self::generateJobcodeNumber();
    }
    $str = random_str(5, 'abcdefghijklmnopqrstuvwxyz');
    $separator = random_str(1, '|@.-');
    // otherwise, it's valid and can be used
    return strtoupper($str).$separator.$number;
  }

  public static function generateJobcodeNumber() {
      $number = mt_rand(100000, 999999); // better than rand()

      // call the same function if the barcode exists already
      if (self::codeNumberExists($number)) {
          return self::generateJobcodeNumber();
      }
      $str = random_str(3, 'abcdefghijklmnopqrstuvwxyz');
      // otherwise, it's valid and can be used
      return strtoupper($str).$number;
  }

  public static function codeNumberExists($number) {
      // query the database and return a boolean
      // for instance, it might look like this in Laravel
      return Job::whereJobCode($number)->exists();
  }

  public static function codeApplicantionCodeExists($number) {
      // query the database and return a boolean
      // for instance, it might look like this in Laravel
      return ApplicantJob::whereApplicationCode($number)->exists();
  }

  public static function codeReferredCodeExists($number) {
      // query the database and return a boolean
      // for instance, it might look like this in Laravel
      return Referral::whereApplicationCode($number)->exists();
  }

  public static function codeApplicantNumberExists($number) {
      // query the database and return a boolean
      // for instance, it might look like this in Laravel
      return Applicant::whereAuid($number)->exists();
  }
}
