<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Traits\Generator;

class Applicant extends Model {

	use Generator;

	// protected $table = 'applicants';
	// public $timestamps = true;

	protected $fillable = [
		'user_id',
		'job_industry_id',
		'education_level_id',
		'last_school',
		'has_professional_license',
		'field_of_study',
		'position',
		'current_province',
		'current_city',
		'current_address',
		'hometown_city',
		'hometown_province',
		'hometown_address',
		'location_region',
		'location_province',
		'location_city',
		'biography',
		'attached_resume',
		'current_employment_status',
		'mobile_number_1',
		'mobile_number_2',
		'house_number_1',
		'skills',
		'languages',
		'date_of_birth',
		'referred_by_id',
		'hf_assigned',
		'referred_by_id',
		'referred_start_date',
		'referred_expired_date',
		'referral_link',
		'willing_to_relocate',
		'current_region',
		'hometown_region',
		'availability',
		'expected_salary',
		'prefered_work_location'
	];

	public static function boot()
	{
	    parent::boot();
	    static::creating(function($model){
	      // $model->user_id = \Auth::user()->id;
				$model->auid =  self::generateApplicantNumber();
	    });
	}

	protected $appends = ['employment_status','full_address','current_fulladdress','hometown_fulladdress'];


	public function prefered_location()
	{
			return $this->belongsTo('App\City','prefered_work_location','citymunCode');
	}

	public function region()
	{
			return $this->belongsTo('App\Region','location_region','regCode');
	}


	public function province()
	{
			return $this->belongsTo('App\Province','location_province','provCode');
	}

	public function city()
	{
			return $this->belongsTo('App\City','location_city','citymunCode');
	}

	public function c_region()
	{
			return $this->belongsTo('App\Region','current_region','regCode');
	}

	public function c_province()
	{
			return $this->belongsTo('App\Province','current_province','provCode');
	}

	public function c_city()
	{
			return $this->belongsTo('App\City','current_city','citymunCode');
	}

	public function h_region()
	{
			return $this->belongsTo('App\Region','hometown_region','regCode');
	}

	public function h_province()
	{
			return $this->belongsTo('App\Province','hometown_province','provCode');
	}

	public function h_city()
	{
			return $this->belongsTo('App\City','hometown_city','citymunCode');
	}

	public function getEmploymentStatusAttribute($value)
	{
	    return ucwords(str_replace('_', ' ',$this->current_employment_status ));
	}

	public function getFullAddressAttribute($value)
	{
		  $reg = ($this->region AND $this->region->regDesc)?$this->region->regDesc:'';
		  $prov = ($this->province AND $this->province->provDesc)?$this->province->provDesc:'';
		  $city = ($this->city AND $this->city->citymunDesc)?$this->city->citymunDesc:'';

	    return strtoupper($this->current_address).PHP_EOL.' '.$reg.', '.$prov.','.$city;
	}

	public function getCurrentFulladdressAttribute($value)
	{
		  $reg = ($this->c_region AND $this->c_region->regDesc)?$this->c_region->regDesc:'';
		  $prov = ($this->c_province AND $this->c_province->provDesc)?$this->c_province->provDesc:'';
		  $city = ($this->c_city AND $this->c_city->citymunDesc)?$this->c_city->citymunDesc:'';

	    return strtoupper($this->current_address).PHP_EOL.' '.$reg.', '.PHP_EOL.$prov.','.PHP_EOL.$city;
	}

	public function getHometownFulladdressAttribute($value)
	{
		  $reg = ($this->h_region AND $this->h_region->regDesc)?$this->h_region->regDesc:'';
		  $prov = ($this->h_province AND $this->h_province->provDesc)?$this->h_province->provDesc:'';
		  $city = ($this->h_city AND $this->h_city->citymunDesc)?$this->h_city->citymunDesc:'';

	    return strtoupper($this->hometown_address).PHP_EOL.' '.$reg.', '.PHP_EOL.$prov.','.PHP_EOL.$city;
	}

	public function appliedJobs()
	{
		return $this->hasMany('App\ApplicantJob','applicant_id');
	}

	public function user()
	{
		return $this->belongsTo('App\User', 'user_id','id');
	}

	public function scopeSearchUser($query,$value)
	{
		return $this->belongsTo('App\User', 'user_id','id')
									->whereRaw('(firstname like ? OR lastname like ? OR email)',
															['%'.$value.'%','%'.$value.'%','%'.$value.'%','%'.$value.'%']);
	}

	public function educations()
	{
		return $this->hasMany('App\ApplicantEducation','applicant_id');
	}

	public function experiences()
	{
		return $this->hasMany('App\ApplicantExperience','applicant_id');
	}

	public function job_industry()
	{
		return $this->belongsTo('App\JobIndustry', 'job_industry_id');
	}

	public function education_level()
	{
		return $this->belongsTo('App\ApplicantEducationLevel', 'education_level_id');
	}

	public function skills()
	{
		return $this->belongsToMany('App\Skill', 'applicant_skill', 'applicant_id', 'skill_id');
	}

	public function internal_recruiter()
	{
		return $this->belongsTo('App\User', 'hf_assigned','id');
	}

	public function external_recruiter()
	{
		return $this->belongsTo('App\User', 'referred_by_id','id');
	}

}
