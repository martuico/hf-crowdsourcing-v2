<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Traits\DateFormats;

class ApplicantEducation extends Model {

  use DateFormats;

	protected $table = 'applicant_educations';
	public $timestamps = true;
  protected $fillable = [
    'applicant_id',
    'school_name',
    'attainment',
    'start_date',
    'end_date',
    'awards'
  ];

  protected $appends = ['from','to'];

  public function getFromAttribute($value)
  {
      return $this->fromTo($this->start_date);
  }

  public function getToAttribute($value)
  {
      return $this->fromTo($this->end_date);
  }


	public function applicant()
	{
		return $this->belongsTo('App\Applicant', 'applicant_id');
	}

}