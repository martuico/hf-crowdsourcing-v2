<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Traits\DateFormats,
		App\Traits\ShortNumberFormat,
		App\Traits\Generator;

class ApplicantJob extends Model {

	protected $table = 'applicant_jobs';
	public $timestamps = true;

	use SoftDeletes, DateFormats, ShortNumberFormat, Generator;

	protected $dates = ['deleted_at'];

	protected $fillable = ['application_code','applicant_id','employer_id','job_id','referred_by_id','hf_assigned','applicant_status_id','referred_start_date','referred_expired_date','is_visible_to_employer','employer_appointment_sched','employer_appointment_type','billing_status','applicant_cover_letter','expected_salary','endorsement_slip','reason','hire_date','hire_account','date_of_training','employer_applicant_status_id','current_employment_status','willing_to_relocate'];

	protected $appends = ['applicant_count'];

	public static function boot()
	{
	    parent::boot();
	    static::creating(function($model){
	      $model->application_code =  self::generateApplicationcodeNumber();
	    });
	    // static::updating(function($model){
	    //   $model->employer_id = \Auth::user()->employer->id;
	    // });
	}

  public function getApplicantCountAttribute($value)
  {
  		$count = $this->hasMany('App\ApplicantJob','job_id')->count();
      return $this->number_format_short($count);
  }

	public function getEmployerAppointmentSchedAttribute($value)
	{
			return $this->toDayDateTimeString($value);
	}

	public function getCreatedAtAttribute($value)
	{
			return $this->dateForHumans($value);
	}

	public function applicant_status()
	{
		return $this->belongsTo('App\ApplicantStatus', 'applicant_status_id');
	}

	public function employer_applicant_status()
	{
		return $this->belongsTo('App\EmployerApplicantStatus', 'employer_applicant_status_id');
	}

	public function applicant()
	{
		return $this->belongsTo('App\Applicant', 'applicant_id','id');
	}

	public function scopeCountbystatus($query,$value)
	{
			return $query->where('applicant_status_id','=',$value);
	}

	public function assginedHFApplicants()
	{
			return $this->belongsTo('App\Applicant', 'applicant_id','id')->where('hf_assigned','=',auth()->user()->id);
	}

	public function activities()
	{
		return $this->hasMany('App\ApplicantJobActivity','applicant_job_id')
								->orderBy('created_at','desc');
	}

	public function job()
	{
		return $this->belongsTo('App\Job', 'job_id');
	}

	public function employer()
	{
		return $this->belongsTo('App\Employer','employer_id');
	}

	public function internal_recruiter()
	{
		return $this->belongsTo('App\User', 'hf_assigned','id');
	}

	public function external_recruiter()
	{
		return $this->belongsTo('App\User', 'referred_by_id');
	}

	public function employerFeedback()
	{
		return $this->hasOne('App\ApplicantEmployerFeedback','applicant_job_id');
	}

}
