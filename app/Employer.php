<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable;
class Employer extends Model {

  use Sluggable;

	protected $table = 'employers';
	public $timestamps = true;
  protected $fillable = ['logo_url',
                        'company_name',
                        'contact_designation',
                        'contact_person_mobile',
                        'contact_person_mobil_2',
                        'office_address',
                        'office_number_1',
                        'office_number_2',
                        'website',
                        'sm_facebook',
                        'sm_linkedin',
                        'sm_twitter',
                        'description'];

	protected $appends = ['logo'];

  public static function boot()
  {
      parent::boot();
      static::creating(function($model){
        $model->user_id = \Auth::user()->id;
      });
      static::updating(function($model){
        $model->user_id = \Auth::user()->id;
      });
  }

  public function sluggable()
  {
      return [
          'slug' => [
              'source' => 'company_name'
          ]
      ];
  }

  public function getLogoAttribute($value)
  {
      return $this->logo_url ?: asset('hfv1/image/logo/placeholder.png');
  }

  public function hiredApplicants()
  {
      return $this->hasMany('App\ApplicantJob','employer_id')
                  ->whereHas('applicant_status',function($query){
                    $query->where('name','like','%Hired%');
                  });
  }

	public function user()
	{
		return $this->belongsTo('App\User', 'user_id');
	}

	// public function companyRecruiter()
	// {
	// 	return $this->belongsToMany('App\User', 'company_recruiter', 'company_id', 'recruiter_id');
	// }

  public function employer_recruiters()
  {
      return $this->hasMany('App\CompanyRecruiter','employer_id','id');
  }

  public function jobs()
  {
    return $this->hasMany('App\Job','employer_id');
  }

  public function applicants()
  {
    return $this->hasMany('App\ApplicantJob','employer_id');
  }

}
