<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ApplicantActivity extends Model
{
    protected $fillable = [
      'applicant_id',
      'user_id',
      'icon',
      'title',
      'description'
    ];

    public function applicant()
    {
        return $this->belongsTo('\App\User','applicant_id');
    }

    public function user()
    {
        return $this->belongsTo('\App\User','user_id');
    }
}
