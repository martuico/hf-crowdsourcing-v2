<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Notifications\ResetPassword as ResetPasswordNotification;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_type_id','avatar','firstname','middlename','lastname','email','password','is_active'
    ];
    public $timestamps = true;

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    protected $dates = ['deleted_at'];

    protected $appends = ['myname','profile_pic'];

    public function getProfilePicAttribute($value)
    {
        return $this->avatar ?: asset('hfv1/image/avatar/avatar.png');
    }

    public function getMyNameAttribute($value)
    {
        return ($this->firstname AND $this->lastname)? $this->firstname . ' ' . $this->lastname :$this->email;
    }

    public function resume()
    {
        return $this->hasOne('App\Applicant', 'user_id');
    }

    public function contacts()
    {
        return $this->hasMany('App\User', 'user_id');
    }

    public function type()
    {
        return $this->belongsTo('App\UserType', 'user_type_id');
    }

    public function employer()
    {
        return $this->hasOne('App\Employer', 'user_id');
    }

    public function irecruiter()
    {
        return $this->hasOne('App\IRecruiter', 'user_id');
    }

    public function companyRecruiter()
    {
        return $this->belongsToMany('App\Employer', 'company_recruiter', 'company_id', 'recruiter_id');
    }

    public function sendPasswordResetNotification($token)
    {
        // Your your own implementation.
        $this->notify(new ResetPasswordNotification($token));
    }
}
