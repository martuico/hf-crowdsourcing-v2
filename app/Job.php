<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Traits\DateFormats,
		App\Traits\Generator,
		App\Traits\ShortNumberFormat;
use Cviebrock\EloquentSluggable\Sluggable;

class Job extends Model {


	use SoftDeletes, Generator ,Sluggable, DateFormats,ShortNumberFormat;

	protected $table = 'jobs';
	protected $fillable = ['employer_id','job_code','job_title','job_type_id','job_industry_id','education_level_id','work_location','skills','salary_from','salary_to','work_shift_id','description','finders_fee','provide_relocation','designation','job_level_id','location_region','location_province','location_city','show_salary_range','referral_reward','applicant_reward','status'];

	public $timestamps = true;

	protected $dates = ['deleted_at'];

	public static function boot()
	{
	    parent::boot();
	    static::creating(function($model){
	      $model->employer_id = \Auth::user()->employer->id;
	      $model->valid_until = \Carbon\Carbon::now('Asia/Manila')->addDays(30);
	      $model->job_code =  self::generateJobcodeNumber();
        // \Session::regenerateToken();
	    });
      // static::created(function($model){

      // });
	    // static::updating(function($model){
	    //   $model->employer_id = \Auth::user()->employer->id;
	    // });
	}

	public function sluggable()
  {
      return [
          'slug' => [
              'source' => 'job_title'
          ]
      ];
  }

  protected $appends = ['applicant_count','applicant_premium_count','forinterview_count','hired_count','full_address'];

	public function getFullAddressAttribute($value)
	{
		  $reg = ($this->region AND $this->region->regDesc)?$this->region->regDesc:'';
		  $prov = ($this->province AND $this->province->provDesc)?$this->province->provDesc:'';
		  $city = ($this->city AND $this->city->citymunDesc)?$this->city->citymunDesc:'';

	    return strtoupper($this->current_address).' '.$reg.', '.$prov.','.$city;
	}

	public function region()
	{
			return $this->belongsTo('App\Region','location_region','regCode');
	}

	public function province()
	{
			return $this->belongsTo('App\Province','location_province','provCode');
	}

	public function city()
	{
			return $this->belongsTo('App\City','location_city','citymunCode');
	}

  public function getApplicantCountAttribute($value)
  {
      return $this->number_format_short($this->hasMany('App\ApplicantJob','job_id')
      																				->count());
  }
	// applicant_premium_count
  public function getApplicantPremiumCountAttribute($value)
  {
      return $this->number_format_short($this->hasMany('App\ApplicantJob','job_id')
																						->where('applicant_status_id','<>',1)
      																			->count());
  }

  public function getForinterviewCountAttribute($value)
  {
      return $this->number_format_short($this->hasMany('App\ApplicantJob','job_id')
      																		->where('applicant_status_id','=',3)
      																		->count());
  }

  public function getHiredCountAttribute($value)
  {
      return $this->number_format_short($this->hasMany('App\ApplicantJob','job_id')
      																->where('applicant_status_id','=',6)
      																->count());
  }

	public function getValidUntilAttribute($value)
	{
			return $this->validUnitFormat($value);
	}

	public function getCreatedAtAttribute($value)
	{
			return $this->dateForHumans($value);
	}

	public function scopeApplied($query)
	{
		return $this->whereHas('applicants',function($query){
									$query->where('applicant_id','=',auth()->user()->resume->id);
							});
	}

	public function scopeAppliedjobs($query,$value)
	{
		return $this->whereHas('applicants',function($query) use ($value){
									$query->where('job_id','=',$value);
									$query->where('applicant_id','=',auth()->user()->resume->id);
							});
	}

	public function scopeSearchPremiumJobs($query,$value)
	{
		return $this->whereHas('employer',function($query) use ($value){
							$query->where('is_premium','=',$value);
						});
	}

	public function scopeSearchJobTitle($query,$value)
	{
			return $this->where('job_title','=','%'.$value.'%');
	}

	public function applicants()
	{
			return $this->hasMany('App\ApplicantJob','job_id');
	}

	public function countEndorsedApplicants()
	{
			return $this->hasMany('App\ApplicantJob','job_id')
											->whereHas('applicant_status',function($query){
												$query->where('applicant_status_id','>',1);
											});
	}

	public function hiredApplicants()
	{
			return $this->hasMany('App\ApplicantJob','job_id')
									->whereHas('applicant_status',function($query){
										$query->where('name','like','%Hired%');
									});
	}

	public function assginedHFApplicants()
	{
			return $this->hasMany('App\ApplicantJob','job_id')
									->whereHas('applicant',function($query){
										$query->where('hf_assigned','=',auth()->user()->id);
									});
	}

	public function employer()
	{
			return $this->belongsTo('App\Employer', 'employer_id');
	}

	public function job_industry()
	{
			return $this->belongsTo('App\JobIndustry', 'job_industry_id');
	}

	public function job_level()
	{
			return $this->belongsTo('App\JobLevel', 'job_level_id');
	}

	public function shift()
	{
			return $this->belongsTo('App\Shift', 'work_shift_id');
	}

	public function type()
	{
			return $this->belongsTo('App\JobType', 'job_type_id');
	}

	public function education_level()
	{
			return $this->belongsTo('App\ApplicantEducationLevel', 'education_level_id');
	}

	public function skills()
	{
		return $this->belongsToMany('App\Skill', 'job_skill', 'job_id', 'skill_id');
	}

}
